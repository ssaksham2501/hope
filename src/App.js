import React, { useEffect } from "react";

import {
    BrowserRouter as Router,
    Route,
    Switch,
    useLocation,
    useHistory
} from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";
import {
    DefaultLayout,
    LayoutFour,
    LayoutThree,
    LayoutTwo,
    LoginLayout,
} from "./components/Layout/Layouts";
import Home from "./components/Home";
import About from "./components/About";
// import Contact from "./components/Contact";
import Landing from "./components/Landing/Index";
import SignUp from "./components/Auth/SignUp";
import Feeds from "./components/Home/components/Feeds";
import Verify from "./components/Auth/components/Verify";
import Privacy from "./components/Privacy";
import Terms from "./components/Terms";
import FAQ from "./components/FAQ";
import Notification from "./components/Notification";
import Postdetail from "./components/Postdetail";
import Contact from "./components/Contact";
import Profile from "./components/Profile";
import Feedback from "./components/Feedback";
import Settings from "./components/Settings";
import Communities from "./components/Communities";
import CommunitiesDetail from "./components/CommunitiesDetail";
import { connect } from "react-redux";
import PublicProfile from "./components/Publicprofile";
import Phonemenuss from "./components/Layout/Phonemenuss";
import MobileChat from "./components/Layout/components/MobileChat";
import { isIOS } from "react-device-detect";
import Reverify from "./components/Auth/components/ReVerify";

global.socket = null;
global.isLogin = isIOS ? false : true;
global.clicked = false;
const App = (props) => {
    return (
        <>
            <Router>
                <Switch>
                    {/* <RouteWrapper 
                        path="/home"
                        exact
                        component={() => <Home />}
                        layout={DefaultLayout}
                    />l
                    <RouteWrapper
                        path="/contact"
                        exact
                        component={() => <Contact />}
                        layout={LayoutTwo}
                    /> */}
                    <RouteWrapper
                        path="/"
                        exact
                        component={() => {
                            if (props.user && props.user.id) return <Home />;
                            else return <Landing />;
                        }}
                        layout={
                            props.user && props.user.id
                                ? DefaultLayout
                                : LayoutTwo
                        }
                    />

                    <RouteWrapper
                        path="/login"
                        exact
                        component={() => {
                            return <Landing />;
                        }}
                        layout={
                            LayoutTwo
                        }
                    />

                    <RouteWrapper
                        path="/sign-up"
                        exact
                        component={() => <SignUp />}
                        layout={LayoutTwo}
                    />
                    <RouteWrapper
                        path="/verification-screen/:slug"
                        exact
                        component={() => <Verify />}
                        layout={LayoutTwo}
                    />

                    <RouteWrapper
                        path="/verification/:slug"
                        exact
                        component={() => <Reverify />}
                        layout={LayoutTwo}
                    />

                    <RouteWrapper
                        path="/community"
                        exact
                        component={() => <Communities />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/community-detail/:slug"
                        exact
                        component={() => <CommunitiesDetail />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/settings"
                        exact
                        component={() => <Settings />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/notifications"
                        exact
                        component={() => <Notification />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/requests"
                        exact
                        component={() => <Notification />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/post/:slug"
                        exact
                        component={() => <Home single={true} />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/profile"
                        exact
                        component={() => <Profile />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/about-us"
                        exact
                        component={() => <About />}
                        layout={LayoutThree}
                    />
                    <RouteWrapper
                        path="/privacy-policy"
                        exact
                        component={() => <Privacy />}
                        layout={LayoutThree}
                    />
                    <RouteWrapper
                        path="/terms-conditions"
                        exact
                        component={() => <Terms />}
                        layout={LayoutThree}
                    />
                    <RouteWrapper
                        path="/faq"
                        exact
                        component={() => <FAQ />}
                        layout={LayoutThree}
                    />
                    <RouteWrapper
                        path="/contact-us"
                        exact
                        component={() => <Contact />}
                        layout={LayoutThree}
                    />
                    <RouteWrapper
                        path="/feedback"
                        exact
                        component={() => <Feedback />}
                        layout={LayoutThree}
                    />
                    <RouteWrapper
                        path="/profile/:slug"
                        exact
                        component={() => <PublicProfile />}
                        layout={DefaultLayout}
                    />
                    <RouteWrapper
                        path="/phonemenu"
                        exact
                        component={() => <Phonemenuss />}
                        layout={LayoutTwo}
                    />
                    <RouteWrapper
                        path="/chat"
                        exact
                        component={() => <MobileChat />}
                        layout={DefaultLayout}
                    />
                </Switch>
            </Router>
        </>
    );
};

function RouteWrapper({ component: Component, layout: Layout, ...rest }) {
    let location = useLocation();
    useEffect(() => {
        if (
            location.pathname === "/" ||
            location.pathname === "/login" ||
            location.pathname === "/publicprofile" ||
            location.pathname === "/about-us" ||
            location.pathname === "/sign-up" ||
            location.pathname === "/privacy-policy" ||
            location.pathname === "/terms-conditions" ||
            location.pathname === "/contact-us" ||
            location.pathname === "/faq" ||
            location.pathname === "/feedback" ||
            !global.isLogin
        ) {
            document.body.classList.add("cms_pages");
        } else {
            document.body.classList.remove("cms_pages");
        }
    }, [location.pathname]);

    return (
        <Route
            {...rest}
            render={(props) => (
                <Layout {...props}>
                    <Component {...props} />
                </Layout>
            )}
        />
    );
}

const mapState = (state) => {
    return {
        user: state.UserReducer.user,
    };
};
const mapDispatch = {};

export default connect(mapState, mapDispatch)(App);
