import { useState, useEffect } from "react";

function useAudioPlayer(props) {
  const [ready, setReady] = useState(false);
  const [duration, setDuration] = useState();
  const [curTime, setCurTime] = useState();
  const [playing, setPlaying] = useState(false);
  const [clickedTime, setClickedTime] = useState();
  const [loaded, setLoaded] = useState(0);
  const [played, setPlayed] = useState(0);

  useEffect(() => {
      setPlaying(props.play);
  }, [props.play]);
    
  useEffect(() => {
    
    if (ready) {
      const audio = document.getElementById(`audio` + (props.id ? props.id : ``));
      
      // state setters wrappers
      const setAudioData = () => {
        setDuration(audio.duration);
        setCurTime(audio.currentTime);
      }

      const setAudioTime = () => {
        setDuration(audio.duration);
        setCurTime(audio.currentTime)
        if (duration === audio.currentTime) {
          setPlaying(false);
        }
      };

      // DOM listeners: update React state on DOM events
      audio.addEventListener("loadeddata", setAudioData);

      audio.addEventListener("timeupdate", setAudioTime);
      audio.addEventListener("durationchange", setAudioData);
      

      // React state listeners: update DOM on React state changes
      playing ? audio.play() : audio.pause();
      if (playing && audio.duration != 'Infinity' && clickedTime && clickedTime !== curTime) {
        
        if (audio && audio.currentTime) {
          audio.currentTime = clickedTime;
        }
        setClickedTime(null);
      }

      // effect cleanup
      return () => {
        audio.removeEventListener("loadeddata", setAudioData);
        audio.removeEventListener("timeupdate", setAudioTime);
      }
    }
  }, [ready, playing, clickedTime]);

  return {
    curTime,
    duration,
    playing,
    setPlaying,
    setClickedTime,
    loaded,
    played,
    ready,
    setReady
  }
}

export default useAudioPlayer;