import React, { useEffect, useState } from "react";

import Song from "./Song";
import Play from "./Play";
import Pause from "./Pause";
import Bar from "./Bar";

import useAudioPlayer from './useAudioPlayer';

function AudioPlayer(props) {
  
  const { curTime, duration, playing, loaded, setPlaying, setClickedTime, ready, setReady } = useAudioPlayer(props);

  useEffect(() => {
    var req = new XMLHttpRequest(); 
    req.onprogress = (evt) => {
      if (evt.lengthComputable) {
        var percentComplete = (evt.loaded / evt.total) * 100;
        console.log(percentComplete);
      }
    };
    req.open('GET', props.file, true);  
    req.onreadystatechange = function (aEvt) {  
        if (req.readyState == 4) 
        {
          setTimeout(() => {
            setReady(true);
          }, 500);
        }  
    };  
    req.send(); 
  }, [])

  useEffect(() => {
    if (!props.slidePause) {
      setPlaying(false);
    }
  }, [props.slidePause])

  useEffect(() => {
    setPlaying(props.playing);
  }, [props.playing])
  
  return (
    <div className="audio-player">
      {
        ready
        &&
        <audio id={`audio` + (props.id ? props.id : ``)}>
          <source src={props.file} />
          Your browser does not support the <code>audio</code> element.
        </audio>
      }
      <div className="controls">
        {
          ready
          ?
            <>
              {playing ?
                <Pause handleClick={() => {
                  setPlaying(false);
                  props.onPause();
                }} /> :
                <Play handleClick={() => {
                  setPlaying(true)
                  props.onPlay();
                }} />
              }
            </>
          :
            <i className="fad fa-spinner-third fa-spin text-white"></i>
        }
        {
          ready
          &&
          <Bar id={`audio` + (props.id ? props.id : ``)} curTime={curTime} duration={duration} onTimeUpdate={(time) => setClickedTime(time)} />
        }
      </div>
    </div>
  );
}

export default AudioPlayer;
