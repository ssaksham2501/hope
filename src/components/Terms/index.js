import React from "react";

//components
import Breadcrumb from "../Layout/Breadcrumb";
import Mains from "./components/Mains";

function Terms() {
    return (
        <div>
            <Breadcrumb breadTitle="Terms & Conditions" />
            <Mains />
        </div>
    );
}

export default Terms;
