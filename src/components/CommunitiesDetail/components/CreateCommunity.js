import React, { useState, useEffect, useRef } from "react";

//assets
import communities_community from "../../../assets/images/Communities/community.png";
import communities_about from "../../../assets/images/Communities/about.png";
import communities_image from "../../../assets/images/Communities/image.png";
import communities_friends from "../../../assets/images/Communities/friends.png";
import communities_user from "../../../assets/images/Communities/user.png";

//helpers
import Validation from "../../../helpers/vaildation";
import CommunityController from "../../../controllers/community.controller";
import { renderImage } from "../../../helpers/General";
import { Toast } from "../../../helpers/Toaster";
import { useHistory } from "react-router-dom";
import ImageCropper from "../../Layout/components/ImageCropper";
import ConfirmationModal from "../../Layout/components/ConfirmationModal";
import { Dropdown } from "react-bootstrap";

const CreateCommunity = (props) => {
    const history = useHistory();
    const avtarInput = useRef(null);
    const bannerInput = useRef(null);
    const [banner, setBanner] = useState(null);
    const [profilePic, setProfilePic] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [id, setId] = useState(null);
    const [deleteCommunityConfirm, setDeleteCommunityConfirm] = useState(false);
    

    //validations  -- start
    const [isError, setError] = useState({
        title: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        description: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        image: {
            rules: [],
            isValid: true,
            message: "",
        },
        banner: {
            rules: [],
            isValid: true,
            message: "",
        },
        invites: {
            rules: [],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        title: "",
        description: "",
        image: null,
        banner: null,
        invites: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        if (props && props.id) {
            getCommunityDetail(props.id);
        } else {
            setValues(defaultValues);
        }
    }, []);
    const getCommunityDetail = async (id) => {
        let response = await new CommunityController().getDetail(id);
        if (response && response.status) {
            let detail = response.community;
            setValues({
                ...values,
                title: detail.title,
                description: detail.description,
                image: detail.image,
                banner: detail.banner,
            });
            if(!banner)
                setBanner(detail.banner);
            if(!profilePic)
                setProfilePic(detail.image);
            setId(detail.id);
        }
    };

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        if (!value || !value.trim()) {
            setValues({ ...values, [field]: "" });
        } else {
            setValues({ ...values, [field]: value });
        }
    };

    //Create Community
    const createCommunity = async () => {
        let validation = new Validation(isError);
        let isValid = await validation.isFormValid(values);
        if (isValid && !isValid.haveError) {
            setIsLoading(true);
            if (props && props.id) {
                upateCommunity();
            } else {
                newCommunity();
            }
        } else {
            setError({ ...isValid.errors });
        }
        setIsLoading(false);
    };

    const deleteCommunity = async () => {
        if (props.id && id) {
            setDeleteCommunityConfirm(true);
        }
    };

    const newCommunity = async () => {
        let data = values;
        data.image = profilePic;
        data.banner = banner;
        let response = await new CommunityController().createCommunity(data);
        if (response && response.status) {
            new Toast().success(response.message);
            props.show(false);
            setValues(defaultValues);
        } else {
            new Toast().error(response.error);
            console.log(response.error);
        }
    };

    const upateCommunity = async () => {
        let data = values;
        data.image = profilePic;
        data.banner = banner;
        let response = await new CommunityController().updateCommunity(
            data,
            id
        );
        if (response && response.status) {
            new Toast().success(response.message);
            props.show(false);
            setValues(defaultValues);
        } else {
            new Toast().error(response.error);
            console.log(response.error);
        }
    };

    const removeBanner = async () => {
        if (id) {
            let response = await new CommunityController().removeBanner(id);
            if (response && response.status) {
                new Toast().success(response.message);
                getCommunityDetail(props.id);
                setBanner(null);
            } else {
                new Toast().error(response.error);
            }
        }
        else
        {
            setBanner(null);
        }
    }

    const removeImage = async () => {
        if (id) {
            let response = await new CommunityController().removeImage(id);
            if (response && response.status) {
                new Toast().success(response.message);
                getCommunityDetail(props.id);
                setProfilePic(null);
            } else {
                new Toast().error(response.error);
            }
        }
        else
        {
            setProfilePic(null);
        }
    }
    
    /** Cropper */
    const [cropImage, setCropImage] = React.useState(null);
    const [cropItem, setCropItem] = React.useState(null);
    const [openCropper, setOpenCropper] = React.useState(false);
    const initImageEditor = (file, type, width, height) => {
        setCropItem({
            image: file,
            type: type,
            width: width,
            height: height,
            croppedArea: null
        });
        setOpenCropper(true);
    }
    const onCropperClose = () => {
        setOpenCropper(false);
        setCropItem(null);
        setCropImage(null);
    };

    /** Cropper */

    //image upload
    const imageUpload = async (value, type) => {
        let response = await new CommunityController().uploadProfileImage(
            value
        );
        if (response && response.status) {
            if (type === "avatar") {
                setProfilePic(response.path);
            } else {
                setBanner(response.path);
            }
        } else {
            console.log("error here", response);
        }
    };
    return (
        <section className="create_community_section community_main_area">
            <div className="community_head">
                <h2>
                    <i
                        className="fas fa-arrow-left"
                        onClick={() => {
                            if (props.edit) {
                                props.show(false);
                                props.setEdit();
                            } else {
                                props.show(false);
                                props.setEdit();
                            }
                        }}
                    ></i>
                    Community
                </h2>
            </div>
            <div className="community_area">
                <div className="sub_section">
                    <div className="row">
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="community_form">
                                <h2>
                                    {props && props.id ? "Edit" : "Create"}{" "}
                                    Community
                                </h2>
                                <form
                                    className="form_area_3 form_area_2"
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                    }}
                                >
                                    <div className="row">
                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label
                                                className={
                                                    !isError.title.isValid
                                                        ? "valid-box form_controls"
                                                        : "form_controls"
                                                }
                                                htmlFor="title"
                                            >
                                                <div className="label_head">
                                                    <div className="imgwrap">
                                                        <img
                                                            src={
                                                                communities_community
                                                            }
                                                            alt="..."
                                                        />
                                                    </div>
                                                    <p className="label_top">
                                                        Community Name
                                                    </p>
                                                </div>

                                                <input
                                                    type="text"
                                                    placeholder="Enter community name"
                                                    onChange={(e) => {
                                                        handleStates(
                                                            "title",
                                                            e.target.value
                                                        );
                                                    }}
                                                    value={
                                                        values.title
                                                            ? values.title
                                                            : ``
                                                    }
                                                />
                                                {!isError.title.isValid ? (
                                                    <p className="valid-helper">
                                                        {isError.title.message}
                                                    </p>
                                                ) : null}
                                            </label>
                                        </div>
                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label
                                                className={
                                                    !isError.description.isValid
                                                        ? "valid-box form_controls"
                                                        : "form_controls"
                                                }
                                                htmlFor="description"
                                            >
                                                <div className="label_head">
                                                    <div className="imgwrap">
                                                        <img
                                                            src={
                                                                communities_about
                                                            }
                                                            alt="..."
                                                        />
                                                    </div>
                                                    <p className="label_top">
                                                        About
                                                    </p>
                                                </div>
                                                <input
                                                    type="text"
                                                    placeholder="About community"
                                                    onChange={(e) => {
                                                        handleStates(
                                                            "description",
                                                            e.target.value
                                                        );
                                                    }}
                                                    value={
                                                        values.description
                                                            ? values.description
                                                            : ``
                                                    }
                                                />
                                                {!isError.description.isValid ? (
                                                    <p className="valid-helper">
                                                        {
                                                            isError.description
                                                                .message
                                                        }
                                                    </p>
                                                ) : null}
                                            </label>
                                        </div>
                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label
                                                className={
                                                    !isError.image.isValid
                                                        ? "valid-box form_controls"
                                                        : "form_controls"
                                                }
                                            >
                                                <div className="label_head">
                                                    <div className="imgwrap">
                                                        <img
                                                            src={
                                                                communities_image
                                                            }
                                                            alt="..."
                                                        />
                                                    </div>
                                                    <p className="label_top">
                                                        Profile Photo
                                                    </p>
                                                </div>
                                                <div className="profile_photo">
                                                    <img
                                                        src={
                                                            profilePic != null
                                                                ? renderImage(
                                                                      profilePic
                                                                  )
                                                                : communities_user
                                                        }
                                                        alt=".."
                                                    />
                                                     <Dropdown
                                                    className={`icon profile`}
                                                    align={{ lg: 'end' }}
                                                >
                                                    <Dropdown.Toggle
                                                            id="dropdown-basic1"
                                                        >
                                                        {
                                                            profilePic
                                                            ?
                                                                <i className="far fa-camera"></i>
                                                            :
                                                                <i
                                                                    className="far fa-camera"
                                                                    onClick={() => {
                                                                        avtarInput.current.click()
                                                                    }}
                                                                ></i>
                                                        }
                                                    </Dropdown.Toggle>
                                                    {
                                                        profilePic
                                                        &&
                                                        <Dropdown.Menu>
                                                            <Dropdown.Item
                                                                onClick={() =>
                                                                    avtarInput.current.click()
                                                                }
                                                            >
                                                                <div className="left">
                                                                    <i className="fa fa-pencil"></i>
                                                                </div>
                                                                <div className="right">
                                                                    Change Image
                                                                </div>
                                                            </Dropdown.Item>
                                                            <Dropdown.Item
                                                                onClick={removeImage}
                                                            >
                                                                <div className="left">
                                                                    <i className="fa fa-times"></i>
                                                                </div>
                                                                <div className="right">
                                                                    Remove Image
                                                                </div>
                                                            </Dropdown.Item>
                                                        </Dropdown.Menu>
                                                    }
                                                </Dropdown>
                                                </div>
                                               
                                                {!isError.image.isValid ? (
                                                    <p className="valid-helper">
                                                        {isError.image.message}
                                                    </p>
                                                ) : null}
                                            </label>
                                            <input
                                                id="file-photo"
                                                ref={avtarInput}
                                                type="file"
                                                onChange={(event) => {
                                                    let file = event.target.files[0];
                                                    let reader = new FileReader();
                                                    reader.readAsDataURL(file);

                                                    reader.onload = function() {
                                                        initImageEditor((reader.result), 'avatar', 200, 200);
                                                    };
                                                    reader.onerror = function() {
                                                        console.log('there are some problems');
                                                    };
                                                    event.target.value = "";
                                                }}
                                                accept="image/*"
                                            />
                                        </div>
                                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label
                                                className={
                                                    !isError.banner.isValid
                                                        ? "valid-box form_controls"
                                                        : "form_controls"
                                                }
                                            >
                                                <div className="label_head">
                                                    <div className="imgwrap">
                                                        <img
                                                            src={
                                                                communities_image
                                                            }
                                                            alt="..."
                                                        />
                                                    </div>
                                                        <p className="label_top">
                                                            Cover Photo
                                                        </p>
                                                    </div>
                                                    <div className="cover_photo">
                                                        {banner ? (
                                                            <img
                                                                src={renderImage(
                                                                    banner
                                                                )}
                                                                alt=".."
                                                            />
                                                        ) : null}
                                                        <label htmlFor={!banner ? `file-banner` : false }>
                                                            {/* {
                                                                !banner
                                                                ?
                                                                    <div
                                                                        className="upload_icon flexie"
                                                                        onClick={() =>
                                                                            bannerInput.current.click()
                                                                        }
                                                                    >
                                                                        <i className="far fa-camera"></i>
                                                                    </div>
                                                                    
                                                                :
                                                                    <div
                                                                        className="upload_icon flexie cross"
                                                                        onClick={removeBanner}
                                                                        title="delete"
                                                                    >
                                                                        <i className="fas fa-times-circle text-danger"></i>
                                                                    </div>
                                                            } */}
                                                        </label>
                                                    </div>
                                                    <Dropdown
                                                        className={`icon`}
                                                        align={{ lg: 'end' }}
                                                    >
                                                        <Dropdown.Toggle
                                                            id="dropdown-basic"
                                                        >
                                                            {
                                                                banner
                                                                ?
                                                                    <i className="far fa-camera"></i>
                                                                :
                                                                    <i
                                                                        className="far fa-camera"
                                                                        onClick={() => {
                                                                            bannerInput.current.click()
                                                                        }}
                                                                    ></i>
                                                            }
                                                        </Dropdown.Toggle>
                                                        {
                                                            banner
                                                            &&
                                                            <Dropdown.Menu>
                                                                <Dropdown.Item
                                                                    onClick={() =>
                                                                        bannerInput.current.click()
                                                                    }
                                                                >
                                                                    <div className="left">
                                                                        <i className="fa fa-pencil"></i>
                                                                    </div>
                                                                    <div className="right">
                                                                        Change Image
                                                                    </div>
                                                                </Dropdown.Item>
                                                                <Dropdown.Item
                                                                    onClick={removeBanner}
                                                                >
                                                                    <div className="left">
                                                                        <i className="fa fa-times"></i>
                                                                    </div>
                                                                    <div className="right">
                                                                        Remove Image
                                                                    </div>
                                                                </Dropdown.Item>
                                                            </Dropdown.Menu>
                                                        }
                                                    </Dropdown>
                                                {!isError.banner.isValid ? (
                                                    <p className="valid-helper">
                                                        {isError.banner.message}
                                                    </p>
                                                ) : null}
                                            </label>
                                            <input
                                                id="file-banner"
                                                ref={bannerInput}
                                                type="file"
                                                onChange={(event) => {
                                                    let file = event.target.files[0];
                                                    let reader = new FileReader();
                                                    reader.readAsDataURL(file);

                                                    reader.onload = function() {
                                                        initImageEditor((reader.result), 'banner', 883, 215);
                                                    };
                                                    reader.onerror = function() {
                                                        console.log('there are some problems');
                                                    };
                                                    event.target.value = "";
                                                }}
                                                accept="image/*"
                                            />
                                        </div>
                                        {/* <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label
                                                className={
                                                    !isError.invites.isValid
                                                        ? "valid-box form_controls form_controls_tarea"
                                                        : "form_controls form_controls_tarea"
                                                }
                                                htmlFor="invites"
                                            >
                                                <div className="label_head">
                                                    <div className="imgwrap">
                                                        <img
                                                            src={
                                                                communities_friends
                                                            }
                                                            alt="..."
                                                        />
                                                    </div>
                                                    <p className="label_top">
                                                        Invite Friends
                                                    </p>
                                                </div>

                                                <input
                                                    type="text"
                                                    placeholder="Enter name or username"
                                                    onChange={(e) => {
                                                        // handleStates(
                                                        //     "invites",
                                                        //     e.target.value
                                                        // );
                                                        console.log(e);
                                                    }}
                                                    // value={
                                                    //     values.invites
                                                    //         ? values.invites
                                                    //         : ``
                                                    // }
                                                />

                                                {isError.invites.message ? (
                                                    <p className="valid-helper">
                                                        {
                                                            isError.invites
                                                                .message
                                                        }
                                                    </p>
                                                ) : null}
                                            </label>
                                        </div> */}
                                    </div>
                                    {
                                        props && props.id
                                        &&
                                        <div className="action_area flexie left">
                                            <button
                                                className="text-danger"
                                                type="button"
                                                onClick={deleteCommunity}
                                            >
                                                <i className="far fa-trash"></i> Delete
                                            </button>
                                        </div>
                                    }
                                    <div className="action_area flexie">
                                        <button
                                            className="main_btn lav_bg"
                                            type="button"
                                            onClick={createCommunity}
                                        >
                                            {props && props.id
                                                ? "Update"
                                                : "Create"}
                                            {isLoading ? (
                                                <i className="fad fa-spinner-third fa-spin"></i>
                                            ) : null}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {
                openCropper && cropItem
                &&
                <ImageCropper
                    item={cropItem}
                    open={openCropper}
                    onClose={onCropperClose}
                    onCrop={imageUpload}
                    isLoading={isLoading}
                />
            }
            {
                deleteCommunityConfirm
                &&
                <ConfirmationModal
                    title={`Are you sure to delete this community?`}
                    description={`Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nulla iste labor iosam omnisillo labore delectus`}
                    show={deleteCommunityConfirm ? true : false}
                    confirm={async () => {
                        if (props.id && id) {   
                            let response = await new CommunityController().deleteCommunity(id);
                            if (response && response.status) {
                                new Toast().success(response.message);
                                props.show(false);
                                setValues(defaultValues);
                                setDeleteCommunityConfirm(false);
                            } else {
                                new Toast().error(response.error);
                                console.log(response.error);
                            }
                        }
                    }}
                    cancel={() => {
                        setDeleteCommunityConfirm(null);
                    }}
                />
            }
        </section>
    );
};

export default CreateCommunity;
