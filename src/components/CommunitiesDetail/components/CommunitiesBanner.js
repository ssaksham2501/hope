import React, { useEffect, useRef, useState } from "react";
import { Nav, Tab } from "react-bootstrap";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import cover from "../../../assets/images/sidebar/Layer-5.jpg";
import CommunityController from "../../../controllers/community.controller";
import { baseUrl, renderImage } from "../../../helpers/General";
import { Toast } from "../../../helpers/Toaster";
import { linkInModal, setCommunityId, showCommunity } from "../../../redux/actions/user";
import Friends from "../../Communities/components/Friends";
import AddPost from "../../Home/components/AddPost";
import Post from "../../Home/components/Post";
import ChatSection from "../../Layout/components/ChatSection";
import Nodata from "../../Layout/components/Nodata";

const CommunitiesBanner = (props) => {
    const [community, setCommunity] = useState(props.detail);
    const [switchView, setSwitchView] = useState(false);
    const [postList, setPostList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [noData, setNoData] = useState(false);
    const [noScroll, setNoScroll] = useState(false);
    const listProps = {
        col: 3,
        actionText1: "Remove",
        actionText2: "Add",
        title: "Members"
    };
    const [post, setPost] = useState({
        group: props.detail.slug,
        lastId: "",
    });

    //scroll to bottom -start
    useEffect(() => {
        setCommunity(props.detail);
        setPost({ ...post, group: props.slug });
        window.scrollTo(0, 0);
        setPostList([]);
        getPostList({ ...post, group: props.slug });
    }, [props.slug]);

    const getPostList = async (p) => {
        if (!loading) {
            setLoading(true);
            let response = await new CommunityController().postList(p);
            setLoading(false);
            if (response && response.status) {
                console.log(p.lastId);
                if (p.lastId) {
                    setPostList([...postList, ...response.posts]);
                    if (response.posts.length < 1) {
                        setNoScroll(true);
                    }
                } else {
                    setPostList(response.posts);
                    if(response.posts.length < 1) {
                        setNoData(true);
                    }
                }
            }
        }
    };

    const updatePosts = () => {
        setNoData(false);
        setPost({ ...post, lastId: "" });
        getPostList({ ...post, lastId: "" });
    };

    // useEffect(() => {
    //     setPostList([]);
    //     if (post.lastId) {
    //         getPostList(post);
    //     }
    // }, [post.lastId]);

    //scroll to bottom -start
    const listInnerRef = useRef();

    useEffect(() => {
        if (props.scroller && !props.single) {
            onScroll(props.scroller);
        }
    }, [props.scroller]);

    const onScroll = ({ scrollTop, scrollHeight, clientHeight }) => {
        if (document.getElementsByClassName('community_banner_section').length > 0 && document.getElementsByClassName('chatev').length > 0) {
            if (scrollTop > 80) {
                document.getElementsByClassName('community_banner_section')[0].classList.add('fixed-header');
                document.getElementsByClassName('chatev')[0].classList.add('t35');
            }
            else {
                document.getElementsByClassName('community_banner_section')[0].classList.remove('fixed-header');
                document.getElementsByClassName('chatev')[0].classList.remove('t35');
            }
            
            if (scrollTop >= scrollHeight - clientHeight - 100) {
                let postId = postList && postList.length > 0 ? postList[postList.length - 1].id : null;
                if (postId && !noScroll && postId !== post.lastId) {
                    setPost({
                        ...post,
                        lastId: postId,
                    });
                    getPostList({
                        ...post,
                        lastId: postId,
                    });
                }
            }
        }
    };

    const history = useHistory();
    const editCommunity = () => {
        console.log(community.slug);
        props.showCommunity(true);
        props.setCommunityId(community.slug);
        history.push("/community#my-communities");
    };

    const followCommunity = async () => {
        let response = await new CommunityController().followCommunity(
            community.id
        );
        if (response && response.status) {
            let commun = { ...community };
            commun.is_followed = true;
            setCommunity(commun);
            new Toast().success(response.message);
        } else {
            new Toast().error(response.message);
        }
    };

    const unFollowCommunity = async () => {
        let response = await new CommunityController().unFollowCommunity(
            community.id
        );
        if (response && response.status) {
            let commun = { ...community };
            commun.is_followed = false;
            setCommunity(commun);
            new Toast().success(response.message);
        } else {
            new Toast().error(response.message);
        }
    };

    return (
        <section className="community_banner_section community_main_area">
            <div className="community_head">
                <h2>
                    <div onClick={() => history.goBack()}>
                        <i className="fas fa-arrow-left"></i>
                    </div>
                    Community
                </h2>
            </div>
            {community ? (
                <div
                    className="timeline_scroll_section"
                >
                    <div className="cover_area ">
                        <div
                            className="cover_img flexie"
                            onClick={() => {
                                if (community.banner) {
                                    props.showLinkInModal(baseUrl(community.banner))
                                }
                            }}
                        >
                            <img
                                src={community.banner ? renderImage(community.banner) : cover}
                                alt="..."
                            />
                        </div>
                    </div>
                    <div className="user_area">
                        <div className="upload_icon_cover">
                            <div
                                className="user-img"
                                onClick={() => {
                                    if (community.image) {
                                        props.showLinkInModal(baseUrl(community.image))
                                    }
                                }}
                            >
                                <img
                                    src={renderImage(community.image)}
                                    alt=".."
                                />
                            </div>
                            {/* <div className="upload_icon flexie">
                                <i className="far fa-camera"></i>
                            </div> */}
                        </div>
                        <div className="user_detail">
                            <div className="content_head flexie">
                                <h4>{community.title}</h4>
                                {/* <i className="fal fa-ellipsis-h"></i> */}
                            </div>
                            <p>{community.description}</p>
                            {props.user &&
                            props.user.id === community.user_id ? (
                                <button
                                    className="main_btn lav_bg"
                                    onClick={() => {
                                        editCommunity();
                                    }}
                                >
                                    Edit
                                </button>
                            ) : (
                                <button
                                    className={
                                        community.is_followed
                                            ? "main_btn lav_bg"
                                            : "main_btn pink_bg"
                                    }
                                    onClick={() => {
                                        community.is_followed
                                            ? unFollowCommunity()
                                            : followCommunity();
                                    }}
                                >
                                    {community.is_followed
                                        ? "Unfollow"
                                        : "Follow"}
                                </button>
                            )}
                        </div>
                    </div>
                    {props.user && props.user.id === community.user_id ? (
                        <>
                            <Tab.Container
                                className="tabs_area"
                                id="left-tabs-example"
                                defaultActiveKey="all"
                            >
                                <Nav variant="pills" className="flex-row">
                                    <Nav.Item>
                                        <Nav.Link
                                            className=""
                                            eventKey="all"
                                            onClick={() => {
                                                setSwitchView(false);
                                            }}
                                        >
                                            Posts
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link
                                            className=" "
                                            eventKey="brand"
                                            onClick={() => {
                                                setSwitchView(true);
                                            }}
                                        >
                                            Members
                                        </Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </Tab.Container>
                            {switchView ? (
                                <div className="community_area">
                                    <div className="sub_section">
                                        <div className="content_head">
                                            <Friends
                                                listProps={listProps}
                                                group={community.id}
                                            />
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                <div className="community_area">
                                    <div className="sub_section">
                                        <div className="row">
                                            <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                                    {
                                                        props.user && (props.user.id === community.user_id || community.is_followed)
                                                        &&
                                                        <AddPost
                                                            updatePosts={updatePosts}
                                                            selectedGroup={community ? {
                                                                slug: community.slug ? community.slug : null,
                                                                title: community.title ? community.title : null,
                                                            }  : null}
                                                        />
                                                    }
                                                    {
                                                        !noData && postList && postList.length > 0
                                                        ?
                                                        <>
                                                            <Post
                                                                postList={postList}
                                                                showAddPost={(value) =>
                                                                    console.log(value)
                                                                }
                                                                editCommunity={true}
                                                                updatePosts={updatePosts}
                                                            />
                                                            {
                                                                loading
                                                                &&
                                                                <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>
                                                            }
                                                        </>
                                                        :
                                                            <>{!noData && <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>}</>
                                                    }
                                                    {
                                                        noData
                                                        &&
                                                        <Nodata  title="No posts available."/>
                                                    }
                                            </div>
                                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 d-lg-block d-none">
                                                    <ChatSection />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </>
                    ) : (
                        <div className="community_area">
                            <div className="sub_section">
                                <div className="row">
                                        <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                            {
                                                props.user && (props.user.id === community.user_id || community.is_followed)
                                                &&
                                                <AddPost
                                                    updatePosts={updatePosts}
                                                    selectedGroup={community ? {
                                                        slug: community.slug ? community.slug : null,
                                                        title: community.title ? community.title : null,
                                                    }  : null}
                                                />
                                            }
                                            {
                                                !noData && postList && postList.length > 0
                                                ?
                                                <>
                                                <Post
                                                    postList={postList}
                                                    showAddPost={(value) =>
                                                        console.log(value)
                                                    }
                                                    editCommunity={true}
                                                    updatePosts={updatePosts}
                                                />
                                                </>
                                                :
                                                    <>{!noData && <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>}</>
                                            }
                                            {
                                                noData
                                                &&
                                                <Nodata  title="No posts available."/>
                                            }
                                    </div>
                                    <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 d-lg-block d-none">
                                            <ChatSection />
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            ) : null}
        </section>
    );
};
const mapState = (state) => {
    return {
        user: state.UserReducer.user,
        scroller: state.ScrollerReducer.scroller
    };
};
const mapDispatch = (dispatch) => {
    return {
        showCommunity: async (info) => dispatch(showCommunity(info)),
        setCommunityId: async (info) => dispatch(setCommunityId(info)),
        showLinkInModal: async (info) => dispatch(linkInModal(info)),
    };
};

export default connect(mapState, mapDispatch)(CommunitiesBanner);
