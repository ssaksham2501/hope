import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import CommunityController from "../../controllers/community.controller";
import CommunitiesBanner from "./components/CommunitiesBanner";

const CommunitiesDetail = () => {
    const [detail, setCommunityDetail] = useState(null);
    const [slug, setSlug] = useState(null);

    const getDetail = async (id) => {
        let response = await new CommunityController().getDetail(id);
        if (response && response.status) {
            setCommunityDetail(response.community);
        }
    };

    let param = useParams();
    useEffect(() => {
        window.scrollTo(0, 0);
        if (param && param.slug) {
            setCommunityDetail(null);
            setSlug(null);
            setSlug(param.slug)    
            getDetail(param.slug);
        }
    }, [param.slug]);

    return <>{detail ? <CommunitiesBanner detail={detail} slug={slug} /> : null}</>;
};
export default CommunitiesDetail;
