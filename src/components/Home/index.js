import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import CommunityController from "../../controllers/community.controller";
import PostController from "../../controllers/post.controller";
import ChatSection from "../Layout/components/ChatSection";
import Nodata from "../Layout/components/Nodata";
import AddPost from "./components/AddPost";
import Post from "./components/Post";
//components
import Story from "./components/Story";

const Home = (props) => {
    const history = useHistory();
    const location = useLocation();
    const [postList, setPostList] = useState([]);
    const [noData, setNoData] = useState(false);
    const [communities, setCommunities] = useState([]);
    const [edit, setEdit] = useState(false);
    const [loader, setLoader] = useState(false);
    const [done, setDone] = useState(false);
    
    useEffect(() => {
        initPage();
    }, [location.pathname]);
    

    const initPage = async () => {
        window.scrollTo(0, 0);
        setLoader(true);
        setPostList([]);
        getPostList();
        if (!props.single) {
            await getTopBarCommunities();
        }
    }

    // posts  --start
    const updatePosts = (posts) => {
        if (posts) {
            setPostList(posts);
            document.querySelector('.home_section').scrollTop = props.scroller.scrollTop;
        }
        else {
            setNoData(false);
            setDone(false);
            setLoader(false);
            getPostList();
        }
    };

    useEffect(() => {
        setPostList(postList);
    }, [postList]);

    const getPostList = async (id) => {
        if (!loader && !done) {
            setEdit(false);
            setLoader(true);
            let singlePost = null;
            if (props.single) {
                singlePost = location.pathname.split('/');
                singlePost = singlePost[singlePost.length - 1];
            }
            let response = await new PostController().get_post(id, singlePost);
            if (response && response.status) {
                if (response.posts.length > 0) {
                    let array = id ? [...postList, ...response.posts] : response.posts;
                    setPostList(array);
                } else if (id) {
                    setDone(true)
                }
                else {
                    setNoData(true);
                }
            }
            setLoader(false);
        }
    };

    //post --end

    //scroll to bottom -start
    const onScroll = ({ scrollTop, scrollHeight, clientHeight }) => {
        if (scrollTop + clientHeight >= scrollHeight - 100) {
            let postId =
                postList.length > 0 ? postList[postList.length - 1].id : null;
            getPostList(postId);
        }
    };

    useEffect(() => {
        if (props.scroller && !props.single) {
            onScroll(props.scroller);
        }
        return window.scrollTo(0, 0);
    }, [props.scroller]);

    const getTopBarCommunities = async () => {
        let response = await new CommunityController().communityList({
            topbar: 1
        });
        if (response && response.status) {
            setCommunities(response.communities);
        } else {
            setCommunities([]);
        }
    };

    /** Chat Modal */

    /** Chat Modal */

    return (
        <div className="row">
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                {
                    props.single
                    ?
                        <div className="timeline_title">
                            <h2>
                                <div onClick={() => history.push('/')}>
                                    <i className="fas fa-arrow-left"></i>
                                </div>
                                Back
                            </h2>
                        </div>
                    :
                        <div id="story-main">
                            <Story list={communities} />
                        </div>
                }
                <div className="timeline_scroll_section">
                    <div className="row">
                        <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <div className="timeline_section">
                                {/* <Spinners />*/}

                                {edit || props.single ? null : (
                                    <AddPost updatePosts={updatePosts} />
                                )}
                                {!noData && postList && postList.length > 0
                                    ? 
                                    <>
                                        <Post
                                            single={props.single ? true : false}
                                            postList={postList}
                                            showAddPost={(value) => setEdit(value)}
                                            updatePosts={updatePosts}
                                        />
                                        {
                                            loader
                                            &&
                                            <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>
                                        }
                                    </>
                                    :
                                    (<>{loader && <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>}</>)
                                }
                                {
                                    noData
                                    &&
                                    <Nodata />
                                }
                            </div>
                        </div>

                        {/* <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <Suggestions />
                        </div> */}
                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 d-lg-block d-none">
                            <ChatSection />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapState = (state) => {
    return {
        scroller: state.ScrollerReducer.scroller
    };
};
const mapDispatch = {};

export default connect(mapState, mapDispatch)(Home);
