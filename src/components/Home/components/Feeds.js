import React from "react";
import { Link } from "react-router-dom";

//assets
import story from "../../../assets/images/sidebar/story.jpg";
import photo from "../../../assets/images/sidebar/photo.png";
import video from "../../../assets/images/sidebar/video.png";
import audio from "../../../assets/images/sidebar/audio.png";
import postimg from "../../../assets/images/sidebar/postimg.jpg";
import home from "../../../assets/images/sidebar/home.png";
import community from "../../../assets/images/sidebar/community.png";
import chat from "../../../assets/images/sidebar/chat.png";
import landing_callie from "../../../assets/images/Landing/callie.png";

const Feeds = () => {
    return (
        <section className="profile_section">
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div className="phone_menu d-sm-none d-block">
                    <ul>
                        <li>
                            <Link to="/">
                                <img src={home} alt=".." />
                            </Link>
                        </li>
                        <li>
                            <Link to="/">
                                <img src={chat} alt=".." />
                            </Link>
                        </li>
                        <li>
                            <Link to="/">
                                <img src={community} alt=".." />
                            </Link>
                        </li>
                        <li>
                            <Link to="/">
                                <img src={chat} alt=".." />
                            </Link>
                        </li>
                        <li>
                            <Link to="/">
                                <img src={community} alt=".." />
                            </Link>
                        </li>
                    </ul>
                </div>
                <div className="story ">
                    <ul>
                        <li>
                            <div className="user-created">
                                <div className="created">
                                    <Link to="/">
                                        <i className="fal fa-plus"></i>
                                    </Link>
                                </div>
                                <div className="title">
                                    <p>Create</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="user-created">
                                <div className="create-img">
                                    <img src={story} alt="..." />
                                </div>
                                <div className="title">
                                    <p>Create</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="user-created">
                                <div className="create-img">
                                    <img src={story} alt="..." />
                                </div>
                                <div className="title">
                                    <p>Friends</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="user-created">
                                <div className="create-img">
                                    <img src={story} alt="..." />
                                </div>
                                <div className="title">
                                    <p>Good Vibes</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="user-created">
                                <div className="create-img">
                                    <img src={story} alt="..." />
                                </div>
                                <div className="title">
                                    <p>Peace</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="user-created">
                                <div className="create-img">
                                    <img src={story} alt="..." />
                                </div>
                                <div className="title">
                                    <p>Dor Sit Lorem</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="user-created">
                                <div className="create-img">
                                    <img src={story} alt="..." />
                                </div>
                                <div className="title">
                                    <p>Sweet Escape</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="user-created">
                                <div className="create-img">
                                    <img src={story} alt="..." />
                                </div>
                                <div className="title">
                                    <p>Daydream</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="story-post">
                    <div className="row">
                        <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <div className="story-post-wrap poster">
                                <div className="post_col post select">
                                    <div className="before_post">
                                        <div className="imgwrap">
                                            <img src={landing_callie} alt="" />
                                        </div>
                                    </div>
                                    <div className="main_post">
                                        <div className="post-message">
                                            <form>
                                                <div className="form-group">
                                                    <input
                                                        className="message"
                                                        type="text"
                                                        name=""
                                                        placeholder="How are you feeling?"
                                                    />
                                                </div>
                                            </form>
                                        </div>
                                        <div className="post_category">
                                            <Link to="/">
                                                <div className="photo">
                                                    <img src={photo} alt=".." />
                                                    <p>Photo</p>
                                                </div>
                                            </Link>
                                            <Link to="/">
                                                <div className="photo">
                                                    <img src={video} alt=".." />
                                                    <p>Video</p>
                                                </div>
                                            </Link>
                                            <Link to="/">
                                                <div className="photo">
                                                    <img src={audio} alt=".." />
                                                    <p>Audio</p>
                                                </div>
                                            </Link>
                                        </div>
                                        <div className="post_addtag">
                                            <div className="form-group">
                                                <select
                                                    id="inputcategory"
                                                    className="form-select"
                                                >
                                                    <option selected="">
                                                        Add tags
                                                    </option>
                                                    <option>..</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="post_addtag">
                                            <div className="form-group">
                                                <select
                                                    id="inputcommunity"
                                                    className="form-select"
                                                >
                                                    <option selected="">
                                                        Select community
                                                    </option>
                                                    <option>..</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="post_public-private">
                                            <div className="custom-control-radio custom-radio">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="upload"
                                                    id="r-3"
                                                    value="3"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    htmlFor="r-3"
                                                >
                                                    Post anonymously
                                                </label>
                                            </div>
                                            <div className="custom-control-radio custom-radio">
                                                <input
                                                    className="form-check-input"
                                                    type="radio"
                                                    name="upload"
                                                    id="r-4"
                                                    value="4"
                                                />
                                                <label
                                                    className="form-check-label"
                                                    htmlFor="r-4"
                                                >
                                                    Public Post
                                                </label>
                                            </div>
                                        </div>
                                        <div className="post_btn">
                                            <button
                                                type="submit"
                                                className="main_btn lav_bg w-100"
                                            >
                                                Post
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="story-post-wrap">
                                <div className="post_col post ">
                                    <div className="before_post">
                                        <div className="imgwrap">
                                            <img src={landing_callie} alt="" />
                                        </div>
                                    </div>
                                    <div className="main_post">
                                        <div className="post_head">
                                            <p className="user_info">
                                                <a href="/" className="user">
                                                    Callie Parker
                                                </a>
                                                <span className="lav_dot lav_bg"></span>
                                                <a href="/" className="comm">
                                                    Good Space
                                                </a>
                                            </p>
                                            <p className="post_time">2m ago</p>
                                        </div>
                                        <div className="post_body">
                                            <div className="post_tags flexie">
                                                <div className="post_tag">
                                                    <a href="/">Gratitude</a>
                                                </div>
                                                <div className="post_tag">
                                                    <a href="/">Hustle</a>
                                                </div>
                                            </div>
                                            <div className="post_text">
                                                <p>
                                                    Can't believe that 2021 has
                                                    ended. It was a mix of
                                                    different
                                                    emotions,experiences and
                                                    adventures. Wanted to
                                                    explore it more but yeah
                                                    eventually everything comes
                                                    to an end.
                                                </p>
                                            </div>
                                            <div className="post_info flexie">
                                                <p className="likes">
                                                    250 Likes
                                                </p>
                                                <p className="comments">
                                                    110 comments
                                                </p>
                                            </div>
                                        </div>
                                        <div className="post_foot flexie">
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="far fa-heart like flexie "></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fal fa-comment-alt comment flexie"></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fas fa-paper-plane share flexie"></i>
                                                    <i className="far fa-paper-plane share flexie"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="after_post">
                                        <i className="fal fa-ellipsis-h"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="story-post-wrap">
                                <div className="post_col post ">
                                    <div className="before_post">
                                        <div className="imgwrap">
                                            <img src={landing_callie} alt="" />
                                        </div>
                                    </div>
                                    <div className="main_post">
                                        <div className="post_head">
                                            <p className="user_info">
                                                <a href="/" className="user">
                                                    Ben Musk
                                                </a>
                                                <span className="lav_dot lav_bg"></span>
                                                <a href="/" className="comm">
                                                    Friends
                                                </a>
                                            </p>
                                            <p className="post_time">4m ago</p>
                                        </div>
                                        <div className="post_body">
                                            <div className="post_tags flexie">
                                                <div className="post_tag">
                                                    <a href="/">Lorem</a>
                                                </div>
                                                <div className="post_tag">
                                                    <a href="/">Ipsme</a>
                                                </div>
                                                <div className="post_tag">
                                                    <a href="/">Dor</a>
                                                </div>
                                            </div>
                                            <div className="post_text">
                                                <p>
                                                    Lorem ipsum dolor sit amet,
                                                    sit adipiscing elit.
                                                    &#128512;
                                                </p>
                                            </div>
                                            <div className="post_img">
                                                <img src={postimg} alt="" />
                                            </div>
                                            <div className="post_info flexie">
                                                <p className="likes">
                                                    305 Likes
                                                </p>
                                                <p className="comments">
                                                    20 comments
                                                </p>
                                            </div>
                                        </div>
                                        <div className="post_foot flexie">
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="far fa-heart like flexie "></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fal fa-comment-alt comment flexie"></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fas fa-paper-plane share flexie"></i>
                                                    <i className="far fa-paper-plane share flexie"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="after_post">
                                        <i className="fal fa-ellipsis-h"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="story-post-wrap">
                                <div className="post_col post ">
                                    <div className="before_post">
                                        <div className="imgwrap">
                                            <img src={landing_callie} alt="" />
                                        </div>
                                    </div>
                                    <div className="main_post">
                                        <div className="post_head">
                                            <p className="user_info">
                                                <a href="/" className="user">
                                                    Anonymous
                                                </a>
                                                <span className="lav_dot lav_bg"></span>
                                                <a href="/" className="comm">
                                                    Good vibes
                                                </a>
                                            </p>
                                            <p className="post_time">10m ago</p>
                                        </div>
                                        <div className="post_body">
                                            <div className="post_tags flexie">
                                                <div className="post_tag">
                                                    <a href="/">Lorem</a>
                                                </div>
                                                <div className="post_tag">
                                                    <a href="/">Ipsme</a>
                                                </div>
                                            </div>
                                            <div className="post_text">
                                                <p>
                                                    Lorem ipsum dolor sit amet,
                                                    sit adipiscing elit.
                                                    &#128519;
                                                </p>
                                            </div>
                                            <div className="post_img">
                                                <img
                                                    className=""
                                                    src={postimg}
                                                    alt="..."
                                                />
                                                <div className="video_icon">
                                                    <a href="sdsd" className="">
                                                        <i className="fas fa-play"></i>
                                                    </a>
                                                </div>
                                                <video
                                                    className="d-none"
                                                    autoPlay=""
                                                    loop=""
                                                    muted=""
                                                >
                                                    <source
                                                        src="/"
                                                        type="video/mp4"
                                                    />
                                                </video>
                                                <iframe
                                                    src=""
                                                    title="sdsds"
                                                    frameBorder="0"
                                                    className="d-none"
                                                ></iframe>
                                            </div>

                                            <div className="post_info flexie">
                                                <p className="likes">
                                                    305 Likes
                                                </p>
                                                <p className="comments">
                                                    20 comments
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="after_post">
                                        <i className="fal fa-ellipsis-h"></i>
                                    </div>
                                </div>
                                <div className="post_foot flexie">
                                    <div className="iconwrap">
                                        <a href="/">
                                            <i className="far fa-heart like flexie "></i>
                                        </a>
                                    </div>
                                    <div className="dividor"></div>
                                    <div className="iconwrap">
                                        <a href="/">
                                            <i className="fal fa-comment-alt comment flexie"></i>
                                        </a>
                                    </div>
                                    <div className="dividor"></div>
                                    <div className="iconwrap">
                                        <a href="/">
                                            <i className="fas fa-paper-plane share flexie"></i>
                                            <i className="far fa-paper-plane share flexie"></i>
                                        </a>
                                    </div>
                                </div>
                                <div className="post_comment-view">
                                    <div className="comment-count">
                                        <p>View 31 previous comments</p>
                                    </div>
                                    <div className="comments_view">
                                        <div className="user_img">
                                            <img
                                                src={landing_callie}
                                                alt=".."
                                            />
                                        </div>
                                        <div className="comment_detail">
                                            <div className="name">
                                                <p>Kavin Musk </p>
                                                <span>
                                                    Lorem ipsum dolor sit amet
                                                </span>
                                                <i>4m</i>
                                            </div>
                                            <div className="likes">
                                                <a href="/">
                                                    3
                                                    <i className="far fa-heart like "></i>
                                                </a>
                                                <a href="/">
                                                    2
                                                    <i className="reply">
                                                        Reply
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="after_post">
                                            <i className="fal fa-ellipsis-h"></i>
                                        </div>
                                    </div>
                                    <div className="comments_view under-comment">
                                        <div className="user_img">
                                            <img
                                                src={landing_callie}
                                                alt=".."
                                            />
                                        </div>
                                        <div className="comment_detail">
                                            <div className="name">
                                                <p>Janessa</p>
                                                <span>Lorem ipsum </span>
                                                <i>2m</i>
                                            </div>
                                            <div className="likes">
                                                <a href="/">
                                                    1
                                                    <i className="far fa-heart like "></i>
                                                </a>
                                                <a href="/">
                                                    <i className="reply">
                                                        Reply
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="after_post">
                                            <i className="fal fa-ellipsis-h"></i>
                                        </div>
                                    </div>
                                    <div className="comments_view under-comment">
                                        <div className="user_img">
                                            <img
                                                src={landing_callie}
                                                alt=".."
                                            />
                                        </div>
                                        <div className="comment_detail">
                                            <div className="name">
                                                <p>Nathalia</p>
                                                <span>Lorem ipsum </span>
                                                <i>1m</i>
                                            </div>
                                            <div className="likes">
                                                <a href="/">
                                                    1
                                                    <i className="far fa-heart like "></i>
                                                </a>
                                                <a href="/">
                                                    <i className="reply">
                                                        Reply
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="after_post">
                                            <i className="fal fa-ellipsis-h"></i>
                                        </div>
                                    </div>
                                    <div className="comments_view">
                                        <div className="user_img">
                                            <img
                                                src={landing_callie}
                                                alt=".."
                                            />
                                        </div>
                                        <div className="comment_detail">
                                            <div className="name">
                                                <p>Harry Lee</p>
                                                <span>
                                                    Lorem ipsum dolor sit amet,
                                                </span>
                                                <i>5m</i>
                                            </div>
                                            <div className="likes">
                                                <a href="/">
                                                    3
                                                    <i className="far fa-heart like "></i>
                                                </a>
                                                <a href="/">
                                                    <i className="reply">
                                                        Reply
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="after_post">
                                            <i className="fal fa-ellipsis-h"></i>
                                        </div>
                                    </div>
                                </div>
                                <div className="comment_write">
                                    <div className="before_post">
                                        <div className="imgwrap">
                                            <img src={landing_callie} alt="" />
                                        </div>
                                    </div>
                                    <div className="comment_type">
                                        <form>
                                            <div className="form-group">
                                                <input
                                                    className="comment"
                                                    type="text"
                                                    name=""
                                                    placeholder="Write a comment"
                                                />
                                                <div className="emoji">
                                                    <p>&#128512;</p>
                                                </div>
                                            </div>
                                            <div className="add_comment">
                                                <button
                                                    type="submit"
                                                    className="main_btn lav_bg"
                                                >
                                                    Add
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="story-post-wrap">
                                <div className="post_col post ">
                                    <div className="before_post">
                                        <div className="imgwrap">
                                            <img src={landing_callie} alt="" />
                                        </div>
                                    </div>
                                    <div className="main_post">
                                        <div className="post_head">
                                            <p className="user_info">
                                                <a href="/" className="user">
                                                    Anonymous
                                                </a>
                                            </p>
                                            <p className="post_time">2h ago</p>
                                        </div>
                                        <div className="post_body">
                                            <div className="post_tags flexie">
                                                <div className="post_tag">
                                                    <a href="/">Lorem </a>
                                                </div>
                                                <div className="post_tag">
                                                    <a href="/">Ipsum</a>
                                                </div>
                                                <div className="post_tag">
                                                    <a href="/">Dor</a>
                                                </div>
                                            </div>
                                            <div className="post_text">
                                                <p>
                                                    Lorem ipsum dolor sit amet,
                                                    consectetur adipiscing elit.
                                                    Mauris hendrerit scelerisque
                                                    mi, eu pretium turpis at.
                                                </p>
                                            </div>
                                            <div className="post_info flexie">
                                                <p className="likes">
                                                    50 Likes
                                                </p>
                                                <p className="comments">
                                                    20 comments
                                                </p>
                                            </div>
                                        </div>
                                        <div className="post_foot flexie">
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="far fa-heart like flexie "></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fal fa-comment-alt comment flexie"></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fas fa-paper-plane share flexie"></i>
                                                    <i className="far fa-paper-plane share flexie"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="after_post">
                                        <i className="fal fa-ellipsis-h"></i>
                                    </div>
                                </div>
                            </div>
                            <div className="story-post-wrap">
                                <div className="post_col post ">
                                    <div className="before_post">
                                        <div className="imgwrap">
                                            <img src={landing_callie} alt="" />
                                        </div>
                                    </div>
                                    <div className="main_post">
                                        <div className="post_head">
                                            <p className="user_info">
                                                <a href="/" className="user">
                                                    Kendra
                                                </a>
                                                <span className="lav_dot lav_bg"></span>
                                                <a href="/" className="comm">
                                                    Good vibes
                                                </a>
                                            </p>
                                            <p className="post_time">8h ago</p>
                                        </div>
                                        <div className="post_body">
                                            <div className="post_tags flexie">
                                                <div className="post_tag">
                                                    <a href="/">Lorem</a>
                                                </div>
                                            </div>
                                            <div className="post_text">
                                                <p>
                                                    Lorem ipsum dolor sit amet,
                                                    consectetur adipiscing elit.
                                                    Mauris hendrerit scelerisque
                                                    mi, eu pretium turpis at.
                                                </p>
                                            </div>
                                            <div className="post_info flexie">
                                                <p className="likes">
                                                    102 Likes
                                                </p>
                                                <p className="comments">
                                                    12 comments
                                                </p>
                                            </div>
                                        </div>
                                        <div className="post_foot flexie">
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="far fa-heart like flexie "></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fal fa-comment-alt comment flexie"></i>
                                                </a>
                                            </div>
                                            <div className="dividor"></div>
                                            <div className="iconwrap">
                                                <a href="/">
                                                    <i className="fas fa-paper-plane share flexie"></i>
                                                    <i className="far fa-paper-plane share flexie"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="after_post">
                                        <i className="fal fa-ellipsis-h"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <div className="community_suggestions d-lg-block d-none">
                                <div className="community-head">
                                    <div className="name">
                                        <p>Suggested Communities</p>
                                    </div>
                                    <div className="icon">
                                        <a href="/">
                                            <i className="fal fa-ellipsis-h"></i>
                                        </a>
                                    </div>
                                </div>
                                <div className="suggestion_list">
                                    <ul>
                                        <li>
                                            <div className="img_area">
                                                <img
                                                    src={landing_callie}
                                                    alt=".."
                                                />
                                            </div>
                                            <div className="name">
                                                <p>Wish</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="img_area">
                                                <img
                                                    src={landing_callie}
                                                    alt=".."
                                                />
                                            </div>
                                            <div className="name">
                                                <p>We Pray</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="img_area">
                                                <img
                                                    src={landing_callie}
                                                    alt=".."
                                                />
                                            </div>
                                            <div className="name">
                                                <p>Happiness</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="img_area">
                                                <img
                                                    src={landing_callie}
                                                    alt=".."
                                                />
                                            </div>
                                            <div className="name">
                                                <p>Placidity</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="img_area">
                                                <img
                                                    src={landing_callie}
                                                    alt=".."
                                                />
                                            </div>
                                            <div className="name">
                                                <p>Calm</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="img_area">
                                                <img
                                                    src={landing_callie}
                                                    alt=".."
                                                />
                                            </div>
                                            <div className="name">
                                                <p>Dreams</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Feeds;
