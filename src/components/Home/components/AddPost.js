import Downshift from "downshift";
import React, { useEffect, useState } from "react";
//helpers
import { WithContext as ReactTags } from "react-tag-input";
import audio from "../../../assets/images/sidebar/audio.png";
//assets
import photo from "../../../assets/images/sidebar/photo.png";
import AuthController from "../../../controllers/auth.controller";
import CommunityController from "../../../controllers/community.controller";
//helpers
import PostController from "../../../controllers/post.controller";
import { baseUrl, renderImage, renderVideoThumb } from "../../../helpers/General";
import { Toast } from "../../../helpers/Toaster";
import Picker from "emoji-picker-react";
import feedback_4 from "../../../assets/images/Feedback/4.png";
import ProgressCircle from "../../Layout/components/ProgressCircle";
import { Link, useHistory } from "react-router-dom";
import ProfileController from "../../../controllers/profile.controller";
import { Dropdown } from "react-bootstrap";
import { Constant } from "../../../services/constants";

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
  </a>
));

const Keys = {
    TAB: 9,
    SPACE: 32,
    COMMA: 188,
    Enter: 13
};
const AddPost = (props) => {
    let history = useHistory();
    let inputRef = React.useRef(null);

    const [keyword, setSearchKeyword] = useState("");
    const [list, setList] = useState([]);
    const [showEmoji, setShowEmoji] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [mediaDisplay, setMediaDisplay] = useState(false);
    const [media, setRemoveMediaList] = useState([]);
    const [files, setMediaFiles] = useState([]);
    const [errMsg, setErrMsg] = useState(false);
    const [tagInput, setTagInput] = useState("");
    const [type, setType] = useState("");
    const [progress, setProgress] = useState(null);
    const [openCommunity, setOpenCommunity] = useState(false);
    const [textCursor, setTextCursor] =  useState(0);
    
    
    const [postAllow, setPostAllow] = useState(false);
    const [postData, setPostData] = useState({
        description: "",
        type: "",
        file: [],
        group_id: null,
        tags: [],
        anonymously: 0,
        is_private: 0
    });

    //validations --start
    let defaultValues = {
        description: "",
        type: "",
        file: [],
        group_id: "",
        tags: [],
        anonymously: 0,
        is_private: 0
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        getUser();
        getList();
        if (props && props.id) {
            getPostDetail();
        } else {
            setPostData(defaultValues);
        }

        console.log(props.selectedGroup);
        if (props.selectedGroup) {
            setSearchKeyword(props.selectedGroup.title);
            setPostData({ ...defaultValues, group_id: props.selectedGroup.slug});
        }
    }, []);
    
    
    const getList = async (value) => {
        const response = await new CommunityController().searchMyCommunities(value);
        if (response && response.status) {
            setList(response.communities);
        } else {
            setList([]);
        }
    };

    //Post Detail(In Case of Edit Post)
    const getPostDetail = async () => {
        let response = await new PostController().postDetail(props.id);
        if (response && response.status) {
            let detail = response.post;
            let myTags = [];
            let imgs = [];
            for (let i in detail.tags) {
                myTags.push({
                    id: detail.tags[i].tag,
                    text: detail.tags[i].tag
                });
            }
            for (let i in detail.media) {
                imgs.push(detail.media[i].file);
            }
            setMediaDisplay(imgs.length > 0 ? true : false);
            setTags(myTags);
            setTagInput("");
            setType(detail.type);
            setMediaFiles(detail.media);
            setPostData({
                description: detail.description,
                type: detail.type,
                file: imgs,
                group_id: detail.group_id ? detail.group.slug : null,
                tags: [],
                anonymously: detail.anonymously,
                is_private: detail.is_private
            });
        } else {
            console.log(response.error);
        }
    };

    //Get Logined user Detail
    const getUser = async () => {
        let user1 = await new AuthController().getLoginUser();
        setUser(user1);
    };

    const handleStates = (field, value) => {
        setPostData({ ...postData, [field]: value });
        /** Validate each field on change */
    };
    //validations --end

    //image upload  --start
    const uploadPhoto = async (value) => {
        if (['image/jpeg', 'image/png', 'image/gif', 'image/webp', 'video/mp4', 'video/quicktime'].includes(value.type)) {
            if (value.size < Constant.maxFileSize) {
                if (isLoading === false) {
                    setErrMsg(false);
                    setIsLoading(true);
                    setMediaDisplay(true);
                    let callback = (p) => {
                        setProgress(p > 5 ? (p * 1 - 5) : p);
                    };

                    let response = null;
                    if ('video/mp4' === value.type) {
                        response = await new PostController().upload_video(value, 'video', callback);
                    }
                    else {
                        response = await new PostController().upload_photo(value, callback);
                    }
                    setIsLoading(false);
                    
                    if (response && response.status) {
                        setTimeout(() => {
                            setProgress(null);
                            let array = postData.file;
                            array.push(response.path);
                            setPostData({ ...postData, file: array });
                        }, 500);
                    } else {
                        if (!postData.file || postData.file.length < 1) {
                            setMediaDisplay(false);
                        }
                        setErrMsg(response.error);
                        setProgress(null);
                    }
                }
            }
            else {
                setErrMsg('File size exceeds the maximum limit of 25MB.');
            }
        }
        else {
            setErrMsg('Please upload file having extension jpg, jpeg, png, gif, webp, mp4 or mov');
        }
    };

    //audio upload  --start
    const uploadAudio = async (value, val) => {
        if (['audio/aac', 'audio/mpeg', 'audio/wav', 'audio/x-wav', 'audio/vnd.wav'].includes(value.type)) {
            if (value.size < Constant.maxFileSize) {
                if (isLoading === false) {
                    setErrMsg(false);
                    setIsLoading(true);
                    setMediaDisplay(true);
                    let callback = (p) => {
                        setProgress(p > 5 ? (p * 1 - 5) : p);
                    };
                    let response = await new PostController().upload_audio(value, callback);
                    setIsLoading(false);
                    setProgress(null);
                    if (response && response.status) {
                        setTimeout(() => {
                            setProgress(null);
                            let array = postData.file;
                            array.push(response.path);
                            setPostData({ ...postData, file: array });
                        }, 500);
                    } else {
                        if (!postData.file || postData.file.length < 1) {
                            setMediaDisplay(false);
                        }
                        setErrMsg(response.error);
                        setProgress(null);
                    }
                }
            }
            else {
                setErrMsg('File size exceeds the maximum limit of 25MB.');
            }
        }
        else {
            setErrMsg('Please upload audio file having extension mp3, wav or aac.');
        }
    };
    //audio upload  --end

    //delete photo  --start
    const deleteMedia = (val) => {
        let newArray = postData;
        let array = [...media];
        if (val !== -1) {
            let oldMedia = [...files];
            let i = oldMedia.indexOf(
                oldMedia.filter(function (item) {
                    return item.file === newArray.file[val];
                })[0]
            );
            newArray.file.splice(val, 1);
            if (i !== -1) {
                array.push(oldMedia[i].id);
            }
            setRemoveMediaList(array);
        }

        setPostData(newArray);
        if (postData.file.length < 1) {
            setMediaDisplay(false);
        }
    };
    //delete photo  --end


    //post  --start
    const createPost = async () => {
        if (props && props.id) {
            updatePost();
        } else {
            newPost();
        }
    };

    //Update existing Post
    const updatePost = async () => {
        if (isLoading === false) {
            let reqData = { ...postData };
            setErrMsg(false);
            setIsLoading(true);
            let oldMedia = [...files];
            let array = [...reqData.file];
            for (let i = 0; i < oldMedia.length; i++) {
                let index = array.indexOf(
                    array.filter(function (item) {
                        return item === oldMedia[i].file;
                    })[0]
                );
                if (index !== -1) {
                    array.splice(index, 1);
                }
            }
            reqData.tags = tags;
            reqData.type = type;
            reqData.file = array;
            let response = await new PostController().editPost(
                reqData,
                props.id,
                media
            );
            setIsLoading(false);
            if (response && response.status) {
                props.updatePosts(response.data);
                new Toast().success(response.message);
                props.close();
                setPostData(defaultValues);
                setTagInput("");
                setTags([]);
                setSearchKeyword("");
                setMediaDisplay(false);
            } else {
                setErrMsg(response.error);
            }
        }
    };
    //Create new Post
    const newPost = async () => {
        if (isLoading === false) {
            setErrMsg(false);
            setIsLoading(true);
            postData.tags = tags;
            postData.type = type;
            let response = await new PostController().create_post(postData);
            setIsLoading(false);
            if (response && response.status) {
                props.updatePosts();
                setPostData(defaultValues);
                setTagInput("");
                setTags([]);
                setSearchKeyword("");
                setMediaDisplay(false);
                new Toast().success(response.message);
            } else {
                setErrMsg(response.error);
            }
        }
    };

    //post  --end

    //tags  --start
    const suggestions = [
        { id: "0", text: "Lorem" },
        { id: "1", text: "Ipsum" }
    ];

    const [tags, setTags] = React.useState([]);

    const handleDelete = (i) => {
        setTags(tags.filter((tag, index) => index !== i));
    };

    const handleAddition = (tag) => {
        setTagInput("");

        if (tags.length >= 3) {
            setErrMsg("Maximum 3 tags are allowed");
        } else {
            setTags([...tags, tag]);
            setErrMsg("");
        }

        
    };

    const handleDrag = (tag, currPos, newPos) => {
        const newTags = tags.slice();

        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);

        // re-render
        setTags(newTags);
    };

    const handleTagClick = (index) => {
        console.log("The tag at index " + index + " was clicked");
    };

    //tags --end

    const onEmojiClick = (event, emojiObject) => {
        if (postData.description.trim() != '') {
            const text = postData.description.slice(0, textCursor) + ' ' + emojiObject.emoji + (postData.description.slice(textCursor).trim() ? ' ' + postData.description.slice(textCursor).trim() : '');
            handleStates(
                "description",
                text
            );
            setTextCursor(textCursor + 3);
        }
        else {
            handleStates(
                "description",
                emojiObject.emoji
            );
            setTextCursor(textCursor + 2);    
        }
    };
    
    return (
        <div className="story-post-wrap poster mod">
            <div className="post_col post select">
                <div className="before_post">
                    <div className="imgwrap">
                        <img src={user && renderImage(user.image)} alt="..." />
                    </div>
                </div>
                <div className="main_post">
                    <form>
                        <div
                            className="post-message"
                        >
                            <label
                                className="form_controls form-group "
                                htmlFor="description"
                            >
                                <textarea
                                    ref={inputRef}
                                    type="text"
                                    rows={2}
                                    className="message custom_scroll"
                                    placeholder="How are you feeling?"
                                    onClick={(e) => {
                                        setTimeout(() => {
                                            setTextCursor(inputRef.current.selectionStart);
                                        }, 100);
                                    }}
                                    onChange={(e) => {
                                        handleStates(
                                            "description",
                                            e.target.value
                                        );
                                        setTextCursor(inputRef.current.selectionStart);
                                    }}
                                    onKeyPress={(e) => {
                                        setTextCursor(inputRef.current.selectionStart);
                                    }}
                                    value={postData.description}
                                />

                                <Dropdown
                                    className={`floatimg active`}
                                >
                                    <Dropdown.Toggle as={CustomToggle}>
                                        <img
                                            src={feedback_4} alt="..."
                                        />
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                            <Picker
                                                onEmojiClick={onEmojiClick}
                                            />
                                    </Dropdown.Menu>
                                </Dropdown>
                            </label>
                        </div>
                        {!mediaDisplay && (
                            <div className="post_category">
                                <label htmlFor="photo">
                                    <div className="photo">
                                        <img src={photo} alt=".." />
                                        <p>Photo / Video</p>
                                    </div>
                                </label>
                                <input
                                    id="photo"
                                    type="file"
                                    onChange={(e) => {
                                        setType("photo");
                                        uploadPhoto(e.target.files[0]);
                                    }}
                                    accept="image/jpeg,image/png,image/gif,image/webp,video/mp4,video/quicktime"
                                />

                                {/* <label htmlFor="file-video">
                                    <div className="photo">
                                        <img src={video} alt=".." />
                                        <p>Video</p>
                                    </div>
                                </label> 
                                <input
                                    id="file-video"
                                    type="file"
                                    onChange={(e) => {
                                        setType("video");
                                        uploadVideo(e.target.files[0]);
                                    }}
                                    accept="video/*"
                                />*/}

                                <label htmlFor="file-audio">
                                    <div className="photo audio">
                                        <img src={audio} alt=".." />
                                        <p>Audio</p>
                                    </div>
                                </label>
                                <input
                                    id="file-audio"
                                    type="file"
                                    onChange={(e) => {
                                        setType("audio");
                                        uploadAudio(e.target.files[0]);
                                    }}
                                    accept="audio/aac,audio/mpeg,audio/wav,audio/x-wav,audio/vnd.wav"
                                />
                            </div>
                        )}
                        {mediaDisplay  && (
                            <div className="media_preview">
                                <div className="media_list flexie">
                                    {postData.file.map((item, key) => {
                                        let ext = item.split('/');
                                        ext = ext[ext.length - 1].split('.');
                                        ext = ext[ext.length - 1].toLowerCase();

                                        return (
                                            <div key={`media` + key}>
                                                {key <= 3 ? (
                                                    <div
                                                        className={`media_box`}
                                                        key={key}
                                                    >
                                                        {
                                                            ext === 'mp4' || ext === 'mov'
                                                            ?
                                                                <>
                                                                    <img
                                                                        src={renderVideoThumb(item)}
                                                                        alt="..."
                                                                    />
                                                                    <i className="fas fa-solid fa-play"></i>
                                                                </>
                                                            :
                                                                (
                                                                    ext === 'mp3' || ext === 'wav' || ext == 'aac'
                                                                    ?
                                                                        <>
                                                                            <div className="audio-background"></div>
                                                                            <div className="play">
                                                                                <i className="fas fa-solid fa-play"></i>
                                                                            </div>
                                                                        </>
                                                                    :
                                                                        <img
                                                                            src={baseUrl(item)}
                                                                            alt="..."
                                                                        />

                                                                )    
                                                                
                                                        }
                                                        
                                                        <div
                                                            className="close_icon flexie"
                                                            onClick={() => {
                                                                deleteMedia(
                                                                    key
                                                                );
                                                            }}
                                                        >
                                                            <i className="fal fa-times"></i>
                                                        </div>
                                                    </div>
                                                ) : null}
                                            </div>
                                        );
                                    })}

                                    {postData.file.length <= 3 ? (
                                        <label htmlFor="file-photo_n">
                                            {
                                                isLoading && progress
                                                ?
                                                    <div className="media_box add_box flexie">
                                                        <ProgressCircle
                                                            progress={progress}
                                                        />
                                                    </div>
                                                :
                                                    <div className="media_box add_box flexie">
                                                        {
                                                            type === 'photo'
                                                                ?
                                                                <input
                                                                    id="file-photo_n"
                                                                    type="file"
                                                                    onChange={(e) => {
                                                                        let files = e.target.files[0];
                                                                        e.target.value = null;
                                                                        uploadPhoto(files);
                                                                    }}
                                                                    accept="image/jpeg,image/png,image/gif,image/webp,video/mp4,video/quicktime"
                                                                />
                                                                :
                                                                <input
                                                                    id="file-photo_n"
                                                                    type="file"
                                                                    onChange={(e) => {
                                                                        let files = e.target.files[0];
                                                                        e.target.value = null;
                                                                        uploadAudio(files);
                                                                    }}
                                                                    accept="audio/mpeg,audio/wav,audio/x-wav,audio/vnd.wav"
                                                                />
                                                        }
                                                        <i className="fal fa-plus"></i>
                                                    </div>
                                            }
                                        </label>
                                    ) : null}
                                </div>
                            </div>
                        )}
                        
                        <div className="post_addtag">
                            <ReactTags
                                tags={tags}
                                delimiters={[Keys.TAB, Keys.SPACE, Keys.COMMA, Keys.Enter]}
                                suggestions={suggestions}
                                handleDelete={handleDelete}
                                handleAddition={handleAddition}
                                handleTagClick={handleTagClick}
                                inputFieldPosition="bottom"
                                placeholder="Add your tags"
                                autoComplete ={false}
                                autofocus={false}
                                inline
                                inputValue={tagInput}
                                handleInputChange={(e) => setTagInput(e)}
                                inputProps={{
                                    disabled: tags.length <= 3 ? false : true
                                }}
                            />
                        </div>
                        {
                            !props.editCommunityOption
                            &&
                            <div className="seacrh-bar">
                                <Downshift
                                    isOpen={openCommunity}
                                    onChange={(item) => {
                                        if (item) {
                                            setSearchKeyword(item.title);
                                            handleStates("group_id", item.slug);
                                            setOpenCommunity(false);
                                        }
                                        else {
                                            setSearchKeyword("");
                                            handleStates("group_id", null);
                                            setOpenCommunity(true);
                                        }
                                    }}
                                    inputValue={keyword}
                                    itemToString={(item) => item.title}
                                    onInputValueChange={(e) => {
                                        if (!e || e !== keyword) {
                                            handleStates("group_id", null);
                                        }
                                        setSearchKeyword(e);
                                        setOpenCommunity(true);
                                    }}
                                >
                                    {({
                                        getInputProps,
                                        getItemProps,
                                        getLabelProps,
                                        getMenuProps,
                                        isOpen,
                                        inputValue,
                                        highlightedIndex,
                                        selectedItem,
                                        getRootProps,
                                    }) => (
                                        <div>
                                            <label
                                                {...getLabelProps()}
                                            ></label>
                                            <div
                                                // style={{
                                                //     display:
                                                //         "block",
                                                // }}
                                                {...getRootProps(
                                                    {},
                                                    {
                                                        suppressRefError: true,
                                                    }
                                                )}
                                            >
                                                <input
                                                    {...getInputProps()}
                                                    disabled={props.selectedGroup ? true : false}
                                                    onFocus={() => {
                                                        setOpenCommunity(true);
                                                    }}
                                                    onBlur={() => {
                                                        setOpenCommunity(false);
                                                    }}
                                                    type="search"
                                                    placeholder="Search community"
                                                />
                                            </div>
                                            <ul {...getMenuProps()}>
                                                {isOpen
                                                    ? list.filter(item => item.title.toLowerCase().indexOf(keyword.toLowerCase()) > -1).map(
                                                        (
                                                            item,
                                                            index
                                                        ) => (
                                                            <li
                                                                key={`baba` + index}
                                                                {...getItemProps(
                                                                    {
                                                                        key: item.id,
                                                                        index,
                                                                        item,
                                                                        style: {
                                                                            backgroundColor:
                                                                                highlightedIndex ===
                                                                                    index
                                                                                    ? "lavender"
                                                                                    : "white",
                                                                            fontWeight:"normal",
                                                                        },
                                                                    }
                                                                )}
                                                            >
                                                                <div className="searcher-filers">
                                                                    <div className="divider">
                                                                        <div className="left">
                                                                            <div className="img-wrap">
                                                                                <img
                                                                                    src={renderImage(
                                                                                        item.image
                                                                                    )}
                                                                                    alt=".../"
                                                                                />
                                                                            </div>
                                                                        </div>
                                                                        <div className="right">
                                                                            <div className="name">
                                                                                <h5>
                                                                                    {
                                                                                        item.title
                                                                                    }
                                                                                </h5>
                                                                                <small>Community</small>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        )
                                                    )
                                                    : null}
                                            </ul>
                                        </div>
                                    )}
                                </Downshift>
                            </div>
                        }
                        <div className="post_public-private">
                            <div className="custom-control-radio custom-radio">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="upload"
                                    id="r-3"
                                    value={1}
                                    checked={
                                        parseInt(postData.anonymously) === 0
                                            ? false
                                            : true
                                    }
                                    onChange={(e) => {
                                        handleStates(
                                            "anonymously",
                                            e.target.value
                                        );
                                    }}
                                />
                                <label
                                    className="form-check-label"
                                    htmlFor="r-3"
                                >
                                    Post Anonymously
                                </label>
                            </div>
                            <div className="custom-control-radio custom-radio">
                                <input
                                    className="form-check-input"
                                    type="radio"
                                    name="upload"
                                    id="r-4"
                                    checked={
                                        parseInt(postData.anonymously) === 1
                                            ? false
                                            : true
                                    }
                                    value={0}
                                    onChange={(e) => {
                                        handleStates(
                                            "anonymously",
                                            e.target.value
                                        );
                                    }}
                                />
                                <label
                                    className="form-check-label"
                                    htmlFor="r-4"
                                >
                                    Public Post
                                </label>
                            </div>
                        </div>
                        <div className="post_btn">
                            <button
                                type="button"
                                className={
                                    postData.description || (postData.file && postData.file.length > 0)
                                        ? "main_btn lav_bg w-100 "
                                        : "main_btn lav_bg w-100 btn_disable"
                                }
                                disabled={!(postData.description || (postData.file && postData.file.length > 0))}
                                onClick={() => {
                                    if (postData.description || (postData.file && postData.file.length > 0)) {   
                                        createPost();
                                    }
                                }}
                            >
                                Post
                            </button>
                            {errMsg ? (
                                <div className="content">
                                    <p className="err_msg">{errMsg}</p>
                                </div>
                            ) : null}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default AddPost;
