import React from "react";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { showCommunity } from "../../../redux/actions/user";
import HorizontalScroll from "./HorizontalScroll";
import StoryComponent from "./storyComponent";

const Story = (props) => {
    const history = useHistory();
    const addCommunity = () => {
        props.showCommunity(true);
        history.push("/community");
    };

    return (
        <>
            <div className="story">
                <ul>
                    <li>
                        <Link to="\">
                            <div className="user-created">
                                <div
                                    className="created"
                                    onClick={addCommunity}
                                >
                                    <i className="fal fa-plus"></i>
                                </div>
                                <div className="title">
                                    <p>Create</p>
                                </div>
                            </div>
                        </Link>
                    </li>
                </ul>
                {props.list.length > 0  && <HorizontalScroll list={props.list} />}
            </div>
        </>
    );
};

const mapState = (state) => {
    return {
        community: state.ShowCommunityReducer.show,
    };
};
const mapDispatch = (dispatch) => {
    return {
        showCommunity: async (info) => dispatch(showCommunity(info)),
    };
};
export default connect(mapState, mapDispatch)(Story);
