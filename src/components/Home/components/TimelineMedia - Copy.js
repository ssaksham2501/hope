import React, { useEffect } from "react";
import { baseUrl, renderVideoThumb, uuId } from "../../../helpers/General";
import Slider from "react-slick";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import AudioPlayer from '../../AudioPlayer/AudioPlayer';
import { linkInModal } from "../../../redux/actions/user";
import { Carousel } from "react-bootstrap";
const TimelineMedia = (props) => {
    
    const [items, setItems] = React.useState(props.items);
    const [play, setPlay] = React.useState(false);
    const [videoMode, setVideoMode] = React.useState('popup');

    useEffect(() => {
        if (!props.disableScroll) {
            let elem = document.getElementById('post-' + props.id);
            if (elem && props.scroller) {
                let scT = props.scroller.scrollTop + 300;
                if (scT > elem.offsetTop && scT < elem.offsetTop + elem.offsetHeight + 200) {
                    setPlay(true)
                }
                else {
                    setPlay(false)
                }
            }
        }
    }, [props.scroller]);

    useEffect(() => {
        if (!props.disableScroll) {
            setPlay(props.index * 1 < 1 ? true : false)
        }
    }, []);

    

    //slider --start
    const settings = {
        dots: true,
        arrows: true,
        fade: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };
    //slider --end

    if (items && items.length > 1) {
        return (
            <div className="post_img" id={`post-` + props.id}>
                <Carousel
                    interval={null}
                >
                    {items.map(
                        (i, key) => {
                            return (
                                <Carousel.Item key={`post-media-` + i.id}>
                                <div key={key} className={i.type === 'video' ? 'slide-media video-thumb' : 'slide-media'}>
                                    {
                                        i.type === 'audio'
                                            ?
                                            <div style={{ width: '100%', height: '100%', background: '#eb6383' }}>
                                                <AudioPlayer 
                                                    file={baseUrl(i.file)}
                                                    play={key < 1 && (play && !(props.pauseMedia && props.pauseMedia.includes(props.id))) ? true : false}
                                                    id={uuId() + props.id}
                                                    onPause={() => {
                                                        props.onPause(props.id);
                                                    }}
                                                />
                                            </div>
                                            :
                                            <img
                                                onClick={() => {
                                                    props.showLinkInGallery(items, key);
                                                }}
                                                className={i.type === 'video' ? 'video-thumb' : ''}
                                                id={key}
                                                src={
                                                    i.type === 'video'
                                                        ?
                                                        renderVideoThumb(i.file)
                                                        :
                                                        baseUrl(`${i.file}`)
                                                }
                                                alt="..."
                                            />
                                    }
                                    {(i.type === 'video')
                                        &&
                                        <div className="played">
                                        <i
                                            onClick={() => {
                                                    props.showLinkInGallery(items, key);
                                            }}
                                            className="fas fa-solid fa-play"
                                        ></i>
                                        </div>
                                    }
                                </div>
                                </Carousel.Item>
                            );
                        }
                    )}
                    
                </Carousel>
            </div>);
    }
    else if(items &&items.length > 0) {
        let item = items[0];
        return (
            <>
                {
                    item.type == 'audio'
                        ?
                        <div className="post_img audio_box flexie" id={`post-` + props.id}>
                            <AudioPlayer
                                file={baseUrl(item.file)}
                                play={(play && !(props.pauseMedia && props.pauseMedia.length > 1 && props.pauseMedia.includes(props.id)) ? true : false)}
                                id={uuId() + props.id}
                                onPause={() => {
                                    props.onPause(props.id);
                                }}
                            />
                        </div>
                    :
                        (
                            item.type == 'video'
                            ?
                                <div className="post_img video_box" id={`post-` + props.id}>
                                    {
                                        videoMode === 'player' || !props.disableScroll
                                            ?
                                            <ReactPlayer
                                                play={play && !(props.pauseMedia && props.pauseMedia.includes(props.id)) ? true : false}
                                                className="player"
                                                controls={true}
                                                width="100%"
                                                height="100%"
                                                url={baseUrl(
                                                    item.file
                                                )}
                                                onPause={() => {
                                                    props.onPause(item.id);
                                                }}
                                                onPlay={(e) =>{
                                                    
                                                }}
                                                config={{ file: { attributes: { controlsList: 'nodownload' } } }}
                                            />
                                        :
                                            <>
                                                <img
                                                    onClick={() => {
                                                        props.showLinkInModal(baseUrl(item.file));
                                                    }}
                                                    className=""
                                                    src={renderVideoThumb(`${item.file}`, props.videoThumbPath ? props.videoThumbPath : `/uploads/posts/thumbs/`)}
                                                    alt="..."
                                                />
                                                <div className="video_icon">
                                                    <a
                                                        onClick={() => {
                                                            props.showLinkInModal(baseUrl(item.file));
                                                        }}
                                                    >
                                                        <i className="fas fa-play"></i>
                                                    </a>
                                                </div>
                                            </>
                                    }
                                    
                                </div>
                                :
                                <div className="post_img" id={`post-` + props.id}>
                                    <img
                                        onClick={() => {
                                            props.showLinkInModal(baseUrl(item.file));
                                        }}
                                        className=""
                                        src={baseUrl(`${item.file}`)}
                                        alt="..."
                                    />
                                </div>
                        )
                }
            </>
        )
    }
    else {
        return <></>;
    }       
}

const mapState = (state) => {
    return {
        scroller: state.ScrollerReducer.scroller
    };
};
const mapDispatch = (dispatch) => {
    return {
        showLinkInModal: async (info) => dispatch(linkInModal(info)),
        showLinkInGallery: async (items, index) => {
            let list = [];
            for (let i in items) {
                if (items[i].type != 'audio') {
                    list.push(baseUrl(items[i].file));
                }
                else if(i <= index) {
                    index = index - 1;
                    index = index < 0 ? 0 : index;
                }
            }
            
            dispatch(linkInModal({index: index, list: list}))
        },
    };
}

export default connect(mapState, mapDispatch)(TimelineMedia);