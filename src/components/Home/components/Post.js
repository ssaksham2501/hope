import React, { useEffect, useRef, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import InputEmoji from "react-input-emoji";
import Moment from "react-moment";
import ReadMoreAndLess from 'react-read-more-less';
import { Link } from "react-router-dom";
import feedback_4 from "../../../assets/images/Feedback/4.png";
import landing_anonymous from "../../../assets/images/Landing/anonymous.png";
import landing_callie from "../../../assets/images/Landing/callie.png";
import AuthController from "../../../controllers/auth.controller";
//components
import PostController from "../../../controllers/post.controller";
import ProfileController from "../../../controllers/profile.controller";
//helpers
import { copyToClipboard, renderImage, uuId } from "../../../helpers/General";
import { Toast } from "../../../helpers/Toaster";
import BlockModal from "../../Layout/BlockModal";
import ConfirmationModal from "../../Layout/components/ConfirmationModal";
import ShareModal from "../../Layout/components/ShareModal";
import Modale from "../../Layout/Modal";
import ReportModale from "../../Layout/ReportModal";
import TimelineMedia from "./TimelineMedia";
import Picker from "emoji-picker-react";

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
  </a>
));

const Post = (props) => {
    const [uKey, setUKey] = useState(null);
    const [posts, setPostList] = useState([]);
    const [fullscreen, setFullscreen] = useState(true);
    const [blockModal, setBlockModal] = useState(false);
    const [reportModal, setReportModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [user, setUser] = useState(null);
    const [showEditModal, setShowEditModal] = useState(false);
    const [showReportModal, setShowReportModal] = useState(false);
    const [postId, setPostId] = useState();
    const [commentId, setCommentId] = useState(null);
    const [deleteCommentConfirm, setDeleteCommentConfirm] = useState(null);
    const [deletePostConfirm, setDeletePostConfirm] = useState(null);
    const [pauseMedia, setPauseMedia] = useState([]);
    const [currentCommentValue, setCurrentCommentValue] = useState(null);
    const [commentCursor, setCommentCursor] = useState(0);
    const [replyCursor, setReplyCursor] = useState(0);
    const [currentReplyValue, setCurrentReplyValue] = useState(null);
    
    let inputRef = useRef(null);
    let replyRef = useRef(null);
    
    useEffect(() => {
        getUser();
        setUKey(uuId());
        if (props.single) {
            let item = props.postList[0];
            item.commentInput = true;
            item.showComments = true;
            item.commentBlock = true;
            getCommentList(item.id, null, [item]);
        }
        else {
            setPostList(props.postList);
        }
    }, [props.postList]);

    useEffect(() => {
        setPauseMedia(pauseMedia);
    }, [pauseMedia])

    //Get Logined user Detail
    const getUser = async () => {
        let user1 = await new AuthController().getLoginUser();
        setUser(user1);
    };
    

    //Save Post
    const savePost = async (item) => {
        let response = null;
        if (item.actions && parseInt(item.actions.saved) === 1) {
            response = await new PostController().unSavePost(item.id);
        } else {
            response = await new PostController().savePost(item.id);
        }
        if (response && response.status) {
            new Toast().success(response.message);
            if (props.update) {
                props.updatePosts();
            } else {
                let array = [...posts];
                item.actions.saved = response.save;
                setPostList([...array]);
            }
        } else {
            console.log(response.error);
        }
    };

    //Delete Post
    const deletePost = async (post, i) => {
        setDeletePostConfirm({post, i});
    };

    //like --start
    const postLike = async (postId, index) => {
        console.log('asdas');
        let response = await new PostController().post_like(postId);
        if (response && response.status) {
            let array = [...posts];
            array[index].actions.likeByMe = response.like ? 1 : 0;
            array[index].actions.likes = response.count;
            setPostList(array);
        } else {
            console.log(response.error);
        }
    };

    //like --start
    const commentLike = async (item) => {
        let response = await new PostController().post_comment_like(item);
        if (response && response.status) {
            let array = [...posts];
            item.likes = response.count;
            item.likeByMe = response.like ? 1 : 0;
            setPostList(array);
        } else {
            console.log(response.error);
        }
    };

    //Add Comment on Post
    const postComment = async (post) => {
        let response = await new PostController().post_comment(
            post.id,
            currentCommentValue && currentCommentValue.post_id == post.id ? currentCommentValue.value : ``
        );
        setCurrentCommentValue(null);
        if (response && response.status) {
            getCommentList(post.id);
            post.actions.comments = response.count;
            post.commentBlock = true;
            post.commentInput = true;
            post.commentVal = "";
            setPostList([...posts]);
        } else {
            console.log(response.error);
        }
    };

    //Edit the post comment
    const editComment = async (post) => {
        let response = await new PostController().editComment(
            post.id,
            post.commentVal
        );
        if (response && response.status) {
            if (post.parent_id === null) {
                post.editComment = false;
                post.comment = response.data.comment;
                post.commentVal = response.data.comment;
                setPostList([...posts]);
            } else {
                post.editReply = false;
                post.comment = response.data.comment;
                post.commentVal = response.data.comment;
                setPostList([...posts]);
            }
        } else {
            console.log(response.error);
        }
    };

    //Delete comment
    const reportComment = async (c, i, j, k) => {
        setPostId(c.post_id);
        setCommentId(c.id)
        setShowReportModal(true);
    };

    const deleteComment = async (post, i, j, k) => {
        setDeleteCommentConfirm({ post, i, j, k });
    };

    //Commentlist --start
    const getCommentList = async (postId, lastId, passPosts = null) => {
        let post = {
            post_id: postId,
            last_id: lastId ? lastId : "",
            parent_id: ""
        };
        let response = await new PostController().get_comment_list(post);
        if (response && response.status) {
            let array = passPosts ? passPosts : [...posts];
            
            let i = array.indexOf(
                array.filter(function (item) {
                    return item.id == postId;
                })[0]
            );
            
            for (let i in response.comments) {
                response.comments[i]["repliesList"] = [];
                response.comments[i]["replyCount"] = 0;
                response.comments[i]["replyValue"] = "";
                response.comments[i]["showReplies"] = false;
                response.comments[i]["replyButton"] = false;
                response.comments[i]["editComment"] = false;
                response.comments[i]["commentVal"] =
                    response.comments[i].comment;
            }
            if (post.last_id === "") {
                array[i].commentCount = response.count;
                array[i].comments = response.comments;
            } else {
                array[i].commentCount = response.count;
                array[i].comments = [
                    ...response.comments,
                    ...array[i].comments
                ];
            }
            
            setPostList(array);
        } else {
            console.log(response.error);
        }
    };

    //CommentList- To preview previous comments
    const previewMoreComments = (post) => {
        let lastId = post.comments[0].id;
        getCommentList(post.id, lastId);
    };

    //CommentList- To preview previous comments
    const previewMoreReplies = (post) => {
        let lastId = post.repliesList[0].id;
        getRepliesList(post, lastId);
    };

    // //Replylist --start
    const getRepliesList = async (item, lastId) => {
        let post = {
            post_id: item.post_id,
            last_id: lastId ? lastId : "",
            parent_id: item.id
        };
        let response = await new PostController().get_comment_list(post);
        if (response && response.status) {
            let array = [...posts];
            for (let i in response.comments) {
                response.comments[i]["editReply"] = false;
                response.comments[i]["commentVal"] =
                    response.comments[i].comment;
            }
            if (post.last_id === "") {
                item.showReplies = true;
                item.replyCount = response.count;
                item.repliesList = response.comments;
            } else {
                item.showReplies = true;
                item.replyCount = response.count;
                item.repliesList = [...response.comments, ...item.repliesList];
            }
            setPostList([...array]);
        } else {
            console.log(response.error);
        }
    };

    //Add Reply on Comment --start
    const postReplyOnComment = async (post) => {
        let response = await new PostController().post_comment(
            post.post_id,
            currentReplyValue && currentReplyValue.comment_id == post.id && currentReplyValue.value ? currentReplyValue.value : ``,
            post.id
        );
        setCurrentReplyValue(null);
        if (response && response.status) {
            getRepliesList(post);
            post.showReplies = true;
            post.replies = response.count;
            post.replyCount = response.count;
            post.replyValue = "";
            setPostList([...posts]);
        } else {
            console.log(response.error);
        }
    };

    //Handle Coments popup open or close
    const showComments = (item) => {
        item.commentBlock = !item.commentBlock;
        if (item.commentBlock) {
            item.commentInput = true;
            getCommentList(item.id);
        } else {
            item.commentInput = false;
            item.commentVal = "";
            setPostList([...posts]);
        }
    };

    // Update the post List
    const updatePost = (item) => {
        let array = [...posts];
        let i = array.indexOf(
            array.filter(function (item) {
                return item.id == postId;
            })[0]
        );
        if (props && props.update) {
            if (array[i].anonymously != item.anonymously) {
                props.updatePosts();
            } else {
                if (i !== -1) {
                    array[i] = item;
                    array[i]["commentBlock"] = false;
                    array[i]["commentInput"] = false;
                    array[i]["commentVal"] = "";
                    array[i]["comments"] = [];
                    array[i]["commentButton"] = false;
                    array[i]["commentCount"] = 0;
                    array[i]["postOptions"] = false;
                }
            }
        } else {
            if (i !== -1) {
                array[i] = item;
                array[i]["commentBlock"] = false;
                array[i]["commentInput"] = false;
                array[i]["commentVal"] = "";
                array[i]["comments"] = [];
                array[i]["commentButton"] = false;
                array[i]["commentCount"] = 0;
                array[i]["postOptions"] = false;
            }
        }

        setPostId(null);
        props.showAddPost(false);
        props.updatePosts(array);
    };

    const blockUser = async () => {
        setBlockModal(false);
        let response = await new ProfileController().blockUser(postId);
        if (response && response.status) {
            let array = [...posts];
            let i = array.indexOf(
                array.filter(function (item) {
                    return item.id == postId;
                })[0]
            );
            if (i != -1) {
                array[i].actions.blocked = 1;
            }
            setPostId(null);
            setPostList([...array]);
            new Toast().success(response.message);
        } else {
            new Toast().error(response.message);
        }
    };

    const commentSection = (item, i, key, postUser) => {
        return (
            <>
                <div key={`comment-s-` + item.id} className="comments_view ">
                    <div className="user_img">
                        <Link
                            to={
                                user && item.user && item.user.id === user.id
                                    ? "/profile"
                                    : `/profile/${item.user.username}`
                            }
                        >
                            <img src={renderImage(item.user.image)} alt=".." />
                        </Link>
                    </div>
                    <div className="comment_detail">
                        <div className="name ">
                            <div className="user_name flexie">
                                <p>
                                    <Link
                                        to={
                                            user && item.user && item.user.id === user.id
                                                ? "/profile"
                                                : `/profile/${item.user.username}`
                                        }
                                    >
                                        {item.user && item.user.first_name}
                                        &nbsp;
                                        {item.user && item.user.last_name}
                                    </Link>
                                </p>
                                <i>
                                    <Moment fromNow>{item.created}</Moment>
                                </i>
                            </div>
                            {item.editComment ? null : (
                                <span>{item.comment}</span>
                            )}
                        </div>
                        <div
                            className={
                                item.editComment ? "likes d-none" : "likes "
                            }
                        >
                            <div
                                className="lik"
                                onClick={() => commentLike(item)}
                            >
                                {item.likes}
                                {parseInt(item.likeByMe) === 1 ? (
                                    <i className="fas fa-heart like "></i>
                                ) : (
                                    <i className="fal fa-heart like "></i>
                                )}
                            </div>
                            <div className="reply">
                                <p
                                    className="reply"
                                    onClick={() => {
                                        item.showReplies = !item.showReplies;
                                        if (item.showReplies) {
                                            setPostList([...posts]);
                                            getRepliesList(item, "");
                                        } else {
                                            setPostList([...posts]);
                                        }
                                    }}
                                >
                                    <span> {item.replies}</span>
                                    Reply
                                </p>
                            </div>
                        </div>
                    </div>

                    {/*Comment Options Section */}
                    <div
                        className={`after_post`}
                    >
                        {
                            <Dropdown>
                                <Dropdown.Toggle id="dropdown-basic1">
                                <svg width="10" height="4" viewBox="0 0 20 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fillRule="evenodd" clipRule="evenodd" d="M4 2C4 3.10457 3.10457 4 2 4C0.895431 4 0 3.10457 0 2C0 0.895431 0.895431 0 2 0C3.10457 0 4 0.895431 4 2ZM12 2C12 3.10457 11.1046 4 10 4C8.89543 4 8 3.10457 8 2C8 0.895431 8.89543 0 10 0C11.1046 0 12 0.895431 12 2ZM18 4C19.1046 4 20 3.10457 20 2C20 0.895431 19.1046 0 18 0C16.8954 0 16 0.895431 16 2C16 3.10457 16.8954 4 18 4Z" fill="#24292E"/>
                                </svg>
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    {/* <Dropdown.Item
                                        onClick={() => {
                                            item.editComment = true;
                                            item.commentVal = item.comment;
                                            setPostList([...posts]);
                                        }}
                                    >
                                        <i className="fal fa-pen"></i> Edit
                                    </Dropdown.Item> */}
                                    {item &&
                                        user &&
                                        item.user &&
                                        item.user.id !== user.id &&
                                        <Dropdown.Item
                                            className={`red`}
                                            onClick={() => {
                                                reportComment(item, key, i);
                                            }}
                                        >
                                            <i className="fal fa-exclamation-circle red"></i>{" "}
                                            Report
                                        </Dropdown.Item>
                                    }
                                    {item &&
                                        user &&
                                        item.user &&
                                        item.user.id === user.id &&
                                        <Dropdown.Item
                                            className="red"
                                            onClick={() => {
                                                deleteComment(item, key, i);
                                            }}
                                        >
                                            <i className="far fa-trash red"></i>{" "}
                                            Delete
                                        </Dropdown.Item>
                                    }
                                </Dropdown.Menu>
                            </Dropdown>
                        }
                    </div>
                </div>

                {/* Replies View More Option */}
                {item.replyCount > 0 && item.showReplies ? (
                    <div
                        className="viewmore"
                        onClick={() => previewMoreReplies(item)}
                    >
                        View More +
                    </div>
                ) : null}

                {/* Replies Section */}
                {item.showReplies && item.repliesList.length > 0
                    ? item.repliesList.map((item, index) => (
                          <div
                              key={`comment-${uKey}-${item.id}`}
                              className="comments_view under-comment"
                          >
                              <div className="user_img">
                                  <Link
                                      to={
                                          user &&
                                          item.user &&
                                          item.user.id === user.id
                                              ? "/profile"
                                              : `/profile/${item.user.username}`
                                      }
                                  >
                                      <img
                                          src={renderImage(item.user.image)}
                                          alt=".."
                                      />
                                  </Link>
                              </div>
                              <div className="comment_detail">
                                  <div className="name">
                                      <div className="user_name flexie">
                                          <p>
                                            <Link
                                                to={
                                                    user &&
                                                    item.user &&
                                                    item.user.id === user.id
                                                        ? "/profile"
                                                        : `/profile/${item.user.username}`
                                                }
                                            >
                                                {item.user && item.user.first_name}
                                                &nbsp;
                                                {item.user && item.user.last_name}
                                            </Link>
                                          </p>

                                          {item.editReply ? null : (
                                              <i>
                                                  <Moment fromNow>
                                                      {item.created}
                                                  </Moment>
                                              </i>
                                          )}
                                      </div>
                                      {item.editReply ? null : (
                                          <span>{item.comment}</span>
                                      )}
                                  </div>

                                  <div
                                      className={
                                          item.editReply
                                              ? "likes d-none"
                                              : "likes"
                                      }
                                  >
                                      <div
                                          className="lik"
                                          onClick={() => commentLike(item)}
                                      >
                                          {item.likes}
                                          {parseInt(item.likeByMe) === 1 ? (
                                              <i className="fas fa-heart like "></i>
                                          ) : (
                                              <i className="fal fa-heart like "></i>
                                          )}
                                      </div>
                                  </div>

                                  {/* Replier Edit Comment Input Section
                                  <div
                                      className={
                                          item.editReply
                                              ? "inputer_el"
                                              : "inputer_el d-none"
                                      }
                                  >
                                      <div className="repler_input">
                                          <div className="repler_space">
                                              <InputEmoji
                                                    value={item.commentVal}
                                                    onEnter={() => editComment(item)}
                                                  onChange={(e) => {
                                                      if (!e || !e.trim()) {
                                                          item.commentVal = "";
                                                          setPostList([
                                                              ...posts
                                                          ]);
                                                      } else {
                                                          item.commentVal = e;
                                                      }
                                                      setPostList([...posts]);
                                                  }}
                                                  placeholder={
                                                      item.commentVal
                                                          ? ""
                                                          : "Write a Reply"
                                                  }
                                                  fontFamily="Nunito"
                                              />
                                              <div className="floatimg">
                                                  <img
                                                      src={feedback_4}
                                                      alt="..."
                                                  />
                                              </div>
                                              <div
                                                  className={
                                                      item.commentVal !== ""
                                                          ? "submit"
                                                          : "submit btn_disable"
                                                  }
                                                  onClick={() =>
                                                      editComment(item)
                                                  }
                                              >
                                                  <a className="btn_sub">Reply</a>
                                              </div>
                                          </div>
                                      </div>
                                      <div
                                          className="cancel"
                                          onClick={() => {
                                              item.editReply = false;
                                              item.commentVal = item.comment;
                                              setPostList([...posts]);
                                          }}
                                      >
                                          Cancel
                                      </div>
                                  </div>
                                */}
                            </div>

                              <div
                                  className={`after_post`}
                            >
                                {
                                    <Dropdown>
                                        <Dropdown.Toggle id="dropdown-basic1">
                                            <i className="fal fa-ellipsis-h"></i>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            {item &&
                                                user &&
                                                item.user &&
                                                item.user.id != user.id && (
                                                    <Dropdown.Item
                                                        className={`red`}
                                                        onClick={() => {
                                                            reportComment(item, key, i);
                                                        }}
                                                    >
                                                        <i className="fal fa-exclamation-circle red"></i>{" "}
                                                        Report
                                                    </Dropdown.Item>
                                                )}
                                            {item &&
                                                user &&
                                                item.user &&
                                                item.user.id ==
                                                user.id && (<Dropdown.Item
                                                    className="red"
                                                    onClick={() => {
                                                        deleteComment(
                                                            item,
                                                            key,
                                                            i,
                                                            index
                                                        );
                                                    }}
                                                >
                                                    <i className="far fa-trash red"></i>{" "}
                                                    Delete
                                                </Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                }
                              </div>
                          </div>
                      ))
                    : null}

                {/* replyer Input*/}
                {item.showReplies ? (
                    <div className="replyer">
                        <div className="repler_img_side">
                            <div className="repler_img">
                                <Link
                                    to={
                                        user &&
                                        item.user &&
                                        item.user.id === user.id
                                            ? "/profile"
                                            : `/profile/${item.user.username}`
                                    }
                                >
                                    <img
                                        src={
                                            user && user.image
                                                ? renderImage(user.image)
                                                : landing_callie
                                        }
                                        alt=".."
                                    />
                                </Link>
                            </div>
                        </div>
                        <div className="repler_input">
                            <div className="repler_space">
                                <label className="message-input" htmlFor="inputemo">
                                    <textarea
                                        ref={replyRef}
                                        type="text"
                                        id="inputemo"
                                        rows={2}
                                        placeholder={currentReplyValue && currentReplyValue.comment_id === item.id && currentReplyValue.value ? "" : "Type Something..."}
                                        onClick={(e) => {
                                            setTimeout(() => {
                                                setReplyCursor(replyRef.current.selectionStart)
                                            }, 100);
                                        }}
                                        onChange={(v) => {
                                            let e = v.target.value;
                                            if (!e || !e.trim()) {
                                                item.replyValue = "";
                                                item.replyButton = false;
                                            } else {
                                                item.replyValue = e;
                                                item.replyButton = true;
                                            }
                                            setCurrentReplyValue({
                                                comment_id: item.id,
                                                value: e
                                            });
                                            setReplyCursor(replyRef.current.selectionStart);
                                        }}
                                        onKeyPress={(e) => {
                                            if (currentReplyValue && currentReplyValue.comment_id === item.id && currentReplyValue.value && (e.key.toLowerCase() === 'enter' || e.which === 13)) {
                                                postReplyOnComment(item);
                                            }
                                            setReplyCursor(replyRef.current.selectionStart)
                                        }}
                                        value={currentReplyValue && currentReplyValue.comment_id === item.id ? currentReplyValue.value : ``}
                                    />
                                    <Dropdown
                                        className={`floatimg active`}
                                        drop={`up`}
                                        align={`end`}
                                    >
                                        <Dropdown.Toggle as={CustomToggle}>
                                            <img
                                                src={feedback_4} alt="..."
                                            />
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                                <Picker
                                                    onEmojiClick={(event, emojiObject) => {
                                                        if (currentReplyValue && currentReplyValue.value.trim() != "") {
                                                            const text = currentReplyValue.value.slice(0, replyCursor) + ' ' + emojiObject.emoji + (currentReplyValue.value.slice(replyCursor).trim() ? ' ' + currentReplyValue.value.slice(replyCursor) : '');
                                                            setCurrentReplyValue({
                                                                comment_id: item.id,
                                                                value: text
                                                            });
                                                            setReplyCursor(replyCursor + 3);
                                                        }
                                                        else {
                                                            setCurrentReplyValue({
                                                                comment_id: item.id,
                                                                value: emojiObject.emoji
                                                            });
                                                            setReplyCursor(replyCursor + 2);
                                                        }
                                                    }}
                                                />
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </label>
                                <div
                                    className={
                                        item.replyButton
                                            ? "submit"
                                            : "submit btn_disable"
                                    }
                                    onClick={() => postReplyOnComment(item)}
                                >
                                    <a className="btn_sub">Reply</a>
                                </div>
                            </div>
                        </div>
                    </div>
                ) : null}
            </>
        );
    };

    const [ shareModal, setShareModal] = useState(false);
    const [sharePost, setSharePost] = useState(null);
    const openShareModal = async (post) => {
        setSharePost(post);
        setShareModal(true);
    }

    const copyLink = async (id) => {
        let response = await new PostController().copyLink(id);
        if (response && response.status && response.message) {
            copyToClipboard(response.message);
            new Toast().success("Link copied.");
        }
    }

    return (
        <>
            {posts &&
                posts.map((item, key) => {
                    return (
                        <div className="story-post-wrap" key={`story-${uKey}-${item.id}`}>
                            <div className="post_col post">
                               
                                <div className="main_post">
                                    <div className="head_name_user">
                                        <div className="before_post">
                                            <div className="imgwrap">
                                                {item.anonymously === 0 ? (
                                                    <Link
                                                        to={
                                                            user &&
                                                            item.user &&
                                                            item.user.id === user.id
                                                                ? "/profile"
                                                                : `/profile/${item.user.username}`
                                                        }
                                                    >
                                                        <img
                                                            src={renderImage(
                                                                item.user.image
                                                            )}
                                                            alt=""
                                                        />
                                                    </Link>
                                                ) : (
                                                    <img
                                                        src={landing_anonymous}
                                                        alt=""
                                                    />
                                                )}
                                            </div>
                                        </div>
                                        <div className="post_head">
                                            <p className="user_info">
                                                {item.anonymously === 0 ? (
                                                    <Link
                                                        to={
                                                            user &&
                                                            item.user &&
                                                            item.user.id === user.id
                                                                ? "/profile"
                                                                : `/profile/${item.user.username}`
                                                        }
                                                        className="user"
                                                    >
                                                        {item.user.first_name}
                                                        &nbsp;
                                                        {item.user.last_name}
                                                    </Link>
                                                ) : (
                                                    <a className="user">
                                                        Anonymous
                                                    </a>
                                                )}
                                                {item.group && item.group.title ? (
                                                    <>
                                                        <span className="lav_dot lav_bg"></span>
                                                        <Link
                                                            to={`/community-detail/${item.group.slug}`}
                                                            className="comm"
                                                        >
                                                            {item.group &&
                                                            item.group.title
                                                                ? item.group.title
                                                                : ""}
                                                        </Link>
                                                    </>
                                                ) : null}
                                            </p>
                                            <p className="post_time">
                                                <Moment fromNow>
                                                    {item.created}
                                                </Moment>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="post_body">
                                        {item && item.tags.length > 0 ? (
                                            <div className="post_tags">
                                                {item.tags.map((i, key) => {
                                                    return (
                                                        <a key={`tag--${uKey}-${item.id}-{${key}}`}>
                                                            <span
                                                                onClick={() =>
                                                                    console.log(
                                                                        "TAGS CLICK "
                                                                    )
                                                                }
                                                                className="post_tag"
                                                                key={key}
                                                            >
                                                                {i.tag}
                                                            </span>
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        ) : null}

                                        {item.description ? (
                                            <div className="post_text">
                                                <p>
                                                    <ReadMoreAndLess
                                                        charLimit={250}
                                                        readMoreText={
                                                            "read more"
                                                        }
                                                        readLessText={
                                                            "read less"
                                                        }
                                                    >
                                                        {item.description}
                                                    </ReadMoreAndLess>
                                                </p>
                                            </div>
                                        ) : null}

                                        {
                                            item.media && item.media.length > 0
                                            &&
                                            <TimelineMedia
                                                items={item.media}
                                                type={item.type}
                                                id={item.id}
                                                index={key}
                                                pauseMedia={pauseMedia}
                                                onPause={(postId) => {
                                                    console.log(`pauseclick`, [...pauseMedia, ...[postId]]);
                                                    setPauseMedia([...pauseMedia, ...[postId]])
                                                }}
                                            />
                                        }
                                        
                                        <div className="post_info flexie">
                                            <p className="likes">
                                                {item.actions.likes === "1" ? (
                                                    <>
                                                        {item.actions.likes}
                                                        &nbsp; Like
                                                    </>
                                                ) : (
                                                    <>
                                                        {item.actions.likes}
                                                        &nbsp; Likes
                                                    </>
                                                )}
                                            </p>
                                            <p
                                                className="comments"
                                                onClick={() =>
                                                    showComments(item)
                                                }
                                            >
                                                {item.actions.comments <= 1 ? (
                                                    <>
                                                        {item.actions.comments}
                                                        &nbsp; Comment
                                                    </>
                                                ) : (
                                                    <>
                                                        {item.actions.comments}
                                                        &nbsp; Comments
                                                    </>
                                                )}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                {/* Popup for Post Options */}
                                <div className="after_post">
                                    <Dropdown>
                                        <Dropdown.Toggle id="dropdown-basic">
                                        <svg width="10" height="4" viewBox="0 0 20 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd" clipRule="evenodd" d="M4 2C4 3.10457 3.10457 4 2 4C0.895431 4 0 3.10457 0 2C0 0.895431 0.895431 0 2 0C3.10457 0 4 0.895431 4 2ZM12 2C12 3.10457 11.1046 4 10 4C8.89543 4 8 3.10457 8 2C8 0.895431 8.89543 0 10 0C11.1046 0 12 0.895431 12 2ZM18 4C19.1046 4 20 3.10457 20 2C20 0.895431 19.1046 0 18 0C16.8954 0 16 0.895431 16 2C16 3.10457 16.8954 4 18 4Z" fill="#24292E"/>
                                        </svg>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            <Dropdown.Item
                                                onClick={() => {
                                                    copyLink(item.id)
                                                }}
                                            >
                                                <i className="fal fa-link"></i>
                                                Copy Link
                                            </Dropdown.Item>
                                            {item &&
                                            user &&
                                            item.user &&
                                            item.user.id === user.id ? (
                                                <Dropdown.Item
                                                    onClick={() => {
                                                        setPostId(item.id);
                                                        setShowEditModal(true);
                                                        props.showAddPost(true);
                                                    }}
                                                >
                                                    <i className="fal fa-pen"></i>
                                                    Edit
                                                </Dropdown.Item>
                                            ) : null}

                                            <Dropdown.Item
                                                onClick={() => savePost(item)}
                                            >
                                                {item.actions &&
                                                item.actions.saved === 0 ? (
                                                    <i className="far fa-bookmark"></i>
                                                ) : (
                                                    <i className="fas fa-bookmark"></i>
                                                )}
                                                {item.actions &&
                                                item.actions.saved === 0
                                                    ? "Save Post"
                                                    : "Unsave Post"}
                                            </Dropdown.Item>
                                            {item &&
                                            user &&
                                            item.user &&
                                            item.user.id === user.id ? null : (
                                                <Dropdown.Item
                                                    className="red"
                                                    onClick={() => {
                                                        setPostId(item.id);
                                                        setShowReportModal(
                                                            true
                                                        );
                                                    }}
                                                >
                                                    <i className="fal fa-exclamation-circle red"></i>{" "}
                                                    Report
                                                </Dropdown.Item>
                                            )}
                                            {item &&
                                            user &&
                                            item.user &&
                                            item.user.id === user.id ? (
                                                <Dropdown.Item
                                                    className="red"
                                                    onClick={() => {
                                                        deletePost(
                                                            item.id,
                                                            key
                                                        );
                                                    }}
                                                >
                                                    <i className="far fa-trash bin red"></i>
                                                    Delete Post
                                                </Dropdown.Item>
                                            ) : null}
                                            {item &&
                                            user &&
                                            item.user &&
                                            item.user.id === user.id ? null : (
                                                <Dropdown.Item
                                                    className="red"
                                                    onClick={() => {
                                                        setBlockModal(true);
                                                        setPostId(item.user.id);
                                                    }}
                                                >
                                                    <i className="fal fa-ban red"></i>{" "}
                                                    Block
                                                </Dropdown.Item>
                                            )}
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </div>
                            </div>
                            <div className="post_foot flexie">
                                <div
                                    className="iconwrap"
                                    onClick={() => {
                                        postLike(item.id, key);
                                    }}
                                >
                                    {item.actions.likeByMe === 1 ? (
                                        <a>
                                            <i className="fas fa-heart like flexie "></i>
                                        </a>
                                    ) : (
                                        <a>
                                            <i className="fal fa-heart like flexie "></i>
                                        </a>
                                    )}
                                </div>
                                <div className="dividor"></div>
                                <div
                                    className="iconwrap"
                                    onClick={() => showComments(item)}
                                >
                                    <a>
                                        <i className="fal fa-comment-alt comment flexie"></i>
                                    </a>
                                </div>
                                <div className="dividor"></div>

                                {/* Popup for share the post */}
                                <div className="iconwrap">
                                    <a
                                        href=""
                                        onClick={(e) => {
                                            e.preventDefault();
                                            openShareModal(item);
                                        }}
                                    ><i className="fas fa-share share flexie"></i></a>
                                    {/* <Dropdown>
                                        <Dropdown.Toggle id="dropdown-basic1">
                                            <i className="fas fa-paper-plane share flexie"></i>
                                    ><i className="far fa-paper-plane share flexie"></i></a>
                                    {/* <Dropdown>
                                        <Dropdown.Toggle id="dropdown-basic1">
                                            <i className="far fa-paper-plane share flexie"></i>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#/action-1">
                                                <ul>
                                                    <li>
                                                        <a href="/">
                                                            <i className="fab fa-facebook-f"></i>
                                                            Facebook
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/">
                                                            <i className="fab fa-instagram"></i>
                                                            Instagram
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/">
                                                            <i className="fab fa-twitter"></i>
                                                            Twitter
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/">
                                                            <i className="fab fa-youtube"></i>
                                                            Youtube
                                                        </a>
                                                    </li>
                                                </ul>
                                            </Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown> */}
                                </div>
                            </div>
                            <div
                                className={
                                    // item.commentBlock
                                    //     ? "post_comment-view custom_scroll "
                                    "post_comment-view custom_scroll no_scroll "
                                }
                            >
                                {item.commentBlock && item.commentCount > 0 ? (
                                    <div
                                        className="comment-count"
                                        onClick={() => {
                                            previewMoreComments(item);
                                        }}
                                    >
                                        <p>
                                            View {item.commentCount} previous
                                            comments
                                        </p>
                                    </div>
                                ) : null}
                                {/* Comment Section */}
                                {item.commentBlock && item.comments.length > 0
                                    ? item.comments.map((c, i) =>
                                          commentSection(c, i, key, item.user_id)
                                      )
                                    : null}
                            </div>
                            {/* Comment Input */}
                            {item.commentInput ? (
                                <>
                                    <div className="comment_write">
                                        <div className="before_post">
                                            <div className="imgwrap">
                                                <Link
                                                    to={
                                                        user &&
                                                        item.user &&
                                                        item.user.id === user.id
                                                            ? "/profile"
                                                            : `/profile/${item.user.username}`
                                                    }
                                                >
                                                    <img
                                                        src={
                                                            user && user.image
                                                                ? renderImage(
                                                                      user.image
                                                                  )
                                                                : landing_callie
                                                        }
                                                        alt=""
                                                    />
                                                </Link>
                                            </div>
                                        </div>
                                        <div className="comment_type">
                                            <form>
                                                <div className="">
                                                    <div className="form-group">
                                                        <label className="message-input" htmlFor="inputemo">
                                                            <textarea
                                                                ref={inputRef}
                                                                type="text"
                                                                id="inputemo"
                                                                className="form-control"
                                                                placeholder={item.commentVal ? "" : "Type Something..."}
                                                                onClick={(e) => {
                                                                    setTimeout(() => {
                                                                        setCommentCursor(inputRef.current.selectionStart)
                                                                    }, 100);
                                                                }}
                                                                onChange={(v) => {
                                                                    let e = v.target.value;
                                                                    if (
                                                                        !e ||
                                                                        !e.trim()
                                                                    ) {
                                                                        item.commentVal =
                                                                            "";
                                                                        item.commentButton = false;
                                                                    } else {
                                                                        item.commentVal =
                                                                            e;
                                                                        item.commentButton = true;
                                                                    } 
                                                                    setCurrentCommentValue({
                                                                        post_id: item.id,
                                                                        value: e
                                                                    });
                                                                    setCommentCursor(inputRef.current.selectionStart)
                                                                }}
                                                                onKeyPress={(e) => {
                                                                    if (currentCommentValue && currentCommentValue.post_id == item.id && currentCommentValue.value && (e.key.toLowerCase() === 'enter' || e.which === 13)) {
                                                                        postComment(item)
                                                                    }
                                                                    setCommentCursor(inputRef.current.selectionStart)
                                                                    
                                                                }}
                                                                value={currentCommentValue && currentCommentValue.post_id == item.id ? currentCommentValue.value : ``}
                                                            />
                                                            <Dropdown
                                                                className={`floatimg active`}
                                                                drop={`up`}
                                                                align={`end`}
                                                            >
                                                                <Dropdown.Toggle as={CustomToggle}>
                                                                    <img
                                                                        src={feedback_4} alt="..."
                                                                    />
                                                                </Dropdown.Toggle>
                                                                <Dropdown.Menu>
                                                                        <Picker
                                                                            onEmojiClick={(event, emojiObject) => {
                                                                                
                                                                                if (currentCommentValue && currentCommentValue.value.trim() != '') {
                                                                                    const text = currentCommentValue && currentCommentValue.value
                                                                                        ?
                                                                                        (currentCommentValue.value.slice(0, commentCursor) + ' ' + emojiObject.emoji + (currentCommentValue.value.slice(commentCursor).trim() ? ' ' + currentCommentValue.value.slice(commentCursor).trim()
                                                                                            : ''))
                                                                                        :
                                                                                        emojiObject.emoji
                                                                                        ;
                                                                                    setCurrentCommentValue({
                                                                                        post_id: item.id,
                                                                                        value: text
                                                                                    });
                                                                                    setCommentCursor(commentCursor + 3);
                                                                                }
                                                                                else
                                                                                {
                                                                                    setCurrentCommentValue({
                                                                                        post_id: item.id,
                                                                                        value: emojiObject.emoji
                                                                                    });
                                                                                    setCommentCursor(commentCursor + 2);
                                                                                }
                                                                            }}
                                                                        />
                                                                </Dropdown.Menu>
                                                            </Dropdown>
                                                        </label>
                                                    </div>
                                                    <div className="add_comment">
                                                        <button
                                                            type="button"
                                                            className={
                                                                currentCommentValue && currentCommentValue.post_id === item.id && currentCommentValue.value
                                                                    ? "main_btns lav_bg"
                                                                    : "main_btns lav_bg btn_disable "
                                                            }
                                                            disabled={
                                                                !item.commentButton
                                                            }
                                                            onClick={() => {
                                                                postComment(
                                                                    item
                                                                );
                                                            }}
                                                        >
                                                            Comment
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </>
                            ) : null}
                        </div>
                    );
                })}
            {showEditModal ? (
                <Modale
                    id={postId}
                    show={showEditModal}
                    editCommunity={
                        props.editCommunity ? props.editCommunity : false
                    }
                    setPost={() => {
                        setPostId(null);
                    }}
                    updatePost={(item) => updatePost(item)}
                    setShow={() => {
                        props.showAddPost(false);
                        setPostId(null);
                        setShowEditModal(false);
                    }}
                />
            ) : null}
            {showReportModal ? (
                <ReportModale
                    show={showReportModal}
                    post={postId}
                    commentId={commentId}
                    setShow={() => setShowReportModal(false)}
                />
            ) : null}
            {/* <Suremodal /> */}
            {/* <DeleteModal /> */}
            {blockModal && (
                <BlockModal
                    show={blockModal}
                    close={() => setBlockModal(false)}
                    yes={() => blockUser()}
                />
            )}
            {
                deleteCommentConfirm
                &&
                <ConfirmationModal
                    title={`Are you sure to delete this comment ?`}
                    description={``}
                    show={deleteCommentConfirm ? true : false}
                    confirm={async () => {
                        let { post, i, j, k } = deleteCommentConfirm;
                        let array = [];
                        let response = await new PostController().deleteComment(post.id);
                        if (response && response.status) {
                            let updated = [...posts];
                            if (post.parent_id === null) {
                                array = updated[i]["comments"];
                                array.splice(j, 1);
                                updated[i].actions.comments = updated[i].actions.comments - 1;
                            } else {
                                array = updated[i]["comments"][j]["repliesList"];
                                updated[i]["comments"][j].replies =
                                    updated[i]["comments"][j].replies - 1;
                                array.splice(k, 1);
                            }
                            setPostList(updated);
                            setDeleteCommentConfirm(null);
                        } else {
                            console.log(response.error);
                        }
                    }}
                    cancel={() => {
                        setDeleteCommentConfirm(null);
                    }}
                />
            }

            {
                deleteCommentConfirm
                &&
                <ConfirmationModal
                    title={`Are you sure to delete this comment?`}
                    description={``}
                    show={deleteCommentConfirm ? true : false}
                    confirm={async () => {
                        let { post, i, j, k } = deleteCommentConfirm;
                        let array = [];
                        let response = await new PostController().deleteComment(post.id);
                        if (response && response.status) {
                            let updated = [...posts];
                            if (post.parent_id === null) {
                                array = updated[i]["comments"];
                                array.splice(j, 1);
                                updated[i].actions.comments = updated[i].actions.comments - 1;
                            } else {
                                array = updated[i]["comments"][j]["repliesList"];
                                updated[i]["comments"][j].replies =
                                    updated[i]["comments"][j].replies - 1;
                                array.splice(k, 1);
                            }
                            setPostList(updated);
                            setDeleteCommentConfirm(null);
                        } else {
                            console.log(response.error);
                        }
                    }}
                    cancel={() => {
                        setDeleteCommentConfirm(null);
                    }}
                />
            }

            {
                deletePostConfirm
                &&
                <ConfirmationModal
                    title={`Are you sure to delete this post?`}
                    description={``}
                    show={deletePostConfirm ? true : false}
                    confirm={async () => {
                        let { post, i } = deletePostConfirm;
                        let response = await new PostController().deletePost(post);
                        if (response && response.status) {
                            new Toast().success(response.message);
                            let updated = [...posts];
                            updated.splice(i, 1);
                            setPostList(updated);
                            setDeletePostConfirm(null);
                        } else {
                            console.log(response.error);
                        }
                    }}
                    cancel={() => {
                        setDeletePostConfirm(null);
                    }}
                />
            }

            {shareModal && sharePost ? (
                <ShareModal
                    show={shareModal}
                    post={sharePost}
                    setShow={() => {
                        setShareModal(false);
                    }}
                />
            ) : null}
        </>
    );
};

export default Post;
