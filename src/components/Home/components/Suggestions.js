import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

//assets
import landing_callie from "../../../assets/images/Landing/callie.png";
import CommunityController from "../../../controllers/community.controller";
import { renderImage } from "../../../helpers/General";
import Nodata from "../../Layout/components/Nodata";

const Suggestions = () => {
    const [list, setCommunities] = useState([]);
    const [loader, setLoader] = useState(false);

    useEffect(() => {
        setLoader(true);
        getCommunitiesSuggestion();
    }, []);

    const getCommunitiesSuggestion = async () => {
        let response = await new CommunityController().suggestedCommunities();
        if (response && response.status) {
            setCommunities(response.communities);
        } else {
            setCommunities([]);
        }
        setLoader(false);
    };
    return (
        <div className="community_suggestions d-md-block d-none">
            <div className="community-head">
                <div className="name">
                    <p>Suggested Communities</p>
                </div>
                {/* <div className="icon">
                    <a href="/">
                        <i className="fal fa-ellipsis-h"></i>
                    </a>
                </div> */}
            </div>
            <div className="suggestion_list">
                {list.length > 0 ? (
                    <ul>
                        {list.map((item, index) => (
                            <li key={index}>
                                <div className="img_suggesyion">
                                    <Link to={`/community-detail/${item.slug}`}>
                                        <div className="img_area">
                                            <img
                                                src={renderImage(item.image)}
                                                alt=".."
                                            />
                                        </div>
                                    </Link>
                                </div>

                                <div className="name">
                                    <Link to={`/community-detail/${item.slug}`}>
                                        <p>{item.title}</p>
                                    </Link>
                                </div>
                            </li>
                        ))}
                    </ul>
                ) : !loader ? (
                    <Nodata />
                ) : null}
                {/*  */}
            </div>
        </div>
    );
};

export default Suggestions;
