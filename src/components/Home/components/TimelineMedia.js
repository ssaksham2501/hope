import React, { useEffect } from "react";
import { baseUrl, renderVideoThumb, uuId } from "../../../helpers/General";
import Slider from "react-slick";
import ReactPlayer from "react-player";
import { connect } from "react-redux";
import AudioPlayer from '../../AudioPlayer/Audio';
import { linkInModal, playedItem } from "../../../redux/actions/user";
import { Carousel } from "react-bootstrap";
import { isIOS, isIOS13 } from "react-device-detect";
const TimelineMedia = (props) => {
    const [items, setItems] = React.useState(props.items);
    const [play, setPlay] = React.useState(false);
    const [videoMode, setVideoMode] = React.useState('popup');
    const [slidePause, setSplidePause] = React.useState(false);
    const [videoClick, setVideoClick] = React.useState(false);
    const [udId, setUdId] = React.useState("");
    useEffect(() => {
        if (!props.disableScroll) {
            let elem = document.getElementById('post-' + props.id);
            if (elem && props.scroller) {
                let scT = props.scroller.scrollTop + 300;
                if (scT > elem.offsetTop && scT < elem.offsetTop + elem.offsetHeight + 200 && !props.pauseMedia.includes(props.id) && global.clicked) {
                    props.playItem(props.id);
                    setPlay(true);
                }
                else {
                    setPlay(false);
                }
            }
        }
    }, [props.scroller]);

    useEffect(() => {
        if (props.id == props.playedItem) {
            setPlay(true);
        }
        else {
            setPlay(false);
;        }
    }, [props.playedItem]);

    useEffect(() => {
        if (!props.disableScroll) {
            setPlay(props.index * 1 < 1 ? true : false)
        }

        for (let i in props.items) {
            if (props.items[i].type == 'audio') { 
                let a = new Audio(baseUrl(props.items[i].file));
                a.load();
            }
            
        }
        let uId = uuId();
        if (!udId) {
            setUdId(uId);
        }
        document.addEventListener("click", () => {
            global.clicked = true;
        });
        document.addEventListener("touchstart", () => {
            global.clicked = true;
        });

        return () => {
            document.removeEventListener("click",  () => {
                global.clicked = true;
            });
            document.removeEventListener("touchstart",  () => {
                global.clicked = true;
            });
        }
    }, []);
    

    //slider --start
    const [index, setIndex] = React.useState(0);

    const handleSelect = (selectedIndex, e) => {    
        setIndex(selectedIndex);
    };
    //slider --end
    

    if (items && items.length > 1) {
        return (
            <>
                <div className={`post_img` + (items[0].type == 'audio' ? ` audio` : ``)} id={`post-` + props.id}>
                <Carousel
                    interval={null}
                    onSlide={() => {
                        if (props.onPause) {
                            props.onPause(props.id);
                        }
                        setSplidePause(false);
                    }}
                    activeIndex={index}
                    onSelect={handleSelect}
                >
                    {items.map(
                        (i, key) => {
                            return (
                                <Carousel.Item key={`post-media-` + i.id}>
                                <div key={key} className={i.type === 'video' ? 'slide-media video-thumb' : 'slide-media'}>
                                    {
                                        i.type === 'audio'
                                            ?
                                            <div style={{ width: '100%', height: '100%', background: '#eb6383' }}>
                                                {key === index
                                                    &&
                                                    <AudioPlayer
                                                        file={baseUrl(i.file)}
                                                        play={play && global.clicked && props.playedItem == props.id ? true : false}
                                                        slidePause={slidePause}
                                                        id={udId + props.id}
                                                        onPlay={() => {
                                                            global.clicked = true;
                                                            props.playItem(props.id);
                                                            setSplidePause(true);
                                                        }}
                                                        onPause={() => {
                                                            setSplidePause(false);
                                                            setPlay(false);
                                                            if (props.onPause) {
                                                                props.onPause(props.id);
                                                            }
                                                        }}
                                                    />
                                                }
                                            </div>
                                            :
                                            <img
                                                onClick={() => {
                                                    props.showLinkInGallery(items, key);
                                                }}
                                                className={i.type === 'video' ? 'video-thumb' : ''}
                                                id={key}
                                                src={
                                                    i.type === 'video'
                                                        ?
                                                        renderVideoThumb(i.file)
                                                        :
                                                        baseUrl(`${i.file}`)
                                                }
                                                alt="..."
                                            />
                                    }
                                    {(i.type === 'video')
                                        &&
                                        <div className="played">
                                        <i
                                            onClick={() => {
                                                    props.showLinkInGallery(items, key);
                                            }}
                                            className="fas fa-solid fa-play"
                                        ></i>
                                        </div>
                                    }
                                </div>
                                </Carousel.Item>
                            );
                        }
                    )}
                    
                </Carousel>
            </div></>);
    }
    else if(items &&items.length > 0) {
        let item = items[0];
        return (
            <>
                {
                    item.type == 'audio'
                        ?
                        <div className="post_img audio_box flexie" id={`post-` + props.id}>
                            <AudioPlayer
                                file={baseUrl(item.file)}
                                slidePause={true}
                                play={play && global.clicked && props.playedItem == props.id ? true : false}
                                id={udId + props.id}
                                onPause={() => {
                                    setPlay(false)
                                    if (props.onPause) {
                                        props.onPause(props.id);
                                    }
                                }}
                                onPlay={() => {
                                    global.clicked = true;
                                    props.playItem(props.id);
                                }}
                            />
                        </div>
                    :
                        (
                            item.type == 'video'
                            ?
                                <div className="post_img video_box" id={`post-` + props.id} onClick={() => {
                                    setVideoClick(true);
                                }}>
                                    {
                                            !(isIOS && isIOS13) && (videoMode === 'player' || !props.disableScroll)
                                            ?
                                            <ReactPlayer
                                                playing={play && props.playedItem == props.id ? true : false}
                                                className="player"
                                                playsinline={true}
                                                controls={true}
                                                muted={true}
                                                width="100%"
                                                height="100%"
                                                url={baseUrl(
                                                    item.file
                                                )}
                                                onPause={() => {
                                                    setPlay(false);
                                                    if (videoClick && props.onPause) {
                                                        props.onPause(props.id);
                                                    }
                                                }}
                                                onPlay={() => {
                                                    setPlay(true);
                                                    props.playItem(props.id);
                                                }}
                                                config={{ file: { attributes: { controlsList: 'nodownload' } } }}
                                            />
                                        :
                                            <>
                                                <img
                                                    onClick={() => {
                                                        props.showLinkInModal(baseUrl(item.file));
                                                    }}
                                                    className=""
                                                    src={renderVideoThumb(`${item.file}`, props.videoThumbPath ? props.videoThumbPath : `/uploads/posts/thumbs/`)}
                                                    alt="..."
                                                />
                                                <div className="video_icon">
                                                    <a
                                                        onClick={() => {
                                                            props.showLinkInModal(baseUrl(item.file));
                                                        }}
                                                    >
                                                        <i className="fas fa-play"></i>
                                                    </a>
                                                </div>
                                            </>
                                    }
                                    
                                </div>
                                :
                                <div className="post_img" id={`post-` + props.id}>
                                    <img
                                        onClick={() => {
                                            props.showLinkInModal(baseUrl(item.file));
                                        }}
                                        className=""
                                        src={baseUrl(`${item.file}`)}
                                        alt="..."
                                    />
                                </div>
                        )
                }
            </>
        )
    }
    else {
        return <></>;
    }       
}

const mapState = (state) => {
    return {
        scroller: state.ScrollerReducer.scroller,
        playedItem: state.PlayedItemReducer.played
    };
};
const mapDispatch = (dispatch) => {
    return {
        showLinkInModal: async (info) => dispatch(linkInModal(info)),
        playItem: (info) => dispatch(playedItem(info)),
        showLinkInGallery: async (items, index) => {
            let list = [];
            for (let i in items) {
                if (items[i].type != 'audio') {
                    list.push(baseUrl(items[i].file));
                }
                else if(i <= index) {
                    index = index - 1;
                    index = index < 0 ? 0 : index;
                }
            }
            
            dispatch(linkInModal({index: index, list: list}))
        },
    };
}

export default connect(mapState, mapDispatch)(TimelineMedia);