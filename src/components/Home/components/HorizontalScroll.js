import React, { useEffect } from "react";
import ScrollMenu from "react-horizontal-scrolling-menu";
import { Link, useHistory } from "react-router-dom";
import { renderImage } from "../../../helpers/General";

// One item component
// selected prop will be passed
const MenuItem = (props) => {
    return (
        <div>
            {/* <Link to={`/community-detail/${props.item.slug}`}> */}
                <div className="user-created">
                    <div className="create-img" style={{backgroundImage: 'url('+renderImage(props.item.image)+')', height: '200px', width: '200px'}}>
                    </div>
                    <div className="title">
                        <p>{props.item.title}</p>
                    </div>
                </div>
            {/* </Link> */}
        </div>
    );
};

// All items component
// Important! add unique key
export const Menu = (list) =>
    list.map((el) => {
        return <MenuItem item={el} key={`cmm-` + el.id} />;
    });

const Arrow = ({ text, className }) => {
    return <div className={className}>{text}</div>;
};

const ArrowLeft = Arrow({ text: "<", className: "arrow-prev" });
const ArrowRight = Arrow({ text: ">", className: "arrow-next" });

function HorizontalScroll(props) {
    let history = useHistory();
    const [selected, setSelected] = React.useState(props.list[0].id);
    let menuItems = Menu(props.list, selected);

    const [translate, setTranslate] = React.useState(0);
    useEffect(() => {
        setTranslate(translate > 0 ? 0 : translate);
    }, [translate]);

    // Create menu from items
    const menu = menuItems;
    const transition = 0.3;

    const onSelect = (key) => {
        let k = key.replace('cmm-', '');
        let filtered = props.list.filter((l, i) => l.id == k)
        if (filtered.length > 0) {
            history.push(`/community-detail/` + filtered[0].slug);
        }
    };
    return (
        <ScrollMenu
            data={menu}
            itemStyle={{ margin: 0, marginBlock: 0 }}
            clickWhenDrag={false}
            alignCenter={false}
            hideArrows={true}
            hideSingleArrow={true}
            scrollToSelected={false}
            selected={selected}
            onSelect={onSelect}
            transition={+transition}
            dragging={true}
            wheel={false}
        />
    );
}

export default HorizontalScroll;
