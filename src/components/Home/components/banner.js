import React from "react";

//assets
import layout_bread from "../../../assets/images/Layout/bread.png";

function Banner() {
    return (
        <div>
            <h1
                className="wip flexie"
                style={{
                    backgroundImage: `url(${layout_bread})`,
                }}
            >
                Work In Progress
            </h1>
        </div>
    );
}

export default Banner;
