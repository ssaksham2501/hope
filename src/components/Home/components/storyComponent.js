import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import ScrollMenu from "react-horizontal-scrolling-menu";
import "./styles.css";
import { Link } from "react-router-dom";
import { renderImage } from "../../../helpers/General";

const MenuItem = (props) => {
    return (
        <Link
            to={`/community-detail/${props.item.slug}`}
            onClick={() => props.selected}
        >
            <div className="user-created">
                <div className="create-img">
                    <img src={renderImage(props.item.image)} alt="..." />
                </div>
                <div className="title">
                    <p>{props.item.title}</p>
                </div>
            </div>
        </Link>
    );
};

export const Menu = (list, selected) =>
    list.map((item) => {
        return <MenuItem item={item} key={item.id} selected={selected} />;
    });

const Arrow = ({ text, className }) => {
    return <div className={className}>{text}</div>;
};
Arrow.propTypes = {
    text: PropTypes.string,
    className: PropTypes.string,
};

export const ArrowLeft = Arrow({ text: "<", className: "arrow-prev" });
export const ArrowRight = Arrow({ text: ">", className: "arrow-next" });

class StoryComponent extends Component {
    state = {
        alignCenter: true,
        clickWhenDrag: false,
        dragging: true,
        hideArrows: true,
        hideSingleArrow: true,
        itemsCount: this.props && this.props.list && this.props.list.length,
        scrollToSelected: false,
        selected: this.props && this.props.list && this.props.list[0].id,
        translate: 0,
        transition: 0.3,
        wheel: true,
    };
    constructor(props) {
        super(props);
        this.menu = null;

        this.menuItems = props.list && Menu(props.list, this.state.selected);
    }

    onFirstItemVisible = () => {
        console.log("first item is visible");
    };

    onLastItemVisible = () => {
        console.log("last item is visible");

        //   const newItems = Array(5)
        //     .fill(1)
        //     .map((el, ind) => ({ name: `item${list.length + ind + 1}` }));
        //   list = list.concat(newItems);
        //   this.menuItems = Menu(list, list.slice(-1)[0].name);
        this.setState({
            selected: this.state.selected,
        });
    };

    onUpdate = ({ translate }) => {
        this.setState({ translate });
    };

    onSelect = (key) => {
        this.setState({ selected: key });
    };

    componentDidUpdate(prevProps, prevState) {
        const { alignCenter } = prevState;
        const { alignCenter: alignCenterNew } = this.state;
        if (alignCenter !== alignCenterNew) {
            this.menu.setInitial();
        }
    }

    setItemsCount = (ev) => {
        const { itemsCount = this.props.list.length, selected } = this.state;
        const val = +ev.target.value;
        const itemsCountNew =
            !isNaN(val) && val <= this.props.list.length && val >= 0
                ? +ev.target.value
                : this.props.list.length;
        const itemsCountChanged = itemsCount !== itemsCountNew;

        if (itemsCountChanged) {
            this.menuItems = Menu(
                this.props.list.slice(0, itemsCountNew),
                selected
            );
            this.setState({
                itemsCount: itemsCountNew,
            });
        }
    };

    setSelected = (ev) => {
        const { value } = ev.target;
        this.setState({ selected: String(value) });
    };

    render() {
        const {
            alignCenter,
            clickWhenDrag,
            hideArrows,
            dragging,
            hideSingleArrow,
            itemsCount,
            scrollToSelected,
            selected,
            translate,
            transition,
            wheel,
        } = this.state;

        const menu = this.menuItems;

        const checkboxStyle = {
            margin: "5px 10px",
        };
        const valueStyle = {
            margin: "5px 10px",
            display: "inline-block",
        };

        return (
            <div className="App">
                {this.props && this.props.list ? (
                    <ScrollMenu
                        alignCenter={alignCenter}
                        arrowLeft={ArrowLeft}
                        arrowRight={ArrowRight}
                        clickWhenDrag={clickWhenDrag}
                        data={menu}
                        dragging={dragging}
                        hideArrows={hideArrows}
                        hideSingleArrow={hideSingleArrow}
                        // onFirstItemVisible={this.onFirstItemVisible}
                        //  onLastItemVisible={this.onLastItemVisible}
                        onSelect={this.onSelect}
                        onUpdate={this.onUpdate}
                        ref={(el) => (this.menu = el)}
                        scrollToSelected={scrollToSelected}
                        selected={selected}
                        transition={+transition}
                        translate={translate}
                        wheel={wheel}
                    />
                ) : null}
            </div>
        );
    }
}

export default StoryComponent;

ReactDOM.render(<StoryComponent />, document.getElementById("root"));
