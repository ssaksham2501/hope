import React, { Component } from 'react'
import photo from "../../assets/images/Landing/banner.jpg";
export class Postdetail extends Component {
  render() {
    return (
      <div className="post-detail_section">
          <div className="container-fluid">
              <div className="row">
                  <div className="col-xxl-12 xol-xl-12 col-md-12 col-sm-12 col-12 p-0">
                      <div className="post_detail_side">
                          <div className="post_detail_img_box">
                              <div className="options">
                                  <div className="back">
                                      <a href="/"><i className="fas fa-arrow-left"></i></a>
                                  </div>
                                  <div className="scan">
                                      <a href="/"><i className="fal fa-link-expand"></i></a>
                                  </div>
                              </div>
                                <div className="img_aera">
                                    <img src={photo} alt=".." />
                                </div>
                          </div>
                          <div className="post_cmt">
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}

export default Postdetail;