import React, { useEffect, useState } from "react";

import { Link, withRouter } from "react-router-dom";

import { Button, Modal } from "react-bootstrap";

function BlockModal(props) {
    const handleClose = () => props.close();
    return (
        <div className="el">
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={props.show}
                onHide={handleClose}
                animation={true}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div className="modal_head">
                            {" "}
                            <h4>Are you sure to block this user?</h4>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="pop_post">
                        <div className="head">
                            <p>
                                Lorem ipsum dolor sit amet consectetur,
                                adipisicing elit. Nulla iste laboriosam omnis
                                illo labore delectus
                            </p>
                        </div>
                        <div className="btnss">
                            <button
                                className="report lav_bg"
                                onClick={() => props.yes()}
                            >
                                Block
                            </button>
                            <button
                                className="cancel"
                                onClick={() => props.close()}
                            >
                                Cancel
                            </button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default withRouter(BlockModal);
