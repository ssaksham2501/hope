import React, { useEffect } from "react";
import { Link, withRouter } from "react-router-dom";

//assets
import footer_flowers from "../../assets/images/Layout/flowers.png";
import landing_logo from "../../assets/images/Landing/logo.png";

function Header() {
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    return (
        <section className="footer_section">
            <div className="imgwrap flowers">
                <img src={footer_flowers} alt="" />
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="footer_area">
                            <div className="row">
                                <div className="col-xl-4 col-lg-4  col-md-5 col-sm-5 col-7">
                                    <div className="clmn_1 clmn">
                                        <div className="imgwrap logo">
                                            <Link to="/">
                                                <img
                                                    src={landing_logo}
                                                    alt=""
                                                />
                                            </Link>
                                        </div>
                                        <p>
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit.
                                            Vestibulum.
                                        </p>
                                    </div>
                                </div>
                                <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-5">
                                    <div className="clmn_2 clmn">
                                        <h4>Company</h4>
                                        <ul>
                                            <li>
                                                <Link to="/about-us">
                                                    About Us
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/privacy-policy">
                                                    Privacy Policy
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/terms-conditions">
                                                    Terms and Conditions
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-5 for-order">
                                    <div className="clmn_3 clmn">
                                        <h4>Help</h4>
                                        <ul>
                                            <li>
                                                <Link to="/contact-us">
                                                    Contact Us
                                                </Link>
                                            </li>
                                            <li>
                                                <Link to="/faq">FAQ</Link>
                                            </li>
                                            <li>
                                                <Link to="/feedback">
                                                    Feedback
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-7">
                                    <div className="clmn_4 clmn">
                                        <h4>Follow Us on</h4>
                                        <ul className="flexie">
                                            <li>
                                                <a href="/">
                                                    <i className="fab fa-facebook"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/">
                                                    <i className="fab fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/">
                                                    <i className="fab fa-instagram"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/">
                                                    <i className="fab fa-linkedin-in"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="copyright">
                            <i className="far fa-copyright"></i>
                            <p>
                                Copyright 2022 | All Rights Reserved | Powered
                                By&nbsp;
                                <a
                                    href="https://globiztechnology.com/"
                                    target="_blank"
                                >
                                    Globiz Technology Inc.
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default withRouter(Header);
