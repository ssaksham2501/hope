import React, { useEffect, useState } from "react";

import { Link, withRouter } from "react-router-dom";

import { Button, Modal } from "react-bootstrap";

//assets
import photo from "../../assets/images/sidebar/photo.png";
import video from "../../assets/images/sidebar/video.png";
import audio from "../../assets/images/sidebar/audio.png";
import landing_callie from "../../assets/images/Landing/callie.png";
import AddPost from "../Home/components/AddPost";
import PostController from "../../controllers/post.controller";
import { Toast } from "../../helpers/Toaster";

function Suremodal(props) {
    const [reports, setReports] = useState([]);
    const handleClose = () => props.setShow(false);

    useEffect(() => {
        getReportList();
    }, []);

    //Report List
    const getReportList = async () => {
        let response = await new PostController().reportList();
        if (response && response.status) {
            setReports(response.reports);
        } else {
            console.log(response.error);
        }
    };

    //Report On Post
    // const reportPost = async (item) => {
    //     let response = await new PostController().reportPost(
    //         props.post,
    //         item.title
    //     );
    //     if (response && response.status) {
    //         new Toast().success(response.message);
    //         handleClose();
    //     } else {
    //         new Toast().success(response.error);
    //         console.log(response.error);
    //     }
    // };

    return (
        <div className="el">
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={false}
                onHide={handleClose}
                animation={true}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div className="modal_head">
                            {" "}
                            <h4>Are you sure ?</h4>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="pop_post">
                        <div className="head">
                            <p>
                                Lorem ipsum dolor sit amet consectetur,
                                adipisicing elit. Nulla iste laboriosam omnis
                                illo labore delectus
                            </p>
                        </div>
                        <div className="btnss">
                            <button className="report lav_bg">Report</button>
                            <button className="cancel">Cancel</button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default withRouter(Suremodal);
