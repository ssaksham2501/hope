import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import InputEmoji from "react-input-emoji";
import { useHistory } from "react-router-dom";
import feedback_4 from "../../../assets/images/Feedback/4.png";
import audio from "../../../assets/images/sidebar/audio.png";
import photo from "../../../assets/images/sidebar/photo.png";
import video from "../../../assets/images/sidebar/video.png";
import sound from "../../../assets/images/sidebar/sound-bars.png";
import gallery from "../../../assets/images/sidebar/gallery.png";
import clip from "../../../assets/images/sidebar/video-clip.png";
import AuthController from "../../../controllers/auth.controller";
import MessagesController from "../../../controllers/messages.controller";
import PostController from "../../../controllers/post.controller";
import ProfileController from "../../../controllers/profile.controller";
import { renderImage, uuId } from "../../../helpers/General";
import { Toast } from "../../../helpers/Toaster";
import Chat from "./Chat";
import ProgressCircle from "./ProgressCircle";
import { isMobile, isTablet } from 'react-device-detect';
import Picker from "emoji-picker-react";

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
  <a
    href=""
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
  >
    {children}
  </a>
));

const ChatboxSection = (props) => {
    
    let inputRef = useRef(null);
    const history = useHistory();
    const [authUser, setAuthUser] = useState(null);
    const [message, setMessage] = useState('');
    const [textCursor, setTextCursor] =  useState(0);
    const [mute, setMute] = useState(false);
    const [queueMessages, setQueueMessages] = useState(null);
    const [blockedUser, setBlockedUser] = useState(false);

    let defaultChatInfo = {
        chatMinimize: false,
        online: false,
        loginUserId: null,
        loginUser: null,
        messages: [],
        toId: null,
        user: null,
        typing: false,
        text: null,
        chatCount: props.count && props.count > 0 ? props.count : 0,
    };
    const [chat, setChatInfo] = useState(defaultChatInfo);
    
    
    useEffect(() => {
        inputRef.current.focus();
        init();
        return () => { 
            setChatInfo(defaultChatInfo);
        }
    }, []);

    const init = async () => {
        let loginUser = await new AuthController().getLoginUser();
        let loginUserId = loginUser.id;
        setAuthUser(loginUser);
        setChatInfo({
            ...chat,
            loginUser: loginUser,
            loginUserId: loginUserId,
            chatMinimize: props.minimize ? props.minimize : false,
            chatCount: props.count && props.count > 0 ? props.count : 0,
            toId: props.to.id,
            user: props.to,
        });
    };

    

    useEffect(() => {
        console.log(props.minimize);
        setChatInfo({ ...chat, chatMinimize: props.minimize });
    }, [props.minimize]);

    const closeChatModal = () => {
        new MessagesController().deleteChatBoxModal(chat.user.id);
        let list = new MessagesController().getChatBoxList();
        if (list.length < 1) {
            global.socket.offMessages();
        }
        setChatInfo(defaultChatInfo);
    };

    
    //Send Message
    const sendMessage = async (attachment, attachment_type) => {
        if (global.socket && (message || attachment)) {
            let msgId = uuId();
            global.socket.submitMessage(
                message,
                chat.user.id,
                msgId,
                attachment ? attachment : null,
                attachment_type ? attachment_type : null,
            );
            setMessage("");
            if(inputRef && inputRef.current)
            inputRef.current.focus();
        }
    };

    

    //Block User
    const blockUser = async () => {
        let response = await new ProfileController().blockUser(chat.user.id);
        if (response && response.status) {
            await new MessagesController().chatList();
            closeChatModal();
            new Toast().success(response.message);
        } else {
            new Toast().error(response.message);
        }
    };

    const unBlockUser = async () => {
        let response = await new ProfileController().unBlockUser(chat.user.id);
        if (response && response.status) {
            setBlockedUser(null);
            await new MessagesController().chatList();
        } else {
            new Toast().error(response.message);
        }
    };

    const muteChat = async () => {
        let response = await new MessagesController().muteChat(chat.user.id);
        if (response && response.status) {
            new Toast().success(response.message);
            setMute(true);
            await new MessagesController().chatList();
        } else {
            new Toast().error(response.message);
        }
    };

    const unmuteChat = async () => {
        let response = await new MessagesController().unmuteChat(chat.user.id);
        if (response && response.status) {
            new Toast().success(response.message);
            setMute(false);
            await new MessagesController().chatList();
        } else {
            new Toast().error(response.message);
        }
    };

    
    const deleteChat = async () => {
        let response = await new MessagesController().deleteChat(chat.user.id);
        if (response && response.status) {
            new Toast().success(response.message);
            closeChatModal();
            await new MessagesController().chatList();
        } else {
            new Toast().error(response.message);
        }
    };

    const [progress, setProgress] = React.useState(0);
    const [isLoading, setIsLoading] = React.useState(false);
    const [errMsg, setErrMsg] = React.useState("");
    
    const uploadMedia = async (file, type) => {
        if (isLoading === false) {
            setErrMsg("");
            setIsLoading(true);
            setProgress(0.5);
            let callback = (p) => {
                setProgress(p >= 100 ? 96 : p);
            };
            let response = null;
            if(type === 'video')
                response = await new PostController().upload_video(file, 'messsage_video', callback);
            else if (type === 'audio')
                response = await new PostController().upload_audio(file, callback);
            else
                response = await new PostController().upload_photo(file, callback);
            
            setIsLoading(false);
            setProgress(100);
            if (response && response.status) {
                sendMessage(response.path, type);
            } else {
                setErrMsg(response.error);
            }
        }
    }


    return (
       
            <div
                className={props.minimize ? "chat-sets miniminze" : "chat-sets"}
            >
                <div className="chat-wrap-aread">
                    <div className="chat_area">
                        <div className="chat-head">
                        {
                            chat.chatMinimize
                                ?
                                <Dropdown>
                                    <Dropdown.Toggle
                                        id="dropdown-basic"
                                        onClick={() => {
                                            setChatInfo({
                                                ...chat,
                                                chatCount: 0,
                                            });
                                            new MessagesController().updateMinimizeStatusChatBox(
                                                chat.toId,
                                                !chat.chatMinimize,
                                                0
                                            );
                                        }}
                                    >
                                        <div className="head-set">
                                            <div className="img_box">
                                                {chat.user && (
                                                    <img
                                                        src={renderImage(
                                                            chat.user.image
                                                        )}
                                                        alt=".."
                                                    />
                                                )}
                                            </div>
                                            <div className="name">
                                                <p>{chat.user && chat.user.name}</p>
                                            </div>
                                        </div>
                                    </Dropdown.Toggle>
                                    <div className="icon">
                                        <a
                                            onClick={() => {
                                                setChatInfo({
                                                    ...chat,
                                                    chatCount: 0,
                                                });
                                                new MessagesController().updateMinimizeStatusChatBox(
                                                    chat.toId,
                                                    !chat.chatMinimize, 
                                                    0
                                                );
                                            }}
                                        >
                                            <i className="far fa-window-maximize"></i>
                                        </a>
                                        <a
                                            onClick={() => {
                                                closeChatModal();
                                            }}
                                        >
                                            <i className="fas fa-times"></i>
                                        </a>
                                    </div>

                                    <Dropdown.Menu>
                                        <Dropdown.Item
                                            onClick={() => {
                                                if (isMobile || isTablet) {
                                                    closeChatModal();
                                                }
                                                history.push(
                                                    `/profile/${chat.user.username}`
                                                )
                                            }}
                                        >
                                            <i className="fal fa-user"></i>View
                                            Profile
                                        </Dropdown.Item>
                                        {/* <Dropdown.Item>
                                                <i className="fal fa-users"></i>Create group
                                            </Dropdown.Item> */}
                                        {
                                            !blockedUser
                                            &&
                                            <>
                                                {
                                                    mute
                                                        ?
                                                        <Dropdown.Item
                                                            onClick={unmuteChat}
                                                        >
                                                            <i className="far fa-volume"></i> Unmute
                                                            Notifications
                                                        </Dropdown.Item>
                                                        :
                                                        <Dropdown.Item onClick={muteChat}>
                                                            <i className="far fa-volume-mute"></i> Mute
                                                            Notifications
                                                        </Dropdown.Item>
                                                }
                                                <Dropdown.Item className={`text-danger`} onClick={() => blockUser()}>
                                                    <i className="fal fa-ban text-danger"></i>Block
                                                </Dropdown.Item>
                                            </>
                                        }
                                        <Dropdown.Item
                                            onClick={() => deleteChat()}
                                            className={`text-danger`}
                                        >
                                            <i className="far fa-trash text-danger"></i>Delete
                                            chat
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                :
                                <Dropdown>
                                    <Dropdown.Toggle id="dropdown-basic">
                                        <div className="head-set">
                                            <div className="img_box">
                                                {chat.user && (
                                                    <img
                                                        src={renderImage(
                                                            chat.user.image
                                                        )}
                                                        alt=".."
                                                    />
                                                )}
                                            </div>
                                            <div className="name">
                                                <p>{chat.user && chat.user.name} <i className="fas fa-caret-down ps-1"></i></p>
                                            </div>
                                        </div>
                                    </Dropdown.Toggle>
                                    <div className="icon">
                                        <a
                                            onClick={() => {
                                                setChatInfo({
                                                    ...chat,
                                                    chatCount: 0,
                                                });
                                                new MessagesController().updateMinimizeStatusChatBox(
                                                    chat.toId,
                                                    !chat.chatMinimize,
                                                    0
                                                );
                                            }}
                                        >
                                            {
                                                chat.chatMinimize
                                                    ?
                                                    <i className="far fa-window-maximize"></i>
                                                    :
                                                    <i className="far fa-window-minimize"></i>
                                            }
                                                
                                        </a>
                                        <a
                                            onClick={() => {
                                                closeChatModal();
                                            }}
                                        >
                                            <i className="fas fa-times"></i>
                                        </a>
                                    </div>

                                    <Dropdown.Menu>
                                        <Dropdown.Item
                                            onClick={() => {
                                                if (isMobile || isTablet) {
                                                    closeChatModal();
                                                }
                                                history.push(
                                                    `/profile/${chat.user.username}`
                                                )
                                            }}
                                        >
                                            <i className="fal fa-user"></i>View
                                            Profile
                                        </Dropdown.Item>
                                        {/* <Dropdown.Item>
                                                <i className="fal fa-users"></i>Create group
                                            </Dropdown.Item> */}
                                        {
                                            !blockedUser
                                            &&
                                            <>
                                                {
                                                    mute
                                                        ?
                                                        <Dropdown.Item
                                                            onClick={unmuteChat}
                                                        >
                                                            <i className="far fa-volume"></i> Unmute
                                                            Notifications
                                                        </Dropdown.Item>
                                                        :
                                                        <Dropdown.Item onClick={muteChat}>
                                                            <i className="far fa-volume-mute"></i> Mute
                                                            Notifications
                                                        </Dropdown.Item>
                                                }
                                                <Dropdown.Item className={`text-danger`} onClick={() => blockUser()}>
                                                    <i className="fal fa-ban text-danger"></i>Block
                                                </Dropdown.Item>
                                            </>
                                        }
                                        <Dropdown.Item
                                            onClick={() => deleteChat()}
                                            className={`text-danger`}
                                        >
                                            <i className="far fa-trash text-danger"></i>Delete
                                            chat
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                        }
                        </div>
                        <div className="chat_box">
                            {
                                props && props.to && !chat.chatMinimize && global.socket
                                &&
                                <Chat
                                    queue={queueMessages}
                                    minimize={chat.chatMinimize}
                                    to={props.to}
                                    socket={global.socket}
                                    onResponse={(response) => {
                                        setBlockedUser(response.blocked_user);
                                        setMute(response.mute);
                                    }}
                                    closeChatModal={closeChatModal}
                                />
                            }

                            {
                                blockedUser && blockedUser !== null
                                ?
                                    <div className="chat_write">
                                        <p>Unblock to send message! <button className="block" onClick={unBlockUser}>Unblock</button></p>
                                    </div>
                                :
                                    <div className="chat_write">
                                        <div className="add">
                                            
                                            {
                                                isLoading
                                                    ?
                                                    <ProgressCircle
                                                        progress={progress}
                                                    />
                                                    :
                                                    <>
                                                        <a>
                                                            <i className="fas fa-plus"></i>
                                                        </a>
                                                        <div className="category">
                                                            <ul>
                                                                <li>
                                                                    <label htmlFor={`attach-photo-` + (props.to ? props.to.id : `` )}>
                                                                        <input
                                                                            className="d-none"
                                                                            id={`attach-photo-` + (props.to ? props.to.id : `` )}
                                                                            type="file"
                                                                            onChange={(e) => {
                                                                                uploadMedia(e.target.files[0], 'photo');
                                                                                e.target.value = null;
                                                                            }}
                                                                            accept="image/jpeg,image/png,image/gif"
                                                                        />
                                                                        <img src={gallery} alt=".." />
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label htmlFor={`attach-video-` + (props.to ? props.to.id : `` )}>
                                                                        <input
                                                                            className="d-none"
                                                                            id={`attach-video-` + (props.to ? props.to.id : `` )}
                                                                            type="file"
                                                                            onChange={(e) => {
                                                                                uploadMedia(e.target.files[0], 'video');
                                                                                e.target.value = null;
                                                                            }}
                                                                            accept="video/mp4,video/quicktime"
                                                                        />
                                                                        <img src={clip} alt=".." />
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label htmlFor={`attach-audio-` + (props.to ? props.to.id : `` )}>
                                                                        <input
                                                                            className="d-none"
                                                                            id={`attach-audio-` + (props.to ? props.to.id : `` )}
                                                                            type="file"
                                                                            onChange={(e) => {
                                                                                uploadMedia(e.target.files[0], 'audio');
                                                                                e.target.value = null;
                                                                            }}
                                                                            accept="audio/mpeg,audio/wav,audio/x-wav,audio/vnd.wav"
                                                                        />
                                                                        <img src={sound} alt=".." />
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </>
                                            }
                                        </div>
                                        <div className="message">
                                            <label className="message-input" htmlFor="inputemo">
                                                <textarea
                                                    ref={inputRef}
                                                    type="text"
                                                    id="inputemo"
                                                    placeholder={message ? "" : "Type Something..."}
                                                    onClick={(e) => {
                                                        setTimeout(() => {
                                                            const cursor = inputRef.current.selectionStart;
                                                            setTextCursor(cursor)
                                                        }, 100);
                                                    }}
                                                    onChange={(e) => {
                                                        let val = e.target.value;
                                                        if (!val.trim()) {
                                                            setMessage("");
                                                        } else {
                                                            setMessage(val);
                                                        }
                                                        const cursor = inputRef.current.selectionStart;
                                                        setTextCursor(cursor)
                                                    }}
                                                    onKeyPress={(e) => {
                                                        if (message && (e.key.toLowerCase() === 'enter' || e.which === 13)) {
                                                            sendMessage()
                                                        }
                                                        const cursor = inputRef.current.selectionStart;
                                                        setTextCursor(cursor)
                                                    }}
                                                    value={message}
                                                />
                                                <Dropdown
                                                    className={`floatimg active`}
                                                    drop={`up`}
                                                >
                                                    <Dropdown.Toggle as={CustomToggle}>
                                                        <img
                                                            src={feedback_4} alt="..."
                                                        />
                                                    </Dropdown.Toggle>
                                                    <Dropdown.Menu>
                                                            <Picker
                                                                onEmojiClick={(event, emojiObject) => {
                                                                    if(message.trim() !== "")
                                                                    {
                                                                        const text = message.slice(0, textCursor) + ' ' + emojiObject.emoji + ( message.slice(textCursor).trim() ? ' ' + message.slice(textCursor).trim() : '');
                                                                        setTextCursor(textCursor + 3);
                                                                        setMessage(text);
                                                                    }
                                                                    else
                                                                    {
                                                                        setMessage(emojiObject.emoji);
                                                                        setTextCursor(textCursor + 2);
                                                                    }
                                                                }}
                                                            />
                                                    </Dropdown.Menu>
                                                </Dropdown>
                                            </label>
                                            {
                                                errMsg && errMsg !== ""
                                                &&
                                                <div className="error-msg">
                                                        {errMsg}
                                                        <a onClick={() => {
                                                            setErrMsg("");
                                                        }}><i className="fa fa-times-circle"></i></a>
                                                </div>
                                            }
                                        </div>
                                        <div
                                            className={
                                                message
                                                    ? "add send"
                                                    : "add send btn_disable"
                                            }
                                        >
                                            <a onClick={() => sendMessage()}>
                                                <i className="fas fa-paper-plane"></i>
                                            </a>
                                        </div>
                                    </div>
                            }
                            
                        </div>
                    </div>
                </div>
            </div>
       
    );
};

export default ChatboxSection;
