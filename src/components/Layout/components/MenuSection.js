import React from "react";
import { Link } from "react-router-dom";

//assets
import home from "../../../assets/images/sidebar/home.png";
import user from "../../../assets/images/sidebar/user.png";
import friends from "../../../assets/images/sidebar/friends.png";
import community from "../../../assets/images/sidebar/community.png";
import setting from "../../../assets/images/sidebar/setting.png";
import chat from "../../../assets/images/sidebar/chat.png";
import { useLocation } from "react-router-dom";

import Phonemenu from "./Phonemenu";

import { setCommunityId, showCommunity } from "../../../redux/actions/user";
import { connect } from "react-redux";


const MenuSection = (props) => {
    const params = useLocation();

    return (
        <div className="pages d-md-block d-none">
            <ul>
                <Link to="/">
                    <li
                        className={` ${
                            params.pathname === "/" ? "active" : ""
                        }`}
                    >
                        <img src={home} alt="" /> Home
                    </li>
                </Link>
                <Link to="/profile">
                    <li
                        className={` ${
                            params.pathname === "/profile" ? "active" : ""
                        }`}
                    >
                        <img src={user} alt="" /> My profile
                    </li>
                </Link>

                {/* <Link to="/community-detail">
                    <li
                        className={` ${
                            params.pathname === "/community-detail"
                                ? "active"
                                : ""
                        }`}
                    >
                        <img src={friends} alt="" /> Friends
                    </li>
                </Link> */}
                <li className="d-lg-none d-block">
                    <Link to="/chat">
                        <img src={chat} alt=".." /> Chat
                    </Link>
                </li>
                <Link
                    to="/community"
                    onClick={() => {
                        props.showCommunity(false);
                        props.setCommunityId(null);
                    }}
                >
                    <li
                        className={` ${
                            params.pathname === "/community" || params.pathname.includes('community-detail') ? "active" : ""
                        }`}
                    >
                        <img src={community} alt="" /> Community
                    </li>
                </Link>
                <Link to="/settings">
                    <li
                        className={` ${
                            params.pathname === "/settings" ? "active" : ""
                        }`}
                    >
                        <img src={setting} alt="" /> Settings
                    </li>
                </Link>
            </ul>
        </div>
    );
};

const mapState = (state) => {
    return {
        community: state.ShowCommunityReducer.show,
        user: state.UserReducer.user,
        id: state.CommunityDetailReducer.id,
    };
    
};
const mapDispatch = (dispatch) => {
    return {
        showCommunity: async (info) => dispatch(showCommunity(info)),
        setCommunityId: async (info) => dispatch(setCommunityId(info)),
    };
};

export default connect(mapState, mapDispatch)(MenuSection);
