import React from "react";
import { Link } from "react-router-dom";
const FooterSection = () => {
    return (
        <div className="side-footer">
            <ul>
                <li>
                    <Link to="/about-us">About Us</Link>
                </li>
                <li>
                    <Link to="/contact-us">Contact Us</Link>
                </li>
                <li>
                    <Link to="/privacy-policy">Privacy Policy </Link>
                </li>
                <li>
                    <Link to="/terms-conditions">Terms & Conditions</Link>
                </li>
                <li>
                    <Link to="/feedback">Feedback</Link>
                </li>
                <li>
                    <Link to="/faq">FAQ</Link>
                </li>
            </ul>
        </div>
    );
};

export default FooterSection;
