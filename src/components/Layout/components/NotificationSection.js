import React, { useEffect } from 'react'
import { Link, useHistory } from "react-router-dom";
import { Toast } from "../../../helpers/Toaster";
//assets
import user from "../../../assets/images/header/user.jpg";
import { renderImage, timeAgo, uuId } from '../../../helpers/General';
import { Tab, Tabs } from "react-bootstrap";
import MessagesController from '../../../controllers/messages.controller';
const NotificationSection = (props) => {
    let history = useHistory();
    const [notifications, setNotifications] = React.useState([]);
    const [requests, setRequests] = React.useState([]);

    useEffect(() => {
        setNotifications(props.notifications);
        setRequests(props.friendRequests);
    }, [props.notifications, props.friendRequests]);

    const openChat = (user) => {
        new MessagesController().setChatBoxModal({
            mId: uuId(),
            id: user.from_id,
            name: user.from_first_name.concat(" " + user.from_last_name),
            image: user.from_image,
            username: user.from_username,
            responseTimings: null,
        });
    };

    const [acceptRequestLoading, setAcceptRequestLoading] = React.useState(false);
    const acceptRequest = async (item) => {
        if (!acceptRequestLoading) {
            setAcceptRequestLoading(true);
            let response = await new MessagesController().acceptChatRequest(item.from_username);
            if (response && response.status) {
                setAcceptRequestLoading(false);
                props.reload();
                new MessagesController().chatList();
            }
            else {
                setAcceptRequestLoading(false);
                new Toast().error(response.message);
            }
        }
    }

    const [deleteRequestLoading, setDeleteRequestLoading] = React.useState(false);
    const deleteRequest = async (item) => {
        if (!deleteRequestLoading) {
            setDeleteRequestLoading(true);
            let response = await new MessagesController().deleteChatRequest(item.from_username);
            if (response && response.status) {
                setDeleteRequestLoading(false);
                props.reload();
                new MessagesController().chatList();
            }
            else {
                setAcceptRequestLoading(false);
                new Toast().error(response.message);
            }
        }
    }

    const renderNotifications = () => {
        let list = notifications.map((item, i) => {
            return (<div
                key={`not-` + i}
                className={`notifi_list ` + (item.is_read ? `unread` : 'read')}
                onClick={() => goToDestination(item)}
            >
                <div className="img_area">
                    <div className="img_box">
                        <span>
                            <img src={renderImage(item.from_image)} alt="" />
                        </span>
                    </div>
                </div>
                <div className="named">
                    <p>{item.from_first_name + ` ` + item.from_last_name}</p>
                    <div className="lke_cmt">
                        <div className="left">
                            <p>{item.message}</p>
                        </div>
                        <div className="righst">
                            <p>
                                {timeAgo(item.created)}
                            </p>
                        </div>
                    </div>
                </div>
            </div>)
        });

        return list;
    }

    const renderRequests = () => {
        let list = requests.map((item, i) => {
            return (<div
                key={`not-` + i}
                className={`notifi_list `}
            >
                <div className="img_area">
                    <div
                        className="img_box"
                        onClick={() => goToDestination(item)}
                    >
                        <span>
                            <img src={renderImage(item.from_image)} alt="" />
                        </span>
                    </div>
                </div>
                <div className="named">
                    <div>
                        <p onClick={() => goToDestination(item)}>{item.from_first_name + ` ` + item.from_last_name}</p>
                    </div>
                    <div className="lke_cmt request">
                        <div className="left">
                            <ul>
                                <li>
                                    <a
                                        className="btn_prim lav_bg w-100"
                                        onClick={() => acceptRequest(item)}
                                    >
                                        Confirm
                                        {
                                            acceptRequestLoading
                                            &&
                                            <i className="fad fa-spinner-third fa-spin"></i>
                                        }
                                    </a>
                                </li>
                                <li>
                                    <a
                                        onClick={() => deleteRequest(item)}
                                        className="btn_prim-1 lav_bg w-100"
                                    >
                                        Delete
                                        {
                                            deleteRequestLoading
                                            &&
                                            <i className="fad fa-spinner-third fa-spin"></i>
                                        }
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="righst">
                            <p>
                                {timeAgo(item.created)}
                            </p>
                        </div>
                    </div>
                </div>
            </div>)
        });

        return list;
    }

    const goToDestination = (item) => {

        if (['accept_request', 'friend_request'].includes(item.type)) {
            props.close();
            history.push(`/profile/${item.from_username}`);
        }
        else if (item.rel_type == 'post') {
            let data = item.data ? JSON.parse(item.data) : null;
            props.close();
            if (data) {
                history.push(`/post/${data.post_id}`);
            }
        }
    }

    return (
        <Tabs defaultActiveKey={props.path && props.path.includes('/requests') ? 'Friend_requests' : 'Notifications'} id="uncontrolled-tab-example" className="mb-3">
            <Tab eventKey="Notifications" title="Notifications" tabClassName={props.notifyCount * 1 > 0 ? `notify-dot` : ``} >
                <div className="friends_requestes">
                    <div className="request_hed">
                        <div className="new_reqst">
                            <p>
                                New Notifications
                            </p>
                        </div>
                        <div className="sell_all">
                            <Link to={'/notifications'} onClick={() => props.close()}>
                                <p>See all</p>
                            </Link>
                        </div>
                    </div>
                    <div className="notifications">
                        <div className="show_notification">
                            {notifications && notifications.length > 0 ? renderNotifications() : <p className="text-center">No notification available!</p>}
                        </div>
                    </div>
                </div>
            </Tab>
            <Tab eventKey="Friend_requests" title="Requests" tabClassName={props.requestCount * 1 > 0 ? `notify-dot rqst` : ``}>
                <div className="friends_requestes">
                    <div className="request_hed">
                        <div className="new_reqst">
                            <p>
                                New Notifications
                            </p>
                        </div>
                        <div className="sell_all">
                            <Link to={'/requests'} onClick={() => props.close()}>
                                <p>See all</p>
                            </Link>
                        </div>
                    </div>
                    <div className="notifications">
                        <div className="show_notification">
                            {requests && requests.length > 0 ? renderRequests() : <p className="text-center">No chat requests available!</p>}
                        </div>
                    </div>
                </div>
            </Tab>
        </Tabs>
    )
}

export default NotificationSection;