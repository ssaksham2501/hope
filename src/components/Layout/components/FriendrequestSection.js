import React from "react";
import { Link } from "react-router-dom";

//assets
import user from "../../../assets/images/header/user.jpg";

const FriendrequestSection = () => {
    return (
        <div className="friends_requestes">
        <div className="request_hed">
            <div className="new_reqst">
                <p>
                    New request
                </p>
            </div>
            <div className="sell_all">
                <Link>
                <p>See all</p>
                </Link>
            </div>
        </div>
        <div className="show_requests">
            <div className="request_list">
                <div className="left">
                    <div className="img_box">
                        <Link to="/">
                            {" "}
                            <img src={user} alt="" />
                        </Link>
                    </div>
                </div>
                <div className="content">
                    <div className="name">
                        <div className="name_left">
                            <p>
                                Rose Wane
                            </p>
                        </div>
                        <div className="days">
                            <p>
                                1d
                            </p>
                        </div>
                    </div>
                    <div className="accepts_bten">
                        <div className="left_confim">
                            <ul>
                                <li>
                                    <a href="/" className="btn_prim lav_bg w-100">Confirm</a>
                                </li>
                                <li>
                                    <a href="/" className="btn_prim-1 lav_bg w-100">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="request_list">
                <div className="left">
                    <div className="img_box">
                        <Link to="/">
                            {" "}
                            <img src={user} alt="" />
                        </Link>
                    </div>
                </div>
                <div className="content">
                    <div className="name">
                        <div className="name_left">
                            <p>
                                Rose Wane
                            </p>
                        </div>
                        <div className="days">
                            <p>
                                1d
                            </p>
                        </div>
                    </div>
                    <div className="accepts_bten">
                        <div className="left_confim">
                            <ul>
                                <li>
                                    <a href="/" className="btn_prim lav_bg w-100">Confirm</a>
                                </li>
                                <li>
                                    <a href="/" className="btn_prim-1 lav_bg w-100">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="request_list">
                <div className="left">
                    <div className="img_box">
                        <Link to="/">
                            {" "}
                            <img src={user} alt="" />
                        </Link>
                    </div>
                </div>
                <div className="content">
                    <div className="name">
                        <div className="name_left">
                            <p>
                                Rose Wane
                            </p>
                        </div>
                        <div className="days">
                            <p>
                                1d
                            </p>
                        </div>
                    </div>
                    <div className="accepts_bten">
                        <div className="left_confim">
                            <ul>
                                <li>
                                    <a href="/" className="btn_prim lav_bg w-100">Confirm</a>
                                </li>
                                <li>
                                    <a href="/" className="btn_prim-1 lav_bg w-100">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="request_list">
                <div className="left">
                    <div className="img_box">
                        <Link to="/">
                            {" "}
                            <img src={user} alt="" />
                        </Link>
                    </div>
                </div>
                <div className="content">
                    <div className="name">
                        <div className="name_left">
                            <p>
                                Rose Wane
                            </p>
                        </div>
                        <div className="days">
                            <p>
                                1d
                            </p>
                        </div>
                    </div>
                    <div className="accepts_bten">
                        <div className="left_confim">
                            <ul>
                                <li>
                                    <a href="/" className="btn_prim lav_bg w-100">Confirm</a>
                                </li>
                                <li>
                                    <a href="/" className="btn_prim-1 lav_bg w-100">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="request_list">
                <div className="left">
                    <div className="img_box">
                        <Link to="/">
                            {" "}
                            <img src={user} alt="" />
                        </Link>
                    </div>
                </div>
                <div className="content">
                    <div className="name">
                        <div className="name_left">
                            <p>
                                Rose Wane
                            </p>
                        </div>
                        <div className="days">
                            <p>
                                1d
                            </p>
                        </div>
                    </div>
                    <div className="accepts_bten">
                        <div className="left_confim">
                            <ul>
                                <li>
                                    <a href="/" className="btn_prim lav_bg w-100">Confirm</a>
                                </li>
                                <li>
                                    <a href="/" className="btn_prim-1 lav_bg w-100">Delete</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    );
};

export default FriendrequestSection;
