import React from "react";
import { Link } from "react-router-dom";

//assets
import home from "../../../assets/images/sidebar/home.png";
import Chat from "../../../assets/images/sidebar/user.png";
import user from "../../../assets/images/sidebar/user.png";
import friends from "../../../assets/images/sidebar/friends.png";
import community from "../../../assets/images/sidebar/community.png";
import setting from "../../../assets/images/sidebar/setting.png";
import chat from "../../../assets/images/sidebar/chat.png";
import { useLocation } from "react-router-dom";
import { connect } from "react-redux";

const Phonemenu = (props) => {
    const params = useLocation();

    return (
        <div className="phone_menu d-md-none d-block">
            <ul>
                <li>
                    <Link to="/">
                        <img src={home} alt=".." />
                    </Link>
                </li>
                <li>
                    <Link to="/community">
                        <img src={community} alt=".." />
                    </Link>
                </li>
                <li>
                    <Link to="/chat">
                        <img src={chat} alt=".." />
                    </Link>
                    {
                        props.messages && props.messages.total > 0
                        &&
                        <span className="msgdot"></span>
                    }
                </li>
                <li>
                    <Link to="/settings">
                        <img src={setting} alt=".." />
                    </Link>
                </li>
            </ul>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        messages: state.MessagesReducer.messages
    };
}
export default connect(mapStateToProps)(Phonemenu);
