import React from 'react'
import { Link, useHistory } from 'react-router-dom';
import { connect } from "react-redux";
import user from "../../../assets/images/header/user.jpg";
import AuthController from '../../../controllers/auth.controller';
import { renderImage } from '../../../helpers/General';
import ChatSection from './ChatSection';
const MobileChat = (props) => {
    let history = useHistory();
    return (
        <>
            <div>
                <ChatSection />
            </div> 
        </>
    )
}

export default MobileChat;
