import React, { useEffect } from 'react';

const ProgressCircle = (props) =>
{
    const [progress, setProgress] = React.useState(0);
    useEffect(() => {
        setProgress(props.progress);
    }, [props.progress])

    return (
        <div className="progress_loading">
            <div 
                className={`progress-circle progress-` + (progress > 0 ? progress.toFixed(0) : 0) + (progress >= 100 ? ` complete` : ``)}
            >
                <span>
                </span>
            </div>  
        </div>
    )
}

export default ProgressCircle;
