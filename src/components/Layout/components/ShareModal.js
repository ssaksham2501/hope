import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import Moment from "react-moment";
import { Link, withRouter } from "react-router-dom";
import landing_anonymous from "../../../assets/images/Landing/anonymous.png";
import AuthController from "../../../controllers/auth.controller";
import { baseUrl, renderImage, renderVideoThumb, uuId } from "../../../helpers/General";
import ReadMoreAndLess from 'react-read-more-less';
import TimelineMedia from "../../Home/components/TimelineMedia";
import MessagesController from "../../../controllers/messages.controller";

function ShareModal(props) {
    const [user, setUser] = useState(null);
    const [sent, setSent] = useState([]);
    const [friends, setFriends] = useState(null);
    const [friendsDefault, setFriendsDefault] = useState(null);
    const [searchFriends, setSearchFriends] = useState(null);
    const [sentShares, setSentShares] = useState([]);
    
    
    const handleClose = () => props.setShow(false);
    useEffect(() => {
        let user = new AuthController().getLoginUser();
        if (user && user.id) {
            setUser(user);
            if (props.post) {
                getFriendsList();
            }
        }
    }, [props.post]);

    const getFriendsList = async () => {
        let response = await new MessagesController().getChatList();
        if (response && response.status && response.chat) {
            setFriendsDefault(response.chat);
            setFriends(response.chat);
        }
    }

    const shareWithFriend = async (index, toId) => {
        console.log(props.post);
        let media = props.post.media && props.post.media.length > 0 ? props.post.media[0] : null;
        let message = null;
        if (media)
            message = `<share>` + (media.type === 'audio' ? '<span class="audio-media"><i class="fal fa-music"></i></span>' : '<span class="image-media"><img src="' + (media.type === 'video' ? renderVideoThumb(media.file) : baseUrl(`${media.file}`)) + '" /></span>') + `<p>${props.post.description}</p><p class="text-muted">Post By: `+(props.post.anonymously ? `Anonymous` : `${props.post.user.first_name} ${props.post.user.last_name}`)+`</p></share>`;
        else
            message = `<share><p>${props.post.description}</p><p class="text-muted">Post By: `+(props.post.anonymously ? `Anonymous` : `${props.post.user.first_name} ${props.post.user.last_name}`)+`</p></share>`;
        global.socket.submitMessage(
            message,
            toId,
            uuId(),
            props.post.id,
            'post',
        );
        setSentShares([...sentShares, ...[toId]]);
    }

    return (
        <div className="el">
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={props.show}
                onHide={handleClose}
                animation={true}
                className="share-modal"
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div className="modal_head">
                            <h4>Share Post</h4>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="pop_post share-post">
                        <div className="head">
                            <div className="post_col post">
                                <div className="main_post">
                                    <div className="share_post_head">
                                        <div className="before_post">
                                            <div className="imgwrap">
                                                {props.post.anonymously === 0 ? (
                                                    <Link
                                                        to={
                                                            user &&
                                                            props.post.user &&
                                                            props.post.user.id === user.id
                                                                ? "/profile"
                                                                : `/profile/${props.post.user.username}`
                                                        }
                                                    >
                                                        <img
                                                            src={renderImage(
                                                                props.post.user.image
                                                            )}
                                                            alt=""
                                                        />
                                                    </Link>
                                                ) : (
                                                    <img
                                                        src={landing_anonymous}
                                                        alt=""
                                                    />
                                                )}
                                            </div>
                                        </div>
                                        <div className="post_head">
                                            <p className="user_info">
                                                {props.post.anonymously === 0 ? (
                                                    <Link
                                                        to={
                                                            user &&
                                                            props.post.user &&
                                                            props.post.user.id === user.id
                                                                ? "/profile"
                                                                : `/profile/${props.post.user.username}`
                                                        }
                                                        className="user"
                                                    >
                                                        {props.post.user.first_name}
                                                        &nbsp;
                                                        {props.post.user.last_name}
                                                    </Link>
                                                ) : (
                                                    <a className="user">
                                                        Anonymous
                                                    </a>
                                                )}
                                                {props.post.group && props.post.group.title ? (
                                                    <>
                                                        <span className="lav_dot lav_bg"></span>
                                                        <Link
                                                            to={`/community-detail/${props.post.group.slug}`}
                                                            className="comm"
                                                        >
                                                            {props.post.group &&
                                                            props.post.group.title
                                                                ? props.post.group.title
                                                                : ""}
                                                        </Link>
                                                    </>
                                                ) : null}
                                            </p>
                                            <p className="post_time">
                                                <Moment fromNow>
                                                    {props.post.created}
                                                </Moment>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="post_body">
                                        {props.post && props.post.tags.length > 0 ? (
                                            <div className="post_tags">
                                                {props.post.tags.map((i, key) => {
                                                    return (
                                                        <a key={`tag--${props.post.id}-{${key}}`}>
                                                            <span
                                                                onClick={() =>
                                                                    console.log(
                                                                        "TAGS CLICK "
                                                                    )
                                                                }
                                                                className="post_tag"
                                                                key={key}
                                                            >
                                                                {i.tag}
                                                            </span>
                                                        </a>
                                                    );
                                                })}
                                            </div>
                                        ) : null}

                                        {props.post.description ? (
                                            <div className="post_text">
                                                <p>
                                                    <ReadMoreAndLess
                                                        charLimit={250}
                                                        readMoreText={
                                                            "read more"
                                                        }
                                                        readLessText={
                                                            "read less"
                                                        }
                                                    >
                                                        {props.post.description}
                                                    </ReadMoreAndLess>
                                                </p>
                                            </div>
                                        ) : null}

                                        {
                                            props.post.media && props.post.media.length > 0
                                            &&
                                            <TimelineMedia
                                                modal="share"
                                                items={props.post.media}
                                                type={props.post.type}
                                                id={'share' + props.post.id}
                                                index={0}
                                                disableScroll={true}
                                            />
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="share-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="search">
                                        <input
                                            type="text"
                                            className="search-friends"
                                            placeholder="Search for people"
                                            value={searchFriends}
                                            onInput={(e) => {
                                                let val = e.target.value;
                                                setSearchFriends(val);
                                                let f = friendsDefault.filter((item) => {
                                                    return item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.last_name.toLowerCase().indexOf(val.toLowerCase()) > -1
                                                });
                                                console.log(f);
                                                setFriends(f);
                                            }}
                                           
                                        />
                                       <div className="icon">
                                        <i className="far fa-search"></i>
                                       </div>
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="title">
                                            <p>Contacts</p>
                                        </div>
                                    <div className="show_suggested">
                                       
                                    {
                                        friends && friends.length > 0 && friends.map((f, i) => {
                                            return (<div className="share_suggestion" key={`f${i}`}>
                                                <div className="user_img">
                                                    <img
                                                        src={renderImage(f.image)}
                                                        alt=""
                                                    />
                                                </div>
                                                <div className="user_name">
                                                    <div className="left">
                                                        {f.first_name + ' ' + f.last_name}
                                                    </div>
                                                    <div className="right">
                                                        {
                                                            sentShares.includes(f.to_id)
                                                            ?
                                                                <button className="btn-muted"><i className="fa fa-check">Sent</i></button>
                                                            :
                                                                <button
                                                                    onClick={() => {
                                                                        shareWithFriend(i, f.to_id);
                                                                    }}
                                                                >Send</button>
                                                        }
                                                        
                                                    </div>
                                                </div>
                                            </div>);
                                        })
                                    }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default withRouter(ShareModal);

