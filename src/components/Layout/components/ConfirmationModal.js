import React, { useEffect, useState } from "react";

import { Link, withRouter } from "react-router-dom";

import { Button, Modal } from "react-bootstrap";

function ConfirmationModal(props) {
    const handleClose = () => props.cancel();
    console.log(props.show);
    return (
        <div className="el">
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={props.show}
                onHide={handleClose}
                animation={true}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div className="modal_head">
                            <h4>{props.title}</h4>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="pop_post">
                        {/* <div className="head">
                            <p>{props.description}</p>
                        </div> */}
                        <div className="btnss">
                            <button
                                className="report lav_bg"
                                onClick={() => props.confirm()}
                            >
                                Confirm
                            </button>
                            <button
                                className="cancel"
                                onClick={() => props.cancel()}
                            >
                                Cancel
                            </button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default withRouter(ConfirmationModal);
