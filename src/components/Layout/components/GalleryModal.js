import React, { useEffect } from 'react';
import { Button, Carousel, Modal } from 'react-bootstrap';
import ReactPlayer from 'react-player';
import { connect } from 'react-redux';
import { renderImage } from '../../../helpers/General';
import { linkInModal, playedItem } from '../../../redux/actions/user';

const GalleryModal = (props) =>
{
    const [show, setShow] = React.useState(false);
    const [activeSlide, setActiveSlide] = React.useState(0);
    
    const onHide = () => {
        props.showLinkInModal(null);
    }

    useEffect(() => {
        console.log(`props.link`, props.link);
        if (props.link && props.link) {
            if (typeof props.link.index !== undefined) {
                setActiveSlide(props.link.index);
            }
            setShow(true);
            // setTimeout(() => {
            //     let obj = document.querySelector(".gallery.modal .carousel");
            //     console.log(obj);
            //     if (obj) {
            //         obj.style.height = `${window.innerHeight * 1 - 150}px`;
            //     }
            // }, 400);
        }
        else {
            setShow(false);
        }
    }, [props.link]);

    const renderImage = (item) => {
        if (item) {   
            let ext = item.split('.').pop();
            let image = null;
            console.log(ext.match(/jpeg|jpg|png|webp/) && ext.match(/jpeg|jpg|png|webp/).length > 0);
            if (ext.match(/jpeg|jpg|png|webp/) && ext.match(/jpeg|jpg|png|webp/).length > 0) {
                image = item.replace(/\\/g, '/').split('/');
                image[image.length - 1] = 'O-' + image[image.length - 1];
                image = image.join('/');
            }
            return image;
        }
    }

    return (
        <Modal
            className={`gallery ` + (props.link && props.link.list && props.link.list.length > 1 ? `` : `nodots`)}
            show={show}
            onHide={() => onHide()}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
         >
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                {
                    props.link && props.link.list && props.link.list.length > 0
                        ?
                        <>
                            <Carousel
                                controls={props.link.list.length > 1 ? true : false}
                                interval={null}
                                activeIndex={activeSlide}
                                onSelect={(e) => {
                                    setActiveSlide(e);
                                }}
                            >
                                {
                                    props.link.list.map((item, i) => {
                                        let ext = item.split('.').pop().toLowerCase();
                                        return (
                                            <Carousel.Item key={i}>
                                                {
                                                    ext.match(/mp4|mov/) && ext.match(/mp4|mov/).length > 0
                                                    ?
                                                    <div className='video_boxes'>
                                                        <ReactPlayer
                                                            playing={activeSlide === i ? true : false}
                                                            className="player"
                                                            controls
                                                            width="100%"
                                                            height="100%"
                                                            url={item}
                                                            onPlay={() => {
                                                                props.playItem(null);
                                                            }}
                                                            onPause={() => {
                                                                props.playItem(null);
                                                            }}
                                                        />
                                                        </div>
                                                    :
                                                    <img
                                                        className="d-block"
                                                        src={renderImage(item)}
                                                        alt="First slide"
                                                    />
                                                }
                                                
                                            </Carousel.Item>
                                        );
                                    })
                                }
                            </Carousel>
                        </>
                        :
                        <Carousel controls={false}>
                            {
                                <Carousel.Item>
                                    {
                                        props.link && props.link.split('.').pop().toLowerCase().match(/mp4|mov/) && props.link.split('.').pop().toLowerCase().match(/mp4|mov/).length > 0
                                        ?
                                            <ReactPlayer
                                                playing={true}
                                                autoplay={true}
                                                className="player"
                                                controls
                                                width="100%"
                                                height="100%"
                                                url={props.link}
                                                onPlay={() => {
                                                    props.playItem(null);
                                                }}
                                                onPause={() => {
                                                    props.playItem(null);
                                                }}
                                            />
                                        :
                                        <img
                                            className="d-block"
                                            src={renderImage(props.link)}
                                            alt="First slide"
                                        />
                                    }
                                    
                                </Carousel.Item>
                            }
                        </Carousel>
                }
                
            </Modal.Body>
        </Modal>
    )
}

const mapStateToProps = (state) => {
    return {
        link: state.LinkInModalReducer.link,
        playedItem: state.PlayedItemReducer.played
    }
};
const mapDispatch = (dispatch) => {
    return {
        showLinkInModal: async (info) => dispatch(linkInModal(info)),
        playItem: (info) => dispatch(playedItem(info)),
    };
}

export default connect(mapStateToProps, mapDispatch)(GalleryModal)