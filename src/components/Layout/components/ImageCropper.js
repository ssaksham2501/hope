import React, { useEffect, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import Form from 'react-bootstrap/Form'
import 'cropperjs/dist/cropper.css';
import Cropper from 'cropperjs';
let cropper;
const ImageCropper = (props) => {
    
    const [show, setShow] = React.useState(false);
    const [zoom, setZoom] = useState(0.5);
    const [spinner, setSpinner] = useState(true);

    useEffect(() => {
        setShow(true);
        setSpinner(true);
        setZoom(0.5);

        setTimeout(() => {
            const image = document.getElementById('image');
            cropper = new Cropper(image, {
                viewMode: 1,
                autoCrop: true,
                autoCropArea: 0,
                modal: true,
                cropBoxResizable: false,
                cropBoxMovable: false,
                dragMode: 'move',
                ready() {
                    setSpinner(false);
                    const containerData = cropper.getContainerData();
                    cropper.setCropBoxData({ width: props.item.width, height: props.item.height, left: (containerData.width - props.item.width) / 2, top: (containerData.height - props.item.height) / 2 });
                    cropper.zoom(0.5);
                }
            });
        }, [1000]);
        

    }, []);

    const saveImage = () => {
        let image = cropper.getCroppedCanvas().toDataURL();
        props.onCrop(image, props.item.type);
        props.onClose();
    }

    const rotateRight = () => {
        cropper.rotate(90);
    }
    


    return (
        <Modal
            show={show}
            onHide={() => {
                props.onClose()
                setShow(false);
            }}
            className={`cropper gallery`}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
             <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body>
                    <div className={`image-editor-block ` + (props.item.type)}>
                        {
                            spinner
                            &&
                            <p className={`spinner`}><i className="fad fa-spinner-third fa-spin"></i></p>
                        }
                        <div className="cropper-body">
                            <div>
                                <img id="image" src={props.item.image} />
                            </div>
                        </div>

                        <div className="image_zoom_slider d-flex pt-3">
                            <div className="range_slider">
                                <h6>Zoom</h6>
                                <div className="d-flex align-items-center">
                                    <div className="range_btn text-left">
                                        <button type="button" 
                                            className="btn_text_only"
                                            onClick={() => {
                                               let z = zoom*1;
                                                z = z - 0.05;
                                                console.log(z);
                                                if (z >= 0) {
                                                    cropper.zoom(zoom > z ? -0.1 : 0.1);
                                                    setZoom(z);
                                                }
                                            }}
                                        >
                                            <i className="far fa-minus"></i>
                                        </button>
                                    </div>
                                    <div className="range_area">
                                        <Form.Range
                                            defaultValue={zoom}
                                            value={zoom}
                                            onChange={(e) => {
                                                let val = e.target.value;    
                                                cropper.zoom(zoom > val ? -0.1 : 0.1);
                                                setZoom(val);
                                            }}
                                            min={0}
                                            max={1}
                                            step={0.05}
                                        />
                                    </div>
                                    <div className="range_btn text-right">
                                        <button type="button" 
                                            className="btn_text_only"
                                            onClick={() => {
                                                let z = zoom*1;
                                                z = z + 0.05;
                                                console.log(z);
                                                if (z <= 1) {
                                                    cropper.zoom(zoom > z ? -0.1 : 0.1);
                                                    setZoom(z);
                                                }
                                            }}
                                        >
                                            <i className="far fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className="text-right ml-2">
                                <h6>Rotate</h6>
                                <div className="d-flex align-items-center justify-content-center">
                                    <button type="button" 
                                        className="rotate_btn btn_text_only"
                                        onClick={() => {
                                            rotateRight();
                                        }}
                                    >
                                        <i className="fas fa-redo"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant="default"
                    onClick={() => {
                        props.onClose();
                    }}
                >Cancel</Button>
                <Button
                    className={`main_btn pink_bg`}
                    variant="default"
                    onClick={saveImage}
                >
                    Save changes
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
export default ImageCropper;
