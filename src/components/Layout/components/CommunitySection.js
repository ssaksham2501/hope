import React from "react";
import { Link } from "react-router-dom";

//assets
import landing_callie from "../../../assets/images/Landing/callie.png";
import Nodata from "./Nodata";

const CommunitySection = () => {
    return (
        <section className="community_suggestions d-lg-none d-sm-block d-none ">
            <div className="community-head">
                <div className="name">
                    <p>Suggested Communities </p>
                </div>
                {/* <div className="icon">
                    <Link to="/">
                        <i className="fal fa-ellipsis-h"></i>
                    </Link>
                </div> */}
            </div>
            <div className="suggestion_list">
                <ul>
                    <li>
                        <div className="img_area">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Wish</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>We Pray</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Happiness</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Placidity</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Calm</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Dreams</p>
                        </div>
                    </li>
                </ul>
                {/* <Nodata /> */}
            </div>
        </section>
    );
};

export default CommunitySection;
