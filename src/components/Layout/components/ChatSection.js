import React, { useEffect, useRef, useState } from "react";
import { Dropdown } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import MessagesController from "../../../controllers/messages.controller";
import { renderImage, timeAgo } from "../../../helpers/General";
import FooterSection from "./FooterSection";
import nodata1 from "../../../assets/images/Layout/no_data.png";
import nodata2 from "../../../assets/images/Layout/chat_1.png";

global.firstOne = null
const ChatSection = (props) => {
    let inputRef = useRef(null);
    const chatRef = useRef(null);
    const [loading, setLoading] = useState(false);
    const [list, setMessagesList] = useState([]);
    const [messages, setMessages] = useState("");
    const [chatEnable, setChatEnable] = useState(false);
    const [search, setSearch] = useState("");

    useEffect(() => {
        getList();
    }, []);

    const getList = async (page) => {
        await new MessagesController().chatList({
            page: page ? page : 1
        });
    };

    const openChat = (item) => {
        new MessagesController().setChatBoxModal({
            mId: item.id,
            id: item.to_id,
            name: item.first_name.concat(" " + item.last_name),
            image: item.image,
            username: item.username,
            responseTimings: null,
            blocked_user: item.blocked_user,
        });
    };

    return (
        <div className="chatev sticky-top">
            <div className="chats ">
                <div className="chat-head">
                    <div className="name">
                        <div className="notification_drop_down">
                            Chat <span>{props.messages && props.messages.total ? `(${props.messages.total})` : ''}</span>
                        </div>
                    </div>
                    <div className="icon">
                        <a onClick={() => {
                            setChatEnable(!chatEnable);
                            if(!chatEnable)
                            {
                                setTimeout(() => {
                                    inputRef.current.focus();
                                }, 400)
                                
                            }
                        }}>
                            <i className="fal fa-search"></i>
                        </a>
                    </div>
                    <div
                        className={`inputer` + ( !chatEnable ? ` d-none` : ``)}
                    >
                        <input
                            type="text"
                            placeholder="Search..."
                            ref={inputRef}
                            value={search}
                            onBlur={(e) => {
                                console.log(e.target.value);
                                if (!e.target.value) {
                                    setChatEnable(false);
                                }
                            }}
                            onChange={(e) => {
                                setSearch(e.target.value);
                            }}
                        />
                    </div>
                </div>
                <div className="chat-users">
                    {props.chats && props.chats.list && props.chats.list.length > 0 ? (
                        <ul
                            ref={(chatRef)}
                            // onScroll={(e) => {
                            //     if (e.target.scrollTop >= e.target.clientHeight - (6 * 45)) {
                            //     }
                            // }}
                        >
                            {props.chats.list
                                .filter((e) => {
                                    return search ? (e.first_name.toLowerCase().indexOf(search.toLowerCase()) > -1 || e.last_name.toLowerCase().indexOf(search.toLowerCase())  > -1) : true
                                })
                                .map((item, index) => (
                                <li key={index} onClick={() => openChat(item)} className={(item.count > 0 ? `highlight-chat` : ``)}>
                                    <div className="users">
                                        <div className="user-img">
                                            <img
                                                src={renderImage(item.image)}
                                                alt=""
                                            />
                                        </div>
                                        <div className="name-detail">
                                            <div className="name">
                                                <p>
                                                    {item.first_name
                                                        ? item.first_name +
                                                          " " +
                                                          (item.last_name
                                                              ? item.last_name
                                                              : "")
                                                        : ""}
                                                        
                                                </p>
                                            </div>
                                            <div className="timed">
                                                <div className="messge">
                                                    <span>{item.message && item.message.trim() ? item.message : 'You are connected on chat.' }</span>
                                                </div>
                                                <div className="time">
                                                    {
                                                        item.mute
                                                        ?
                                                            <i className="far fa-volume-mute"></i>
                                                        :
                                                            null
                                                    }
                                                    <div className="time-view">
                                                        <p>
                                                            {timeAgo(
                                                                item.modified
                                                            )}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    ) : (
                        <div className="nodata_wrap">
                            <div className="img_area">
                                <img src={nodata1} alt="" />
                            </div>
                            <div className="nodata-message">
                                <h4>No chat available!</h4>
                            </div>
                        </div>
                    )}
                </div>
            </div>
            <div className="d-lg-block d-none">
                <FooterSection />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        messages: state.MessagesReducer.messages,
        chats: state.ChatListReducer.list,
    };
}
export default connect(mapStateToProps)(ChatSection);

