import React, { Component } from "react";
import Spinner from "react-bootstrap/Spinner";
export class Spinners extends Component {
    render() {
        return (
            <>
                <Spinner animation="border" />
            </>
        );
    }
}

export default Spinners;
