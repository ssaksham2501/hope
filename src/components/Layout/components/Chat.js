import React, { useEffect, useRef, useState } from 'react';
import { isMobile, isTablet } from 'react-device-detect';
import { useHistory } from 'react-router-dom';
import AuthController from '../../../controllers/auth.controller';
import MessagesController from '../../../controllers/messages.controller';
import { dateText, linkify, renderImage, timeAgo, _msdate } from '../../../helpers/General';
import TimelineMedia from '../../Home/components/TimelineMedia';

const Chat = (props) => {
    var messageList = [];
    let history = useHistory();    
    var listing = {
        to: props.to.id,
        chat: []
    };
    var minimizeChat = props.minimize;
    let chatScrollBox = useRef(null);
    const [scrollPoint, setScrollPoint] = useState(null);
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [nextFetch, setNextFetch] = useState(true);
    const [loginUser, setLoginUser] = useState(null);
    const [lastMessageId, setLastMessageId] = useState(null);
    const [newMessage, setNewMessage] = useState(false);
    
    
    useEffect(() => {
        init()
    }, []);

    const init = async () => {
        listing = {
            to: props.to.id,
            chat: []
        };
        let loginUser = await new AuthController().getLoginUser();
        setLoginUser(loginUser);
        setNewMessage(true);
        await getMessagesList();
        if(chatScrollBox && chatScrollBox.current)
        {
            chatScrollBox.current.scrollTo(0, chatScrollBox.current.scrollHeight);
        }
    }

    useEffect(() => {
        if (props.to.id) {
            props.socket.markRead(props.to.id);
        }
        props.socket.recieveMessage(recieveMessage);
    }, [props.socket]);

    useEffect(() => {
        minimizeChat = props.minimize;
    }, [props.minimize])
    
    const getMessagesList = async (lastid = null) => {
        setNewMessage(false);
        const response = await new MessagesController().getMessages({
            id: props.to.id,
            last_id: lastid
        });
        if (response && response.status) {
            if (response.messages.length > 0) {
                messageList = [...response.messages, ...messageList];
                setList(messageList);
                if (chatScrollBox && chatScrollBox.current) {
                    chatScrollBox.current.scrollTo(0, chatScrollBox.current.scrollHeight - scrollPoint);
                    setScrollPoint(chatScrollBox.current.scrollHeight - scrollPoint);
                }
                props.onResponse(response);
            }
            else {
                setNextFetch(false);
            }
        }
    };
        
    const nextFetchRecords = async () => {
        if(!loading && messageList && messageList.length > 0)
        {
            setLoading(true);
            await getMessagesList(messageList[0].id);
            setLoading(false);
        }
    }

    const updateMessageList = (message) => {
        messageList = [...messageList, ...[message]];
        setList(messageList);
    }

    const recieveMessage = async (message) => {
        let loginUserId = await new AuthController().getLoginUserId();
        message = message.data;
        setNewMessage(true);
        if (message && message.from.id === props.to.id && message.to.id === loginUserId) {
            //Receive Message
            updateMessageList(message);
        }
        else if (message && message.from.id === loginUserId && message.to.id === props.to.id) {
            //Send Message
            updateMessageList(message);
        }
        if (chatScrollBox && chatScrollBox.current) {
            chatScrollBox.current.scrollTop = chatScrollBox.current.scrollHeight + 200;
        }
    }

    const renderMessages = () => {
        let lastCreatedAt = null;
        let m = [];
        list && list.length > 0 && list.map((item, index) => {
            if (item.id) {
                m.push(<div key={`msg-` + item.id} id={`msg-` + item.id}>
                    {
                        (item.created && (lastCreatedAt === null || _msdate(item.created) != lastCreatedAt))
                        &&
                        <div className="chat_date">
                            <span>{dateText(item.created)}</span>
                        </div>
                    }

                    {item.from_id === props.to.id ? (
                        <div className="chat_box_left">
                            <div className="chat-do">
                                <div className="left">
                                    <div className="user-img">
                                        <img
                                            src={renderImage(props.to.image)}
                                            alt=".."
                                        />
                                    </div>
                                </div>
                                <div className="conversation">
                                    <div className="msg">
                                        {
                                            item.attachment && item.attachment_type !== 'post'
                                                ?
                                                <TimelineMedia
                                                    disableScroll={true}
                                                    id={`chat-${item.id}`}
                                                    items={[{
                                                        type: item.attachment_type,
                                                        file: item.attachment
                                                    }]}
                                                    videoThumbPath={`/uploads/messages/thumbs/`}
                                                />
                                                :
                                                <>
                                                    {
                                                        item.message && item.message.indexOf('<share>') > -1 && item.attachment_type === 'post'
                                                        ?
                                                            <a className="post_sender"
                                                                dangerouslySetInnerHTML={{ __html: item.message }}
                                                                onClick={() => {
                                                                    if (isMobile || isTablet) {
                                                                        props.closeChatModal();
                                                                    }
                                                                    history.push(item.attachment);
                                                                }}
                                                            ></a>
                                                        :
                                                            <p dangerouslySetInnerHTML={{ __html: linkify(item.message) }}></p>
                                                    }
                                                </>
                                        }
                                    </div>
                                    <div className="time">
                                        <p>
                                            {timeAgo(
                                                item.created
                                            )}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className="chat_box_right">
                            <div className="chat-do">
                                <div className="conversation">
                                    <div className="time">
                                        <p>
                                            {timeAgo(
                                                item.created
                                            )}
                                        </p>
                                    </div>
                                    <div className="msg">
                                        {
                                            item.attachment && item.attachment_type !== 'post'
                                                ?
                                                <TimelineMedia
                                                    disableScroll={true}
                                                    id={`chat-${item.id}`}
                                                    items={[{
                                                        type: item.attachment_type,
                                                        file: item.attachment
                                                    }]}
                                                    videoThumbPath={`/uploads/messages/thumbs/`}
                                                />
                                                :
                                                <>
                                                    {
                                                        item.message && item.message.indexOf('<share>') > -1 && item.attachment_type === 'post'
                                                            ?
                                                            <a className="post_sender"
                                                                dangerouslySetInnerHTML={{ __html: item.message }}
                                                                    onClick={() => {
                                                                        if (isMobile || isTablet) {
                                                                            props.closeChatModal();
                                                                        }
                                                                        history.push(item.attachment);
                                                                    }}
                                                            ></a>
                                                            :
                                                            <p dangerouslySetInnerHTML={{ __html: linkify(item.message) }}></p>
                                                    }
                                                </>
                                        }
                                    </div>
                                </div>
                                <div className="left">
                                    <div className="user-img">
                                        <img
                                            src={renderImage(
                                                loginUser
                                                    .image
                                            )}
                                            alt=".."
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>);
                lastCreatedAt = _msdate(item.created);
            }
        });
        return m;
    }
    
    return (<div
        className="lets_chat"
        ref={chatScrollBox}
        onScroll={async () => {
            setScrollPoint(chatScrollBox.current.scrollHeight);
            if(chatScrollBox.current.scrollTop < 100 && nextFetch)
            {
                await nextFetchRecords();
            }
        }}

    >
        {
            loading
            &&
            <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>
        }

        {renderMessages()}
        
    </div>);
}

export default Chat;