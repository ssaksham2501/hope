import React, { Component } from "react";
//assets
import nodata1 from "../../../assets/images/Layout/no_data.png";
import nodata2 from "../../../assets/images/Layout/chat_1.png";
const Nodata = (props) => {
        return (
            <>
                <div className="nodata_wrap">
                    <div className="img_area">
                        <img src={nodata1} alt="" />
                    </div>
                    <div className="nodata-message">
                        <h4>{props.title ? props.title : `No Data Found`}</h4>
                    </div>
                </div>
            </>
        );
}

export default Nodata;
