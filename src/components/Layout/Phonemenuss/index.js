import React from 'react'
import { Link, useHistory } from 'react-router-dom';
import { connect } from "react-redux";
import user from "../../../assets/images/header/user.jpg";
import AuthController from '../../../controllers/auth.controller';
import { renderImage } from '../../../helpers/General';
const Phonemenuss = (props) => {
    let history = useHistory();
    return (
     <>
     <div className="phoneside d-lg-none d-block">
         <div className='container'>
             <div className='row'>
                 <div className='col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-0'>
                     <div className='head text-center'>
                         <h6>Hope</h6>
                         <div className='icon-Back'>
                            <a 
                                onClick={() => {
                                    history.goBack();
                                }}
                            ><i className="fas fa-long-arrow-left"></i></a>
                         </div>
                     </div>
                     <div className='profile'>
                         <ul>
                            <li>
                                <Link to="/profile">
                                    <div className='useree'>
                                        <div className='left'>
                                            <div className='user-img'>
                                                <img
                                                    src={
                                                        props.user &&
                                                        renderImage(
                                                            props.user.image
                                                        )
                                                    }
                                                    alt="../"
                                                />
                                            </div>
                                        </div>
                                        <div className='right'>
                                            <div className='name'>
                                                <p>{props.user.first_name + ` ` + props.user.last_name}</p>
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            </li>

                             <li>
                                 <Link to="/about-us">About us</Link>
                             </li>
                             <li>
                                 <Link to="/privacy-policy">Privacy Policy </Link>
                             </li>
                             <li>
                                 <Link to="/contact-us">Contact Us</Link>
                             </li>
                             <li>
                                 <Link to="/terms-conditions">Terms & Conditions</Link>
                             </li>
                             <li>
                                <Link to="/feedback">Feedback</Link>
                            </li>
                             <li>
                                 <Link to="/faq">FAQ</Link>
                             </li>
                             <li>
                                <a 
                                    onClick={() => {
                                        new AuthController().logout();
                                        history.push("/");
                                    }}
                                >Logout</a>
                             </li>
                         </ul>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </>
    )
}

const mapState = (state) => {
    return {
        user: state.UserReducer.user,
    };
};
const mapDispatch = (dispatch) => {
};

export default connect(mapState, mapDispatch)(Phonemenuss);
