import Downshift from "downshift";
import React, { useEffect, useState } from "react";
import Dropdown from "react-bootstrap/Dropdown";
import { connect } from "react-redux";
import { Link, useHistory } from "react-router-dom";
//assets
import logo from "../../assets/images/Landing/logo.png";
import AuthController from "../../controllers/auth.controller";
import ProfileController from "../../controllers/profile.controller";
import { renderImage } from "../../helpers/General";
import { setHeaderHeight } from "../../redux/actions/user";
import NotificationSection from "./components/NotificationSection";
import Phonemenu from "./components/Phonemenu";
import getHistory from "react-router-global-history";
import FriendrequestSection from "./components/FriendrequestSection";
import Nodata from "../Layout/components/Nodata";
import MessagesController from "../../controllers/messages.controller";

function Header(props) {
    let history = useHistory();
    useEffect(() => {
        let headerHeight = document.getElementById("header");
        props.setHeader(headerHeight.offsetHeight);
    }, []);

    const [list, setList] = useState([]);
    const [keyword, setSearchKeyword] = useState("");
    const [notifications, setNotifications] = useState([]);
    const [friendRequests, setFriendRequests] = useState([]);
    const [notificationCount, setNotificationCount] = useState(0);    
    const [notifyCount, setNotifyCount] = useState(0);    
    const [requestCount, setRequestCount] = useState(0);    
    const [noNotificaitons, setNoNotifications] = useState(false);
    const [showNotifications, setShowNotifications] = useState(false);
    

    

    const getList = async (value) => {
        const response = await new ProfileController().searchList(value);
        if (response && response.status) {
            setList(response.users);
        } else {
            setList([]);
        }
    };

    const getNotifications = async () => {
        const response = await new ProfileController().getNotifications();
        if (response && response.status && (response.requests.length > 0 || response.notifications.length > 0)) {
            setNoNotifications(false);
            setNotifications(response.notifications);
            setFriendRequests(response.requests);
            setNotificationCount(response.count);
            setRequestCount(response.requestCount);
            setNotifyCount(response.notificationCount);
        } else {
            setFriendRequests([]);
            setNotifications([]);
            setNoNotifications(true);
            setNotificationCount(0);
            setRequestCount(0);
            setNotifyCount(0);
        }
    }

    return (
        <>
            <div className="header_section" id="header">
                <div className="container">
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="header_area">
                                <div className="left">
                                    <div className="logo-box">
                                        <Link to="/">
                                            <img src={logo} alt="" />
                                        </Link>
                                    </div>
                                </div>
                                <div className="right">
                                    <div className="seacrh-bar">
                                        <Downshift
                                            onChange={(item) => {
                                                history.push(
                                                    item.type === "commumnity"
                                                        ? `/community-detail/${item.slug}`
                                                        : ( props.user && props.user.id == item.row_id ? `/profile`: `/profile/${item.slug}`)
                                                );
                                                setSearchKeyword("");
                                            }}
                                            
                                            inputValue={keyword}
                                            itemToString={(item) => keyword}
                                            onInputValueChange={(e) => {
                                                if (e) {
                                                    getList(e);
                                                } else {
                                                    setList([]);
                                                }
                                                setSearchKeyword(e);
                                            }}
                                        >
                                            {({
                                                getInputProps,
                                                getItemProps,
                                                getLabelProps,
                                                getMenuProps,
                                                isOpen,
                                                inputValue,
                                                highlightedIndex,
                                                selectedItem,
                                                getRootProps,
                                            }) => (
                                                <div>
                                                    <label
                                                        {...getLabelProps()}
                                                    ></label>
                                                    <div
                                                        style={{
                                                            display:
                                                                "inline-block",
                                                        }}
                                                        {...getRootProps(
                                                            {},
                                                            {
                                                                suppressRefError: true,
                                                            }
                                                        )}
                                                    >
                                                        <input
                                                            {...getInputProps()}
                                                            type="search"
                                                            placeholder="Search"
                                                            value={keyword}
                                                        />
                                                    </div>
                                                    <ul {...getMenuProps()}>
                                                        {isOpen
                                                            ? list.map(
                                                                  (
                                                                      item,
                                                                      index
                                                                  ) => (
                                                                      <li
                                                                          {...getItemProps(
                                                                              {
                                                                                  key: item.row_id,
                                                                                  index,
                                                                                  item,
                                                                                  style: {
                                                                                    backgroundColor:
                                                                                        highlightedIndex ===
                                                                                            index
                                                                                            ? "lavender"
                                                                                            : "white",
                                                                                    fontWeight:
                                                                                        selectedItem ===
                                                                                            item
                                                                                            ? "bold"
                                                                                            : "normal",
                                                                                },
                                                                              }
                                                                          )}
                                                                      >
                                                                          <div className="searcher-filers">
                                                                              <div className="divider">
                                                                                  <div className="left">
                                                                                      <div className="img-wrap">
                                                                                          <img
                                                                                              src={renderImage(
                                                                                                  item.image
                                                                                              )}
                                                                                              alt=".../"
                                                                                          />
                                                                                      </div>
                                                                                  </div>
                                                                                  <div className="right">
                                                                                      <div className="name">
                                                                                          <h5>
                                                                                              {
                                                                                                  item.name
                                                                                              }
                                                                                          </h5>
                                                                                          <small>{item.type ===
                                                                                          "commumnity" ? 'Community' : 'User'}</small>
                                                                                      </div>
                                                                                  </div>
                                                                              </div>
                                                                          </div>
                                                                      </li>
                                                                  )
                                                              )
                                                            : null}
                                                    </ul>
                                                </div>
                                            )}
                                        </Downshift>
                                    </div>
                                    <div className={`bell-icon` + (props.notificationCount && props.notificationCount.total > 0 ? ` dot-active` : ``)}>
                                        <Dropdown
                                            className=""
                                            show={showNotifications}
                                            onToggle={(isOpen) => {
                                                setShowNotifications(isOpen);
                                                if (isOpen) {
                                                    setNoNotifications(false);
                                                    setNotifications([]);
                                                    setFriendRequests([]);
                                                    getNotifications();
                                                }
                                            }}
                                            autoClose="outside"
                                        >
                                            <Dropdown.Toggle
                                                id="dropdown-autoclose-outside"
                                            >
                                                <i className="fal - fa-bell"></i>
                                            </Dropdown.Toggle>
                                            <Dropdown.Menu>
                                                <Dropdown.Item href="#">
                                                    <div className="notification_drop_down">
                                                        <div className="notification_head">
                                                            <h5>
                                                                Notifications {notificationCount && notificationCount > 0 ? `(${notificationCount})` : ``}
                                                            </h5>
                                                            <div className="notification_btns">
                                                                {
                                                                    (notifications && notifications.length > 0 || friendRequests && friendRequests.length > 0)
                                                                    ?
                                                                        <NotificationSection
                                                                            notifications={notifications}
                                                                            friendRequests={friendRequests}
                                                                            notifyCount={notifyCount}
                                                                            requestCount={requestCount}
                                                                            close={() => {
                                                                                setShowNotifications(false);
                                                                            }}
                                                                            reload={() => {
                                                                                getNotifications();
                                                                            }}
                                                                        />
                                                                    :
                                                                        (!noNotificaitons ? <p className="spinner-notfi"><i className="fad fa-spinner-third fa-spin"></i></p> : <Nodata title="No Notification available." />)
                                                                }
                                                            </div>

                                                        </div>
                                                    </div>
                                                </Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </div>
                                    <div className="user-img d-lg-block d-none">
                                        <Dropdown
                                            className=""
                                            // autoClose="outside"
                                        >
                                            <Dropdown.Toggle id="dropdown-autoclose-outside">
                                                <img
                                                    src={
                                                        props.user &&
                                                        renderImage(
                                                            props.user.image
                                                        )
                                                    }
                                                    alt="../"
                                                />
                                            </Dropdown.Toggle>
                                            <Dropdown.Menu>
                                                <Dropdown.Item onClick={() => {
                                                   history.push("/profile");
                                                }}>
                                                    <i className="fal fa-user"></i>
                                                    My Profile
                                                </Dropdown.Item>
                                                <div className="d-lg-none d-block">
                                                    <Dropdown.Item href="/about-us">
                                                    <i className="fal fa-link-building"></i>
                                                        About Us
                                                    </Dropdown.Item>
                                                    <Dropdown.Item href="/contact-us">
                                                    <i className="fal fa-link-address-book"></i>
                                                        Contact Us
                                                    </Dropdown.Item>
                                                    <Dropdown.Item href="/privacy-policy">
                                                    <i className="far fa-file-alt"></i>
                                                    Privacy Policy
                                                    </Dropdown.Item>
                                                    <Dropdown.Item href="/terms-conditions">
                                                    <i className="far fa-file-alt"></i>
                                                        Terms & Conditions
                                                    </Dropdown.Item>
                                                    <Dropdown.Item href="/feedback">
                                                    <i className="fal fa-thumbs-up"></i>
                                                        Feedback
                                                    </Dropdown.Item>
                                                    <Dropdown.Item href="/faq">
                                                    <i className="fal fa-link-interrogation"></i>
                                                        FaQ
                                                    </Dropdown.Item>
                                                </div>
                                                <Dropdown.Item
                                                    onClick={() => {
                                                        new AuthController().logout();
                                                        history.push("/");
                                                    }}
                                                >
                                                    <i className="fal fa-sign-out"></i>
                                                    Logout
                                                </Dropdown.Item>
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </div>
                                    <div className="phone-ue d-lg-none d-block">
                                        <Link to="/phonemenu">
                                            <i className="fas fa-bars"></i>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <Phonemenu />
            </div>
        </>
    );
}

const mapState = (state) => {
    return {
        user: state.UserReducer.user,
        notificationCount: state.NotificationReducer.notifications,
    };
};
const mapDispatch = (dispatch) => {
    return {
        setHeader: async (info) => dispatch(setHeaderHeight(info)),
    };
};

export default connect(mapState, mapDispatch)(Header);
