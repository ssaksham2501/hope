import React, { useEffect, useState } from "react";
import { Toaster } from "react-hot-toast";
import { connect } from "react-redux";
import MessagesController from "../../controllers/messages.controller";
import SocketController from "../../controllers/SocketController";
import { setScroller } from "../../redux/actions/user";
import store from "../../redux/store";
import ChatboxSection from "./components/ChatboxSection";
import GalleryModal from "./components/GalleryModal";
import Footer from "./Footer";
import Header from "./Header";
import Sidebar from "./Sidebar";
import { useLocation, useHistory } from 'react-router-dom';
import { baseUrl } from "../../helpers/General";
import Landing from "../Landing/Index";
import Log from "../Auth/components/Log";
import Login from "../Auth/LogIn";
import { isIOS, isMobile } from "react-device-detect";

let intervalVar = null;
let loadingCounts = false;
const audio = new Audio(baseUrl('/uploads/messsage-pop.wav'));
const DefaultLayout1 = (props) => {
    const history = useHistory();
    const location = useLocation();
    const [lastLocation, setLastLocation] = useState(null);

    useEffect(() => {
        console.log(isIOS);
        if (isIOS && !global.isLogin) {
            history.push('/login');
        }

        pageHeightInit();
        setTimeout(() => {
            initSocket();
        }, 1000);
        updateMessagesCount();

        return () => {
            if (intervalVar) {
                clearInterval(intervalVar);
            }
            if (global.socket) {
                global.socket.disconnect();    
            }
        }

    }, []);

    /** Scroll Page Update */
    const listInnerRef = React.useRef();
    const onScroll = () => {
        const { scrollTop, scrollHeight, clientHeight } = listInnerRef.current;
        store.dispatch(setScroller({ scrollTop, scrollHeight, clientHeight }));
    };
    useEffect(() => {
        window.scrollTo(0, 0);
        if (document.querySelector('.home_section')) {
            document.querySelector('.home_section').scrollTo(0, 0);
            document.querySelector('.home_section').scrollTop = 0;
        }

        if(isMobile && lastLocation !== location.pathname) {
            new MessagesController().deleteChatBoxModal();
            if(global.socket) {
                global.socket.offMessages();
            }
        }
        setLastLocation(location.pathname);
        return () => {

        };
    }, [location.pathname]);

    useEffect(() => {
        pageHeightInit();
    }, [global.isLogin]);

    const pageHeightInit = () => {
        if (document.querySelector(".home_section")) {
            document.querySelector(".home_section").style.height = `${window.innerHeight * 1}px`;

            window.addEventListener('resize', function (event) {
                document.querySelector(".home_section").style.height = `${window.innerHeight * 1}px`;
            }, true);
        }
    }
    /** Scroll Page Update */

    /** Socket Connection */
    useEffect(() => {
        if (props.user && props.user.id) {
            initSocket();
        }
        else {
            
            if(global.socket) {
                global.socket.disconnect();
                global.socket = null;
            }
            history.push('/');
        }
    }, [props.user])

    const initSocket = async () => {
        await new MessagesController().chatList();
        if(props.user && props.user.id)
        {
            if (!global.socket) {
                global.socket = new SocketController({ userId: props.user.id });
                global.socket.recieveNotification(async (message) => {
                    message = message.data;
                    if (message.to_id === props.user.id) {
                        let playSound = false;
                        let chats = new MessagesController().getChatBoxList();
                        for (let i in chats) {
                            if (chats[i].id === message.from_id && !chats[i].minimize) {
                                await global.socket.markRead(message.from_id);
                            }
                            else if(chats[i].id === message.from_id) {
                                playSound = true;
                            }
                        }

                        if (!message.mute && (playSound || chats.length < 1)) {
                            audio.play();
                        }
                    }
                    await new MessagesController().chatList();
                });

                if (intervalVar) {
                    clearInterval(intervalVar);
                }
                intervalVar = setInterval(() => {
                    updateMessagesCount();
                }, 2000);
            }
        }
        else if(global.socket)
        {
            global.socket.disconnect();
        }
    }

    /** Socket Connection */    

    /*Chat Messages Count*/
    const [messagesCount, setMessagesCount] = React.useState(0);
    const [notificationCount, setNotificationCount] = React.useState(0);
    const updateMessagesCount = async () => {
        if (!loadingCounts) {
            loadingCounts = true;
            let msg = new MessagesController();
            let response = await msg.getTotalChatCount();
            if (response && response.status) {
                loadingCounts = false;
                msg.updateCount(response.count);
                msg.updateNotificationCount(response.notifications);
                setMessagesCount(msg.getMessageCount())
            }
        }
    }

    /*Chat Messages Count*/

    /* Chat Boxes */
    const [chatBoxes, setChatBoxes] = React.useState(
        props.chatBoxes ? props.chatBoxes : null
    );

    useEffect(() => {
        setChatBoxes(props.chatBoxes);
    }, [props.chatBoxes]);

    const renderChatBoxes = () => {
         
        let c =
            props.chatBoxes && props.chatBoxes.chats && props.chatBoxes.chats.length > 0
                ? props.chatBoxes.chats
                : [];
        
        let boxes = [];
        if (c && c.length > 0) {
            for (let i in c) {
                let chatBoxData = c[i];
                boxes.push(
                    <ChatboxSection
                        key={`chat-` + chatBoxData.mId}
                        to={chatBoxData}
                        minimize={
                            chatBoxData.minimize ? chatBoxData.minimize : false
                        }
                        count={
                            chatBoxData.count && chatBoxData.count > 0
                                ? chatBoxData.count
                                : 0
                        }
                    />
                );
            };
        }
        return boxes;
    };
    /* Chat Boxes */
    if (props.user && props.user.id && global.isLogin) {
        return (
            <>
                <Header />
                <section
                    className="home_section"
                    onScroll={onScroll}
                    ref={listInnerRef}
                >
                    <div className="container">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="home_area">
                                    <div className="row">
                                        <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12 pe-lg-2 pe-lg-0">
                                            <Sidebar />
                                        </div>
                                        <div className="col-xxl-9 col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">
                                            <main>
                                                {props.children}
                                                <Toaster />
                                                {
                                                    props.chatBoxes && props.chatBoxes.chats && props.chatBoxes.chats.length > 0
                                                    &&
                                                    <div className="chatbox_section">{renderChatBoxes()} </div>
                                                }
                                                <GalleryModal />
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        );
    }
    else {
        return (
            <div>
                <Login />
                {/* <Spinners /> */}
                <Toaster />
            </div>
        );
    }
};

const LayoutTwo2 = ({ children }) => {
    return (
        <div>
            {children}
            {/* <Spinners /> */}
            <Toaster />
        </div>
    );
};
const LayoutThree3 = ({ children }) => {
    return (
        <>
            <Header />

            <main>
                {children}
                {/* <Spinners /> */}
                <Toaster />
            </main>

            <Footer />
        </>
    );
};

const LayoutFour4 = ({ children }) => {
    return (
        <>
            <Header />
            <main>
                {children}
                {/* <Spinners /> */}
                <Toaster />
            </main>
        </>
    );
};

const LoginLayout1 = (props) => {
    if (props.user && props.user.id && global.isLogin) {
        return DefaultLayout1(props);
    }
    else {
        return LayoutTwo2(props);
    }
}

const mapStateToProps = (state) => ({
    user: state.UserReducer.user,
    messages: state.MessagesReducer.messages,
    chatBoxes: state.ChatBoxReducer.boxes,
});
export const DefaultLayout = connect(mapStateToProps)(DefaultLayout1);
export const LayoutTwo = connect(mapStateToProps)(LayoutTwo2);
export const LayoutThree = connect(mapStateToProps)(LayoutThree3);
export const LayoutFour = connect(mapStateToProps)(LayoutFour4);
export const LoginLayout = connect(mapStateToProps)(LoginLayout1);
