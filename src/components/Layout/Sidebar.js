import React, { useEffect, useState } from "react";
import { useLocation, withRouter } from "react-router-dom";

//components
import ChatSection from "./components/ChatSection";
import CommunitySection from "./components/CommunitySection";
import FooterSection from "./components/FooterSection";
import MenuSection from "./components/MenuSection";
import Suggestions from "../Home/components/Suggestions";
import useWindowDimensions from "../../helpers/windowHeight";

function Sidebar() {
    let location = useLocation();
    const [showChat, setShowChat] = useState(false);
    var { width } = useWindowDimensions();
    useEffect(() => {
        if (location.pathname === "/community") {
            if (width >= 768 && width <= 992) {
                setShowChat(false);
            } else {
                setShowChat(true);
            }
        } else {
            setShowChat(false);
        }
    }, [location.pathname]);
    return (
        <section className="profile_section sticky-top">
            <div className="left-side">
                <MenuSection />
                <div className="d-lg-block d-none">
                    <Suggestions />
                </div>
            </div>
        </section>
    );
}

export default withRouter(Sidebar);
