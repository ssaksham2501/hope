import React from "react";

//assets
import layout_bread from "../../assets/images/Layout/bread.png";

const Breadcrum = (props) => {
    return (
        <section className="breadcrumb_section">
            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="breadcrumb_area flexie">
                            <img src={layout_bread} alt="" />
                            <div className="content_bread">
                                <h1>{props.breadTitle}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Breadcrum;
