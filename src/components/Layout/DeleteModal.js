import React, { useEffect, useState } from "react";

import { Link, useHistory, withRouter } from "react-router-dom";

import { Button, Modal } from "react-bootstrap";

//assets
import photo from "../../assets/images/sidebar/photo.png";
import video from "../../assets/images/sidebar/video.png";
import audio from "../../assets/images/sidebar/audio.png";
import landing_callie from "../../assets/images/Landing/callie.png";
import AddPost from "../Home/components/AddPost";
import PostController from "../../controllers/post.controller";
import { Toast } from "../../helpers/Toaster";
import ProfileController from "../../controllers/profile.controller";

function DeleteModal(props) {
    let history = useHistory();
    const [error, setError] = useState(null);
    const [password, setPassword] = useState(``);
    const handleClose = () => props.setShow(false);

    const deleteAccount = async () => {
        if (password) {
            let response = await new ProfileController().deleteAccount(password);
            if (response && response.status) {
                history.push('/');
            } else {
                setError(response.error)
            }
        }
        else {
            setError('Password is required.')
        }
    };

    return (
        <div className="el">
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={true}
                onHide={handleClose}
                animation={true}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div className="modal_head">
                            {" "}
                            <h4>Are you sure ?</h4>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="pop_post">
                        <div className="head">
                            <p>
                                Deleted account can not be recover. All of your posts and information will be removed.
                            </p>
                        </div>
                        <div className="form-group head">
                            <input
                                type="password"
                                className="form-control"
                                placeholder="Enter your password."
                                value={password}
                                onInput={(e) => {
                                    setPassword(e.target.value);
                                }}
                            />
                            {error && <small className="text-pink">{error}</small>}
                        </div>
                        <div className="btnss">
                            <button
                                className="report red_bg text-white"
                                onClick={deleteAccount}
                            >Delete</button>
                            <button
                                className="cancel"
                                onClick={handleClose}
                            >Cancel</button>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default DeleteModal;

