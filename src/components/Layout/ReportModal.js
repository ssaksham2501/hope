import React, { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import MessagesController from "../../controllers/messages.controller";
import PostController from "../../controllers/post.controller";
import ProfileController from "../../controllers/profile.controller";

import { Toast } from "../../helpers/Toaster";

function ReportModale(props) {
    const [reports, setReports] = useState([]);
    const handleClose = () => props.setShow(false);

    useEffect(() => {
        getReportList();
    }, []);

    //Report List
    const getReportList = async () => {
        let response = await new PostController().reportList();
        if (response && response.status) {
            setReports(response.reports);
        } else {
            console.log(response.error);
        }
    };

    //Report On Post
    const reportPost = async (item) => {
        let response = await new PostController().reportPost(
            props.post,
            item.title
        );
        if (response && response.status) {
            new Toast().success(response.message);
            handleClose();
        } else {
            new Toast().success(response.error);
            console.log(response.error);
        }
    };

    //Report On Comment
    const reportComment = async (item) => {
        let response = await new PostController().reportComment(
            props.post,
            props.commentId,
            item.title
        );
        if (response && response.status) {
            new Toast().success(response.message);
            handleClose();
        } else {
            new Toast().success(response.error);
            console.log(response.error);
        }
    };

    //Report on User
    const reportUser = async (id, message) => {
        let response = await new ProfileController().reportUser({
            id,
            message
        });
        if (response && response.status) {
            new MessagesController().deleteChatBoxModal(id);
            await new MessagesController().chatList({page: 1});
            new Toast().success(response.message);
            handleClose();
        } else {
            new Toast().success(response.error);
            console.log(response.error);
        }
    }

    return (
        <div className="el">
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={props.show}
                onHide={handleClose}
                animation={true}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div className="modal_head">
                            {" "}
                            <h4>Report</h4>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="pop_post">
                        <div className="head">
                            <h4>Please Select a Problem</h4>
                            <p>
                                Lorem ipsum dolor sit amet consectetur,
                                adipisicing elit. Nulla iste laboriosam omnis
                                illo labore delectus
                            </p>
                        </div>
                        <div className="problems_list">
                            <ul>
                                {reports.map((item, index) => (
                                    <li key={`re-` + index} onClick={() => {
                                        if (props.userId)
                                            reportUser(props.userId, item.title);
                                        else if (props.commentId)
                                            reportComment(item)
                                        else
                                            reportPost(item)
                                    }}>
                                        {item.title}
                                        <i className="fal fa-link-angle-right"></i>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default withRouter(ReportModale);

