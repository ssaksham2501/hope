import React, { useState } from "react";

import { withRouter } from "react-router-dom";

import { Modal } from "react-bootstrap";

//assets
import AddPost from "../Home/components/AddPost";

function Modale(props) {
    const [show, setShow] = useState(false);
    const handleClose = () => props.setShow(false);
    const handleShow = () => setShow(true);

    return (
        <div className="el">
            <Modal
                aria-labelledby="contained-modal-title-vcenter"
                centered
                size="lg"
                show={props.show}
                onHide={handleClose}
                animation={true}
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div className="modal_head">
                            {" "}
                            <h4>Edit Post</h4>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="pop_post">
                        <AddPost
                            id={props.id}
                            setPost={() => {
                                props.setPost();
                            }}
                            editCommunityOption={
                                props.editCommunity
                                    ? props.editCommunity
                                    : false
                            }
                            updatePosts={(item) => props.updatePost(item)}
                            close={() => handleClose()}
                        />
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    );
}

export default withRouter(Modale);
