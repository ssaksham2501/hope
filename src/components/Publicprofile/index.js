import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import MessagesController from "../../controllers/messages.controller";
import ProfileController from "../../controllers/profile.controller";
import { uuId } from "../../helpers/General";
import { Toast } from "../../helpers/Toaster";
import Post from "../Home/components/Post";
import BlockModal from "../Layout/BlockModal";
import ChatSection from "../Layout/components/ChatSection";
import Nodata from "../Layout/components/Nodata";
import Profile1 from "../Profile/components/Profile1";
import cover from "../../assets/images/sidebar/Layer-5.jpg";
import Dropdown from "react-bootstrap/Dropdown";
import ReportModale from "../Layout/ReportModal";

const PublicProfile = (props) => {
    const [loader, setLoader] = useState(false);
    const [user, setUser] = useState();
    const [postList, setPostList] = useState([]);
    const [blockModal, setBlockModal] = useState(false);
    const [showChatRequest, setShowChatRequest] = useState(true);
    const [showReportModal, setShowReportModal] = useState(false);
    
    const [post, setPost] = useState({
        profile: "",
        lastId: "",
    });

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const getPostList = async () => {
        let response = await new ProfileController().postList(post);
        if (response && response.status) {
            if (post.lastId) {
                setPostList([...postList, ...response.posts]);
            } else {
                setPostList(response.posts);
            }
        }
        setLoader(false);
    };

    let param = useParams();
    useEffect(() => {
        window.scrollTo(0, 0);
        setPostList([]);
        if (param && param.slug) {
            setLoader(true);
            getProfile(param.slug);
        }
    }, [param.slug]);

    useEffect(() => {
        if (user && user.id && !user.is_blocked && post.profile) {
            setLoader(true);
            setPostList([]);
            getPostList();
        } else {
            setPostList([]);
        }
    }, [post.profile, user && user.is_blocked]);

    const getProfile = async (id) => {
        let response = await new ProfileController().getPublicProfile(id);
        if (response && response.status) {
            setPostList([]);
            setUser(response.user);
            setPost({ ...post, profile: response.user.id });
        }
        setLoader(false);
    };

    const [blockRequestLoading, setBlockRequestLoading] = React.useState(false);
    const blockUser = async () => {
        if (!blockRequestLoading) {
            setAcceptRequestLoading(true);
            setBlockModal(false);
            let response = await new ProfileController().blockUser(user.id);
            if (response && response.status) {
                setBlockRequestLoading(false);
                getProfile(param.slug);
                new MessagesController().deleteChatBoxModal(user.id);
                await new MessagesController().chatList({page: 1});
                new Toast().success(response.message);
            } else {
                setBlockRequestLoading(false);
                new Toast().error(response.message);
            }
        }
    };

    const [unblockRequestLoading, setUnblockRequestLoading] = React.useState(false);
    const unBlockUser = async (id) => {
        if (!unblockRequestLoading) {
            setUnblockRequestLoading(true);
            let response = await new ProfileController().unBlockUser(id);
            if (response && response.status) {
                setUnblockRequestLoading(false);
                getProfile(param.slug);
                new Toast().success(response.message);
            } else {
                setUnblockRequestLoading(false);
                console.log(response.error);
            }
        }
    };

    const openChat = () => {
        new MessagesController().setChatBoxModal({
            mId: uuId(),
            id: user.id,
            name: user.first_name.concat(" " + user.last_name),
            image: user.image,
            username: user.username,
            responseTimings: null,
        });
    };

    const [acceptRequestLoading, setAcceptRequestLoading] = React.useState(false);
    const acceptRequest = async () => {
        if (!acceptRequestLoading) {
            setAcceptRequestLoading(true);
            let response = await new MessagesController().acceptChatRequest(user.username);
            if (response && response.status) {
                setAcceptRequestLoading(false);
                setShowChatRequest(false);
                openChat();
            }
            else {
                setAcceptRequestLoading(false);
                new Toast().error(response.message);
            }
        }
    }

    const [deleteRequestLoading, setDeleteRequestLoading] = React.useState(false);
    const deleteRequest = async () => {
        if (!deleteRequestLoading) {
            setDeleteRequestLoading(true);
            let response = await new MessagesController().deleteChatRequest(user.username);
            if (response && response.status) {
                setDeleteRequestLoading(false);
                setShowChatRequest(false);
            }
            else {
                setAcceptRequestLoading(false);
                new Toast().error(response.message);
            }
        }
    }

    return (
        <>
            {user && user.id ? (
                <>
                    <Profile1 user={user} />
                    <div className="publicprofilebio text-center">
                        <h4>
                            {user && user.first_name
                                ? user.first_name +
                                  " " +
                                  (user.last_name ? user.last_name : "")
                                : ""}
                        </h4>
                        <p>{user.bio ? user.bio : ""}</p>

                        
                        {user.id != props.user.id && user.received_request && !user.send_request && showChatRequest
                            ?
                            <div className="chat-bt">
                                <button
                                    className="chat lav_bg"
                                    onClick={() => acceptRequest()}
                                >
                                    {
                                        acceptRequestLoading
                                        &&
                                        <i className="fad fa-spinner-third fa-spin"></i>
                                    }
                                     Confirm
                                </button>
                                <button
                                    className="chat block"
                                    onClick={() => deleteRequest()}
                                >
                                    {
                                        deleteRequestLoading
                                        &&
                                        <i className="fad fa-spinner-third fa-spin"></i>
                                    } Delete
                                </button>
                            </div>
                            :
                                (user.id !== props.user.id
                                    &&
                                    <div className="chat-bt">
                                        <button
                                            className={`chat lav_bg` + (user.is_blocked ? ` disabled` : ``)}
                                            onClick={() => {
                                                if (!user.is_blocked)
                                                    openChat();
                                            }}
                                        >
                                            Chat
                                        </button>
                                        <Dropdown>
                                            <Dropdown.Toggle id="dropdown-basic1">
                                                <i className="fal fa-ellipsis-h"></i>
                                            </Dropdown.Toggle>
                                            <Dropdown.Menu>
                                                <Dropdown.Item
                                                    className={`red`}
                                                    onClick={() => {
                                                        setShowReportModal(
                                                            true
                                                        );
                                                    }}
                                                >
                                                        <i className="fal fa-exclamation-circle red"></i>
                                                        Report
                                                </Dropdown.Item>
                                                {user.is_blocked ? (
                                                    <Dropdown.Item
                                                        className={`red`}
                                                        onClick={() => unBlockUser(user.id)}
                                                    >
                                                        {
                                                            unblockRequestLoading
                                                            ?
                                                            <i className="fad fa-spinner-third fa-spin"></i>
                                                            :
                                                            <i className="fal fa-exclamation-circle red"></i>
                                                        } Unblock
                                                    </Dropdown.Item>
                                                ) : (
                                                    <Dropdown.Item
                                                        className={`red`}
                                                        onClick={() => setBlockModal(true)}
                                                    >
                                                        {
                                                            blockRequestLoading
                                                            ?
                                                            <i className="fad fa-spinner-third fa-spin"></i>
                                                            :
                                                            <i className="fal fa-exclamation-circle red"></i>
                                                        } Block
                                                    </Dropdown.Item>
                                                )}
                                                
                                            </Dropdown.Menu>
                                        </Dropdown>
                                        
                                    </div>)
                        }
                    </div>
                    <div className="profile_under">
                        <div className="row">
                            <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                <div className="content">
                                    <div className="show">
                                        {postList.length > 0 ? (
                                            <Post
                                                postList={postList}
                                                showAddPost={(value) =>
                                                    console.log(value)
                                                }
                                                updatePosts={console.log("gg")}
                                            />
                                        ) : !loader ? (
                                            <Nodata />
                                        ) : null}
                                    </div>
                                </div>
                            </div>
                            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div className="d-lg-block d-none">
                                    <ChatSection />
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
            {blockModal && (
                <BlockModal
                    show={blockModal}
                    close={() => setBlockModal(false)}
                    yes={() => blockUser()}
                />
            )}
            {user && user.id && showReportModal ? (
                <ReportModale
                    show={showReportModal}
                    userId={user.id}
                    post={null}
                    commentId={null}
                    setShow={() => setShowReportModal(false)}
                />
            ) : null}
        </>
    );
};
const mapState = (state) => {
    return {
        user: state.UserReducer.user,
    };
};
const mapDispatch = {};

export default connect(mapState, mapDispatch)(PublicProfile);
