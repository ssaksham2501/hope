import React, { useState, useEffect } from "react";
import CmsController from "../../../controllers/cms.controller";

//assets
import landing_logo from "../../../assets/images/Landing/logo.png";
import landing_birb from "../../../assets/images/Landing/birb.png";
import landing_birb2 from "../../../assets/images/Landing/birb_2.png";
import about_people from "../../../assets/images/About/people.png";
import about_line from "../../../assets/images/About/line.png";
import about_line2 from "../../../assets/images/About/line_2.png";

function Mains() {
    const [about, setAbout] = useState("");
    const [mission, setMission] = useState("");
    const [vision, setVision] = useState("");

    useEffect(() => {
        window.scrollTo(0, 0);
        getAbout();
    }, []);

    // about  --start

    const getAbout = async () => {
        let response = await new CmsController().dataAbout();
        if (response && response.status) {
            setAbout(response.about);
            setMission(response.mission);
            setVision(response.vision);
        }
    };
    // about  --end

    //parse
    const htmlAbout = () => {
        return { __html: `${about.description}` };
    };
    const htmlMission = () => {
        return { __html: `${mission.description}` };
    };
    const htmlVision = () => {
        return { __html: `${vision.description}` };
    };

    return (
        <section className="mains_section">
            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="mains_area">
                            <div className="content">
                                <div className="sub_section">
                                    <div className="imgwrap ">
                                        <img src={landing_logo} alt="" />
                                        <div className="float_img ">
                                            <img src={landing_birb2} alt="" />
                                        </div>
                                    </div>
                                    <p
                                        dangerouslySetInnerHTML={htmlAbout()}
                                    ></p>
                                </div>
                                <div className="sub_section">
                                    <h1 className="head_one head">
                                        our mission
                                        <img src={about_line} alt="" />
                                    </h1>
                                    <p
                                        dangerouslySetInnerHTML={htmlMission()}
                                    ></p>
                                </div>
                                <div className="sub_section">
                                    <h1 className="head_two head">
                                        our vision
                                        <img src={about_line2} alt="" />
                                        <div className="float_img float_img_1">
                                            <img src={landing_birb} alt="" />
                                        </div>
                                    </h1>
                                    <p
                                        dangerouslySetInnerHTML={htmlVision()}
                                    ></p>
                                </div>
                            </div>
                            <div className="imgwrap main_img">
                                <img src={about_people} alt="..." />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Mains;
