import React from "react";

//components
import Breadcrumb from "../Layout/Breadcrumb";
import Mains from "./components/Mains";

function About() {
    return (
        <div>
            <Breadcrumb breadTitle="About Us" />
            <Mains />
        </div>
    );
}

export default About;
