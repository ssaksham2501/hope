import React, { useEffect, useState } from "react";
import CommunityList from "./components/CommunityList";
import CreateCommunity from "../CommunitiesDetail/components/CreateCommunity";
import { connect } from "react-redux";
import { setCommunityId, showCommunity } from "../../redux/actions/user";
import ChatSection from "../Layout/components/ChatSection";

const Communities = (props) => {
    const [edit, setEditScreen] = useState(false);    

    useEffect(() => {
        // props.showCommunity(false);
        // props.setCommunityId(null);
    }, []);

    const showComunityHandle = (value) => {
        props.showCommunity(value);
        props.setCommunityId(null);
    };

    const editCommunity = (id) => {
        props.showCommunity(true);
        setEditScreen(true);
        props.setCommunityId(id);
    };

    return (
        <section className="settings_section">
            <div className="row">
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div className="row">
                        <div className="col-xxl-9 col-xl-9 col-lg-10 col-md-12 col-sm-12 col-12">
                            {props.community ? (
                                <CreateCommunity
                                    id={props.id}
                                    edit={edit}
                                    setEdit={() => setEditScreen(false)}
                                    show={(value) => showComunityHandle(value)}
                                />
                            ) : null}
                            {!props.community ? (
                                <CommunityList
                                    show={(value) => showComunityHandle(value)}
                                    user={props.user}
                                    edit={(id) => editCommunity(id)}
                                />
                            ) : null}
                        </div>
                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapState = (state) => {
    return {
        community: state.ShowCommunityReducer.show,
        user: state.UserReducer.user,
        id: state.CommunityDetailReducer.id,
    };
};
const mapDispatch = (dispatch) => {
    return {
        showCommunity: async (info) => dispatch(showCommunity(info)),
        setCommunityId: async (info) => dispatch(setCommunityId(info)),
    };
};

export default connect(mapState, mapDispatch)(Communities);
