import React, { Component, useRef, useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { renderImage } from "../../../helpers/General";
import landing_callie from "../../../assets/images/Landing/callie.png";
import users from "../../../assets/images/Communities/users.png";
import Validation from "../../../helpers/vaildation";
import CommunityController from "../../../controllers/community.controller";

const CommunityForm = (props) => {
    const avtarInput = useRef(null);
    const bannerInput = useRef(null);
    let defaultValues = {
        title: "",
        description: "",
        image: null,
        banner: null,
        invites: null,
    };
    const [values, setValues] = useState(defaultValues);
    const [isError, setError] = useState({
        title: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        description: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        image: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        banner: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        invites: {
            rules: [],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);
    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        if (!value || !value.trim()) {
            setValues({ ...values, [field]: "" });
        } else {
            setValues({ ...values, [field]: value });
        }
    };

    //Create Community
    const createCommunity = async () => {
        let validation = new Validation(isError);
        let isValid = await validation.isFormValid(values);
        if (isValid && !isValid.haveError) {
            let response = await new CommunityController().createCommunity(
                values
            );
            if (response && response.status) {
                setValues(defaultValues);
            } else {
                console.log(response.error);
            }
        } else {
            setError({ ...isValid.errors });
        }
    };

    //image upload
    const imageUpload = async (value, type) => {
        let response = await new CommunityController().uploadProfileImage(
            value
        );
        if (response && response.status) {
            if (type === "avtar") {
                handleStates("image", response.path);
            } else {
                handleStates("banner", response.path);
            }
        } else {
            console.log("error here", response);
            // setErrMsg(response.error);
        }
    };
    return (
        <div>
            <div className="community_main_area">
                <div className="community_head ">
                    <h2>
                        <i
                            className="fas fa-arrow-left"
                            onClick={() => props.show(false)}
                        ></i>
                        Community
                    </h2>
                </div>
                <div className="create_community">
                    <div className="created_inner">
                        <div className="created_head">
                            <h3>Create community</h3>
                        </div>
                        <div className="created_formed">
                            <Form>
                                <Form.Group
                                    className="groper"
                                    controlId="formBasicname"
                                    validated={true}
                                >
                                    <div className="icon">
                                        <i className="fal fa-user"></i>{" "} 
                                    </div>
                                    <div className="filler">
                                        <Form.Label>Community name </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter community name "
                                            onChange={(e) => {
                                                handleStates(
                                                    "title",
                                                    e.target.value
                                                );
                                            }}
                                            value={values.title}
                                        />
                                        {!isError.title.isValid ? (
                                            <p className="valid-helper">
                                                {isError.title.message}
                                            </p>
                                        ) : null}
                                    </div>
                                </Form.Group>

                                <Form.Group
                                    className="groper"
                                    controlId="formBasicabout"
                                >
                                    <div className="icon">
                                        <i className="fal fa-exclamation-circle"></i>{" "}
                                    </div>
                                    <div className="filler">
                                        <Form.Label> About </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="About community"
                                            onChange={(e) => {
                                                handleStates(
                                                    "description",
                                                    e.target.value
                                                );
                                            }}
                                            value={values.description}
                                        />
                                        {!isError.description.isValid ? (
                                            <p className="valid-helper">
                                                {isError.description.message}
                                            </p>
                                        ) : null}
                                    </div>
                                </Form.Group>

                                <Form.Group
                                    className="groper"
                                    controlId="formBasicprofile"
                                >
                                    <div className="icon">
                                        <i className="far fa-image-polaroid"></i>{" "}
                                    </div>
                                    <div className="filler">
                                        <Form.Label>Profile </Form.Label>
                                        <div className="profile_img">
                                            <img
                                                src={renderImage(values.image)}
                                            />
                                            <a
                                                onClick={() =>
                                                    avtarInput.current.click()
                                                }
                                            >
                                                <i className="far fa-camera"></i>
                                            </a>
                                            <input
                                                ref={avtarInput}
                                                id="photo"
                                                type="file"
                                                onChange={(e) => {
                                                    imageUpload(
                                                        e.target.files[0],
                                                        "avtar"
                                                    );
                                                }}
                                                style={{ display: "none" }}
                                                accept="image/*"
                                            />
                                        </div>
                                        {!isError.image.isValid ? (
                                            <p className="valid-helper">
                                                {isError.image.message}
                                            </p>
                                        ) : null}
                                    </div>
                                </Form.Group>

                                <Form.Group
                                    className="groper"
                                    controlId="formBasiccover"
                                >
                                    <div className="icon">
                                        <i className="far fa-image-polaroid"></i>{" "}
                                    </div>
                                    <div className="filler">
                                        <Form.Label> Cover photo </Form.Label>
                                        <div className="cover_img">
                                            <img
                                                src={renderImage(values.banner)}
                                            />
                                            <a
                                                onClick={() =>
                                                    bannerInput.current.click()
                                                }
                                            >
                                                <i className="far fa-camera"></i>
                                            </a>
                                            <input
                                                ref={bannerInput}
                                                id="photo"
                                                type="file"
                                                onChange={(e) => {
                                                    imageUpload(
                                                        e.target.files[0],
                                                        "banner"
                                                    );
                                                }}
                                                style={{ display: "none" }}
                                                accept="image/*"
                                            />
                                        </div>
                                        {isError.banner.message ? (
                                            <p className="valid-helper">
                                                {isError.banner.message}
                                            </p>
                                        ) : null}
                                    </div>
                                </Form.Group>

                                <Form.Group
                                    className="groper"
                                    controlId="formBasicinvite"
                                >
                                    <div className="icon">
                                        {" "}
                                        <img src={users} />{" "}
                                    </div>
                                    <div className="filler">
                                        <Form.Label>Invite friends </Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter name or username "
                                        />
                                    </div>
                                </Form.Group>
                                <div className="submiter">
                                    <Button
                                        variant="primary"
                                        className="lav_bg"
                                        onClick={createCommunity}
                                    >
                                        Create
                                    </Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CommunityForm;
