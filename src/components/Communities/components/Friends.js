import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

//assets
import communities_musk from "../../../assets/images/Communities/musk.png";
import CommunityController from "../../../controllers/community.controller";
import MessagesController from "../../../controllers/messages.controller";
import ProfileController from "../../../controllers/profile.controller";
import { renderImage, uuId } from "../../../helpers/General";
import { Toast } from "../../../helpers/Toaster";
import Nodata from "../../Layout/components/Nodata";

const Friends = (props) => {
    const [list, setUsers] = useState([]);

    useEffect(() => {
        if (props && props.listProps.group == 'unblock') {
            getBlockedList();
        } else {
            getCommunityList();
        }
    }, []);

    const getBlockedList = async () => {
        let response = await new ProfileController().blockedUsers();
        if (response && response.status) {
            setUsers(response.users);
        }
    };

    const getCommunityList = async () => {
        let response = await new CommunityController().followedCommunityList(
            props.group
        );
        if (response && response.status) {
            setUsers(response.users);
        }
    };

    const unBlockUser = async (id, i) => {
        let response = await new ProfileController().unBlockUser(id);
        if (response && response.status) {
            await new MessagesController().chatList();
            let array = [...list];
            array[i].chat = true;
            setUsers([...array]);
        } else {
            console.log(response.error);
        }
    };

    const followCommunity = async (item, i) => {
        let response = await new CommunityController().followCommunity(item.group_id);
        if (response && response.status) {
            let array = [...list];
            array[i].follow = false;
            setUsers([...array]);
        } else {
            new Toast().error(response.message);
        }
    };


    const unFollowCommunity = async (item, i) => {
        let response = await new CommunityController().unFollowCommunity(
            item.group_id,
            item.user_id
        );
        if (response && response.status) {
            let array = [...list];
            array[i].follow = true;
            setUsers([...array]);
            // new Toast().success(response.message);
        } else {
            new Toast().error(response.message);
        }
    };

    const openChat = async (item) => {
        new MessagesController().setChatBoxModal({
            mId: uuId(),
            id: item.blocked_id,
            name: item.user_first_name.concat(" " + item.user_last_name),
            image: item.user_image,
            username: item.username,
            responseTimings: null,
            blocked_user: null,
        });
    };

    return (
        <>
            <h3>
                {list.length > 0 ? props.listProps.title + `(${list.length})` : ``}
            </h3>
            {list.length > 0 ?
                <>   
                    <div className="friends_list flexie">
                        <div className="row w-100 ">
                            {list.map((item, key) => {
                                return (
                                    <div
                                        className={
                                            props.listProps.col
                                                ? `col-xl-${props.listProps.col} col-lg-${props.listProps.col} col-md-4 col-sm-6 col-6`
                                                : "col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"
                                        }
                                        key={key}
                                    >
                                        <div className="friend_box flexie" key={key}>
                                            <div className="imgwrap">
                                                <Link
                                                    to={`/profile/${item.username}`}
                                                >
                                                    <img
                                                        src={renderImage(
                                                            item.user_image
                                                        )}
                                                        alt="..."
                                                    />
                                                </Link>
                                            </div>

                                            <p className="name">
                                                <Link
                                                    to={`/profile/${item.username}`}
                                                >
                                                    {item.user_first_name
                                                        ? item.user_first_name
                                                        : "" + item.user_last_name
                                                            ? " " + item.user_last_name
                                                            : ""}
                                                </Link>
                                            </p>
                                            {
                                                props.listProps.group === 'unblock'
                                                &&
                                                <button
                                                    className={`main_btn ` + (item.chat ? `lav_bg` : `pink_bg`)}
                                                    onClick={() => {
                                                        if (item.chat) {
                                                            openChat(item);
                                                        }
                                                        else {
                                                            unBlockUser(
                                                                item.blocked_id,
                                                                key
                                                            );
                                                        }
                                                    }}
                                                >
                                                    {item.chat ? 'Chat' : `Unblock`}
                                                </button>
                                            }
                                            {
                                                props.listProps.group === 'unfollow'
                                                &&
                                                <button
                                                    className={`main_btn ` + (item.chat ? `lav_bg` : `pink_bg`)}
                                                    onClick={() => {
                                                        if (item.follow) {
                                                            followCommunity(item, key);
                                                        }
                                                        else {
                                                            unFollowCommunity(item, key);
                                                        }
                                                    }}
                                                >
                                                    {item.follow ? 'Follow' : `Unfollow`}
                                                </button>
                                            }
                                        
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </>
                :
                <div className="friends_list flexie">
                    <Nodata />
                </div>

            }
        </>
    );
};

export default Friends;
