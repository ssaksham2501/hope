import React, { useEffect } from "react";
import { useState } from "react";
import { Tab, Nav } from "react-bootstrap";
import { Link, useHistory, useLocation } from "react-router-dom";
import CommunityController from "../../../controllers/community.controller";
import Communitytags from "./CommunityTags";
import useWindowDimensions from "../../../helpers/windowHeight";
import AuthController from "../../../controllers/auth.controller";

import { Toast } from "../../../helpers/Toaster";
import Nodata from "../../Layout/components/Nodata";
import ConfirmationModal from "../../Layout/components/ConfirmationModal";

const CommunityList = (props) => {
    const history = useHistory();
    const location = useLocation();
    const [commValue, setCommValue] = useState(location.hash && location.hash === '#my-communities' ? 1 : (location.hash && location.hash === '#sugessions' ? 2 : 0));
    const [list, setCommunities] = useState([]);
    const [loader, setLoader] = useState(false);
    const [deleteCommunityConfirm, setDeleteCommunityConfirm] = useState(null);
    
    //My Communities
    const getCommunities = async () => {
        let response = await new CommunityController().communityList({
            my_communities: 1,
        });
        if (response && response.status) {
            setCommunities(response.communities);
        } else {
            setCommunities([]);
        }
        setLoader(false);
    };

    //Communities that i followed
    const getFollowedCommunities = async () => {
        let response = await new CommunityController().communityList({
            follow: 1,
        });
        if (response && response.status) {
            setCommunities(response.communities);
        } else {
            setCommunities([]);
        }
        setLoader(false);
    };

    //All Communities
    const getAllCommunities = async () => {
        let response = await new CommunityController().suggestedCommunities();
        if (response && response.status) {
            setCommunities(response.communities);
        } else {
            setCommunities([]);
        }
        setLoader(false);
    };

    useEffect(() => {
        window.scrollTo(0, 0);
        setLoader(true);
        if (location.hash && location.hash === '#my-communities') {
            setCommValue(1);
            getCommunities();
        }
        else if (location.hash && location.hash === '#sugessions') {
            setCommValue(2);
            getAllCommunities();
        }
        else{
            setCommValue(0);
            getFollowedCommunities();
        }
    }, [location]);

    const { height, width } = useWindowDimensions();
    useEffect(() => {
        scrollerHeight();
    }, [height]);

    const scrollerHeight = async () => {
        let commnt = document.getElementById("commnt");
        let header = await new AuthController().getHeaderHeight();
        let commntarea = document.getElementById("commntarea");
        let final = height - (header + commnt.offsetHeight);
        commntarea.style.height = `${final}px`;
    };

    const deleteCommunity = async (id, i) => {
        setDeleteCommunityConfirm({id, i})
    };

    const unFollowCommunity = async (id, i) => {
        let response = await new CommunityController().unFollowCommunity(id);
        if (response && response.status) {
            new Toast().success(response.message);
            let array = [...list];
            array[i].follow = true;
            setCommunities(array);
        } else {
            new Toast().error(response.message);
        }
    };

    const followCommunity = async (id, i) => {
        let response = await new CommunityController().followCommunity(id);
        if (response && response.status) {
            new Toast().success(response.message);
            let array = [...list];
            array[i].follow = false;
            setCommunities(array);
        } else {
            new Toast().error(response.message);
        }
    };

    return (
        <>
        <div className="community_main_area settings_main_area">
            <div className="community_head " id="commnt">
                <h2>
                    Community{" "}
                    <a onClick={() => props.show(true)}>
                        <i className="fal fa-plus el"></i>
                    </a>
                </h2>

                <Tab.Container
                    className="tabs_area"
                    id="left-tabs-example"
                    activeKey={commValue}
                    defaultActiveKey={0}
                >
                    <Nav variant="pills" className="flex-row">
                        <Nav.Item>
                            <Nav.Link
                                className=" "
                                eventKey={0}
                                onClick={() => {
                                    setCommValue(0);
                                    setLoader(true);
                                    getFollowedCommunities();
                                    history.push('/community');
                                }}
                            >
                                Communities
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link
                                className=""
                                eventKey={1}
                                onClick={() => {
                                    setCommValue(1);
                                    setLoader(true);
                                    getCommunities();
                                    history.push('/community#my-communities');
                                }}
                            >
                                My communities
                            </Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link
                                className=" "
                                eventKey={2}
                                onClick={() => {
                                    setCommValue(2);
                                    setLoader(true);
                                    getAllCommunities();
                                    history.push('/community#sugessions');
                                }}
                            >
                                Suggested
                            </Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Tab.Container>
            </div>
            <div className="community_area" id="commntarea">
                <div className="sub_section el">
                    <div className="content_head">
                        {list.length > 0 ? (
                            <Communitytags
                                commValue={commValue}
                                list={list}
                                edit={(id) => props.edit(id)}
                                deleteCommunity={(id, i) =>
                                    deleteCommunity(id, i)
                                }
                                unFollow={(id, i) => unFollowCommunity(id, i)}
                                follow={(id, i) => followCommunity(id, i)}
                            />
                        ) : !loader ? (
                            <Nodata />
                        ) : null}
                    </div>
                </div>
            </div>
        </div>
        {
            deleteCommunityConfirm
            &&
            <ConfirmationModal
                title={`Are you sure to delete this community?`}
                description={`Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nulla iste labor iosam omnisillo labore delectus`}
                show={deleteCommunityConfirm ? true : false}
                confirm={async () => {
                    let { id, i } = deleteCommunityConfirm;
                    let response = await new CommunityController().deleteCommunity(id);
                    if (response && response.status) {
                        new Toast().success(response.message);
                        let array = [...list];
                        if (i != -1) {
                            array.splice(i, 1);
                        }
                        setCommunities(array);
                        setDeleteCommunityConfirm(null);
                    }
                }}
                cancel={() => {
                    setDeleteCommunityConfirm(null);
                }}
            />
        }
        </>
    );
};

export default CommunityList;
