import React from "react";
import { Link } from "react-router-dom";
import { renderImage } from "../../../helpers/General";
const Communitytags = (props) => {
    return (
        <div className="friends_list flexie">
            <div className="row w-100 ">
                {props.list.map((item, key) => (
                    <div
                        className={
                            props.fourCols
                                ? `col-xl-${props.fourCols} col-lg-${props.fourCols} col-md-4 col-sm-6 col-6`
                                : "col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6"
                        }
                        key={key}
                    >
                        <div className="friend_box flexie" key={key}>
                            <Link to={`/community-detail/${item.slug}`}>
                                <div className="imgwrap">
                                    <img
                                        src={renderImage(item.image)}
                                        alt="..."
                                    />
                                </div>
                            </Link>

                            <p className="name"><Link to={`/community-detail/${item.slug}`}>{item.title}</Link></p>
                            {props.commValue == 0 && (
                                <>
                                    {
                                        item.follow
                                        ?
                                            <button
                                                className="main_btn pink_bg "
                                                onClick={() => props.follow(item.id, key)}
                                            >
                                                Follow
                                            </button>
                                        :
                                            <button
                                                className="main_btn lav_bg follow_btn"
                                                onClick={() => props.unFollow(item.id, key)}
                                            >
                                                Unfollow
                                            </button>
                                    }
                                </>
                            )}
                            {props.commValue == 2 && (
                                <>
                                {
                                    (typeof item.follow == 'undefined' || item.follow)
                                    ?
                                        <button
                                            className="main_btn pink_bg"
                                            onClick={() => props.follow(item.id, key)}
                                        >
                                            Follow
                                        </button>
                                    :
                                        <button
                                            className="main_btn lav_bg follow_btn"
                                            onClick={() => props.unFollow(item.id, key)}
                                        >
                                            Unfollow
                                        </button>
                                }
                                </>
                            )}

                            {props.commValue == 1 && (
                                <div className="edit_btn_set flexie">
                                    <button
                                        className="main_btn lav_bg"
                                        onClick={() => props.edit(item.slug)}
                                    >
                                        Edit
                                    </button>
                                    <button
                                        className="main_btn border_btn"
                                        onClick={() =>
                                            props.deleteCommunity(item.id, key)
                                        }
                                    >
                                        Delete
                                    </button>
                                </div>
                            )}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Communitytags;
