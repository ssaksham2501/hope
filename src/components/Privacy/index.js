import React from "react";

//components
import Breadcrumb from "../Layout/Breadcrumb";
import Mains from "./components/Mains";

function Privacy() {
    return (
        <div>
            <Breadcrumb breadTitle="Privacy Policy" />
            <Mains />
        </div>
    );
}

export default Privacy;
