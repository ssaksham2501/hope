import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";

//helpers
import CmsController from "../../../controllers/cms.controller";

//assets
import landing_birb2 from "../../../assets/images/Landing/birb_2.png";

function Mains() {
    const params = useLocation();
    let slug = params.pathname.split("/");
    slug = slug.slice(-1);

    const [detail, setDetail] = useState([]);

    useEffect(() => {
        window.scrollTo(0, 0);
        getDetail();
    }, [params]);

    const getDetail = async () => {
        let response = await new CmsController().dataDetail(
            slug[slug.length - 1]
        );
        if (response && response.status) {
            setDetail(response.page.description);
        }
    };
    //parse
    const htmlDetail = () => {
        return { __html: `${detail}` };
    };
    return (
        <section className="mains_section static_section">
            <div className="float_img">
                <img src={landing_birb2} alt="..." />
            </div>
            <div className="float_img float_img_1">
                <img src={landing_birb2} alt="" />
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="mains_area static_area">
                            <div className="static_content content">
                                <div
                                    className="sub_section"
                                    dangerouslySetInnerHTML={htmlDetail()}
                                ></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Mains;
