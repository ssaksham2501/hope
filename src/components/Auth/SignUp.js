import React from "react";

//assets
import landing_signup from "../../assets/images/SignUp/signup.png";

//components
import Sign from "./components/Sign";

const SignUp = () => {
    return (
        <section className="signup_section">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="signup_area">
                            <div className="row row-no-padding ">
                                <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                                    <div className="left_area">
                                        <img src={landing_signup} alt="" />
                                    </div>
                                </div>
                                <div className="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                                    <Sign />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default SignUp;
