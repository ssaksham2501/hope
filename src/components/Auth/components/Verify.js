import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";

//helpers
import AuthController from "../../../controllers/auth.controller";

//assets
import landing_signup from "../../../assets/images/SignUp/signup.png";
import landing_404bg from "../../../assets/images/Landing/errbg.png";
import landing_404txt from "../../../assets/images/Landing/errtxt.png";
const Verify = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [isVerified, setIsVerified] = useState(false);
    const [verifyMsg, setVerifyMsg] = useState("");
    const params = useLocation();

    let slug = params.pathname.split("/");
    slug = slug.slice(-1);

    React.useEffect(() => {
        verify_token();
    }, [params]);
    
    const verify_token = async () => {
        if (slug && slug[slug.length - 1]) {
            let response = await new AuthController().verify_token(
                slug[slug.length - 1]
            );

            if (response && response.status) {
                setIsLoading(false);
                setIsVerified(true);
            }
            if (response) {
                setIsLoading(false);
            }

            setVerifyMsg(response.message);
        }
    };
    return (
        <section className="signup_section verify_section">
            {isLoading ? (
                <i className="fad fa-spinner-third fa-spin center-load"></i>
            ) : (
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            {isVerified ? (
                                <div className="signup_area verify_area">
                                    <div className="row row-no-padding ">
                                        <div className="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div className="left_area">
                                                <img
                                                    src={landing_signup}
                                                    alt=""
                                                />
                                            </div>
                                        </div>
                                        <div className="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div className=" auth_area sign_area sign_success flexie">
                                                <div className="content">
                                                    <h1>
                                                        Verification Successful
                                                    </h1>
                                                </div>
                                                <div className="shadow_box success_area">
                                                    <div className="success_msg flexie">
                                                        <i className="fad fa-badge-check"></i>
                                                        <p className="main_txt">
                                                            {verifyMsg}
                                                        </p>
                                                        <p className="minilink">
                                                            You can now&nbsp;
                                                            <Link
                                                                className="minipink"
                                                                to="/"
                                                            >
                                                                Log in
                                                            </Link>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                <div className="signup_area verify_area">
                                    <div className="row row-no-padding ">
                                        <div className="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div className="left_area">
                                                <img
                                                    src={landing_signup}
                                                    alt=""
                                                />
                                            </div>
                                        </div>
                                        <div className="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div className=" auth_area sign_area sign_success flexie">
                                                <div className="content">
                                                    <h1>
                                                        Verification Failed
                                                    </h1>
                                                </div>
                                                <div className="shadow_box success_area">
                                                    <div className="success_msg flexie">
                                                        <i className="fad fa-badge-check far fa-exclamation-triangle"></i>
                                                        <p className="main_txt">
                                                            Link is expired or already used.
                                                        </p>
                                                        <p className="minilink">
                                                            <Link
                                                                className="minipink"
                                                                to="/"
                                                            >
                                                                Back
                                                            </Link>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            )}
        </section>
    );
};

export default Verify;
