import React, { useState, useEffect } from "react";

//helpers
import AuthController from "../../../controllers/auth.controller";
import Validation from "../../../helpers/vaildation";

const Forgot = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    //validations --start
    const [isError, setError] = useState({
        email: {
            rules: ["required", "email"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        email: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        setValues(defaultValues);
    }, []);

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };
    //validations --end

    //forgot password --start
    const forgotPass = async () => {
        if (isLoading === false) {
            setErrMsg(false);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new AuthController().forgot_pass(values);
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    setErrMsg(response.message);
                    props.isOtp(true);
                    props.isOtpResponse(response);
                } else {
                    setErrMsg(response.error);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //forgot password --end
    return (
        <div className=" auth_area forgot_area flexie">
            <h1>Forgot password</h1>
            <p>
                Enter your registered email we'll send you a link to reset your
                password
            </p>
            {errMsg ? (
                <div className="content">
                    <p className="err_msg">{errMsg}</p>
                </div>
            ) : null}
            <form
                className="form_area"
                onSubmit={(e) => {
                    e.preventDefault();
                    forgotPass();
                }}
            >
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <label
                            className={
                                !isError.email.isValid
                                    ? "valid-box form_controls"
                                    : "form_controls"
                            }
                            htmlFor="email"
                        >
                            <input
                                type="email"
                                placeholder="Email"
                                onChange={(e) => {
                                    handleStates("email", e.target.value);
                                }}
                                value={values.email ? values.email : ``}
                            />
                            {!isError.email.isValid ? (
                                <p className="valid-helper">
                                    {isError.email.message}
                                </p>
                            ) : null}
                        </label>
                    </div>
                </div>

                <div className="action_area">
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <button
                                className="main_btn lav_bg"
                                onClick={() => {
                                    forgotPass();
                                }}
                            >
                                Submit
                                {isLoading ? (
                                    <i className="fad fa-spinner-third fa-spin"></i>
                                ) : null}
                            </button>
                        </div>
                        <p className="dividor dividor-alt"></p>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <button
                                className="main_btn "
                                onClick={() => {
                                    props.isForgotPass(false);
                                }}
                            >
                                <i className="fad fa-long-arrow-left back_icon"></i>
                                Back
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default Forgot;
