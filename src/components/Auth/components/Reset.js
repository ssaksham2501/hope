import React, { useState, useEffect } from "react";

//helpers
import Validation from "../../../helpers/vaildation";
import AuthController from "../../../controllers/auth.controller";

const Reset = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    //password icon
    const [showPass1, setShowPass1] = useState(false);
    const [showPass2, setShowPass2] = useState(false);
    //validations  -- start
    const [isError, setError] = useState({
        password: {
            rules: ["required", "password"],
            isValid: true,
            message: "",
        },
        password2: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);
    let defaultValues = {
        otp: null,
        token: null,
        password: null,
        password2: null,
    };
    const [values, setValues] = useState(defaultValues);
    useEffect(() => {
        window.scrollTo(0, 0);
        setValues({
            ...values,
            otp: props.resetResponse,
            token: props.otpResponse.token,
        });
    }, []);
    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };
    //validations  -- end

    //sign up  --start
    const reestPass = async () => {
        if (values.password !== values.password2) {
            setErrMsg("Confirm password doesn't match the entered password.");
        } else if (isLoading === false) {
            setErrMsg(false);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new AuthController().reset_pass(values);
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    setIsSuccess(true);
                    setErrMsg(response.message);
                } else {
                    setErrMsg(response.message);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //sign up --end

    return (
        <>
            {!isSuccess ? (
                <div className=" auth_area reset_area flexie">
                    <h1>Reset password</h1>

                    <p>
                        Lorem ipsum dolor sit amet, sit dor conctetur dor
                        adipiscing elit.
                    </p>
                    {errMsg ? (
                        <div className="content">
                            <p className="err_msg">{errMsg}</p>
                        </div>
                    ) : null}
                    <form
                        className="form_area"
                        onSubmit={(e) => {
                            e.preventDefault();
                            reestPass();
                        }}
                    >
                        <div className="row">
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.password.isValid
                                            ? "valid-box form_controls pass el"
                                            : "form_controls pass  el"
                                    }
                                    htmlFor="password"
                                >
                                    <input
                                        type={showPass1 ? "text" : "password"}
                                        placeholder="Password"
                                        onChange={(e) => {
                                            handleStates(
                                                "password",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.password
                                                ? values.password
                                                : ``
                                        }
                                    />
                                    {showPass1 ? (
                                        <i
                                            className="far fa-eye pass_icon "
                                            onClick={() => setShowPass1(false)}
                                        ></i>
                                    ) : (
                                        <i
                                            className="far fa-eye-slash pass_icon"
                                            onClick={() => setShowPass1(true)}
                                        ></i>
                                    )}
                                    
                                </label>
                                {!isError.password.isValid ? (
                                    <p className="valid-helper">
                                        {isError.password.message}
                                    </p>
                                ) : null}
                            </div>
                            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.password2.isValid
                                            ? "valid-box form_controls pass el"
                                            : "form_controls pass  el"
                                    }
                                    htmlFor="password2"
                                >
                                    <input
                                        type={showPass2 ? "text" : "password"}
                                        placeholder="Confirm Password"
                                        onChange={(e) => {
                                            handleStates(
                                                "password2",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.password2
                                                ? values.password2
                                                : ``
                                        }
                                    />
                                    {showPass2 ? (
                                        <i
                                            className="far fa-eye pass_icon "
                                            onClick={() => setShowPass2(false)}
                                        ></i>
                                    ) : (
                                        <i
                                            className="far fa-eye-slash pass_icon"
                                            onClick={() => setShowPass2(true)}
                                        ></i>
                                    )}
                                   
                                </label>
                                {!isError.password2.isValid ? (
                                        <p className="valid-helper">
                                            {isError.password2.message}
                                        </p>
                                    ) : null}
                            </div>
                        </div>

                        <div className="action_area">
                            <div className="row">
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <button
                                        className="main_btn lav_bg"
                                        onClick={() => {
                                            reestPass();
                                        }}
                                    >
                                        Reset Password
                                        {isLoading ? (
                                            <i className="fad fa-spinner-third fa-spin"></i>
                                        ) : null}
                                    </button>
                                    <p className="dividor dividor-alt"></p>
                                    <button
                                        className="main_btn "
                                        onClick={() => {
                                            props.isForgotPass(false);
                                        }}
                                    >
                                        <i className="fad fa-long-arrow-left back_icon"></i>
                                        Back
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            ) : (
                <div className="auth_area reset_area flexie ">
                    <div className="content">
                        <h1>Reset password</h1>
                    </div>
                    <div className="shadow_box success_area">
                        <div className="success_msg flexie">
                            <i className="fad fa-badge-check"></i>
                            <p className="main_txt">{errMsg}</p>
                            <p className="minilinks">
                                You can now&nbsp;
                                <a
                                    className="minipink"
                                    onClick={() => {
                                        props.isForgotPass(false);
                                    }}
                                >
                                    Log in
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};

export default Reset;
