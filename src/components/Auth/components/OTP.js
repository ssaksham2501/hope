import React, { useState, useEffect } from "react";

//helpers
import AuthController from "../../../controllers/auth.controller";
import Validation from "../../../helpers/vaildation";

const OTP = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(false);

    //validations --start
    const [isError, setError] = useState({
        otp: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        otp: null,
        token: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);

        setValues({
            otp: null,
            token: props.otpResponse.token,
        });
    }, []);

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };
    //validations --end

    //submitotp --start
    const submitOtp = async () => {
        if (isLoading === false) {
            setErrMsg(false);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new AuthController().send_otp(values);
                setIsLoading(false);
                if (response && response.status) {
                    props.isReset(true);
                    props.isResetResponse(values.otp);
                } else {
                    setErrMsg(response.error);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //submitotp --end
    return (
        <div className="auth_area otp_area flexie">
            <div className="content">
                <h1>Check Your Email</h1>
                {errMsg ? (
                    <div className="content">
                        <p className="err_msg">{errMsg}</p>
                    </div>
                ) : null}
            </div>
            <div className="shadow_box success_area">
                <div className="success_msg flexie">
                    <i className="fad fa-badge-check"></i>
                    <p className="main_txt">{props.otpResponse.message}</p>
                    <form
                        className="form_area"
                        onSubmit={(e) => {
                            e.preventDefault();
                            submitOtp();
                        }}
                    >
                        <label
                            className={
                                !isError.otp.isValid
                                    ? "valid-box form_controls"
                                    : "form_controls"
                            }
                            htmlFor="otp"
                        >
                            <input
                                type="number"
                                placeholder="OTP"
                                onChange={(e) => {
                                    handleStates("otp", e.target.value);
                                }}
                                value={values.otp ? values.otp : ``}
                            />
                            {!isError.otp.isValid ? (
                                <p className="valid-helper">
                                    {isError.otp.message}
                                </p>
                            ) : null}
                        </label>
                        <div className="msg_action_area flexie ">
                            <button
                                className="main_btn lav_bg"
                                onClick={() => {
                                    submitOtp();
                                }}
                            >
                                Submit
                                {isLoading ? (
                                    <i className="fad fa-spinner-third fa-spin"></i>
                                ) : null}
                            </button>

                            <button
                                className="main_btn "
                                onClick={() => {
                                    props.isForgotPass(false);
                                }}
                            >
                                <i className="fad fa-long-arrow-left back_icon"></i>
                                Back
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default OTP;
