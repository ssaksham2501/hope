import React, { useState, useEffect } from "react";

//helpers
import AuthController from "../../../controllers/auth.controller";
import landing_signup from "../../../assets/images/SignUp/signup.png";
import { Link, useHistory, useLocation } from "react-router-dom";
const Reverify = (props) => {
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(``);
    const params = useLocation();

    let slug = params.pathname.split("/");
    slug = slug.slice(-1);

    //resend link --start
    const resend_link = async () => {
        if (isLoading === false) {
            setIsLoading(true);
            let response = await new AuthController().resend_link(slug[slug.length - 1]);
            if (response && response.status) {
                setErrMsg(response.message);
                setIsLoading(false);
            } else {
                history.push('/');
            }
        }
    };
    //resend link --end

    return (
        <section className="signup_section verify_section">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="signup_area verify_area">
                                <div className="row row-no-padding ">
                                    <div className="col-xl-3 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div className="left_area">
                                            <img
                                                src={landing_signup}
                                                alt=""
                                            />
                                        </div>
                                    </div>
                                    <div className="col-xl-9 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div className=" auth_area sign_area sign_success flexie">
                                            <div className="content">
                                                <h1>
                                                    Verification Required
                                                </h1>
                                            </div>
                                            <div className="shadow_box success_area">
                                                <div className="success_msg flexie">
                                                    <i className="fad fa-badge-check far fa-exclamation-triangle"></i>
                                                    <p className="main_txt">We've sent you an email having verification link to your registered email. Please verify your account and proceed.</p>
                                                    {
                                                        errMsg
                                                            ?
                                                            <p className="main_txt text-danger">{errMsg}</p>
                                                            :
                                                            <button
                                                                className="main_btn lav_bg"
                                                                onClick={() => {
                                                                    resend_link();
                                                                }}
                                                            >
                                                                Resend Email
                                                                {isLoading ? (
                                                                    <i className="fad fa-spinner-third fa-spin"></i>
                                                                ) : null}
                                                            </button>
                                                    }
                                                    <p className="dividor"></p>
                                                    <button
                                                        className="main_btn "
                                                        onClick={() => {
                                                            history.push('/')
                                                        }}
                                                    >
                                                        <i className="fad fa-long-arrow-left back_icon"></i>
                                                        Back
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                </div>
        </section>
    );
};

export default Reverify;
