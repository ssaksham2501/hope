import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import { useHistory } from "react-router-dom";

//helpers
import AuthController from "../../../controllers/auth.controller";
import Validation from "../../../helpers/vaildation";
import GoogleLogin from "react-google-login";
import FacebookLogin from 'react-facebook-login';
import { Constant } from "../../../services/constants";
import { Toast } from "react-bootstrap";
import { renderImage } from "../../../helpers/General";
import Reverify from "./ReVerify";

const Log = (props) => {
    const history = useHistory();
    const location = useLocation()
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    const [user, setUser] = useState(null);
    const [askReverification, setAskReverification] = useState(null);

    //password icon
    const [showPass, setShowPass] = useState(false);
    //validations  -- start
    const [isError, setError] = useState({
        email: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        password: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        email: null,
        password: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        let loginUser = new AuthController().getLoginUser();
        setUser(loginUser);
        setValues(defaultValues);
       // setAskReverification(null);
        return () => {
            setValues(defaultValues);
            setUser(null);
        }
    }, []);

    // useEffect(() => {
    //     console.log(`askReverification`, askReverification);
    //     setAskReverification(askReverification);
    // }, [askReverification])

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };
    //validations  -- end

    //login --start
    const logIn = async (e) => {
        e.preventDefault();
        if (isLoading === false) {
            setErrMsg(false);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new AuthController().log_in(values);
                if (response && response.status) {
                    setValues(defaultValues);
                    if (response.verifyAccountPop) {
                        history.push('/verification/' + response.token);
                    } else {
                        //log in to home page
                        history.push("/");
                    }
                } else {
                    setErrMsg(response.error);
                }
                setIsLoading(false);
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //login --end

    //google redirect  --start
    const responseGoogle = async (response) => {
        if (response && response.googleId && response.accessToken && response.profileObj) {
            let user = await new AuthController().googleLogin(response);

            if (user && user.status) {
                history.push("/");
            }
            else {
                new Toast().error(user.message);    
            }
        }
        else {
            new Toast().error('Google account could not be connect.');
        }
    };
    //google redirect  --end

    //Facebook Login
    const responseFacebook = (response) => {
        console.log(response);
    }
    //Facebook Login

    console.log("---------", askReverification, values);
    return (
        <>
            {values.askReverification
                ?
                    <Reverify
                        setBack={() => {
                            setValues(defaultValues);
                        }}
                        logResponse={values.askReverification}
                    />
                :
                    <div className="log_area auth_area flexie">
                        <h1>LOG In</h1>
                        <p>
                            Lorem ipsum dolor sit amet, sit dor conctetur dor adipiscing
                            elit.
                        </p>
                        {errMsg ? (
                            <div className="content">
                                <p className="err_msg">{errMsg}</p>
                            </div>
                        ) : null}

                        <form
                            className="form_area"
                            onSubmit={logIn}
                        >   
                            <div
                                className={`login_form ` + ( user && user.id ? 'd-none' : '')}

                            >
                                <div className="row">
                                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label
                                            className={
                                                !isError.email.isValid
                                                    ? "valid-box form_controls els"
                                                    : "form_controls els"
                                            }
                                            htmlFor="email"
                                        >
                                            <input
                                                type="text"
                                                className="eld"
                                                placeholder="Username or Email"
                                                onChange={(e) => {
                                                    handleStates("email", e.target.value.toLowerCase().replace(' ', ''));
                                                }}
                                                value={values.email ? values.email : ``}
                                            />
                                            {!isError.email.isValid ? (
                                                <p className="valid-helper">
                                                    {isError.email.message}
                                                </p>
                                            ) : null}
                                        </label>
                                    </div>
                                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label
                                            className={
                                                !isError.password.isValid
                                                    ? "valid-box form_controls pass els"
                                                    : "form_controls pass els"
                                            }
                                            htmlFor="password"
                                        >
                                            <input
                                                type={showPass ? "text" : "password"}
                                                placeholder="Password"
                                                onChange={(e) => {
                                                    handleStates("password", e.target.value);
                                                }}
                                                value={values.password ? values.password : ``}
                                            />
                                            {showPass ? (
                                                <i
                                                    className="  fas fa-eye pass_icon"
                                                    onClick={() => setShowPass(false)}
                                                ></i>
                                            ) : (
                                                <i
                                                    className="fal fa-eye-slash pass_icon"
                                                    onClick={() => setShowPass(true)}
                                                ></i>
                                            )}
                                        
                                        </label>
                                        {!isError.password.isValid ? (
                                                <p className="valid-helper">
                                                    {isError.password.message}
                                                </p>
                                            ) : null}
                                        <a
                                            className={
                                                isError.password.message
                                                    ? "minilink mini_padd"
                                                    : "minilink"
                                            }
                                            onClick={() => {
                                                props.isForgotPass(true);
                                            }}
                                        >
                                            Forgot password?
                                        </a>
                                    </div>
                                </div>
                            </div>
                            {
                                user && user.id
                                &&
                                <div
                                    className={`recent_lgoin`}
                                >
                                    <div className="recent_log">
                                        <div
                                            className="img_set"
                                            onClick={() => {
                                                global.isLogin = true;
                                                global.clicked = true;
                                                history.push('/');
                                            }}
                                        >
                                                
                                            <img src={renderImage(user.image)} alt="../" />
                                        </div>
                                        <div
                                            className="name_user"
                                            onClick={() => {
                                                global.isLogin = true;
                                                global.clicked = true;
                                                history.push('/');
                                            }}
                                        >
                                            <p>
                                                {user.first_name + ' ' + user.last_name}
                                            </p>
                                        </div>
                                        <div className="cross">
                                            <Link
                                                to=""
                                                onClick={() => {
                                                    new AuthController().logout();
                                                }}
                                            >
                                                <i className="fal fa-times"></i>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            }
                            <div className={`action_area ` + (user && user.id ? ' d-none' : '')}>
                                <div className="row">
                                    <div className={`col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12`}>
                                        <button
                                            type="submit"
                                        className="main_btn lav_bg"
                                        >
                                            Log in
                                            {isLoading ? (
                                                <i className="fad fa-spinner-third fa-spin"></i>
                                            ) : null}
                                        </button>
                                    </div>
                                    <p className="dividor">or</p>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <GoogleLogin
                                            className="google_btn flexie"
                                            clientId={Constant.googleClientId}
                                            buttonText="Log in with Google"
                                            onSuccess={responseGoogle}
                                            onFailure={responseGoogle}
                                            cookiePolicy={"single_host_origin"}
                                            disabled={false}
                                        />
                                    </div>
                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 mt-sm-0 mt-3">
                                        <FacebookLogin
                                            cssClass="google_btn facebook flexie"
                                            icon={<i className="fab fa-facebook pe-2"></i>}
                                            appId={Constant.fbKey}
                                            autoLoad={false}
                                            fields="name,email,picture"
                                            callback={responseFacebook}
                                        />
                                    </div>
                                    <p className="minilinks">
                                        New here?&nbsp;
                                        <Link className="minipink" to="/sign-up">
                                            Sign up
                                        </Link>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
            }
        </>
        
    );
};

export default Log;
