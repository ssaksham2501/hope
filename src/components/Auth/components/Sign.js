import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";

//helpers
import AuthController from "../../../controllers/auth.controller";
import Validation from "../../../helpers/vaildation";
import GoogleLogin from "react-google-login";
import { Toast } from "../../../helpers/Toaster";
import { Constant } from "../../../services/constants";
import { getMonths, range, leapYear, getYear } from "../../../helpers/General";
import FacebookLogin from 'react-facebook-login';

const Sign = () => {
    const history = useHistory();
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    const [succeccMsg, setSuccessMsg] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    //password icon
    const [showPass1, setShowPass1] = useState(false);
    const [showPass2, setShowPass2] = useState(false);
    const [dob, setDob] = useState({
        day: null,
        month: null,
        year: null
    });
    //validations  -- start
    const [isError, setError] = useState({
        first_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        last_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        username: {
            rules: ["required", "username"],
            isValid: true,
            message: "",
        },
        email: {
            rules: ["required", "email"],
            isValid: true,
            message: "",
        },

        phonenumber: {
            rules: ["numeric", "minmax:7:14"],
            isValid: true,
            message: "Please enter a valid phone number",
        },
        dob: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        password: {
            rules: ["required", "password"],
            isValid: true,
            message: "",
        },
        password2: {
            rules: ["required", "password"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        first_name: null,
        last_name: null,
        username: null,
        email: null,
        phonenumber: null,
        dob: null,
        password: null,
        password2: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        setValues(defaultValues);
    }, []);

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
    
        if (field == 'password2') {
            let node2 = validation.matchValues(`password2`, values.password, value, "Confirm password does not match.");
            setError({ ...isError, ["password2"]: node2 });
        }

        /** Validate each field on change */
        if (!value || !value.trim()) {
            setValues({ ...values, [field]: "" });
        } else {
            setValues({ ...values, [field]: value });
        }
    };
    //validations  -- end

    //sign up  --start
    const signUp = async () => {
        if (isLoading === false) {
            setErrMsg(false);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            let node = validation.matchValues(`password2`, values.password, values.password2, "Confirm password does not match.");
            setError({ ...isError, ["password2"]: node });
            if (isValid && node.isValid && !isValid.haveError) {
                
                setIsLoading(true);
                let response = await new AuthController().sign_up(values);
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    setIsSuccess(true);

                    setSuccessMsg(response.message);
                } else {
                    setErrMsg(response.error);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //sign up --end

    //google redirect  --start
    const responseGoogle = async (response) => {
        console.log(response);
        if (response && response.googleId && response.accessToken && response.profileObj) {
            let user = await new AuthController().googleLogin(response);

            if (user && user.status) {
                history.push("/");
            }
            else {
                new Toast().error(user.message);    
            }
        }
        else {
            new Toast().error('Google account could not be connect.');
        }
    };
    //google redirect  --end

    //Facebook Login
    const responseFacebook = (response) => {
        console.log(response);
    }
    //Facebook Login

    //date validation
    const datePickerId = new Date().toISOString().split("T")[0];
    return (
        <>
            {!isSuccess ? (
                <div className="auth_area sign_area flexie">
                    <div className="content">
                        <h1>Sign up</h1>
                        {errMsg ? <p className="err_msg">{errMsg}</p> : null}
                    </div>

                    <form
                        className="form_area"
                        onSubmit={(e) => {
                            e.preventDefault();
                            signUp();
                        }}
                    >
                        <div className="row">
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.first_name.isValid
                                            ? "valid-box form_controls els"
                                            : "form_controls els"
                                    }
                                    htmlFor="first_name"
                                >
                                    <input
                                        type="text"
                                        placeholder="First Name"
                                        onChange={(e) => {
                                            handleStates(
                                                "first_name",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.first_name
                                                ? values.first_name
                                                : ``
                                        }
                                    />
                                    {!isError.first_name.isValid && isError.first_name.message ? (
                                        <p className="valid-helper">
                                            {isError.first_name.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.last_name.isValid
                                            ? "valid-box form_controls els"
                                            : "form_controls els"
                                    }
                                    htmlFor="last_name"
                                >
                                    <input
                                        type="text"
                                        placeholder="Last Name"
                                        onChange={(e) => {
                                            handleStates(
                                                "last_name",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.last_name
                                                ? values.last_name
                                                : ``
                                        }
                                    />
                                    {!isError.last_name.isValid && isError.last_name.message ? (
                                        <p className="valid-helper">
                                            {isError.last_name.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.username.isValid
                                            ? "valid-box form_controls els"
                                            : "form_controls els"
                                    }
                                    htmlFor="username"
                                >
                                    <input
                                        type="text"
                                        placeholder="Username"
                                        onChange={(e) => {
                                            handleStates(
                                                "username",
                                                e.target.value.trim().toLowerCase().replace(" ", "")
                                            );
                                        }}
                                        value={
                                            values.username
                                                ? values.username.trim()
                                                : ``
                                        }
                                    />
                                    {!isError.username.isValid && isError.username.message ? (
                                        <p className="valid-helper">
                                            {isError.username.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.email.isValid
                                            ? "valid-box form_controls els"
                                            : "form_controls els"
                                    }
                                    htmlFor="email"
                                >
                                    <input
                                        type="text"
                                        placeholder="Email"
                                        onChange={(e) => {
                                            handleStates(
                                                "email",
                                                e.target.value.trim().replace(" ", "").toLowerCase()
                                            );
                                        }}
                                        value={
                                            values.email
                                                ? values.email.trim()
                                                : ``
                                        }
                                    />
                                    {!isError.email.isValid && isError.email.message ? (
                                        <p className="valid-helper">
                                            {isError.email.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.phonenumber.isValid
                                            ? "valid-box form_controls els"
                                            : "form_controls els"
                                    }
                                    htmlFor="phonenumber"
                                >
                                    <input
                                        type="number"
                                        placeholder="Contact number"
                                        onChange={(e) => {
                                            handleStates(
                                                "phonenumber",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.phonenumber
                                                ? values.phonenumber
                                                : ``
                                        }
                                    />
                                    {!isError.phonenumber.isValid && isError.phonenumber.message ? (
                                        <p className="valid-helper">
                                            {isError.phonenumber.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.dob.isValid
                                            ? "valid-box form_controls els"
                                            : "form_controls els"
                                    }
                                    htmlFor="dob"
                                >
                                    <select
                                        className="day"
                                        onChange={(e) => {
                                            let value = e.target.value;
                                            setDob({ ...dob, day: (value ? value : null) });
                                            handleStates("dob", value && dob.month && dob.year ? (dob.year + '-' + dob.month + '-' + value) : null);
                                        }}
                                        value={dob.day !== null ? dob.day : ``}
                                    >
                                        <option value="">Day</option>
                                        {
                                            range(1, 31, true).map((item, i) => {
                                                let cond = (!(dob.month % 2) && item == 31 && dob.month <= 7) || ((dob.month % 2) && item == 31 && dob.month > 7) || (dob.month == 2 && item > (dob.year && leapYear(dob.year) ? 29 : 28));

                                                return <option
                                                    key={`day-` + i}
                                                    value={item}
                                                    disabled={cond ? true : false}
                                                >{item}</option>
                                            })
                                        }
                                    </select>
                                    <select
                                        className="month"
                                        onChange={(e) => {
                                            let value = e.target.value;
                                            let cond = (!(value % 2) && dob.day == 31 && value <= 7) || ((value % 2) && dob.day == 31 && value > 7) || (value == 2 && dob.day > (dob.year && leapYear(dob.year) ? 29 : 28));
                                            setDob({ ...dob, month: (value ? value : null), day: cond ? null : dob.day });
                                            if (!cond) {
                                                handleStates("dob", dob.day && value && dob.year ? (dob.year + '-' + value + '-' + dob.day) : null);
                                            }
                                            else {
                                                handleStates("dob", null);
                                            }
                                        }}
                                        value={dob.month !== null ? dob.month : ``}
                                    >
                                        <option value="">Month</option>
                                        {
                                            getMonths().map((item, i) => {
                                                return <option
                                                    key={`month-` + i}
                                                    value={(i+1 <= 9 ? '0'+(i*1+1) : i+1)}
                                                >{item}</option>
                                            })
                                        }
                                    </select>
                                    <select
                                        className="year"
                                        onChange={(e) => {
                                            let value = e.target.value;
                                            let cond = (dob.month == 2 && dob.day > (value && leapYear(value) ? 29 : 28));
                                            setDob({ ...dob, year: (value ? value : null), day: cond ? null : dob.day });
                                            if (!cond) {
                                                handleStates("dob", dob.day && dob.month && value ? (value + '-' + dob.month + '-' + dob.day) : null);
                                            }
                                            else {
                                                handleStates("dob", null);
                                            }
                                        }}
                                        value={dob.year !== null ? dob.year : ``}
                                    >
                                        <option value="">Year</option>
                                        {
                                            range(getYear() - 100, getYear()-9).reverse().map((item, i) => {
                                                return <option
                                                    key={`year-` + i}
                                                    value={item}
                                                >{item}</option>
                                            })
                                        }
                                    </select>
                                    {!isError.dob.isValid && isError.dob.message ? (
                                        <p className="valid-helper">
                                            {isError.dob.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.password.isValid
                                            ? "valid-box form_controls pass els"
                                            : "form_controls pass els"
                                    }
                                    htmlFor="password"
                                >
                                    <input
                                        type={showPass1 ? "text" : "password"}
                                        placeholder="Password"
                                        onChange={(e) => {
                                            handleStates(
                                                "password",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.password
                                                ? values.password
                                                : ``
                                        }
                                    />
                                    {showPass1 ? (
                                        <i
                                            className="far fa-eye pass_icon "
                                            onClick={() => setShowPass1(false)}
                                        ></i>
                                    ) : (
                                        <i
                                            className="far fa-eye-slash pass_icon"
                                            onClick={() => setShowPass1(true)}
                                        ></i>
                                    )}
                                    
                                </label>
                                    {!isError.password.isValid && isError.password.message ? (
                                        <p className="valid-helper">
                                            {isError.password.message}
                                        </p>
                                    ) : null}
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <label
                                    className={
                                        !isError.password2.isValid
                                            ? "valid-box form_controls pass els"
                                            : "form_controls pass els"
                                    }
                                    htmlFor="password2"
                                >
                                    <input
                                        type={showPass2 ? "text" : "password"}
                                        placeholder="Confirm Password"
                                        onChange={(e) => {
                                            handleStates(
                                                "password2",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.password2
                                                ? values.password2
                                                : ``
                                        }
                                    />
                                    {showPass2 ? (
                                        <i
                                            className="far fa-eye pass_icon "
                                            onClick={() => setShowPass2(false)}
                                        ></i>
                                    ) : (
                                        <i
                                            className="far fa-eye-slash pass_icon "
                                            onClick={() => setShowPass2(true)}
                                        ></i>
                                    )}
                                   
                                </label>
                                {!isError.password2.isValid && isError.password2.message ? (
                                        <p className="valid-helper">
                                            {isError.password2.message}
                                        </p>
                                    ) : null}
                            </div>
                        </div>
                        <div className="action_area">
                            <div className="row">
                                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <button
                                        className="main_btn lav_bg"
                                        type="button"
                                        onClick={() => {
                                            signUp();
                                        }}
                                    >
                                        Sign up
                                        {isLoading ? (
                                            <i className="fad fa-spinner-third fa-spin"></i>
                                        ) : null}
                                    </button>
                                </div>
                                <p className="dividor">or</p>
                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <GoogleLogin
                                        className="google_btn flexie"
                                        clientId={Constant.googleClientId}
                                        buttonText="Sign up with Google"
                                        onSuccess={responseGoogle}
                                        onFailure={responseGoogle}
                                        cookiePolicy={"single_host_origin"}
                                        disabled={false}
                                    />
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 mt-sm-0 mt-3">
                                    <FacebookLogin
                                        cssClass="google_btn facebook flexie"
                                        icon={<i className="fab fa-facebook pe-2"></i>}
                                        appId={Constant.fbKey}
                                        autoLoad={false}
                                        fields="name,email,picture"
                                        callback={responseFacebook}
                                    />
                                </div>
                                <p className="minilinks">
                                    Already have an account?&nbsp;
                                    <Link className="minipink" to="/">
                                        Log in
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            ) : (
                <div className="auth_area sign_area  sign_success flexie">
                    <div className="content">
                        <h1>Sign up Successful</h1>
                        {errMsg ? <p className="err_msg">{errMsg}</p> : null}
                    </div>
                    <div className="shadow_box success_area">
                        <div className="success_msg flexie">
                            <i className="fad fa-badge-check"></i>
                            <p className="main_txt">{succeccMsg}</p>
                            <Link to="/">
                                <button className="main_btn lav_bg">
                                    <i className="fad fa-long-arrow-left back_icon"></i>
                                    Back to Log in
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
};

export default Sign;
