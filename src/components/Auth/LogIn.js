import React, { useEffect, useState } from "react";

//assets
import landing_banner from "../../assets/images/Landing/banner.png";
import landing_logo from "../../assets/images/Landing/logo.png";

//components
import Log from "./components/Log";
import Forgot from "./components/Forgot";
import Reverify from "./components/ReVerify";
import OTP from "./components/OTP";
import Reset from "./components/Reset";
import AuthController from "../../controllers/auth.controller";

const Login = () => {
    const [loginForm, setLoginForm] = useState(true);
    const [forgotPass, setForgotPass] = useState(false);
    const [reverify, setReverify] = useState(false);
    const [logResponse, setLogResponse] = useState("");
    const [otp, setOtp] = useState(false);
    const [otpResponse, setOtpResponse] = useState("");
    const [reset, setReset] = useState(false);
    const [resetResponse, setResetResponse] = useState("");
    const [user, setUser] = useState(null);

    useEffect(() => {
        let loginUser = new AuthController().getLoginUser();
        setUser(loginUser);
        window.scrollTo(0, 0);
    }, []);

    const hideAll = () => {
        setLoginForm(false);
        setReverify(false);
        setForgotPass(false);
        setOtp(false);
        setReset(false);
    };

    //forget Password logic
    const isForgotPass = (state) => {
        hideAll();
        if (state === true) {
            setForgotPass(true);
        } else {
            setLoginForm(true);
        }
    };

    //OTP Screen logic
    const isOtp = (state) => {
        hideAll();
        if (state === true) {
            setOtp(true);
        } else {
            setForgotPass(true);
        }
    };

    //reset password logic
    const isReset = (state) => {
        hideAll();
        if (state === true) {
            setReset(true);
        } else {
            setOtp(true);
        }
    };

    const islogResponse = (resp) => {
        setLogResponse(resp);
    };
    const isOtpResponse = (resp) => {
        setOtpResponse(resp);
    };
    const isResetResponse = (resp) => {
        setResetResponse(resp);
    };

    return (
        <section className="login_section">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="login_area">
                            <div className="row">
                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 px-0">
                                    <div
                                        className="left_area"
                                    // style={{
                                    //     backgroundImage: `url(${landing_banner})`,
                                    // }}
                                    >
                                        <div className="content_main flexie">
                                            <h3>Be Somebody's</h3>
                                            <img src={landing_logo} alt="..." />
                                        
                                        </div>
                                        {
                                            !(user && user.id)
                                            ?
                                                <>
                                                    <div className="img_area_dov">
                                                        <img src={landing_banner} alt="..." />
                                                    </div>
                                                    <div className="log_dov_set"></div>
                                                </>
                                            :
                                                <div className="log_dov_set" style={{height: '20px'}}></div>
                                        }
                                        
                                    </div>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    {loginForm && (
                                        <Log
                                            isForgotPass={isForgotPass}
                                            islogResponse={islogResponse}
                                        />
                                    )}
                                    
                                    {forgotPass && (
                                        <Forgot
                                            isOtp={isOtp}
                                            isForgotPass={isForgotPass}
                                            isOtpResponse={isOtpResponse}
                                        />
                                    )}

                                    {otp && otpResponse && (
                                        <OTP
                                            isForgotPass={isForgotPass}
                                            otpResponse={otpResponse}
                                            isReset={isReset}
                                            isResetResponse={isResetResponse}
                                        />
                                    )}

                                    {reset && resetResponse && otpResponse && (
                                        <Reset
                                            isForgotPass={isForgotPass}
                                            resetResponse={resetResponse}
                                            otpResponse={otpResponse}
                                        />
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Login;
