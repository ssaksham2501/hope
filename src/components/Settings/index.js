import React from "react";

//components
import Suggestions from "../Home/components/Suggestions";
import ChatSection from "../Layout/components/ChatSection";
import SettingsMain from "./components/SettingsMain";

//components

const Settings = () => {
    return (
        <section className="settings_section">
            <div className="row">
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div className="row">
                        <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <SettingsMain />
                        </div>
                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 d-lg-block d-none">
                                <ChatSection />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Settings;
