import React, { useState, useEffect } from "react";
import Validation from "../../../helpers/vaildation";
import AuthController from "../../../controllers/auth.controller";
import Friends from "../../Communities/components/Friends";
import { Toast } from "../../../helpers/Toaster";
import ProfileController from "../../../controllers/profile.controller";
import GeneralInformation from "./GeneralInformation";
import DeleteModal from "../../Layout/DeleteModal";

const SettingsMain = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [blockedList, setBlockedList] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    //validations  -- start
    const [isError, setError] = useState({
        password: {
            rules: ["required", "password"],
            isValid: true,
            message: "",
        },
        password1: {
            rules: ["required", "password"],
            isValid: true,
            message: "",
        },
        password2: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);
    let defaultValues = {
        password: null,
        password1: null,
        password2: null,
    };
    const [values, setValues] = useState(defaultValues);
    const [user, setUser] = useState(null);
    const [privacy, setPrivacy] = useState(null);
    const [deleteModal, setDeleteModal] = useState(null);
    
    
    useEffect(() => {
        window.scrollTo(0, 0);
        getProfile();
        setValues(values);
    }, []);

    const getProfile = async () => {
        let response = await new ProfileController().getProfile();
        if (response && response.status) {
            setUser(response.user);
            setPrivacy(response.user.visibility);
        }
    };

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };
    //validations  -- end

    //sign up  --start
    const updatePass = async () => {
        // else if (isLoading === false) {
        /** Check full form validation and submit **/
        let validation = new Validation(isError);
        let isValid = await validation.isFormValid(values);
        if (isValid && !isValid.haveError) {
            let node = validation.matchValues(`password2`, values.password1, values.password2, "Confirm password does not match.");
            setError({ ...isError, ["password2"]: node });
            let node2 = validation.notMatchValues(`password1`, values.password, values.password1, "New password can't be same as old password.");
            setError({ ...isError, ["password1"]: node2 });
            
            if(node2.isValid && node.isValid) {
                setIsLoading(true);
                let response = await new AuthController().changePassword(
                    values
                );
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    new Toast().success(response.message);
                } else {
                    new Toast().error(response.message);
                }
            }
        } else {
            setError({ ...isValid.errors });
        }
        // }
    };
    //sign up --end

    const updatePrivacy = async (value) => {
        if (value) {
            let old = privacy;
            setPrivacy(value);
            let response = await new ProfileController().updatePrivacy(value);
            if (response && response.status) {
                new AuthController().setUpLogin(response.user);
                getProfile();
                new Toast().success(response.message);
            } else {
                setPrivacy(old);
                new Toast().error(response.message);
            }    
        }
    }

    const listProps = {
        col: 4,
        group: "unblock",
        title: "Blocked Members"
    };
    return (
        <div className="community_main_area settings_main_area">
            {!blockedList ? (
                <>   
                        <h2>
                        Settings</h2>
                   
                    <div className="form_area community_area">
                        <GeneralInformation
                            user={user}
                            updated={() => {
                                getProfile();
                            }}
                        />
                        <div className="sub_section">
                            <div className="content_head">
                                <h3><i className="far fa-shield-check"></i> Privacy</h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Nullam ac erat lorem.
                                </p>
                            </div>
                            <div className="setting_option">
                                <label className="form_controls">
                                    <p>Profile visibility</p>
                                    <div className="select_box">
                                        {
                                            privacy
                                            &&
                                            <select
                                                required=""
                                                value={privacy}
                                                onChange={(e) => updatePrivacy(e.target.value)}
                                            >
                                                <option value="public">Public</option>
                                                <option value="private">Private</option>
                                            </select>
                                        }
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div className="sub_section password_sub">
                            <div className="content_head">
                                <h3><i className="far fa-key"></i> Change Password</h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Nullam ac erat lorem.
                                </p>
                            </div>

                            <div className="setting_option">
                                <label
                                    className={
                                        !isError.password.isValid
                                            ? "valid-box form_controls"
                                            : "form_controls"
                                    }
                                    htmlFor="password"
                                >
                                    <p>Old Password</p>

                                    <input
                                        className={
                                            isError.password.message
                                                ? ""
                                                : ""
                                        }
                                        type="password"
                                        placeholder="Enter your old password"
                                        onChange={(e) => {
                                            handleStates(
                                                "password",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.password
                                                ? values.password
                                                : ``
                                        }
                                    />

                                    {!isError.password.isValid ? (
                                        <p className="valid-helper">
                                            {isError.password.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="setting_option">
                                <label
                                    className={
                                        !isError.password1.isValid
                                            ? "valid-box form_controls"
                                            : "form_controls"
                                    }
                                    htmlFor="password"
                                >
                                    <p>New Password</p>

                                    <input
                                        className={
                                            isError.password1.message
                                                ? ""
                                                : ""
                                        }
                                        type="password"
                                        placeholder="Enter new password"
                                        onChange={(e) => {
                                            handleStates(
                                                "password1",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.password1
                                                ? values.password1
                                                : ``
                                        }
                                    />

                                    {!isError.password1.isValid ? (
                                        <p className="valid-helper">
                                            {isError.password1.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="setting_option">
                                <label
                                    className={
                                        !isError.password2.isValid
                                            ? "valid-box form_controls"
                                            : "form_controls"
                                    }
                                    htmlFor="password2"
                                >
                                    <p>Confirm Password</p>

                                    <input
                                        className={
                                            isError.password2.message
                                                ? ""
                                                : ""
                                        }
                                        type="password"
                                        placeholder="Confirm new password"
                                        onChange={(e) => {
                                            handleStates(
                                                "password2",
                                                e.target.value
                                            );
                                        }}
                                        value={
                                            values.password2
                                                ? values.password2
                                                : ``
                                        }
                                    />

                                    {!isError.password2.isValid ? (
                                        <p className="valid-helper">
                                            {isError.password2.message}
                                        </p>
                                    ) : null}
                                </label>
                            </div>
                            <div className="action_area">
                                <button
                                    className="main_btn lav_bg"
                                    onClick={() => {
                                        updatePass();
                                    }}
                                >
                                    Update Password
                                    {isLoading ? (
                                        <i className="fad fa-spinner-third fa-spin"></i>
                                    ) : null}
                                </button>
                            </div>
                        </div>
                        <div className="sub_section">
                            <div className="content_head">
                                <h3><i className="far fa-user-lock"></i> Blocking</h3>
                            </div>

                            <div className="action_area">
                                <button
                                    className="main_btn lav_bg"
                                    onClick={() => {
                                        setBlockedList(true);
                                    }}
                                >
                                    Blocked Members
                                    {/* {isLoading ? (
                                        <i className="fad fa-spinner-third fa-spin"></i>
                                    ) : null} */}
                                </button>
                            </div>
                        </div>
                        <div className="sub_section">
                            <div className="content_head">
                                <h3><i className="far fa-trash-alt"></i> Delete Account</h3>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit.
                                </p>
                            </div>
                            <div className="action_area">
                                <button
                                    className="main_btn red_bg"
                                    onClick={() => {
                                        setDeleteModal(true);        
                                    }}
                                >
                                    Delete Account
                                </button>
                            </div>
                        </div>
                    </div>
                </>
            ) : (
                <>
                    <div className="community_head blocked_head">
                        <h2>
                            <i
                                className="fas fa-arrow-left"
                                onClick={() => {
                                    setBlockedList(false);
                                }}
                            ></i>
                            Settings
                        </h2>
                    </div>
                    <div className="blocked_area community_area">
                        <div className="sub_section">
                            <div className="content_head">
                                <Friends listProps={listProps} />
                            </div>
                        </div>
                    </div>
                </>
            )}
            {
                deleteModal
                &&
                <DeleteModal
                    show={deleteModal}
                    setShow={(b) => setDeleteModal(b)}
                />
            }
        </div>
    );
};

export default SettingsMain;
