import React, { useEffect, useState } from "react";
import AuthController from "../../../controllers/auth.controller";
import ProfileController from "../../../controllers/profile.controller";
import { getMonths, getYear, leapYear, range } from "../../../helpers/General";
import { Toast } from "../../../helpers/Toaster";
//helpers
import Validation from "../../../helpers/vaildation";

const GeneralInformation = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [dob, setDob] = useState({
        day: null,
        month: null,
        year: null
    });
    //validations  -- start
    const [isError, setError] = useState({
        first_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        last_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        dob: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        username: {
            rules: [],
            isValid: true,
            message: "",
        },
        email: {
            rules: [],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);
    let defaultValues = {
        first_name: ``,
        last_name: ``,
        dob: ``,
        email: ``,
        username: ``
    };
    const [values, setValues] = useState(defaultValues);
    
    useEffect(() => {
        if (props.user) {
            setValues({
                first_name: props.user.first_name,
                last_name: props.user.last_name,
                dob: props.user.dob,
                email: props.user.email,
                username: props.user.username,
            });
            if (props.user.dob) {
                let bday = props.user.dob.split('-');
                setDob({
                    day: bday[2],
                    month: bday[1],
                    year: bday[0]
                });
            }
        }
    }, [props.user]);
    

    const handleStates = (field, value) => {
        /** Validate each field on change */
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        /** Validate each field on change */
        setValues({ ...values, [field]: value });
    };
    //validations  -- end


    const updateProfile = async (value) => {
        if(!isLoading) {
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new ProfileController().updateProfile(
                    values
                );
                setIsLoading(false);
                if (response && response.status) {
                    new AuthController().setUpLogin(response.user);
                    props.updated();
                    new Toast().success(response.message);
                } else {
                    new Toast().error(response.message);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };

    return (
        <div className="sub_section password_sub" >
            <div className="content_head">
                <h3><i className="far fa-info-circle"></i> Information</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Nullam ac erat lorem.
                </p>
            </div>

            <div className="setting_option name">
                <p>Your Name</p>
                <label
                    className={
                        !isError.first_name.isValid
                            ? "valid-box form_controls"
                            : "form_controls"
                    }
                    htmlFor="first_name"
                >
                    

                    <input
                        className={
                            isError.first_name.isValid
                                ? ""
                                : ""
                        }
                        type="text"
                        placeholder="First name"
                        onChange={(e) => {
                            handleStates(
                                "first_name",
                                e.target.value
                            );
                        }}
                        value={
                            values.first_name
                                ? values.first_name
                                : ``
                        }
                    />
                    {!isError.first_name.isValid ? (
                        <p className="valid-helper">
                            {isError.first_name.message}
                        </p>
                    ) : null}
                </label>
                <label
                    className={
                        !isError.last_name.isValid
                            ? "valid-box form_controls"
                            : "form_controls"
                    }
                    htmlFor="last_name"
                >
                    <input
                        className={
                            isError.last_name.message
                                ? ""
                                : ""
                        }
                        type="text"
                        placeholder="Last name"
                        onChange={(e) => {
                            handleStates(
                                "last_name",
                                e.target.value
                            );
                        }}
                        value={
                            values.last_name
                                ? values.last_name
                                : ``
                        }
                    />
                </label>
                    
            </div>
            <div className="setting_option name">
                <p>User Name</p>
                <label
                    className={
                        
                        !isError.username.isValid
                            ? "valid-box form_controls"
                            : "form_controls d-block el "
                           
                    }
                    htmlFor="username"
                >
                    

                    <input
                        className={
                            isError.username.isValid
                                ? ""
                                : ""
                        }
                        type="text"
                        placeholder="john.doe"
                        onChange={(e) => {
                            handleStates(
                                "username",
                                e.target.value
                            );
                        }}
                        value={
                            values.username
                                ? values.username
                                : ``
                        }
                        readOnly={`readonly`}
                    />
                    {!isError.username.isValid ? (
                        <p className="valid-helper">
                            {isError.username.message}
                        </p>
                    ) : null}
                </label>
            </div>
            <div className="setting_option name">
                <p>Email Address</p>
                <label
                    className={
                        !isError.first_name.isValid
                            ? "valid-box form_controls"
                            : "form_controls d-block el"
                    }
                    htmlFor="first_name"
                >
                    

                    <input
                        className={
                            isError.email.isValid
                                ? ""
                                : ""
                        }
                        type="text"
                        placeholder="john.doe@example.com"
                        onChange={(e) => {
                            handleStates(
                                "email",
                                e.target.value
                            );
                        }}
                        value={
                            values.email
                                ? values.email
                                : ``
                        }
                        readOnly={`readonly`}
                    />
                    {!isError.email.isValid ? (
                        <p className="valid-helper">
                            {isError.email.message}
                        </p>
                    ) : null}
                </label> 
            </div>
            <div className="setting_option dob">
                <p>Date of Birth</p>
                <label
                    className={
                        !isError.dob.isValid
                            ? "valid-box form_controls"
                            : "form_controls"
                    }
                    htmlFor="dob"
                >
                    <select
                        onChange={(e) => {
                            let value = e.target.value;
                            setDob({ ...dob, day: (value ? value : null) });
                            handleStates("dob", value && dob.month && dob.year ? (dob.year + '-' + dob.month + '-' + value) : null);
                        }}
                        value={dob.day !== null ? dob.day : ``}
                    >
                        <option value="">Day</option>
                        {
                            range(1, 31, true).map((item, i) => {
                                let cond = (!(dob.month % 2) && item == 31 && dob.month <= 7) || ((dob.month % 2) && item == 31 && dob.month > 7) || (dob.month == 2 && item > (dob.year && leapYear(dob.year) ? 29 : 28));

                                return <option
                                    key={`day-` + i}
                                    value={item}
                                    disabled={cond ? true : false}
                                >{item}</option>
                            })
                        }
                    </select>
                    <select
                        onChange={(e) => {
                            let value = e.target.value;
                            let cond = (!(value % 2) && dob.day == 31 && value <= 7) || ((value % 2) && dob.day == 31 && value > 7) || (value == 2 && dob.day > (dob.year && leapYear(dob.year) ? 29 : 28));
                            setDob({ ...dob, month: (value ? value : null), day: cond ? null : dob.day });
                            if (!cond) {
                                handleStates("dob", dob.day && value && dob.year ? (dob.year + '-' + value + '-' + dob.day) : null);
                            }
                            else {
                                handleStates("dob", null);
                            }
                        }}
                        value={dob.month !== null ? dob.month : ``}
                    >
                        <option value="">Month</option>
                        {
                            getMonths().map((item, i) => {
                                return <option
                                    key={`month-` + i}
                                    value={(i+1 <= 9 ? '0'+(i*1+1) : i+1)}
                                >{item}</option>
                            })
                        }
                    </select>
                    <select
                        onChange={(e) => {
                            let value = e.target.value;
                            let cond = (dob.month == 2 && dob.day > (value && leapYear(value) ? 29 : 28));
                            setDob({ ...dob, year: (value ? value : null), day: cond ? null : dob.day });
                            if (!cond) {
                                handleStates("dob", dob.day && dob.month && value ? (value + '-' + dob.month + '-' + dob.day) : null);
                            }
                            else {
                                handleStates("dob", null);
                            }
                        }}
                        value={dob.year !== null ? dob.year : ``}
                    >
                        <option value="">Year</option>
                        {
                            range(getYear() - 100, getYear()-9).reverse().map((item, i) => {
                                return <option
                                    key={`year-` + i}
                                    value={item}
                                >{item}</option>
                            })
                        }
                    </select>
                    {!isError.dob.isValid && isError.dob.message ? (
                        <p className="valid-helper">
                            {isError.dob.message}
                        </p>
                    ) : null}
                </label>
            </div>
            <div className="action_area">
                <button
                    className="main_btn lav_bg"
                    onClick={updateProfile}
                >
                    Update
                    {isLoading ? (
                        <i className="fad fa-spinner-third fa-spin"></i>
                    ) : null}
                </button>
            </div>
        </div>
    );
};

export default GeneralInformation;

