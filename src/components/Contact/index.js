import React from "react";

//components
import Breadcrumb from "../Layout/Breadcrumb";
import ContactForm from "./components/ContactForm";

function Contact() {
    return (
        <div>
            <Breadcrumb breadTitle="Contact US" />
            <ContactForm />
        </div>
    );
}

export default Contact;
