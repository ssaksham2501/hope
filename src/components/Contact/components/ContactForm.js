import React, { useState, useEffect } from "react";
import Validation from "../../../helpers/vaildation";
import ContactController from "../../../controllers/contact.controller";

//assets
import contact_layer from "../../../assets/images/Contact/layer.png";
import contact_phone from "../../../assets/images/Contact/phone.png";
import contact_at from "../../../assets/images/Contact/at.png";
import contact_pin from "../../../assets/images/Contact/pin.png";
import contact_comms from "../../../assets/images/Contact/comms.png";
import landing_birbstars from "../../../assets/images/Contact/birbstars.png";

const ContactForm = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [contactDetails, setContactDetails] = useState(false);

    //validations  -- start
    const [isError, setError] = useState({
        first_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        last_name: {
            rules: ["required", "alphabetic"],
            isValid: true,
            message: "",
        },
        email: {
            rules: ["required", "email"],
            isValid: true,
            message: "",
        },
        message: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
    });
    let validation = new Validation(isError);

    let defaultValues = {
        first_name: null,
        last_name: null,
        email: null,
        message: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        setValues(defaultValues);
    }, []);

    const handleStates = (field, value) => {
        if (!value || !value.trim()) {
            /** Validate each field on change */
            let node = validation.validateField(field, value);
            setError({ ...isError, [field]: node });
            /** Validate each field on change */
            setValues({ ...values, [field]: "" });
        } else {
            /** Validate each field on change */
            let node = validation.validateField(field, value);
            setError({ ...isError, [field]: node });
            /** Validate each field on change */
            setValues({ ...values, [field]: value });
        }
    };

    //validations  -- end

    //contact  --start
    const contactUs = async () => {
        if (isLoading === false) {
            setErrMsg(false);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new ContactController().contact_us(values);
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    setIsSuccess(true);
                    setErrMsg(response.message);
                } else {
                    setErrMsg(response.error);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //contact --end

    //getcontact --start
    useEffect(() => {
        window.scrollTo(0, 0);
        getContact();
    }, []);

    const getContact = async () => {
        let response = await new ContactController().dataContact();
        if (response && response.status) {
            setContactDetails(response.contact);
        }
    };
    //getcontact --end
    return (
        <section className="contact_form_section">
            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="contact_form_area">
                            <div className="row">
                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div
                                        className="left_area"
                                        style={{
                                            backgroundImage: `url(${contact_layer})`,
                                        }}
                                    >
                                        <div className="content">
                                            <h1>Get in touch</h1>
                                            <p>{contactDetails.info}</p>
                                            <div className="contact_details">
                                                <div className="icon_set">
                                                    <a
                                                        href={`tel:${contactDetails.phone}`}
                                                    >
                                                        <img
                                                            src={contact_phone}
                                                            alt="..."
                                                        />
                                                        {contactDetails.phone}
                                                    </a>
                                                </div>
                                                <div className="icon_set">
                                                    <a
                                                        href={`mailto:${contactDetails.email}`}
                                                    >
                                                        <img
                                                            src={contact_at}
                                                            alt="..."
                                                        />
                                                        {contactDetails.email}
                                                    </a>
                                                </div>
                                                <div className="icon_set">
                                                    <a
                                                        href="https://goo.gl/maps/4TvyXCfNQTQ4U8sR7"
                                                        target="_blank"
                                                    >
                                                        <img
                                                            src={contact_pin}
                                                            alt="..."
                                                        />
                                                        {contactDetails.address}
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="imgwrap">
                                                <img
                                                    src={contact_comms}
                                                    alt="..."
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <div className="right_area">
                                        {!isSuccess ? (
                                            <form
                                                className="form_area form_area_2"
                                                onSubmit={(e) => {
                                                    e.preventDefault();
                                                    contactUs();
                                                }}
                                            >
                                                <div className="row">
                                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                        <label
                                                            className={
                                                                !isError
                                                                    .first_name
                                                                    .isValid
                                                                    ? "valid-box form_controls"
                                                                    : "form_controls"
                                                            }
                                                            htmlFor="first_name"
                                                        >
                                                            <p className="label_top eld">
                                                                First Name
                                                            </p>
                                                            <input
                                                                type="text"
                                                                placeholder="Enter your first name"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    handleStates(
                                                                        "first_name",
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                                value={
                                                                    values.first_name
                                                                        ? values.first_name
                                                                        : ``
                                                                }
                                                            />
                                                            {!isError.first_name
                                                                .isValid ? (
                                                                <p className="valid-helper">
                                                                    {
                                                                        isError
                                                                            .first_name
                                                                            .message
                                                                    }
                                                                </p>
                                                            ) : null}
                                                        </label>
                                                    </div>
                                                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                                        <label
                                                            className={
                                                                !isError
                                                                    .last_name
                                                                    .isValid
                                                                    ? "valid-box form_controls"
                                                                    : "form_controls"
                                                            }
                                                            htmlFor="last_name"
                                                        >
                                                            <p className="label_top eld">
                                                                Last Name
                                                            </p>
                                                            <input
                                                                type="text"
                                                                placeholder="Enter your last name"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    handleStates(
                                                                        "last_name",
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                                value={
                                                                    values.last_name
                                                                        ? values.last_name
                                                                        : ``
                                                                }
                                                            />
                                                            {!isError.last_name
                                                                .isValid ? (
                                                                <p className="valid-helper">
                                                                    {
                                                                        isError
                                                                            .last_name
                                                                            .message
                                                                    }
                                                                </p>
                                                            ) : null}
                                                        </label>
                                                    </div>
                                                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <label
                                                            className={
                                                                !isError.email
                                                                    .isValid
                                                                    ? "valid-box form_controls"
                                                                    : "form_controls"
                                                            }
                                                            htmlFor="email"
                                                        >
                                                            <p className="label_top eld">
                                                                Email
                                                            </p>
                                                            <input
                                                                type="email"
                                                                placeholder="Enter your email"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    handleStates(
                                                                        "email",
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                                value={
                                                                    values.email
                                                                        ? values.email
                                                                        : ``
                                                                }
                                                            />
                                                            {!isError.email
                                                                .isValid ? (
                                                                <p className="valid-helper">
                                                                    {
                                                                        isError
                                                                            .email
                                                                            .message
                                                                    }
                                                                </p>
                                                            ) : null}
                                                        </label>
                                                    </div>
                                                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <label
                                                            className={
                                                                !isError.message
                                                                    .isValid
                                                                    ? "valid-box form_controls form_controls_tarea"
                                                                    : "form_controls form_controls_tarea"
                                                            }
                                                            htmlFor="message"
                                                        >
                                                            <p className="label_top elds">
                                                                Message
                                                            </p>
                                                            <textarea
                                                                type="text"
                                                                rows={5}
                                                                placeholder="Write your message"
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    handleStates(
                                                                        "message",
                                                                        e.target
                                                                            .value
                                                                    );
                                                                }}
                                                                value={
                                                                    values.message
                                                                        ? values.message
                                                                        : ``
                                                                }
                                                            />

                                                            {!isError.message
                                                                .isValid ? (
                                                                <p className="valid-helper">
                                                                    {
                                                                        isError
                                                                            .message
                                                                            .message
                                                                    }
                                                                </p>
                                                            ) : null}
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="action_area flexie">
                                                    <button
                                                        className="main_btn lav_bg"
                                                        type="button"
                                                        onClick={() => {
                                                            contactUs();
                                                        }}
                                                    >
                                                        Send
                                                        {isLoading ? (
                                                            <i className="fad fa-spinner-third fa-spin"></i>
                                                        ) : null}
                                                    </button>
                                                </div>
                                            </form>
                                        ) : (
                                            <div className="success_area flexie">
                                                <div className="success_msg flexie">
                                                    <i className="fad fa-badge-check"></i>
                                                    <p className="main_txt">
                                                        {errMsg}
                                                    </p>
                                                    <button
                                                        className="main_btn lav_bg"
                                                        onClick={() =>
                                                            setIsSuccess(false)
                                                        }
                                                    >
                                                        <i className="fad fa-long-arrow-left back_icon"></i>
                                                        Back
                                                    </button>
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                            <div className="imgwrap contact_img">
                                <img src={landing_birbstars} alt="..." />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default ContactForm;
