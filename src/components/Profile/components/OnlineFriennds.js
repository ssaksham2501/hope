import React from "react";
import { Link } from "react-router-dom";

//assets
import landing_callie from "../../../assets/images/Landing/callie.png";

const OnlineFriends = () => {
    return (
        <section className="community_suggestions d-lg-block d-sm-none d-none">
            <div className="community-head">
                <div className="name">
                    <p>Friends<span>208</span></p>
                </div>
                <div className="icon">
                    <Link to="/">
                        <span>View all</span>
                    </Link>
                </div>
            </div>
            <div className="suggestion_list">
                <ul>
                    <li>
                        <div className="img_area frnds online">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Callie Parker</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area frnds online">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Ben Musk </p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area frnds online ">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Lee </p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area frnds online">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Nathalia</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area frnds online">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Sunny</p>
                        </div>
                    </li>
                    <li>
                        <div className="img_area frnds online">
                            <img src={landing_callie} alt=".." />
                        </div>
                        <div className="name">
                            <p>Kavin Musk </p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
    );
};

export default OnlineFriends;
