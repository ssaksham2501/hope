import React from 'react'
//assets
import cover from "../../../assets/images/sidebar/Layer-5.jpg";
import user from "../../../assets/images/sidebar/user.jpg";

function Profile2() {
  return (
    <section className="User_seaction">
            <div className="container">
                <div className="row">
                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="cover_area">
                            <div className="cover_img">
                                <img src={cover} alt="..." />
                            </div>
                        </div>
                        <div className="user_area">
                            <div className="user-img">
                                <img src={user} alt=".." />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  )
}

export default Profile2;


