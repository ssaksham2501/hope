import React from "react";
import { connect } from "react-redux";
//assets
import cover from "../../../assets/images/sidebar/Layer-5.jpg";
import user from "../../../assets/images/sidebar/user.jpg";
import { baseUrl, renderImage } from "../../../helpers/General";
import { linkInModal } from "../../../redux/actions/user";

function Profile1(props) {
    return (
        <section className="User_seaction">
            <div className="container p-0">
                <div className="row">
                    <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="cover_area">
                            <div
                                className="cover_img"
                                onClick={() => {
                                    if (props.user.banner) {
                                        props.showLinkInModal(baseUrl(props.user.banner))
                                    }
                                }}
                            >
                                <img
                                    src={props.user.banner ? renderImage(props.user.banner) : cover}
                                    alt="..."
                                />
                                {/* <div className="icon">
                                    <i className="far fa-camera"></i>
                                </div> */}
                            </div>
                        </div>
                        <div className="user_area">
                            <div
                                className="user-img"
                                onClick={() => {
                                    if (props.user.image) {
                                        props.showLinkInModal(baseUrl(props.user.image))
                                    }
                                }}
                            >
                                <img
                                    src={renderImage(props.user.image)}
                                    alt=".."
                                />
                                {/* <div className="icon">
                                    <i className="far fa-camera"></i>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

const mapState = (state) => {
    return {
    };
};
const mapDispatch = (dispatch) => {
    return {
        showLinkInModal: async (info) => dispatch(linkInModal(info)),
    };
};

export default connect(mapState, mapDispatch)(Profile1);
