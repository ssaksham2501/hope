import React, { useEffect, useRef, useState } from "react";

//components
import OnlineFriends from "./OnlineFriennds";

//assets
import cover from "../../../assets/images/sidebar/Layer-5.jpg";
import user1 from "../../../assets/images/Landing/callie.png";
import ProfileController from "../../../controllers/profile.controller";
import { Constant } from "../../../services/constants";
import AuthController from "../../../controllers/auth.controller";
import Form from "react-bootstrap/Form";
import { baseUrl, renderImage } from "../../../helpers/General";
import PostController from "../../../controllers/post.controller";
import Post from "../../Home/components/Post";
import Suggestions from "../../Home/components/Suggestions";
import useWindowDimensions from "../../../helpers/windowHeight";
import ChatSection from "../../Layout/components/ChatSection";
import { Toast } from "../../../helpers/Toaster";
import { connect } from "react-redux";
import Nodata from "../../Layout/components/Nodata";
import ImageCropper from "../../Layout/components/ImageCropper";
import { Dropdown } from "react-bootstrap";
import { linkInModal } from "../../../redux/actions/user";

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
        className={`del`}
        href=""
        ref={ref}
        onClick={(e) => {
        e.preventDefault();
        onClick(e);
        }}
    >
    {children}
  </a>
));

let profile_tab_index = localStorage.getItem('profile_tab_index');
profile_tab_index = profile_tab_index ? JSON.parse(profile_tab_index) : {};
const UserSection = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [user, setUser] = useState();
    const [edit, setEdit] = useState(false);
    const [bio, setBio] = useState("");
    const fileInput = useRef(null);
    const bannerInput = useRef(null);
    const [doneListing, setDoneListing] = useState(false);
    const [loading, setLoading] = useState(false);
    const [noData, setNoData] = useState(false);
    const [postList, setPostList] = useState([]);
    const [index, setIndex] = useState(profile_tab_index && profile_tab_index.index > 0 ? profile_tab_index.index : 0);
    const [errMsg, setErrMsg] = useState(false);
    
    let defaultValue = {
        id: 1,
        lastId: "",
        saved: "",
        public: "",
        anonymously: "",
    };
    const [post, setPost] = useState({
        id: 1,
        lastId: "",
        saved: "",
        public: "",
        anonymously: "",
    });


    const getPostList = async (data, reset = false) => {
        if (!loading && !doneListing) {
            setNoData(false);
            setLoading(true);
            let response = await new ProfileController().postList(data);
            setLoading(false);
            if (!reset && response && response.status) {
                if (post.lastId && response.posts.length > 0) {
                    setPostList([...postList, ...response.posts]);
                }
                else if (!post.lastId && response.posts.length > 0) {
                    setPostList(response.posts);
                }
                else if (postList.length > 0) {
                    setDoneListing(true);
                }
                else {
                    setPostList([]);
                    setNoData(true);
                }
            }
            else if (reset && response && response.status && response.posts.length > 0) {
                setPostList(response.posts);
            }
            else {
                setNoData(true);
            }
        }
    };

    // posts  --start
    const updatePosts = () => {
        setPost({ ...post, lastId: "" });
        setDoneListing(false);
        setNoData(false);
        setIsLoading(false);
        getPostList({ ...post, lastId: "" }, true);
    };

    useEffect(() => {
        setIndex(profile_tab_index && profile_tab_index.index > 0 ? profile_tab_index.index : 0);
        setPostList([]);
        getProfile();
        setTimeout(() => {
            if(profile_tab_index)
                getPostList({ ...post, ...profile_tab_index });    
            else
                getPostList({ ...post });    
        }, 1000)
        
        return;
    }, []);

    //scroll to bottom -start
    const onScroll = ({ scrollTop, scrollHeight, clientHeight }) => {
        if (scrollTop + clientHeight >= scrollHeight - 100) {
            let postId = postList.length > 0 ? postList[postList.length - 1].id : null;
            setPost({
                ...post,
                lastId: postId,
            });
            getPostList({
                ...post,
                lastId: postId,
            });
        }
    };

    useEffect(() => {
        if (props.scroller && postList.length > 0) {
            onScroll(props.scroller);
        }
    }, [props.scroller]);


    const getProfile = async () => {
        let response = await new ProfileController().getProfile();
        if (response && response.status) {
            setUser(response.user);
            // handleStates("id", response.user.id);
            setBio(response.user.bio ? response.user.bio : "");
        }
    };

    //Edit Bio  --start
    const editBio = async () => {
        if (isLoading === false) {
            setIsLoading(true);
            let response = await new ProfileController().editBio(bio ? bio : ``);
            setIsLoading(false);
            if (response && response.status) {
                new AuthController().setUpLogin(response.user);
                setUser(response.user);
                setBio(response.user.bio);
                new Toast().success(response.message);
                setEdit(false);
            } else {
                console.log("error here", response);
                // setErrMsg(response.error);
            }
        }
    };

    //image upload  --start
    const avtarUpload = async (value) => {
        if (isLoading === false) {
            setIsLoading(true);
            let response = await new ProfileController().uploadProfileImage(
                value
            );
            // setIsLoading(false);
            if (response && response.status) {
                setTimeout(() => {
                    new AuthController().setUpLogin(response.user);
                    new Toast().success(response.message);
                    setUser(response.user);
                }, 500);
            } else {
                console.log("error here", response);
                // setErrMsg(response.error);
            }
        }
    };

    //image upload  --start
    const bannerUpload = async (value) => {
        if (isLoading === false) {
            setIsLoading(true);
            let response = await new ProfileController().uploadProfileBanner(
                value
            );
            setIsLoading(false);
            if (response && response.status) {
                setTimeout(() => {
                    new AuthController().setUpLogin(response.user);
                    new Toast().success(response.message);
                    setUser(response.user);
                }, 500);
            } else {
                console.log("error here", response);
                // setErrMsg(response.error);
            }
        }
    };

    /** Cropper */
    const [cropImage, setCropImage] = React.useState(null);
    const [cropItem, setCropItem] = React.useState(null);
    const [openCropper, setOpenCropper] = React.useState(false);
    const initImageEditor = (file, type, width, height) => {
        setCropItem({
            image: file,
            type: type,
            width: width,
            height: height,
            croppedArea: null
        });
        setOpenCropper(true);
    }
    const onCropperClose = () => {
        setOpenCropper(false);
        setCropItem(null);
        setCropImage(null);
    };

    const cropImageSetup = async (item, type) => {
        if (type === 'avatar') {
            avtarUpload(item);
        }
        else {
            bannerUpload(item)
        }
    }

    const removeBanner = async () => {
        let response = await new ProfileController().removeBanner();
        if (response && response.status) {
            new Toast().success(response.message);
            new AuthController().setUpLogin(response.user);
            getProfile();
        } else {
            new Toast().error(response.error);
        }
    }

    const removeImage = async () => {
        let response = await new ProfileController().removeImage();
        if (response && response.status) {
            new Toast().success(response.message);
            new AuthController().setUpLogin(response.user);
            getProfile();
        } else {
            new Toast().error(response.error);
        }
    }


    /** Cropper */


    // const { height, width } = useWindowDimensions();
    // useEffect(() => {
    //     scrollerHeight();
    // }, [height]);

    // const scrollerHeight = async () => {
    //     let header = await new AuthController().getHeaderHeight();
    //     let scroll = document.getElementById("scroll");
    //     let final = height - header;
    //     scroll.style.height = `${final}px`;
    // };

    return (
        <>
            <section className="User_seaction">
                <div className="container px-sm-2 px-0">
                    <div id="scroll" className="timeline_scroll_section">
                        <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div className="cover_area">
                                    <div className="cover_img">
                                        <img
                                            src={
                                                user && user.banner
                                                    ? Constant.host + user.banner
                                                    : cover
                                            }
                                            alt="..."
                                            onClick={() => {
                                                if (user && user.banner) {
                                                    props.showLinkInModal(baseUrl(user.banner))
                                                }
                                            }}
                                        />
                                        {
                                            user && user.banner
                                            ?
                                                <Dropdown
                                                    className={`icon`}
                                                    align={{ lg: 'end' }}
                                                >
                                                    <Dropdown.Toggle as={CustomToggle} >
                                                        <i className="far fa-camera"></i>
                                                    </Dropdown.Toggle>
                                                    <Dropdown.Menu>
                                                        <Dropdown.Item
                                                            onClick={() =>
                                                                bannerInput.current.click()
                                                            }
                                                        >    
                                                            <div className="left">
                                                                <i className="fa fa-pencil"></i>
                                                            </div>
                                                            <div className="right">
                                                                Change Image
                                                            </div>
                                                        </Dropdown.Item>
                                                        <Dropdown.Item
                                                            onClick={removeBanner}
                                                        >
                                                             <div className="left">
                                                                <i className="fa fa-times"></i>
                                                            </div>
                                                            <div className="right">
                                                                Remove Image
                                                            </div>
                                                        </Dropdown.Item>
                                                    </Dropdown.Menu>
                                                </Dropdown>
                                            :
                                                <>
                                                    <div
                                                        className="icon"
                                                        onClick={() =>
                                                            bannerInput.current.click()
                                                        }
                                                    >
                                                        <i className="far fa-camera"></i>
                                                    </div>
                                                    
                                                </>
                                        }
                                        <input
                                            ref={bannerInput}
                                            id="photo1"
                                            type="file"
                                            onChange={(event) => {
                                                let file = event.target.files[0];
                                                if (['image/jpeg', 'image/png', 'image/gif'].includes(file.type)) {
                                                    if (file.size < Constant.maxFileSize) {
                                                        let reader = new FileReader();
                                                        reader.readAsDataURL(file);

                                                        reader.onload = function () {
                                                            initImageEditor((reader.result), 'banner', 883, 215);
                                                        };
                                                        reader.onerror = function () {
                                                            console.log('there are some problems');
                                                        };
                                                        event.target.value = "";
                                                    }
                                                    else {
                                                        new Toast().error('File size exceeds the maximum limit of 25MB.');
                                                    }
                                                }
                                                else {
                                                    new Toast().error('Please upload file having extension jpg, jpeg, png, gif, webp or mp4');
                                                }
                                            }}
                                            style={{ display: "none" }}
                                            accept="image/*"
                                        />
                                    </div>
                                </div>
                                <div className="user_area">
                                    <div className="user-img">
                                        {
                                            isLoading
                                            &&
                                            <span className={`spinner`}><i className="fad fa-spinner-third fa-spin"></i></span>
                                        }
                                        <img
                                            onClick={() => {
                                                if (user && user.image) {
                                                    props.showLinkInModal(baseUrl(user.image))
                                                }
                                            }}
                                            src={
                                                user && user.image
                                                    ? renderImage(user.image)
                                                    : user1
                                            }
                                            alt=".."
                                        />
                                        {
                                            user && user.image
                                                ?
                                                    <Dropdown
                                                        className={`icon`}
                                                        align={{ lg: 'end' }}
                                                    >
                                                        <Dropdown.Toggle as={CustomToggle} >
                                                            <i className="far fa-camera"></i>
                                                        </Dropdown.Toggle>
                                                        <Dropdown.Menu>
                                                            <Dropdown.Item
                                                                onClick={() =>
                                                                    fileInput.current.click()
                                                                }
                                                            >
                                                                 <div className="left">
                                                                    <i className="fa fa-pencil"></i>
                                                                </div>
                                                                <div className="right">
                                                                    Change Image
                                                                </div>
                                                            </Dropdown.Item>
                                                            <Dropdown.Item
                                                                onClick={removeImage}
                                                            >     
                                                                <div className="left">
                                                                    <i className="fa fa-times"></i>
                                                                </div>
                                                                <div className="right">
                                                                Remove Image
                                                                </div>
                                                            </Dropdown.Item>
                                                        </Dropdown.Menu>
                                                    </Dropdown>
                                                :
                                                <>
                                                    <div
                                                        className="icon"
                                                        onClick={() =>
                                                            fileInput.current.click()
                                                        }
                                                    >
                                                        <i className="far fa-camera"></i>
                                                    </div>
                                                    
                                                </>
                                        }
                                        <input
                                            ref={fileInput}
                                            id="photo2"
                                            type="file"
                                            onChange={(event) => {
                                                let file = event.target.files[0];
                                                if (['image/jpeg', 'image/png', 'image/gif'].includes(file.type)) {
                                                    if (file.size <= Constant.maxFileSize) {
                                                        let reader = new FileReader();
                                                        reader.readAsDataURL(file);

                                                        reader.onload = function () {
                                                            initImageEditor((reader.result), 'avatar', 200, 200);
                                                        };
                                                        reader.onerror = function () {
                                                            console.log('there are some problems');
                                                        };
                                                        event.target.value = "";
                                                    }
                                                    else {
                                                        new Toast().error('File size exceeds the maximum limit of 25MB.');
                                                    }
                                                }
                                                else {
                                                    new Toast().error('Please upload file having extension jpg, jpeg, png, gif, webp or mp4');
                                                }
                                            }}
                                            style={{ display: "none" }}
                                            accept="image/*"
                                        />
                                    </div>
                                    <div className="user_detail el">
                                        <h4>
                                            {user && user.first_name
                                                ? user.first_name +
                                                " " +
                                                (user.last_name
                                                    ? user.last_name
                                                    : "")
                                                : ""}
                                        </h4>
                                        <div className={edit ? "d-none" : "bio"}>
                                            <p onClick={() => setEdit(true)}>
                                                {user && user.bio ? user.bio : "Click edit to enter the bio.. "}
                                                <i className="fas fa-pen ps-2"></i>
                                            </p>
                                        </div>

                                        <div
                                            className={
                                                edit ? "inputer my-2" : "d-none"
                                            }
                                        >
                                            <Form.Group
                                                className="mb-3"
                                                controlId="exampleForm.ControlInput1"
                                            >
                                                <Form.Control
                                                    as="textarea"
                                                    placeholder="Enter bio Here"
                                                    value={bio}
                                                    rows={3}
                                                    onChange={(e) => {
                                                        let value = e.target.value;
                                                        if (
                                                            !value ||
                                                            !value.trim()
                                                        ) {
                                                            setBio("");
                                                        } else {
                                                            setBio(value);
                                                        }
                                                    }}
                                                />
                                            </Form.Group>
                                            <div className="btnss">
                                                <button
                                                    className={"save"}
                                                    onClick={() => editBio()}
                                                >
                                                    Save
                                                </button>
                                                <button
                                                    className="cancel"
                                                    onClick={() => setEdit(false)}
                                                >
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div className="tabs">
                                <ul>
                                    <li className={index === 0 ? "active" : ""}>
                                        <a
                                            onClick={() => {
                                                setPostList([]);
                                                setDoneListing(false);
                                                setNoData(false);
                                                let d = {
                                                    ...post,
                                                    lastId: "",
                                                    saved: "",
                                                    public: "",
                                                    anonymously: "",
                                                    index: 0,
                                                };
                                                setPost(d);
                                                setIndex(0);
                                                localStorage.setItem('profile_tab_index', JSON.stringify(d));
                                                getPostList({ ...d }, true);
                                            }}
                                        >
                                            Posts
                                        </a>
                                    </li>
                                    <li className={index === 1 ? "active" : ""}>
                                        <a
                                            onClick={() => {
                                                setPostList([]);
                                                setDoneListing(false);
                                                setNoData(false);
                                                let d = {
                                                    ...post,
                                                    lastId: "",
                                                    saved: "",
                                                    public: "",
                                                    anonymously: 1,
                                                    index: 1
                                                };
                                                setPost(d);
                                                setIndex(1);
                                                localStorage.setItem('profile_tab_index', JSON.stringify(d));
                                                getPostList({ ...d }, true);
                                            }}
                                        >
                                            Anonymous
                                        </a>
                                    </li>
                                    <li className={index === 2 ? "active" : ""}>
                                        <a
                                            onClick={() => {
                                                setPostList([]);
                                                setDoneListing(false);
                                                setNoData(false);
                                                let d = {
                                                    ...post,
                                                    lastId: "",
                                                    saved: "",
                                                    public: 1,
                                                    anonymously: "",
                                                    index: 2,
                                                };
                                                setPost(d);
                                                
                                                setIndex(2);
                                                localStorage.setItem('profile_tab_index', JSON.stringify(d));
                                                getPostList({ ...d }, true);
                                            }}
                                        >
                                            Public
                                        </a>
                                    </li>
                                    <li className={index === 3 ? "active" : ""}>
                                        <a
                                            onClick={() => {
                                                setPostList([]);
                                                setDoneListing(false);
                                                setNoData(false);
                                                let d = {
                                                    ...post,
                                                    lastId: "",
                                                    saved: 1,
                                                    public: "",
                                                    anonymously: "",
                                                    index: 3,
                                                };
                                                setPost(d);
                                                setIndex(3);
                                                localStorage.setItem('profile_tab_index', JSON.stringify(d));
                                                getPostList({ ...d }, true);
                                            }}
                                        >
                                            Saved posts
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="profile_under">
                                <div className="row">
                                    <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                                        <div className="content">
                                            <div className="show">
                                                {!noData && postList && postList.length > 0
                                                    ? 
                                                    <>
                                                        <Post
                                                            update={true}
                                                            postList={postList}
                                                            showAddPost={(value) =>
                                                                console.log(value)
                                                            }
                                                            updatePosts={updatePosts}
                                                        />
                                                        {
                                                            loading
                                                            &&
                                                            <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>
                                                        }
                                                    </>
                                                    :
                                                        <>{!noData && <p className="loading"><i className="fad fa-spinner-third fa-spin"></i></p>}</>
                                                }
                                                
                                                {
                                                    noData
                                                    &&
                                                    <Nodata title="No posts available." />
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 d-lg-block d-none">
                                            <ChatSection />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {
                user && openCropper && cropItem
                &&
                <ImageCropper
                    item={cropItem}
                    open={openCropper}
                    onClose={onCropperClose}
                    onCrop={cropImageSetup}
                    isLoading={isLoading}
                />
            }
        </>

    );
};
const mapState = (state) => {
    return {
        scroller: state.ScrollerReducer.scroller,
    };
};
const mapDispatch = (dispatch) => {
    return {
        showLinkInModal: async (info) => dispatch(linkInModal(info)),
    };
};

export default connect(mapState, mapDispatch)(UserSection);
