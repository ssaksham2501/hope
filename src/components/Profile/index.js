import React, { useEffect } from "react";
import useWindowDimensions from "../../helpers/windowHeight";
import AuthController from "../../controllers/auth.controller";

//components
import UserSection from "./components/UserSection";

const Profile = () => {
    
    return (
        <section className="home_sections">
            <div className="row">
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <UserSection />
                </div>
            </div>
        </section>
    );
};

export default Profile;
