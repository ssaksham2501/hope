import React, { useEffect, useState } from "react";
import ProfileController from "../../controllers/profile.controller";
//components
import Suggestions from "../Home/components/Suggestions";
import ChatSection from "../Layout/components/ChatSection";
import NotificationSection from "../Layout/components/NotificationSection";
import { Link, useLocation } from "react-router-dom";

//assets
import user from "../../assets/images/header/user.jpg";
import { connect } from "react-redux";

//components

const Notification = (props) => {
    const location = useLocation();
    const [list, setList] = useState([]);
    const [keyword, setSearchKeyword] = useState("");
    const [notifications, setNotifications] = useState([]);
    const [friendRequests, setFriendRequests] = useState([]);
    const [notificationCount, setNotificationCount] = useState(0);
    const [notifyCount, setNotifyCount] = useState(0);
    const [requestCount, setRequestCount] = useState(0);
    const [noNotificaitons, setNoNotifications] = useState(false);
    const [showNotifications, setShowNotifications] = useState(false);
    const [loader, setLoader] = useState(false);
    const [page, setPage] = useState(1);
    const [done, setDone] = useState(false);
    useEffect(() => {
        getNotifications();
    }, [location.pathname]);

    const getNotifications = async (page) => {
        if (!loader && !done) {
            let post = {
                page: page ? page : 1,
            };
            setLoader(true);
            const response = await new ProfileController().getNotifications(post);
            // if (response && response.status && (response.requests.length > 0 || response.notifications.length > 0)) {
            if (response && response.status) {
                let list = response.notifications;
                if (list.length > 0) {
                    if (post.page === 1) {
                        setNotifications(list);
                    } 
                    else {
                        setNotifications([...notifications, ...list]);
                    }
                    if (list.length < 30) {
                        setLoader(false);
                    }
                    else {
                        setLoader(true);
                    }
                    setPage(post.page + 1);

                }
                else if(page > 1) {
                    setDone(true);
                }
                setNoNotifications(false);
                // setNotifications(response.notifications);
                setFriendRequests(response.requests);
                setNotificationCount(response.count);
                setRequestCount(response.requestCount);
                setNotifyCount(response.notificationCount);
            } else {
                setFriendRequests([]);
                setNotifications([]);
                setNoNotifications(true);
                setNotificationCount(0);
                setRequestCount(0);
                setNotifyCount(0);
            }
            setLoader(false);
        }
    }

    const onScroll = ({ scrollTop, scrollHeight, clientHeight }) => {
        if (scrollTop + clientHeight >= scrollHeight - 100 && !done) {
            getNotifications(page);
        }
    };

    useEffect(() => {
        if (props.scroller && !props.single) {
            onScroll(props.scroller);
        }
        return window.scrollTo(0, 0);
    }, [props.scroller]);

    return (
        <section className="settings_section">
            <div className="row">
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div className="row">
                        <div className="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <div className="friends_requestes el page">
                                <div className="request_hed">
                                    <div className="tabed">
                                        <NotificationSection
                                            path={location.pathname}
                                            notifications={notifications}
                                            friendRequests={friendRequests}
                                            notifyCount={notifyCount}
                                            requestCount={requestCount}
                                            close={() => {
                                            }}
                                            reload={() => {
                                                getNotifications();
                                            }}
                                        />
                                        {loader && <p className="spinner-notfi"><i className="fad fa-spinner-third fa-spin"></i></p>}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 d-lg-block d-none">
                            <ChatSection />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapState = (state) => {
    return {
        scroller: state.ScrollerReducer.scroller
    };
};
const mapDispatch = {};

export default connect(mapState, mapDispatch)(Notification);
