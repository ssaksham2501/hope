import React from "react";
import { Link } from "react-router-dom";

//assets
import landing_bow from "../../../assets/images/Landing/bow.png";
import landing_birb from "../../../assets/images/Landing/birb.png";

const About = () => {
    return (
        <section className="about_section">
            <div className="container-fluid">
                <div className="row row-no-padding">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="about_area">
                            <div className="top_area">
                                <h1>About Us</h1>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit. Vestibulum blandit urna ut
                                    dignissim bibendum. Duis ac urna ut
                                    dignissim diam vel massa lacinia elementum
                                    varius vitae justo.urna ut dignissim
                                    Pellentesque habitant morbi tristique
                                    senectus et netusurna et malesuada fames ac
                                    turpis egestas.
                                </p>
                                <Link to="/about-us">
                                    <button className="main_btn lav_bg">
                                        Learn More
                                    </button>
                                </Link>
                                <div className="imgwrap">
                                    <img src={landing_birb} alt="" />
                                </div>
                            </div>
                            <div className="bot_area">
                                <div className="imgwrap ">
                                    <img src={landing_bow} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default About;
