import React, { useState } from "react";

//assets
import landing_hope from "../../../assets/images/Landing/hope.png";
import landing_callie from "../../../assets/images/Landing/callie.png";
import landing_harry from "../../../assets/images/Landing/harry.png";

const Demo = () => {
    //like
    const [like1, setLike1] = useState(false);
    const [like2, setLike2] = useState(false);

    return (
        <section className="demo_section">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="demo_area">
                            <div className="row">
                                <div className="col-xxl-4 col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                                    <div className="left_area">
                                        <div className="content">
                                            <h1>HOPE</h1>
                                            <h3>is around the world</h3>
                                        </div>
                                        <div className="imgwrap flexie">
                                            <img src={landing_hope} alt="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xxl-8 col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 my-auto">
                                    <div className="right_area flexie">
                                        <div className="demo_postes">
                                            <div className="post_col post ">
                                                <div className="main_post">
                                                <div className="head_name_user">
                                                        <div className="before_post">
                                                            <div className="imgwrap">
                                                                <img
                                                                    src={landing_callie}
                                                                    alt=""
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="post_head">
                                                            <p className="user_info">
                                                                <a
                                                                    href="/"
                                                                    className="user"
                                                                >
                                                                    Callie Parker
                                                                </a>
                                                                <span className="lav_dot lav_bg"></span>
                                                                <a
                                                                    href="/"
                                                                    className="comm"
                                                                >
                                                                    Good Space
                                                                </a>
                                                            </p>
                                                            <p className="post_time">
                                                                2m ago
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="post_body">
                                                        <div className="post_tags">
                                                            
                                                                <a href="/">
                                                                    <span className="post_tag">
                                                                    Gratitude
                                                                    </span>
                                                                </a>
                                                   
                                                           
                                                                <a href="/">
                                                                    <span className="post_tag">
                                                                    Hustle
                                                                    </span>
                                                                </a>
                                                     
                                                        </div>
                                                        <div className="post_text">
                                                            <p>
                                                                Can't believe that
                                                                2021 has ended. It
                                                                was a mix of
                                                                different
                                                                emotions,experiences
                                                                and adventures.
                                                                Wanted to explore it
                                                                more but yeah
                                                                eventually
                                                                everything comes to
                                                                an end.
                                                            </p>
                                                        </div>
                                                        <div className="post_info flexie">
                                                            <p className="likes">
                                                                250 Likes
                                                            </p>
                                                            <p className="comments">
                                                                110 comments
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="after_post">
                                                    <i className="fal fa-ellipsis-h"></i>
                                                </div>
                                            </div>
                                            {/* <div className="post_foot flexie">
                                                    <div className="iconwrap">
                                                        <a>
                                                            <i
                                                                className={
                                                                    like1
                                                                        ? "fas fa-heart like flexie "
                                                                        : "fal fa-heart like flexie"
                                                                }
                                                                onClick={() => {
                                                                    setLike1(
                                                                        !like1
                                                                    );
                                                                }}
                                                            ></i>
                                                        </a>
                                                    </div>
                                                    <div className="dividor"></div>
                                                    <div className="iconwrap">
                                                        <a href="/">
                                                            <i className="fal fa-comment-alt comment flexie"></i>
                                                        </a>
                                                    </div>
                                                    <div className="dividor"></div>
                                                    <div className="iconwrap">
                                                        <a href="/">
                                                            <i className="far fa-paper-plane share flexie"></i>
                                                        </a>
                                                    </div>
                                            </div> */}
                                        </div>
                                        <div className="demo_postes">
                                            <div className="post_col post ">
                                                <div className="main_post">
                                                <div className="head_name_user">
                                                        <div className="before_post">
                                                            <div className="imgwrap">
                                                                <img
                                                                    src={landing_callie}
                                                                    alt=""
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="post_head">
                                                            <p className="user_info">
                                                                <a
                                                                    href="/"
                                                                    className="user"
                                                                >
                                                                    Callie Parker
                                                                </a>
                                                                <span className="lav_dot lav_bg"></span>
                                                                <a
                                                                    href="/"
                                                                    className="comm"
                                                                >
                                                                    Good Space
                                                                </a>
                                                            </p>
                                                            <p className="post_time">
                                                                2m ago
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="post_body">
                                                        <div className="post_tags">
                                                            
                                                                <a href="/">
                                                                    <span className="post_tag">
                                                                    Gratitude
                                                                    </span>
                                                                </a>
                                                   
                                                           
                                                                <a href="/">
                                                                    <span className="post_tag">
                                                                    Hustle
                                                                    </span>
                                                                </a>
                                                     
                                                        </div>
                                                        <div className="post_text">
                                                            <p>
                                                                Can't believe that
                                                                2021 has ended. It
                                                                was a mix of
                                                                different
                                                                emotions,experiences
                                                                and adventures.
                                                                Wanted to explore it
                                                                more but yeah
                                                                eventually
                                                                everything comes to
                                                                an end.
                                                            </p>
                                                        </div>
                                                        <div className="post_info flexie">
                                                            <p className="likes">
                                                                250 Likes
                                                            </p>
                                                            <p className="comments">
                                                                110 comments
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="after_post">
                                                    <i className="fal fa-ellipsis-h"></i>
                                                </div>
                                            </div>
                                            {/* <div className="post_foot flexie">
                                                    <div className="iconwrap">
                                                        <a>
                                                            <i
                                                                className={
                                                                    like1
                                                                        ? "fas fa-heart like flexie "
                                                                        : "fal fa-heart like flexie"
                                                                }
                                                                onClick={() => {
                                                                    setLike1(
                                                                        !like1
                                                                    );
                                                                }}
                                                            ></i>
                                                        </a>
                                                    </div>
                                                    <div className="dividor"></div>
                                                    <div className="iconwrap">
                                                        <a href="/">
                                                            <i className="fal fa-comment-alt comment flexie"></i>
                                                        </a>
                                                    </div>
                                                    <div className="dividor"></div>
                                                    <div className="iconwrap">
                                                        <a href="/">
                                                            <i className="far fa-paper-plane share flexie"></i>
                                                        </a>
                                                    </div>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Demo;
