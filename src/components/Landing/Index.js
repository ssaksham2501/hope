import React from "react";
//components
import Login from "../Auth/LogIn";
import Footer from "../Layout/Footer";
import About from "./components/About";
import Demo from "./components/Demo";

const Landing = () => {
    return (
        <div>
            <Login />
            <Demo />
            <About />
            <Footer />
        </div>
    );
};

export default Landing;
