import React, { useEffect, useState } from "react";
import { Accordion } from "react-bootstrap";
import CmsController from "../../../controllers/cms.controller";

//assets
import landing_birb2 from "../../../assets/images/Landing/birb_2.png";

function Mains() {
    const [questions, setQuestions] = useState([]);
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    useEffect(() => {
        window.scrollTo(0, 0);
        getFaq();
    }, []);

    const getFaq = async () => {
        let response = await new CmsController().dataFaq();
        if (response && response.status) {
            setQuestions(response.faqs);
        }
    };

    return (
        <section className="mains_section accordion_section">
            <div className="float_img">
                <img src={landing_birb2} alt="" />
            </div>
            <div className="float_img float_img_1">
                <img src={landing_birb2} alt="" />
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="mains_area accordion_area">
                            <Accordion defaultActiveKey="0">
                                {questions.map((item, key) => {
                                    return (
                                        <Accordion.Item eventKey={key}>
                                            <Accordion.Header>
                                                <p className="head">
                                                    {item.question}
                                                </p>
                                            </Accordion.Header>

                                            <Accordion.Body>
                                                <p>{item.answer}</p>
                                            </Accordion.Body>
                                        </Accordion.Item>
                                    );
                                })}
                            </Accordion>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Mains;
