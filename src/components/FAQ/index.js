import React from "react";

//components
import Breadcrumb from "../Layout/Breadcrumb";
import Mains from "./components/Mains";

function FAQ() {
    return (
        <div>
            <Breadcrumb breadTitle="FAQ" />
            <Mains />
        </div>
    );
}

export default FAQ;
