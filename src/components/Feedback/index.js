import React from "react";

//components
import Breadcrumb from "../Layout/Breadcrumb";
import FeedbackForm from "./components/FeedbackForm";

function Feedback() {
    return (
        <div>
            <Breadcrumb breadTitle="Feedback" />
            <FeedbackForm />
        </div>
    );
}

export default Feedback;
