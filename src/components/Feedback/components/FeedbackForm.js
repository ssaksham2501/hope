import React, { useState, useEffect } from "react";
import Validation from "../../../helpers/vaildation";
import FeedbackController from "../../../controllers/feedback.controller";

//assets
import feedback_1 from "../../../assets/images/Feedback/1.png";
import feedback_2 from "../../../assets/images/Feedback/2.png";
import feedback_3 from "../../../assets/images/Feedback/3.png";
import feedback_4 from "../../../assets/images/Feedback/4.png";
import feedback_5 from "../../../assets/images/Feedback/5.png";
import feedback_feedline from "../../../assets/images/Feedback/feedline.png";
import feedback_feedbg from "../../../assets/images/Feedback/feedbg.png";
import feedback_leaf from "../../../assets/images/Feedback/leaf.png";

const FeedbackForm = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);
    const [errMsg, setErrMsg] = useState(false);
    const imgList = [
        feedback_1,
        feedback_2,
        feedback_3,
        feedback_4,
        feedback_5,
    ];
    const feedList = ["Complaint", "Suggestion", "Compliment"];

    //validations  -- start
    let defaultErrors = {
        rating: {
            rules: ["required"],
            isValid: true,
            message: "",
        },
        category: {
            rules: [],
            isValid: true,
            message: "",
        },
        email: {
            rules: ["required", "email"],
            isValid: true,
            message: "",
        },
        message: {
            rules: [],
            isValid: true,
            message: "",
        },
    };
    const [isError, setError] = useState(defaultErrors);
    let validation = new Validation(isError);

    let defaultValues = {
        email: null,
        message: null,
        rating: null,
        category: null,
    };
    const [values, setValues] = useState(defaultValues);

    useEffect(() => {
        window.scrollTo(0, 0);
        setValues(defaultValues);
    }, []);

    const handleStates = (field, value) => {
        if (!value || !value.trim()) {
            /** Validate each field on change */
            let node = validation.validateField(field, value);
            setError({ ...isError, [field]: node });
            /** Validate each field on change */
            setValues({ ...values, [field]: "" });
        } else {
            /** Validate each field on change */
            let node = validation.validateField(field, value);
            setError({ ...isError, [field]: node });
            /** Validate each field on change */
            setValues({ ...values, [field]: value });
        }
    };
    //validations  -- end

    //sign up  --start
    const sendFeedback = async () => {
        if (isLoading === false) {
            setErrMsg(false);
            /** Check full form validation and submit **/
            let validation = new Validation(isError);
            let isValid = await validation.isFormValid(values);
            if (isValid && !isValid.haveError) {
                setIsLoading(true);
                let response = await new FeedbackController().send_feedback(
                    values
                );
                setIsLoading(false);
                if (response && response.status) {
                    setValues(defaultValues);
                    setError(defaultErrors);
                    setIsSuccess(true);
                    setErrMsg(response.message);
                } else {
                    setErrMsg(response.error);
                }
            } else {
                setError({ ...isValid.errors });
            }
        }
    };
    //sign up --end
    return (
        <section className="feedback_form_section">
            <div className="container">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        {!isSuccess ? (
                            <div className="feedback_form_area">
                                <div className="content">
                                    <h1>
                                        We would like your feedback to improve
                                        our website
                                    </h1>
                                    <img src={feedback_feedbg} alt="..." />
                                </div>

                                <form
                                    className="form_area form_area_2"
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                        sendFeedback();
                                    }}
                                >
                                    <div className="sub_section lav_bg sec_1 valid-box">
                                        <div className="float_img">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <div className="float_img float_img_1">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <p className="main_head">
                                            How much do you like our website?
                                        </p>

                                        <fieldset
                                            id="group1"
                                            className="rating_bag flexie valid-box field_valid"
                                        >
                                            {imgList.map((item, key) => {
                                                return (
                                                    <>
                                                        <input
                                                            id={`"em${key}"`}
                                                            type="radio"
                                                            name="rating"
                                                            onChange={(e) => {
                                                                handleStates(
                                                                    "rating",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={key}
                                                        />
                                                        <label
                                                            htmlFor={`"em${key}"`}
                                                            className="clickable"
                                                        >
                                                            <div className="rating">
                                                                <img
                                                                    src={item}
                                                                    alt="..."
                                                                />
                                                            </div>
                                                        </label>
                                                    </>
                                                );
                                            })}
                                            
                                        </fieldset>
                                        {!isError.rating.isValid && isError.rating.message ? (
                                            <p className="valid-helper">
                                                {isError.rating.message}
                                            </p>
                                        ) : null}
                                    </div>
                                    <div className="sub_section lav_bg sec_2 valid-box">
                                        <div className="float_img">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <div className="float_img float_img_1">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <p className="main_head">
                                            Please select your feedback category
                                            below
                                        </p>

                                        <fieldset
                                            id="group2"
                                            className="category_bag flexie valid-box field_valid"
                                        >
                                            {feedList.map((item, key) => {
                                                return (
                                                    <>
                                                        <input
                                                            id={`"fe${key}"`}
                                                            type="radio"
                                                            name="category"
                                                            onChange={(e) => {
                                                                handleStates(
                                                                    "category",
                                                                    e.target
                                                                        .value
                                                                );
                                                            }}
                                                            value={item}
                                                        />
                                                        <label
                                                            htmlFor={`"fe${key}"`}
                                                            className="clickable"
                                                        >
                                                            <div className="category">
                                                                <p>{item}</p>
                                                            </div>
                                                        </label>
                                                    </>
                                                );
                                            })}
                                            
                                        </fieldset>
                                        {!isError.category.isValid && isError.category.message ? (
                                            <p className="valid-helper">
                                                {isError.category.message}
                                            </p>
                                        ) : null}
                                    </div>
                                    <div className="sub_section lav_bg sec_2">
                                        <div className="float_img">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <div className="float_img float_img_1">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <p className="main_head">Email</p>
                                        <div className="category_bag flexie">
                                            <label
                                                className={
                                                    !isError.email.isValid
                                                        ? "valid-box form_controls"
                                                        : "form_controls"
                                                }
                                                htmlFor="email"
                                            >
                                                <input
                                                    type="email"
                                                    placeholder="Enter your email"
                                                    onChange={(e) => {
                                                        handleStates(
                                                            "email",
                                                            e.target.value
                                                        );
                                                    }}
                                                    value={
                                                        values.email
                                                            ? values.email
                                                            : ``
                                                    }
                                                />
                                                {!isError.email.isValid && isError.email.message ? (
                                                    <p className="valid-helper">
                                                        {isError.email.message}
                                                    </p>
                                                ) : null}
                                            </label>
                                        </div>
                                    </div>
                                    <div className="sub_section lav_bg sec_2">
                                        <div className="float_img">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <div className="float_img float_img_1">
                                            <img
                                                src={feedback_feedline}
                                                alt=""
                                            />
                                        </div>
                                        <p className="main_head">
                                            Please leave your feedback below
                                        </p>
                                        <div className="category_bag flexie">
                                            <label
                                                className={
                                                    !isError.message.isValid
                                                        ? "valid-box form_controls form_controls_tarea"
                                                        : "form_controls form_controls_tarea"
                                                }
                                                htmlFor="message"
                                            >
                                                <textarea
                                                    type="text"
                                                    rows={5}
                                                    placeholder="Write your feedback"
                                                    onChange={(e) => {
                                                        handleStates(
                                                            "message",
                                                            e.target.value
                                                        );
                                                    }}
                                                    value={
                                                        values.message
                                                            ? values.message
                                                            : ``
                                                    }
                                                />

                                                {!isError.message.isValid && isError.message.message ? (
                                                    <p className="valid-helper">
                                                        {
                                                            isError.message
                                                                .message
                                                        }
                                                    </p>
                                                ) : null}
                                            </label>
                                        </div>
                                    </div>
                                    <div className="action_area ">
                                        <button
                                            className="main_btn lav_bg"
                                            type="button"
                                            onClick={() => {
                                                sendFeedback();
                                            }}
                                        >
                                            Send
                                            {isLoading ? (
                                                <i className="fad fa-spinner-third fa-spin"></i>
                                            ) : null}
                                        </button>
                                        <div className="imgwrap">
                                            <img
                                                src={feedback_leaf}
                                                alt="..."
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        ) : (
                            <div className="feedback_form_area">
                                <div className="success_area shadow_box flexie">
                                    <div className="success_msg flexie">
                                        <i className="fad fa-badge-check"></i>
                                        <p className="main_txt">{errMsg}</p>
                                        <button
                                            className="main_btn lav_bg"
                                            onClick={() => setIsSuccess(false)}
                                        >
                                            <i className="fad fa-long-arrow-left back_icon"></i>
                                            Back
                                        </button>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </section>
    );
};

export default FeedbackForm;
