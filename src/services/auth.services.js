import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const AuthService = {
    sign_up,
    log_in,
    forgot_pass,
    verify_token,
    send_otp,
    reset_pass,
    resend_link,
    changePassword,
    googleLogin
};

function sign_up(params) {
    return mainWrapper.post(Constant.host + "/auth/signup", params);
}
function verify_token(params) {
    return mainWrapper.post(
        Constant.host + "/auth/email-verification/",
        params
    );
}
function resend_link(params) {
    return mainWrapper.post(
        Constant.host + "/auth/resend-verification-email",
        params
    );
}
async function log_in(params) {
    return await mainWrapper.post(Constant.host + "/auth/login", params);
}
function forgot_pass(params) {
    return mainWrapper.post(Constant.host + "/auth/forgot-password", params);
}
function send_otp(params) {
    return mainWrapper.post(Constant.host + "/auth/validate-otp", params);
}
function reset_pass(params) {
    return mainWrapper.post(Constant.host + "/auth/reset-password", params);
}

function changePassword(params) {
    return mainWrapper.put(Constant.host + "/change-password", params);
}
function googleLogin(params) {
    return mainWrapper.post(Constant.host + "/auth/google-login", params);
}

export default AuthService;
