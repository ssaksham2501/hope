import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const PostService = {
    upload_photo,
    upload_video,
    upload_audio,
    create_post,
    get_post,
    post_like,
    post_comment,
    get_comment_list,
    post_comment_like,
    editPost,
    deletePost,
    savePost,
    unSavePost,
    reportList,
    reportPost,
    postDetail,
    editComment,
    deleteComment,
    copyLink
};

function upload_photo(params, progressCallback) {
    return mainWrapper.upload(
        Constant.host + "/actions/upload-media/photo",
        params,
        progressCallback
    );
}
function upload_video(params,progressCallback) {
    return mainWrapper.upload(
        Constant.host + "/actions/upload-media/video",
        params,
        progressCallback
    );
}
function upload_audio(params,progressCallback) {
    return mainWrapper.upload(
        Constant.host + "/actions/upload-media/audio",
        params,
        progressCallback
    );
}

function create_post(params) {
    return mainWrapper.post(Constant.host + "/posts/create", params);
}

function editPost(params, id) {
    return mainWrapper.put(Constant.host + `/posts/update/${id}`, params);
}

function get_post(params) {
    return mainWrapper.get(Constant.host + "/posts", params);
}

function post_like(params) {
    return mainWrapper.post(Constant.host + "/posts/like", params);
}

function post_comment(params) {
    return mainWrapper.post(Constant.host + "/comment/create", params);
}

function post_comment_like(params) {
    return mainWrapper.post(Constant.host + "/comment/like/create", params);
}

function get_comment_list(params) {
    return mainWrapper.get(Constant.host + "/comment", params);
}

function deletePost(params) {
    return mainWrapper._delete(Constant.host + "/posts/delete", params);
}

function savePost(params) {
    return mainWrapper.post(Constant.host + "/posts/save", params);
}

function unSavePost(params) {
    return mainWrapper._delete(Constant.host + "/posts/save/delete", params);
}

function reportPost(params) {
    return mainWrapper.post(Constant.host + "/posts/report", params);
}

function postDetail(params) {
    return mainWrapper.get(Constant.host + "/posts/detail", params);
}

function deleteComment(params) {
    return mainWrapper._delete(Constant.host + "/comment/delete", params);
}
function editComment(params, id) {
    return mainWrapper.put(Constant.host + `/comment/update/${id}`, params);
}

function reportList(params) {
    return mainWrapper.get(Constant.host + "/posts/report/list", params);
}

function copyLink(id) {
    return mainWrapper.post(Constant.host + "/actions/copy-post-link/" + id, {});
}

export default PostService;
