import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const FeedbackService = {
    send_feedback,
};

function send_feedback(params) {
    return mainWrapper.post(Constant.host + "/feedback", params);
}

export default FeedbackService;
