import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const CmsService = {
    getDetail,
    getAbout,
    getFaq,
};

function getDetail(params) {
    return mainWrapper.get(Constant.host + "/pages/detail", params);
}
function getAbout(params) {
    return mainWrapper.get(Constant.host + "/pages/about", params);
}
function getFaq(params) {
    return mainWrapper.get(Constant.host + "/pages/faq", params);
}

export default CmsService;
