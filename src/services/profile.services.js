import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const ProfileService = {
    uploadProfileImage,
    uploadProfileBanner,
    getProfile,
    editBio,
    blockUser,
    unBlockUser,
    blockedUsersList,
    getPublicProfile,
    searchList,
    removeImage,
    removeBanner,
    getNotifications,
    updatePrivacy,
    updateProfile,
    deleteAccount,
    reportUser
};

function uploadProfileImage(params) {
    return mainWrapper.put(Constant.host + "/profile/upload-picture", params);
}

function uploadProfileBanner(params) {
    return mainWrapper.put(Constant.host + "/profile/upload-banner", params);
}

function editBio(params) {
    return mainWrapper.put(Constant.host + "/profile/edit-bio", params);
}

function getProfile() {
    return mainWrapper.get(Constant.host + "/profile");
}

function getPublicProfile(username) {
    return mainWrapper.get(Constant.host + `/profile/${username}`);
}

function blockUser(params) {
    return mainWrapper.post(Constant.host + "/block/user", params);
}

function unBlockUser(params) {
    return mainWrapper._delete(Constant.host + "/block/user/delete", params);
}

function blockedUsersList() {
    return mainWrapper.get(Constant.host + "/block/list");
}

function searchList(param) {
    return mainWrapper.getAxios(Constant.host + "/search", param);
}


function removeImage() {
    return mainWrapper._delete(Constant.host + "/profile/removeImage");
}

function removeBanner() {
    return mainWrapper._delete(Constant.host + "/profile/removeBanner");
}

function getNotifications(param) {
    return mainWrapper.get(Constant.host + "/notifications", param);
}

function updatePrivacy(post) {
    return mainWrapper.put(Constant.host + "/profile/updatePrivacy", post);
}

function updateProfile(post) {
    return mainWrapper.put(Constant.host + "/profile/updateProfile", post);
}

function deleteAccount(data) {
    return mainWrapper._delete(Constant.host + "/profile/deleteAccount", data);
}

function reportUser(data) {
    return mainWrapper.post(Constant.host + "/profile/reportUser", data);
}
    
export default ProfileService;
