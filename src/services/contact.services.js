import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const ContactService = {
    contact_us,
    getContact,
};

function contact_us(params) {
    return mainWrapper.post(Constant.host + "/contact-us", params);
}

function getContact(params) {
    return mainWrapper.get(Constant.host + "/contact-us", params);
}

export default ContactService;
