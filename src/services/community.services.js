import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const CommunityService = {
    uploadProfileImage,
    create,
    getList,
    detail,
    getAll,
    update,
    deleteCommunity,
    followCommunity,
    unFollowCommunity,
    followedUsersList,
    suggestedCommunities,
    removeImage,
    removeBanner,
    searchMyCommunities
};

function uploadProfileImage(params) {
    return mainWrapper.post(Constant.host + "/actions/upload-photo", params);
}
function create(params) {
    return mainWrapper.post(Constant.host + "/groups/create", params);
}

function update(params, id) {
    return mainWrapper.put(Constant.host + `/groups/update/${id}`, params);
}

function getList(post) {
    return mainWrapper.get(Constant.host + "/groups/list", post);
}
function getAll() {
    return mainWrapper.get(Constant.host + "/groups/all-communities");
}

function detail(post) {
    return mainWrapper.get(Constant.host + "/groups/detail", post);
}

function deleteCommunity(post) {
    return mainWrapper._delete(Constant.host + "/groups/delete", post);
}

function followCommunity(post) {
    return mainWrapper.post(Constant.host + "/group-follow", post);
}

function unFollowCommunity(post) {
    return mainWrapper._delete(Constant.host + "/group-follow/delete", post);
}

function followedUsersList(post) {
    return mainWrapper.get(Constant.host + "/group-follow/list", post);
}
function suggestedCommunities() {
    return mainWrapper.get(Constant.host + "/groups/suggestions");
}

function removeImage(post) {
    return mainWrapper._delete(Constant.host + "/groups/removeImage", post);
}

function removeBanner(post) {
    return mainWrapper._delete(Constant.host + "/groups/removeBanner", post);
}

function searchMyCommunities(post) {
    return mainWrapper.getAxios(Constant.host + "/groups/search-my-communities", post);
}

export default CommunityService;
