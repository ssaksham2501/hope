import { Constant } from "./constants";
import { mainWrapper } from "./main.services";

const MessagesService = {
    getMessages,
    getChatList,
    acceptChatRequest,
    deleteChatRequest,
    getTotalChatCount,
    muteChat,
    deleteChat
};

function getMessages(post) {
    return mainWrapper.get(Constant.host + "/messages", post);
}

function getChatList(post) {
    return mainWrapper.get(Constant.host + "/chat/list", post);
}

function acceptChatRequest(data) {
    return mainWrapper.post(Constant.host + "/chat/accept-request", data);
}

function deleteChatRequest(data) {
    return mainWrapper.post(Constant.host + "/chat/delete-request", data);
}

function getTotalChatCount() {
    return mainWrapper.post(Constant.host + "/messages/count");
}

function muteChat(data) {
    return mainWrapper.post(Constant.host + "/chat/mute", data);
}

function deleteChat(data) {
    return mainWrapper._delete(Constant.host + "/chat/delete", data);
}

export default MessagesService;
