import React from "react";
import toast, { Toaster } from "react-hot-toast";

export class Toast extends React.Component {
    constructor(props) {
        super(props);
    }

    success(message) {
        if (message && message !== "" && message !== null) {
            toast.success(message, {
                duration: 3000,
                position: "top-center",
                // className: "toast",
                iconTheme: {
                    primary: "#ccccff",
                    secondary: "#fff",
                },
            });
        }
    }

    error(message) {
        if (message && message !== "" && message !== null) {
            toast.error(message, {
                duration: 3000,
                position: "top-center",
                iconTheme: {
                    primary: "  #eb6383",
                    secondary: "#fff",
                },
            });
        }
    }

    render() {
        return <></>;
    }
}
