import ActionType from "../constants/index";

export const setUserData = (param) => {
    let stored = localStorage.setItem("SET_USER_DATA", JSON.stringify(param));
    return {
        type: ActionType.SET_USER_DATA,
        payload: param,
    };
};

export const setAPIToken = (param) => {
    let stored = localStorage.setItem("SET_API_TOKEN", JSON.stringify(param));
    return {
        type: ActionType.SET_API_TOKEN,
        payload: param,
    };
};

export const showCommunity = (param) => {
    let stored = localStorage.setItem("SHOW_COMMUNITY", param);
    return {
        type: ActionType.SHOW_COMMUNITY,
        payload: param,
    };
};

export const setCommunityId = (param) => {
    let stored = localStorage.setItem(
        "SET_COMMUNITY_DETAIL",
        JSON.stringify(param)
    );
    return {
        type: ActionType.SET_COMMUNITY_DETAIL,
        payload: param,
    };
};

export const setHeaderHeight = (param) => {
    let stored = localStorage.setItem(
        "SET_HEADER_HEIGHT",
        JSON.stringify(param)
    );
    return {
        type: ActionType.SET_HEADER_HEIGHT,
        payload: param,
    };
};

export const setScroller = (param) => {
    let stored = localStorage.setItem("SET_SCROLLER", param);
    return {
        type: ActionType.SET_SCROLLER,
        payload: param,
    };
};

export const setMessageData = (param) => {
    return {
        type: ActionType.SET_MESSAGE_DATA,
        payload: param,
    };
};

export const setChatBox = (param) => {
    let stored = localStorage.setItem(
        "SET_CHAT_BOX_DATA",
        JSON.stringify(param)
    );
    return {
        type: ActionType.SET_CHAT_BOX_DATA,
        payload: param,
    };
};

export const setChatList = (param) => {
    let stored = localStorage.setItem(
        "SET_CHAT_LIST",
        JSON.stringify(param)
    );
    return {
        type: ActionType.SET_CHAT_LIST,
        payload: param,
    };
};

export const linkInModal = (param) => {
    let stored = localStorage.setItem(
        "SET_LINK_IN_MODAL",
        JSON.stringify(param)
    );
    return {
        type: ActionType.SET_LINK_IN_MODAL,
        payload: param,
    };
};

export const playedItem = (param) => {
    let stored = localStorage.setItem(
        "SET_PLAYED_ITEM",
        JSON.stringify(param)
    );
    return {
        type: ActionType.SET_PLAYED_ITEM,
        payload: param,
    };
};

export const setNotificationCountData = (param) => {
    return {
        type: ActionType.SET_NOTIFICATION_COUNT_DATA,
        payload: param,
    };
};

