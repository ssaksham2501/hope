import ActionType from "../constants/index";

const stored = null;

const initialState = {
    link: stored,
};

const LinkInModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_LINK_IN_MODAL:
            return Object.assign({}, state, {
                link: action.payload,
            });

        default:
            return state;
    }
};

export default LinkInModalReducer;
