import ActionType from "../../constants/index";

let stored = localStorage.getItem("SET_COMMUNITY_DETAIL");
try {
    stored = stored ? JSON.parse(stored) : null;
} catch (err) {
    console.log("Error==>", err);
}

const initialState = {
    id: stored,
};

const CommunityDetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_COMMUNITY_DETAIL:
            return Object.assign("", state, {
                id: action.payload,
            });
        default:
            return state;
    }
};

export default CommunityDetailReducer;
