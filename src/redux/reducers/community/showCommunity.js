import ActionType from "../../constants/index";

let stored = localStorage.getItem("SHOW_COMMUNITY");
try {
    stored = stored ? JSON.parse(stored) : false;
} catch (err) {
    console.log("Error==>", err);
}
const initialState = {
    show: stored,
};

const ShowCommunityReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SHOW_COMMUNITY:
            return Object.assign(new Boolean(), state, {
                show: action.payload,
            });
        default:
            return state;
    }
};
export default ShowCommunityReducer;
