import ActionType from "../constants/index";

let stored = localStorage.getItem("SET_HEADER_HEIGHT");
try {
    stored = stored ? JSON.parse(stored) : 81  ;
} catch (err) {
    console.log("Error==>", err);
}

const initialState = {
    header: stored,
};

const HeaderHeightReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_HEADER_HEIGHT:
            return Object.assign('', state, {
                header: action.payload,
            });
        default:
            return state;
    }
};

export default HeaderHeightReducer;
