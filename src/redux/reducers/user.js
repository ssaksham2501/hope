import ActionType from "../constants/index";

let stored = localStorage.getItem("SET_USER_DATA");
try {
    stored = stored ? JSON.parse(stored) : {};
} catch (err) {
    console.log("Error==>", err);
}

const initialState = {
    user: stored,
};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_USER_DATA:
            return Object.assign({}, state, {
                user: action.payload,
            });
        default:
            return state;
    }
};

export default UserReducer;
