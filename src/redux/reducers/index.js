import { combineReducers } from "redux";
import UserReducer from "./user";
import APITokenReducer from "./apiToken";
import ShowCommunityReducer from "./community/showCommunity";
import CommunityDetailReducer from "./community/communityDetail";
import HeaderHeightReducer from "./header";
import ScrollerReducer from "./scroller";
import MessagesReducer from "./chat/messages";
import ChatBoxReducer from "./chat/chatbox";
import ChatListReducer from "./chat/chatlist";
import LinkInModalReducer from "./linkInModal";
import NotificationReducer from "./notificationCount";
import PlayedItemReducer from "./playedItemReducer";


const appReducer = combineReducers({
    UserReducer,
    APITokenReducer,
    ShowCommunityReducer,
    CommunityDetailReducer,
    HeaderHeightReducer,
    ScrollerReducer,
    MessagesReducer,
    ChatBoxReducer,
    ChatListReducer,
    LinkInModalReducer,
    NotificationReducer,
    PlayedItemReducer
});
const rootReducer = (state, action) => {
    return appReducer(state, action);
};
export default rootReducer;
