import ActionType from "../constants/index";

const stored = {};

const initialState = {
    notifications: stored,
};

const NotificationReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_NOTIFICATION_COUNT_DATA:
            return Object.assign({}, state, {
                notifications: action.payload,
            });

        default:
            return state;
    }
};

export default NotificationReducer;
