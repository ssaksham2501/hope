import ActionType from "../../constants/index";

const stored = {};

const initialState = {
    messages: stored,
};

const MessagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_MESSAGE_DATA:
            return Object.assign({}, state, {
                messages: action.payload,
            });

        default:
            return state;
    }
};

export default MessagesReducer;
