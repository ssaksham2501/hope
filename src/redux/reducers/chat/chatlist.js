import ActionType from "../../constants/index";

const stored = {};

const initialState = {
    list: stored,
};

const ChatListReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_CHAT_LIST:
            return Object.assign({}, state, {
                list: action.payload,
            });

        default:
            return state;
    }
};

export default ChatListReducer;
