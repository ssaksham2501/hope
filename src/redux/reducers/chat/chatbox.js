import ActionType from "../../constants/index";

let stored = localStorage.getItem("SET_CHAT_BOX_DATA");
stored = stored ? JSON.parse(stored) : {};

const initialState = {
    boxes: stored,
};

const ChatBoxReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_CHAT_BOX_DATA:
            return Object.assign({}, state, {
                boxes: action.payload,
            });

        default:
            return state;
    }
};

export default ChatBoxReducer;
