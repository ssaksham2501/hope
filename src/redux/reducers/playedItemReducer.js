import ActionType from "../constants/index";

const stored = null;

const initialState = {
    played: stored,
};

const PlayedItemReducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_PLAYED_ITEM:
            return Object.assign({}, state, {
                played: action.payload,
            });

        default:
            return state;
    }
};

export default PlayedItemReducer;
