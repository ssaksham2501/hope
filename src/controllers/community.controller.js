import React from "react";
import CommunityService from "../services/community.services";
import PostService from "../services/post.services";

class CommunityController extends React.Component {
    constructor(props) {
        super(props);
    }

    async uploadProfileImage(data) {
        let response = await CommunityService.uploadProfileImage({ photo: data });
        return response;
    }

    async getDetail(id) {
        let post = {
            slug: id,
        };
        let response = await CommunityService.detail(post);
        return response;
    }

    async createCommunity(data) {
        let response = await CommunityService.create(data);

        return response;
    }

    async updateCommunity(data, id) {
        let response = await CommunityService.update(data, id);
        return response;
    }
    async deleteCommunity(id) {
        let post = {
            id: id,
        };
        let response = await CommunityService.deleteCommunity(post);
        return response;
    }
    async communityList(data) {
        let post = {
            my_communities: data.user ? data.user : "",
            last_id: data.lastId ? data.lastId : "",
            follow: data.follow ? data.follow : "",
            topbar: data.topbar ? data.topbar : "",
        };
        let response = await CommunityService.getList(post);
        return response;
    }

    async suggestedCommunities() {
        let response = await CommunityService.suggestedCommunities();
        return response;
    }

    async getAllCommunties() {
        let response = await CommunityService.getAll();
        return response;
    }

    async searchMyCommunities(value) {
        let post = {
            q: value,
            limit: 30,
            sort: `title`,
            direction: `asc`
        };
        let response = await CommunityService.searchMyCommunities(post);
        return response;
    }

    async postList(data) {
        let post = {
            group: data.group,
            last_id: data.lastId ? data.lastId : "",
        };

        let response = await PostService.get_post(post);
        for (let i in response.posts) {
            response.posts[i]["commentBlock"] = false;
            response.posts[i]["commentInput"] = false;
            response.posts[i]["commentVal"] = "";
            response.posts[i]["comments"] = [];
            response.posts[i]["commentButton"] = false;
            response.posts[i]["commentCount"] = 0;
            response.posts[i]["postOptions"] = false;
        }

        return response;
    }

    //Follow Community
    async followCommunity(id) {
        let post = {
            group_id: id,
        };
        let response = await CommunityService.followCommunity(post);
        return response;
    }

    //unFollow Community
    async unFollowCommunity(id, user) {
        let post = {
            group_id: id,
            user_id: user ? user : null,
        };
        let response = await CommunityService.unFollowCommunity(post);
        return response;
    }

    //Followed Users List for  Community
    async followedCommunityList(id) {
        let post = {
            group_id: id,
        };
        let response = await CommunityService.followedUsersList(post);
        return response;
    }

    async removeBanner(id) {
        let post = {id: id};
        let response = await CommunityService.removeBanner(post);
        return response;
    }

    async removeImage(id) {
        let post = {id: id};
        let response = await CommunityService.removeImage(post);
        return response;
    }
}
export default CommunityController;
