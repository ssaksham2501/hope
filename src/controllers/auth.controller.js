import React from "react";
import AuthService from "../services/auth.services";
import { setUserData, setChatBox, setChatList } from "../redux/actions/user";
import store from "../redux/store";

class AuthController extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * To sign up the customer.
     * @param {Array} data
     * @return {Array} user
     */
    async sign_up(data) {
        let post = {
            first_name: data.first_name,
            last_name: data.last_name,
            username: data.username,
            phonenumber: data.phonenumber,
            email: data.email,
            dob: data.dob,
            password: data.password,
        };
        let response = await AuthService.sign_up(post);
        return response;
    }
    async verify_token(slug) {
        let post = {
            token: slug,
        };
        let response = await AuthService.verify_token(post);
        return response;
    }
    async resend_link(token) {
        let post = {
            token: token,
        };
        let response = await AuthService.resend_link(post);
        return response;
    }
    async log_in(data) {
        let post = {
            email: data.email,
            password: data.password,
            device_id: null,
            device_type: null,
            device_name: null,
            fcm_token: null,
        };
        let response = await AuthService.log_in(post);
        if (response && response.status) {
            this.setUpLogin(response.user);
        }
        return response;
    }
    async forgot_pass(data) {
        let post = {
            email: data.email,
        };
        let response = await AuthService.forgot_pass(post);
        return response;
    }
    async send_otp(data) {
        let post = {
            otp: data.otp,
            token: data.token,
        };
        let response = await AuthService.send_otp(post);
        return response;
    }
    async reset_pass(data) {
        let post = {
            otp: data.otp,
            token: data.token,
            new_password: data.password,
            confirm_password: data.password2,
        };
        let response = await AuthService.reset_pass(post);
        return response;
    }

    async changePassword(data) {
        let post = {
            old_password: data.password,
            new_password: data.password1,
            confirm_password: data.password2,
        };
        let response = await AuthService.changePassword(post);
        return response;
    }

    setUpLogin(user) {
        global.isLogin = true;
        global.clicked = true;
        store.dispatch(setUserData(user));
        return user;
    }

    getToken() {
        let user = store.getState().UserReducer.user;
        return user && user.access && user.access.token
            ? user.access.token
            : null;
    }

    getLoginUser() {
        let user = store.getState().UserReducer.user;
        return user && user.id ? user : null;
    }

    getLoginUserId() {
        let user = store.getState().UserReducer.user;
        return user && user !== null && user.id ? user.id : null;
    }

    logout() {
        if (global.socket) {
            global.socket.disconnect();
            global.socket = null;
        }
        store.dispatch(setUserData({}));
        store.dispatch(setChatBox({}));
        store.dispatch(setChatList({}));
        global.isLogin = false;
        localStorage.clear();
    }

    getHeaderHeight() {
        let header = store.getState().HeaderHeightReducer.header;
        return header ? header : null;
    }

    async googleLogin(data) {
        let post = {
            google_id: data.googleId,
            access_token: data.accessToken,
            first_name: data.profileObj.givenName,
            last_name: data.profileObj.familyName,
            image: data.profileObj.imageUrl,
            email: data.profileObj.email
        };
        
        let response = await AuthService.googleLogin(post);
        if (response && response.status) {
            this.setUpLogin(response.user);
        }
        return response;
    }
}
export default AuthController;
