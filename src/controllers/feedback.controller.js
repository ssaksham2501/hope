import React from "react";
import FeedbackService from "../services/feedback.services";

class FeedbackController extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * To sign up the customer.
     * @param {Array} data
     * @return {Array} user
     */
    async send_feedback(data) {
        let post = {
            first_name: null,
            last_name: null,
            email: data.email,
            message: data.message,
            rating: parseInt(data.rating),
            category: data.category,
        };
        let response = await FeedbackService.send_feedback(post);
        return response;
    }
}
export default FeedbackController;
