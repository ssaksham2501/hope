import React from "react";
import PostService from "../services/post.services";
import ProfileService from "../services/profile.services";

class ProfileController extends React.Component {
    constructor(props) {
        super(props);
    }

    async uploadProfileImage(data) {

        let response = await ProfileService.uploadProfileImage({avatar: data});
        return response;
    }

    async uploadProfileBanner(data) {
        let response = await ProfileService.uploadProfileBanner({banner: data});
        return response;
    }

    async editBio(bio) {
        let post = {
            bio,
        };
        let response = await ProfileService.editBio(post);
        return response;
    }

    async getProfile() {
        let response = await ProfileService.getProfile();
        return response;
    }

    async postList(data) {
        console.log(data);
        let post = {
            my_posts: data.id ? data.id : "",
            last_id: data.lastId ? data.lastId : "",
            saved: data.saved ? data.saved : "",
            anonymously: data.anonymously ? data.anonymously : "",
            public: data.public ? data.public : "",
            profile: data.profile ? data.profile : "",
        };

        let response = await PostService.get_post(post);
        for (let i in response.posts) {
            response.posts[i]["commentBlock"] = false;
            response.posts[i]["commentInput"] = false;
            response.posts[i]["commentVal"] = "";
            response.posts[i]["comments"] = [];
            response.posts[i]["commentButton"] = false;
            response.posts[i]["commentCount"] = 0;
            response.posts[i]["postOptions"] = false;
        }

        return response;
    }

    async blockUser(id) {
        let post = {
            blocked_user: id,
        };
        let response = await ProfileService.blockUser(post);
        return response;
    }

    async unBlockUser(id) {
        let post = {
            blocked_user: id,
        };
        let response = await ProfileService.unBlockUser(post);
        return response;
    }

    async blockedUsers() {
        let response = await ProfileService.blockedUsersList();
        return response;
    }

    async searchList(search) {
        let post = {
            search,
        };
        let response = await ProfileService.searchList(post);
        return response;
    }

    async getPublicProfile(username) {
        let response = await ProfileService.getPublicProfile(username);
        return response;
    }

    async removeImage() {
        let response = await ProfileService.removeImage();
        return response;
    }

    async removeBanner() {
        let response = await ProfileService.removeBanner();
        return response;
    }

    async getNotifications(data = null) {
        let post = {
            page: data && data.page ? data.page : 1
        };
        let response = await ProfileService.getNotifications(post);
        return response;
    }

    async updatePrivacy(value) {
        let post = {
            privacy: value,
        };
        let response = await ProfileService.updatePrivacy(post);
        return response;
    }

    async updateProfile(value) {
        let post = {
            first_name: value.first_name,
            last_name: value.last_name,
            dob: value.dob ? value.dob : null,
        };
        let response = await ProfileService.updateProfile(post);
        return response;
    }

    async deleteAccount(password) {
        let response = await ProfileService.deleteAccount({password});
        return response;
    }

    async reportUser(data) {
        let response = await ProfileService.reportUser(data);
        return response;
    }
}
export default ProfileController;
