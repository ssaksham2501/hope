import React from "react";
import { Constant } from "../services/constants";
import PostService from "../services/post.services";

class PostController extends React.Component {
    constructor(props) {
        super(props);
    }

    async upload_photo(data, progressCallback) {
        let form = new FormData();
        if (data.size < Constant.maxFileSize) {
            form.append("media", data);

            let response = await PostService.upload_photo(form, progressCallback);
            return response;
        }
        else {
            return {
                status: false,
                error: 'File size exceeds maximum limit 25MB.'
            }
        }
    }
    async upload_video(data, type, progressCallback) {
        let form = new FormData();
        if (data.size < Constant.maxFileSize) {
            form.append("media", data);
            form.append("type", type ? type : 'video');

            let response = await PostService.upload_video(form, progressCallback);
            return response;
        }
        else {
            return {
                status: false,
                error: 'File size exceeds maximum limit 25MB.'
            }
        }
    }
    
    async upload_audio(data, progressCallback) {
        let form = new FormData();
        if (data.size < Constant.maxFileSize) {
            form.append("media", data);
            let response = await PostService.upload_audio(form, progressCallback);
            return response;
        }
        else {
            return {
                status: false,
                error: 'File size exceeds maximum limit 25MB.'
            }
        }
    }

    //Create Post
    async create_post(data) {
        let tagsArray = [];
        data.tags.map((tag) => tagsArray.push(tag.text));
        let post = {
            description: data.description,
            type: data.type,
            file: data.file,
            group_id: data.group_id ? data.group_id : null,
            tags: tagsArray,
            anonymously: data.anonymously,
            is_private: data.is_private,
        };

        let response = await PostService.create_post(post);

        return response;
    }

    //Edit Post

    async editPost(data, id, media) {
        let tagsArray = [];
        data.tags.map((tag) => tagsArray.push(tag.text));
        let post = {
            description: data.description,
            type: data.type,
            file: data.file.length > 0 ? data.file : null,
            group_id: data.group_id ? data.group_id : null,
            tags: tagsArray,
            anonymously: data.anonymously,
            is_private: data.is_private,
            remove_media: media.length > 0 ? media : null,
        };

        let response = await PostService.editPost(post, id);

        return response;
    }

    //Delete Post
    async deletePost(id) {
        let post = {
            id,
        };
        let response = await PostService.deletePost(post);

        return response;
    }

    //Save Post
    async savePost(id) {
        let post = {
            post_id: id,
        };
        let response = await PostService.savePost(post);

        return response;
    }
    //UnSave Post
    async unSavePost(id) {
        let post = {
            post_id: id,
        };
        let response = await PostService.unSavePost(post);

        return response;
    }

    //Report Post
    async reportPost(id, reason) {
        let post = {
            post_id: id,
            subject: reason,
            comment_id: null,
        };
        let response = await PostService.reportPost(post);

        return response;
    }

    //Report Comment
    async reportComment(post_id, id, reason) {
        let post = {
            post_id: post_id,
            comment_id: id,
            subject: reason,
        };
        let response = await PostService.reportPost(post);

        return response;
    }

    //Report Problems List
    async reportList() {
        let response = await PostService.reportList();

        return response;
    }

    //Post Detail
    async postDetail(id) {
        let post = {
            id,
        };
        let response = await PostService.postDetail(post);
        return response;
    }

    async get_post(id, singlePost) {
        let post = {single: singlePost};
        if (id) {
            post = {
                last_id: id
            };
        }

        let response = await PostService.get_post(post);
        for (let i in response.posts) {
            response.posts[i]["commentBlock"] = false;
            response.posts[i]["commentInput"] = false;
            response.posts[i]["commentVal"] = "";
            response.posts[i]["comments"] = [];
            response.posts[i]["commentButton"] = false;
            response.posts[i]["commentCount"] = 0;
            response.posts[i]["postOptions"] = false;
        }

        return response;
    }

    async post_like(postId) {
        let post = {
            post_id: postId,
        };

        let response = await PostService.post_like(post);
        return response;
    }

    //Post Comments
    async get_comment_list(post) {
        let response = await PostService.get_comment_list(post);
        return response;
    }

    async post_comment(postId, val, parent) {
        let post = {
            post_id: postId,
            comment: val,
            parent_id: parent ? parent : null,
        };

        let response = await PostService.post_comment(post);
        return response;
    }

    async post_comment_like(data) {
        let post = {
            post_id: data.post_id,
            comment_id: data.id,
        };

        let response = await PostService.post_comment_like(post);
        return response;
    }

    //Edit Comment
    async editComment(id, data) {
        let post = {
            comment: data,
        };
        let response = await PostService.editComment(post, id);
        return response;
    }

    //Delete Comment
    async deleteComment(id) {
        let post = {
            id,
        };
        let response = await PostService.deleteComment(post);

        return response;
    }

    async copyLink(id) {
        let response = await PostService.copyLink(id);
        return response;
    }
}
export default PostController;
