import React from "react";
import CmsService from "../services/cms.services";

class CmsController extends React.Component {
    constructor(props) {
        super(props);
    }
    async dataDetail(slug) {
        let post = {
            slug: slug,
        };
        let response = await CmsService.getDetail(post);
        return response;
    }
    async dataAbout() {
        let response = await CmsService.getAbout();
        return response;
    }
    async dataFaq() {
        let response = await CmsService.getFaq();
        return response;
    }
}
export default CmsController;
