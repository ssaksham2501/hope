import React from "react";
import io from "socket.io-client";
import { Constant } from "../services/constants";
import AuthController from "./auth.controller";

class SocketController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            socket: io(Constant.socketUrl, {
                query: {
                    user_id: props.userId,
                },
            }),
        };
        // this.state.socket.emit("isonline", { user_id: props.userId });
    }

    async submitMessage(message, toId, msgId, attachment, attachment_type) {
        let token = await new AuthController().getToken();

        this.state.socket.emit("outgoing-message", {
            token: token,
            to_id: toId,
            message: message,
            message_id: msgId,
            attachment_type: attachment_type,
            attachment: attachment
        });
    }

    recieveMessage(callback) {
        this.state.socket.on("outgoing-message", callback);
    }

    recieveNotification(callback) {
        this.state.socket.on("message-notification", callback);
    }

    async markRead(chatWith) {
        let token = await new AuthController().getToken();
        this.state.socket.emit("message-read", {
            token: token,
            chat: chatWith,
        });
    }

    async isOnline(id, callback) {
        let login = false;
        this.state.socket.emit("isonline", {
            id: id,
        });
        this.state.socket.on("isonline", callback);
    }

    async isOffLine(id, callback) {
        this.state.socket.on("isoffline", callback);
    }

    onMessageRecieved(callback) {
        this.state.socket.on("message-read", callback);
    }

    isTyping(id, typing) {
        this.state.socket.emit("typing", {
            typing: typing,
            id: id,
        });
    }

    detectTyping(callback) {
        this.state.socket.on("typing", callback);
    }

    onDeleteMessage(id, productId) {
        this.state.socket.emit("delete-message", {
            id,
            productId,
        });
    }

    receiveDeleteMessage(callback) {
        this.state.socket.on("delete-message", callback);
    }

    offMessages() {
        this.state.socket.off("message-read");
        this.state.socket.off("delete-message");
        this.state.socket.off("outgoing-message");
    }

    disconnect() {
        try {
            this.state.socket.disconnect();
        } catch (err) { }
    }
}

export default SocketController;
