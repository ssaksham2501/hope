import React from "react";
import { setChatBox, setMessageData, setChatList, setNotificationCountData } from "../redux/actions/user";
import store from "../redux/store/index";
import getHistory from "react-router-global-history";
import AuthController from "./auth.controller";
import MessagesService from "../services/messages.services";

class MessagesController extends React.Component {
    constructor(props) {
        super(props);
    }

    async getMessages(data) {
        let post = {
            to: data.id,
            last_id: data.last_id ? data.last_id : null
        };

        let response = await MessagesService.getMessages(post);
        return response;
    }

    async getChatList(data) {
        let response = await MessagesService.getChatList(data);
        return response;
    }

    async getTotalChatCount() {
        let response = await MessagesService.getTotalChatCount();
        return response;
    }

    async acceptChatRequest(username) {
        let post = {
            to: username,
        };
        let response = await MessagesService.acceptChatRequest(post);
        return response;
    }

    async muteChat(id) {
        let post = {
            to: id,
            mute: 1
        };
        let response = await MessagesService.muteChat(post);
        return response;
    }

    async unmuteChat(id) {
        let post = {
            to: id,
            mute: 0
        };
        let response = await MessagesService.muteChat(post);
        return response;
    }

    async deleteChat(id) {
        let post = {
            to: id,
        };
        let response = await MessagesService.deleteChat(post);
        return response;
    }

    

    async deleteChatRequest(username) {
        let post = {
            to: username,
        };
        let response = await MessagesService.deleteChatRequest(post);
        return response;
    }

    async chatList(data = null) {
        let response = await this.getChatList(data);
        if (response && response.status) {
            store.dispatch(setChatList({ list: response.chat }));
            return response.chat;
        }
        else {
            return [];
        }
    }
    
    async getFriendsList() {
        return store.getState().ChatListReducer.list;
    }

    getMessageCount() {
        let counts = store.getState().MessagesReducer.messages;
        return counts && counts.total ? counts.total : 0;
    }

    updateCount(count) {
        store.dispatch(setMessageData({ total: count }));
    }

    getNotificationCount() {
        let counts = store.getState().NotificationReducer.notifications;
        return counts && counts.total ? counts.total : 0;
    }
    
    updateNotificationCount(count) {
        store.dispatch(setNotificationCountData({ total: count }));
    }

    getChatBoxList() {
        let boxes = store.getState().ChatBoxReducer.boxes;
        return boxes.chats ? boxes.chats : [];
    }

    getChatBoxListIds() {
        let boxes = store.getState().ChatBoxReducer.boxes;
        boxes = boxes.chats ? boxes.chats : [];
        let ids = [];
        for (let box of boxes)
        {
            ids.push(box.id);
        }
        return ids;
    }

    setChatBoxModal(box) {
        let loginId = new AuthController().getLoginUserId();
        if (loginId) {
            let boxes = store.getState().ChatBoxReducer.boxes.chats;
            let screenWidth = window.screen.width;
            let limit =
                screenWidth >= 2100
                    ? 4
                    : screenWidth >= 1570
                    ? 3
                    : screenWidth >= 1070
                    ? 2
                    : 1;
            boxes = boxes && boxes.length > 0 ? boxes : [];
            let index = null;
            let exist = false;
            boxes.filter((f, i) => {
                if (f.id == box.id) {
                    exist = true;
                    index = i;
                }
            });
            
            if (!exist) {
                boxes.push(box);
                if (boxes.length > limit) {
                    boxes.splice(0, 1);
                }
            } else if (index > -1 && boxes[index].minimize) {
                boxes[index].minimize = false;
            }

           
            store.dispatch(
                setChatBox({
                    count: boxes.length,
                    chats: boxes,
                    id: Math.random().toString(36).substring(7),
                })
            );
        } else {
            getHistory().push("/");
        }
    }

    updateMinimizeStatusChatBox(id, status, count) {
        let boxes = store.getState().ChatBoxReducer.boxes.chats;
        if (boxes && boxes.length) {
            boxes.map((f, i) => {
                if (f.id === id) {
                    if (status !== null) {
                        boxes[i].minimize = status;
                    }

                    if (count !== null) {
                        boxes[i].count = count;
                    }
                }
            });
        }

        store.dispatch(
            setChatBox({
                count: boxes && boxes.length > 0 ? boxes.length : 0,
                chats: boxes && boxes.length > 0 ? boxes : [],
                id: Math.random().toString(36).substring(7),
            })
        );
    }

    deleteChatBoxModal(id = null) {
        let boxes = store.getState().ChatBoxReducer.boxes.chats;
        let index = -1;
        if(id) {
            boxes.filter((f, i) => {
                if (f.id === id) {
                    index = i;
                }
            });
        }
        else if(boxes && boxes.length > 0) {
            index = 0;
        }
        
        if (index > -1) {
            boxes.splice(index, 1);
            store.dispatch(
                setChatBox({
                    count: boxes.length,
                    chats: boxes,
                    id: Math.random().toString(36).substring(7),
                })
            );
        }
    }
}
export default MessagesController;
