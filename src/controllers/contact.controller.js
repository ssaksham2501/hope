import React from "react";
import ContactService from "../services/contact.services";

class ContactController extends React.Component {
    constructor(props) {
        super(props);
    }

    /**
     * To sign up the customer.
     * @param {Array} data
     * @return {Array} user
     */
    async contact_us(data) {
        let post = {
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            message: data.message,
        };
        let response = await ContactService.contact_us(post);
        return response;
    }
    async dataContact() {
        let response = await ContactService.getContact();
        return response;
    }
}
export default ContactController;
